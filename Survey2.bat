echo off
set SURVEY_PATH=./build/classes;./jar/javadbf-0.4.1.jar;./jar/jcommon-1.0.16.jar;./jar/jfreechart-1.0.13.jar;./jar/jdom.jar
set SURVEY_CFG=config/surveyFrench.xml
set LOGGER_CFG=config/logger.properties
echo %SURVEY_PATH%
java -Xmx256m -Djava.util.logging.config.file=%LOGGER_CFG% -Dsurvey.messages=%SURVEY_CFG% -classpath %SURVEY_PATH% controls.CLauncherControl 