package calcul;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import cs.util.FileUtils;

public class ZipAnalysisFile {
	/** Flux d'�criture dans le fichier archive. */
	protected ZipOutputStream out;

	/**
	 * Constructeur
	 */
	public ZipAnalysisFile() {
	}

	/**
	 * Ouverture du fichier archive
	 * 
	 * @param aName
	 *            Nom du fichier resultats de l'analyse
	 */
	public void open(String aName) throws IOException {
		// 1. Construction nom du fichier archive
		String zipName = aName.endsWith(".zip") ? aName : aName + ".zip";

		// 2. Cr�ation r�f�rence au fichier archive
		File zipFile = new File(zipName);

		if (zipFile.exists()) {
			// 1. Suppression du fichier archive existant
			zipFile.delete();
		}

		// 3. Ouverture du fichier archive
		this.out = new ZipOutputStream(new FileOutputStream(zipName));
	}

	/**
	 * Fermeture du fichier archive
	 */
	public void close() {
		if (this.out != null) {
			try {
				// 1. Fermeture du fichier archive
				this.out.close();

				// 2. R�initialisation des attributs
				this.out = null;
			} catch (IOException e) {
			}
		}
	}

	/**
	 * Ajout d'un fichier � l'archive
	 * 
	 * @param aName
	 *            Nom de l'entr�e dans l'archive
	 * @param aPath
	 *            Chemin du fichier
	 */
	public void addFile(String aName, String aPath) throws IOException {
		if (this.out != null) {
			// 1. Cr�ation r�f�rence au fichier � include
			File file = new File(aPath);

			if (file.exists() && file.canRead()) {
				FileInputStream in = null;
				ZipEntry entry = null;
				try {
					// 1. Cr�ation d'une nouvelle entr�e
					entry = new ZipEntry(aName);

					// 2. Ajout de l'entr�e � l'archive
					this.out.putNextEntry(entry);

					// 3. Cr�ation flux de lecture du fichier source
					in = new FileInputStream(aPath);

					// 4. Enregistrement du fichier
					FileUtils.write(in, this.out);
				} finally {
					try {
						// 1. Fermeture flux de lecture du fichier source
						if (in != null)
							in.close();

						// 2. Fermeture de l'entree
						if (entry != null)
							this.out.closeEntry();
					} catch (IOException ign) {
					}
				}
			}
		}
	}
}
