package calcul;

import tools.CIniFileReader;

public class NASOUS {
	public static enum Model {
		/**
		 * Codification des mod�les. H repr�sente l'effet hydrostatique S
		 * l'effet saisonnier, T l'effet du temps (Age de l'ouvrage) et P
		 * l'effet de la pluie.
		 */
		HSTP, HST, HTP, HSP, HS, HP
	}

	/** Mod�le d'ajustement de l'analyse. */
	private Model ajustmentModel;

	/**
	 * Constructeur
	 * 
	 * @param aModel
	 *            Mod�le d'ajustement de l'analyse
	 */
	public NASOUS(Model aModel) {
		this.ajustmentModel = aModel;
	}

	/**
	 * Le mod�le d'ajustement de l'analyse necessite-t-il la pr�sence de mesures de la
	 * pluviom�trie
	 * 
	 * @return true Si des mesures de la pluie sont n�cessaires
	 */
	public boolean isRainMeasuresRequired() {
		return this.ajustmentModel != Model.HST
				&& this.ajustmentModel != Model.HS;
	}

	/**
	 * Lecture mod�le d'ajustement de l'analyse
	 * 
	 * @return Mod�le d'ajustement de l'analyse
	 */
	public Model getAjustmentModel() {
		return this.ajustmentModel;
	}

	@Override
	public String toString() {
		return CIniFileReader.getString(this.ajustmentModel.name());
	}
}
