package calcul;

import java.util.Calendar;

import cs.util.DateUtils;

public class AnalysisUtils {

	/**
	 * Calcul de l'angle saisonnier theta(t) = 2*PI*t/365,25
	 * 
	 * @param aDate
	 *            Date du calcul
	 */
	public static double getSeasonalAngle(Calendar aDate) {
		// 1. Lecture ann�e de la mesure
		int year = aDate.get(Calendar.YEAR);

		// 2. Initialisation nombre de jours dans l'ann�e
		double yearDayCount = DateUtils.isLeapYear(year) ? 366. : 365.;

		// 3. Lecture num?ro du jour de la mesure dans l'ann�e
		double day = (double) aDate.get(Calendar.DAY_OF_YEAR) - 1;

		// 4. Calcul de l'angle saisonnier
		return Math.PI * 2. * (day / yearDayCount);
	}

	/**
	 * Calcul de l'effet saisonnier S(t) =
	 * b1*sin(theta)+b2*cos(theta)+b3*sin(theta
	 * )*sin(theta)+b4*sin(theta)cos(theta)
	 * 
	 * @param aTheta
	 *            Valeur de l'angle saisonnier
	 * @param b1
	 *            Coefficient b1
	 * @param b2
	 *            Coefficient b2
	 * @param b3
	 *            Coefficient b3
	 * @param b4
	 *            Coefficient b4
	 * @return Valeur de l'effet saisonnier
	 */
	public static double getSeasonalEffect(double theta, double b1, double b2,
			double b3, double b4) {
		// 1. Calcul sinus et cosinus de l'angle saisonnier
		double sinus = Math.sin(theta);
		double cosinus = Math.cos(theta);

		// 2. Calcul de l'effet saisonnier
		return sinus * b1 + cosinus * b2 + sinus * cosinus * b3 + sinus * sinus
				* b4;
	}

	/**
	 * Calcul effet du temps T(t) = c0 + c1*tr + c2*exp(tr) + c3*exp(-tr)
	 * 
	 * 
	 * @param c0
	 *            Coefficient c0
	 * @param c1
	 *            Coefficient c1
	 * @param c2
	 *            Coefficient c2
	 * @param c3
	 *            Coefficient c3
	 * @param aTR
	 *            Valeur du temps r�duit
	 */
	public static double getTimeEffect(double tr, double c0, double c1,
			double c2, double c3) {
		return c0 + c1 * tr + c2 * Math.exp(tr) + c3 * Math.exp(-tr);
	}

	/**
	 * Calcul de l'effet de la pluie P(t) = d1*p1(t) + d2*p2(t) + d3*p3(t) +
	 * d4*p4(t) + d5*p56(t) + d6*p78(t)
	 * 
	 * @param aRain
	 *            Mesures ant?rieures de la pluviom?trie
	 * @param aD1
	 *            coefficient d1
	 * @param aD2
	 *            coefficient d2
	 * @param aD3
	 *            coefficient d3
	 * @param aD4
	 *            coefficient d4
	 * @param aD4
	 *            coefficient d5
	 * @param aD6
	 *            coefficient d6
	 * 
	 */
	public static double getRainEffect(AverageRainfall aRain, double d1, double d2,
			double d3, double d4, double d5, double d6) throws IllegalArgumentException  {
		// 1. Calcul des valeurs moyennes de la pluie
		double p1 = aRain.mean(7, 7);
		double p2 = aRain.mean(14, 7);
		double p3 = aRain.mean(21, 7);
		double p4 = aRain.mean(28, 7);
		double p56 = aRain.mean(42, 14);
		double p78 = aRain.mean(56, 14);

		return p1 * d1 + p2 * d2 + p3 * d3 + p4 * d4 + p56 * d5 + p78
				* d6;
	}

	/**
	 * Calcul de l'effet hydrostatique H(t) = a1*z+a2*z2+a3*z3+a4*z4
	 * 
	 * @param aZ
	 *            C?te centr?e r?duite du plan d'eau
	 */
	public static double getHydrostaticEffect(double aZ, double a1, double a2,
			double a3, double a4) {
		return a1 * aZ + a2 * aZ * aZ + a3 * aZ * aZ * aZ + a4 * aZ * aZ * aZ
				* aZ;

	}
}
