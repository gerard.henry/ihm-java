package calcul;

import java.util.Calendar;
import java.util.Date;

import db.SensorMeasures;

public class AverageRainfall {
	/** Nombre maximal de valeurs de la hauteur de pluie. */
	public static final int MAX_SIZE = 56;

	/** Liste des mesures de la pluie. */
	protected double[] values;

	/**
	 * Constructeur
	 */
	public AverageRainfall() {
		// 1. Initialisation des attributs
		this.values = new double[MAX_SIZE];

		// 2. Initialisation de la liste de valeurs
		for (int i = 0; i < MAX_SIZE; i++) {
			values[i] = -1.;
		}
	}

	/**
	 * R�initialisation de la liste de valeurs
	 */
	public void clear() {
		// 1. Cr�ation de la liste de valeurs
		this.values = new double[MAX_SIZE];

		// 2. Initialisation de la liste de valeurs
		for (int i = 0; i < MAX_SIZE; i++) {
			values[i] = -1.;
		}
	}

	/**
	 * Enregistrement des mesures de la pluviom�trie
	 * 
	 * @param aMeasures
	 *            Liste des mesures sur la pluie
	 * @param aFrom
	 *            Date de d�but de mesure
	 */
	public void setMeasures(SensorMeasures aMeasures, Date aFrom) {
		// 1. Cr�ation it�rateur de date
		Calendar idate = Calendar.getInstance();

		// 2. Initialisation it�rateur � la date de la mesure
		idate.setTime(aFrom);

		for (int i = 0; i < MAX_SIZE; i++) {
			if (aMeasures.hasMeasure(idate.getTime())) {
				// 1. Enregistrement valeur de la mesure
				this.values[i] = aMeasures.getMeasure(idate.getTime());
			} else {
				// 1. Enregistrement valeur indetermin�
				this.values[i] = -1.;
			}

			// 1. Positionnement sur le jour pr�c�dent
			idate.add(Calendar.DATE, -1);
		}
	}

	/**
	 * Enregistrement d'une liste de mesures de la pluviom�trie
	 * 
	 * @param aMeasures
	 *            Liste des mesures sur la pluie
	 * @param aFrom
	 *            Date de r�f�rence
	 * @param aDayCount
	 *            Nombre de jours
	 */
	public void addMeasures(SensorMeasures aMeasures, Date aFrom, int dayCount) {
		// 1. Cr�ation it�rateur de date
		Calendar idate = Calendar.getInstance();

		// 2. Initialisation it�rateur sur les dates
		idate.setTime(aFrom);

		// 3. Cr�ation nouvelle liste de valeurs
		double newValues[] = new double[MAX_SIZE];

		for (int i = 0; i < MAX_SIZE; i++) {
			if ( i < dayCount) {
			if (aMeasures.hasMeasure(idate.getTime())) {
				newValues[i] = aMeasures.getMeasure(idate.getTime());
			} else {
				newValues[i] = -1;
			}

			// 1. Positionnement sur le jour suivant
			idate.add(Calendar.DATE, -1);
			} else {
				// 1. R�cup�ration ancienne valeurs
				newValues[i] = values[i-dayCount];
			}
		}
		
		// 4. Actualisation liste des valeurs
		this.values = newValues;
	}

	/**
	 * Calcul valeur moyenne des mesures de la pluie
	 * 
	 * @param aAnteriotit�
	 *            Ant�riorit� (Jours) de d�but de calcul de la moyenne
	 * @param aDuree
	 *            Dur�e (Jours) de calcul de la moyenne
	 */
	public double mean(int aAnteriorite, int aDuree) {
		// 1. Initialisation cumul des mesures de pluie sur la p�riode
		double cumul = 0.;

		// 2. Initialisation nombre de mesures effectives
		int nbMeasures = 0;

		for (int i = aAnteriorite - aDuree; i < aAnteriorite; i++) {
			if (values[i] >= 0.) {
				// 1. Actualisation cumul des mesures
				cumul += values[i];

				// 2. Actualisation nombre de mesures effectives
				nbMeasures++;
			}
		}

		if (nbMeasures < 0.7 * aDuree) {
			// 1. Nombre insuffisant de mesures
			throw new IllegalArgumentException();
		}

		return cumul / nbMeasures;
	}

	/**
	 * Ajout d'une mesure
	 * 
	 * @param aValue
	 *            Valeur de la hauteur de pluie
	 */
	protected void add(double aValue) {
		// 1. D�calage des valeurs
		for (int i = MAX_SIZE - 1; i >= 1; i--) {
			values[i] = values[i - 1];
		}

		// 2. Enregistrement de la nouvelle valeeur
		values[0] = aValue;
	}
}
