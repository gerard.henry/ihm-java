package calcul;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import cs.util.DateUtils;
import db.Barrage;
import db.Sensor;
import db.SensorMeasures;
import db.SensorResultats;
import db.Survey;

public class SensorCorrection {
	/** Mesures de la cote du barrage. */
	protected SensorMeasures z;

	/** Mesures de la pluviom�trie. */
	protected SensorMeasures pluie;

	/** Mesures de l'instrument. */
	protected SensorMeasures measures;

	/** Intervalle de corection. */
	protected Date fromDate;
	protected Date toDate;

	/** Dates de corrections des mesures. */
	protected List<Date> measureDates;

	/** Valeurs des mesures brutes,calcul�es,corrig�es. */
	protected List<Double> measureValues;
	protected List<Double> measureComputed;
	protected List<Double> measureCorrected;

	/** Ecart mesure / mesure calcul�e. */
	protected List<Double> measureEps;

	/** Effets saisonnier, hydrostatique, de la pluie et du vieillissement. */
	protected List<Double> seasonEffects;
	protected List<Double> hydroEffects;
	protected List<Double> rainEffects;
	protected List<Double> timeEffects;

	/** Valeur du temps r�duit. */
	protected List<Double> reductedTime;

	/** Valeur de l'angle saisonnier. */
	protected List<Double> seasonAngle;

	/** Drapeau indiquant que le calcul de correction est effectu�. */
	protected boolean isComputed;

	/**
	 * Constructeur
	 * 
	 * @param aMeasures
	 *            Mesures de l'instrument
	 * @param aZ
	 *            Mesures de la cote du barrage
	 * @param aPluie
	 *            Mesures de la pluviom�trie
	 * @param aFrom
	 *            Date de d�but de l'intervalle de correction
	 * @param aTo
	 *            Date de fin de l'intervalle de correction
	 */
	public SensorCorrection(SensorMeasures aMeasures, SensorMeasures aZ,
			SensorMeasures aPluie, Date aFrom, Date aTo) {
		// 1. Initialisation des attributs
		this.isComputed = false;
		this.measures = aMeasures;
		this.z = aZ;
		this.pluie = aPluie;
		this.fromDate = aFrom;
		this.toDate = aTo;

	}

	/**
	 * Lecture r�f�rence � l'instrument
	 * 
	 * @return R�f�rence � l'instrument
	 */
	public Sensor getSensor() {
		return this.measures.getSensor();
	}

	/**
	 * Lecture r�f�rence au barrage
	 * 
	 * @return R�f�rence au barrage
	 */
	public Barrage getBarrage() {
		return this.getSensor().getBarrage();
	}

	/**
	 * Lecture dates des mesures corrig�es
	 * 
	 * @return Dates des mesures corrig�es
	 */
	public List<Date> getMeasureDates() {
		this.doCorrection();

		return measureDates;
	}

	/**
	 * Valeurs des mesures de l'instrument
	 * 
	 * @return Valeurs des mesures de l'instrument
	 */
	public List<Double> getMeasureValues() {
		this.doCorrection();

		return measureValues;
	}

	/**
	 * Valeurs des mesures calcul�es de l'instrument
	 * 
	 * @return Valeurs des mesures calcul�es de l'instrument
	 */
	public List<Double> getMeasureComputed() {
		this.doCorrection();

		return measureComputed;
	}

	/**
	 * Valeurs des mesures corrig�es de l'instrument
	 * 
	 * @return Valeurs des mesures corrig�es de l'instrument
	 */
	public List<Double> getMeasureCorrected() {
		this.doCorrection();

		return measureCorrected;
	}

	/**
	 * Ecarts entre la mesure et la mesure calcul�
	 * 
	 * @return Ecarts entre la mesure et la mesure calcul�t
	 */
	public List<Double> getMeasureEps() {
		this.doCorrection();

		return measureEps;
	}

	/**
	 * Valeurs des termes saisonnier
	 * 
	 * @return Valeurs des termes saisonnier
	 */
	public List<Double> getSeasonEffects() {
		this.doCorrection();

		return seasonEffects;
	}

	/**
	 * Valeurs des termes d�pendant de la cote du barrage
	 * 
	 * @return Valeurs des termes d�pendant de la cote du barrage
	 */
	public List<Double> getHydroEffects() {
		this.doCorrection();

		return hydroEffects;
	}

	/**
	 * Valeurs des termes d�pendant de la pluviom�trie
	 * 
	 * @return Valeurs des termes d�pendant de la pluviom�trie
	 */
	public List<Double> getRainEffects() {
		this.doCorrection();

		return rainEffects;
	}

	/**
	 * Valeurs des termes d�pendant du vieillissement
	 * 
	 * @return Valeurs des termes d�pendant du vieillissement
	 */
	public List<Double> getTimeEffects() {
		this.doCorrection();

		return timeEffects;
	}

	/**
	 * Valeurs du temps r�duit
	 * 
	 * @return Valeurs du temps r�duit
	 */
	public List<Double> getReductedTime() {
		this.doCorrection();

		return reductedTime;
	}

	/**
	 * Valeurs de l'angle saisonnier
	 * 
	 * @return Valeurs de l'angle saisonnier
	 */
	public List<Double> getSeasonAngle() {
		this.doCorrection();

		return seasonAngle;
	}

	/**
	 * Valeurs de la cote du barrage
	 * 
	 * @return Valeurs de cote du barrage
	 */
	public List<Double> getCote() {
		this.doCorrection();

		// 1. Cr�ation liste des valeurs
		List<Double> cotes = new ArrayList<Double>();

		// 2. Initialisation liste des valeurs
		for (Iterator<Date> i = this.getMeasureDates().iterator(); i.hasNext();) {
			cotes.add(this.z.getMeasure(i.next()));
		}

		return cotes;
	}

	/**
	 * Calcul des mesures corrig�es
	 */
	protected void doCorrection() {
		// 1. Le calcul de correction a-t-il �t� effectu�e
		if (isComputed)
			return;

		// 2. Cr�ation des listes de valeurs
		this.measureDates = new ArrayList<Date>();
		this.measureValues = new ArrayList<Double>();
		this.measureComputed = new ArrayList<Double>();
		this.measureCorrected = new ArrayList<Double>();
		this.measureEps = new ArrayList<Double>();
		this.seasonEffects = new ArrayList<Double>();
		this.hydroEffects = new ArrayList<Double>();
		this.rainEffects = new ArrayList<Double>();
		this.timeEffects = new ArrayList<Double>();
		this.reductedTime = new ArrayList<Double>();
		this.seasonAngle = new ArrayList<Double>();

		// 3. Cr�ation iterateur de date
		Calendar calendar = Calendar.getInstance();

		// 4. Initialisation nombre de jours entre deux mesures significatives
		int dayCount = 56;

		// 5. Construction liste des dates de mesures
		List<Date> dates = SensorMeasures.getDatesMeasure(new SensorMeasures[] {
				z, pluie, measures });

		// 6. Lecture r�sultats de l'analyse de l'instrument
		SensorResultats resultats = this.measures.getSensor().getBarrage()
				.getSensorResultats(this.measures.getSensor().getId());

		// 7. Cr�ation liste des mesures retard�es de la pluie
		AverageRainfall pluieRetardees = new AverageRainfall();

		for (int j = 1; j < dates.size(); j++) {
			double rainEffect = 0.;
			double timeEffect = 0.;
			double seasonEffect = 0.;
			double hydroEffect = 0.;

			// 1. Lecture date courante
			Date jdate = dates.get(j - 1);

			// 2. Filtrage sur la date de d�but
			if (jdate.compareTo(this.fromDate) < 0)
				continue;

			// 3. Filtrage sur la date de d�but
			if (jdate.compareTo(this.toDate) > 0)
				break;

			// 4. Initialisation date courante
			calendar.setTime(jdate);
			

			if (z.hasMeasure(jdate) && measures.hasMeasure(jdate) ) {
				if (pluie != null) {
					// 1. Enregistrement des valeurs retard�es de la pluie
					pluieRetardees.addMeasures(pluie, jdate,dayCount);
					
			
					try {
						// 1. Calcul des effets de la pluie
						rainEffect = AnalysisUtils.getRainEffect(
								pluieRetardees, resultats.getLP1(), resultats
										.getLP2(), resultats.getLP3(),
								resultats.getLP4(), resultats.getLP56(),
								resultats.getLP78());
					} catch (IllegalArgumentException ex) {
						// 1. Calcul nombre de jours s�parant la mesure
						// courante et la suivante
						dayCount = DateUtils.getDayCount(jdate, dates.get(j));

						// 2. Poursuite du traitement
						continue;
					}
				}

				// 1. Calcul de l'age de la mesure par rapport � la derni�re
				// analyse
				int ageMeasure = DateUtils.getDayCount(resultats
						.getLAnalyseBeginDate(), jdate);

				// 2. Calcul valeur du temps r�duit
				double tr = (double) ageMeasure
						/ (double) resultats.getLDuration();

				// 3. Calcul valeur de l'angle saisonnier
				double theta = AnalysisUtils.getSeasonalAngle(calendar);

				// 4. Calcul de la cote centree r�duite du plan d'eau
				double zr = (z.getMeasure(jdate) - resultats.getLMeanCote())
						/ resultats.getLSigCote();

				// 5. Calcul des effets du viellissement
				timeEffect = AnalysisUtils.getTimeEffect(tr, resultats
						.getLConstantTerm(), resultats.getLT(), resultats
						.getLExpT(), resultats.getLExpMT());

				// 6. Calcul de l'effet saisonnier
				seasonEffect = AnalysisUtils.getSeasonalEffect(theta, resultats
						.getLSin(), resultats.getLCos(), resultats.getLSico(),
						resultats.getLSin2());

				// 7. Calcul de l'effet hydrostatique du � la retenue
				hydroEffect = AnalysisUtils.getHydrostaticEffect(zr, resultats
						.getLZ(), resultats.getLZ2(), resultats.getLZ3(),
						resultats.getLZ4());

				// 8. Lecture valeur de la mesure
				double mesVal = measures.getMeasure(jdate);

				// 9. Calcul de la mesure suivant le mod�le EDF HST
				double mesCal = measures.getSensor().getRefValue() + timeEffect
						+ seasonEffect + hydroEffect + rainEffect;

				// 10. Calcul ecart entre la mesure et sa valeur calcul�e
				double mesEps = mesVal - mesCal;

				// 11. Calcul valeur corrig�e de la mesure
				double mesCor = measures.getSensor().getRefValue() + timeEffect
						+ mesEps;

				// 12. Enregistrement des r�sultats
				this.measureDates.add(jdate);
				this.measureValues.add(mesVal);
				this.measureComputed.add(mesCal);
				this.measureEps.add(mesEps);
				this.measureCorrected.add(mesCor);
				this.seasonEffects.add(seasonEffect);
				this.hydroEffects.add(hydroEffect);
				this.rainEffects.add(rainEffect);
				this.timeEffects.add(timeEffect);
				this.reductedTime.add(tr);
				this.seasonAngle.add(theta);
			
				// 3. Calcul nombre de jours s�parant la mesure courante et la
				// suivante
				dayCount = DateUtils.getDayCount(jdate, dates.get(j));
			}
		}

		// 8. Actualisation drapeau de calcul effectu�
		this.isComputed = true;
	}
}
