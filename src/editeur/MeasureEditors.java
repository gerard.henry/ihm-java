package editeur;

import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

import javax.swing.AbstractCellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import db.SensorMeasures;
import editeur.MeasureEditorPane.EditionMode;

public class MeasureEditors {
	/** Format d'�dition des valeurs de mesures. */
	protected static final NumberFormat doubleFormat = new DecimalFormat(
			"#####0.000");

	/** Editeurs des valeurs de mesures. */
	public static DoubleCellEditor measureEditor = new DoubleCellEditor();

	@SuppressWarnings("serial")
	protected static class DoubleCellEditor extends DefaultCellEditor {
		/** Valeur de la mesure. */
		protected Double value;

		/** Liste des mesures de l'instrument/variable. */
		protected SensorMeasures measures;

		/** Date de la mesure � editer. */
		protected Date datmes;

		/** Mode d'edition de la mesure. */
		protected EditionMode editMode;

		/**
		 * Constructeur
		 */
		public DoubleCellEditor() {
			// 1. Initialisation des attributs
			super(new JTextField());
		}

		@Override
		public Component getTableCellEditorComponent(JTable aTable,
				Object aValue, boolean isSelected, int aRow, int aColumn) {
			// 1. Initialisation valeur � editer
			if (this.editMode == EditionMode.BRUT) {
				// 1. Lecture valeur brute relev�e
				this.value = measures.getRawValue(datmes);
				
				// 2. Affectation valeur par defaut
				if ( this.value == null ) {
					this.value = new Double(0.0);
				}
			} else {
				this.value = (Double) aValue;
			}

			// 2. Mise en forme de la valeur
			String editedValue = (this.value == null ? null : doubleFormat
					.format(this.value));

			// 3. Construction du composant
			JTextField field =  (JTextField) super.getTableCellEditorComponent(aTable, editedValue,
					isSelected, aRow, aColumn);
			
			// 4. S�lection du texte � editer
			field.selectAll();
			
			return field;
		}

		@Override
		public Object getCellEditorValue() {
			// 1. Lecture valeur edit�e
			String editedValue = (String) super.getCellEditorValue();

			if (editedValue.trim().length() != 0) {
				try {
					// 1. D�codage de la valeur edit�e
					this.value = Double.parseDouble(editedValue);
				} catch (Exception ex) {
				}
				return this.value;

			} else {
				return null;
			}
		}

		/**
		 * Initialisation du contexte d'edition
		 * 
		 * @param aMeasures
		 *            Liste des mesures de l'instrument/variable
		 * @param aMesdat
		 *            Date de la mesure � editer
		 * @param aEditMode
		 *            Mode d'edition de la mesure
		 */
		public void setEditionContext(SensorMeasures aMeasures, Date aMesdat,
				EditionMode aEditMode) {
			this.measures = aMeasures;
			this.datmes = aMesdat;
			this.editMode = aEditMode;
		}
	}
}
