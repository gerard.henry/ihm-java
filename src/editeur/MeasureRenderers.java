package editeur;

import java.awt.Component;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

public class MeasureRenderers {
	/** Format par d�faut des dates et des nombres. */
	protected static final DateFormat dateFormat = new SimpleDateFormat(
			"dd/MM/yyyy");
	protected static final NumberFormat doubleFormat = new DecimalFormat(
			"#####0.000");

	/** Objets de rendu des mesures. */
	public static final DateRenderer dateRenderer = new DateRenderer();
	public static final NumberRenderer numberRenderer = new NumberRenderer();
	public static final HeaderRenderer headerRenderer = new HeaderRenderer();

	@SuppressWarnings("serial")
	protected static class DateRenderer extends DefaultTableCellRenderer {
		/**
		 * Constructeur
		 */
		public DateRenderer() {
			// 1. Initialisation position du texte
			setHorizontalAlignment(SwingConstants.CENTER);
		}

		@Override
		public void setValue(Object aValue) {
			if (aValue == null) {
				setText("");
			} else {
				// 1. Conversion dans le type requis
				Date date = (Date) aValue;

				setText(dateFormat.format(date));
			}
		}
	}

	@SuppressWarnings("serial")
	protected static class NumberRenderer extends DefaultTableCellRenderer {
		/**
		 * Constructeur
		 */
		public NumberRenderer() {
			// 1. Initialisation position du texte
			setHorizontalAlignment(SwingConstants.RIGHT);
		}

		@Override
		public void setValue(Object aValue) {
			if (aValue == null) {
				setText("");
			} else {
				// 1. Conversion dans le type requis
				Double value = (Double) aValue;

				setText(doubleFormat.format(value));

			}
		}
	}

	@SuppressWarnings("serial")
	protected static class HeaderRenderer extends DefaultTableCellRenderer {
		/**
		 * Constructeur
		 */
		public HeaderRenderer() {
			// 1. Initialisation position du texte
			setHorizontalAlignment(SwingConstants.CENTER);
		}

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			if (table != null) {
				JTableHeader header = table.getTableHeader();
				if (header != null) {
					setForeground(header.getForeground());

					if (table.getSelectedColumn() == column) {
						setBackground(table.getSelectionBackground());
						setForeground(table.getSelectionForeground());
					} else {
						setBackground(header.getBackground());
						setForeground(header.getForeground());
					}
					setFont(header.getFont());
				}
			}
			setText(value != null ? value.toString() : "");
			setBorder(UIManager.getBorder("TableHeader.cellBorder"));
			setHorizontalAlignment(JLabel.CENTER);
			return this;
		}
	}

}
