package editeur;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import tools.CIniFileReader;
import db.Barrage;
import db.Sensor;
import db.SensorMeasures;

@SuppressWarnings("serial")
public class MeasureEditor extends JDialog implements ActionListener,
		PropertyChangeListener {
	/** Pages de l'assistant. */
	protected SensorChooserPane chooserPage;
	protected MeasureEditorPane editorPage;

	/** Composants internes. */
	protected JPanel pagesPane;
	protected JPanel navigPane;
	protected JButton edit;
	protected JButton quit;
	protected JButton select;
	protected JButton save;

	/** Barrage � editer. */
	protected Barrage barrage;

	/**
	 * Constructeur
	 * 
	 * @param aBarrage
	 *            Un barrage
	 */
	public MeasureEditor(Barrage aBarrage) {
		// 1. Initialisation des attributs
		this.barrage = aBarrage;

		// 2.Construction de l'interface
		this.buildUI();
	}

	/**
	 * Construction de l'interface
	 */
	protected void buildUI() {
		// 1. Lecture panneau principal de la fen�tre
		JPanel panel = (JPanel) this.getContentPane();

		// 2. Initialisation layout de la fen�tre
		panel.setLayout(new BorderLayout());

		// 3. Cr�ation des composants internes
		this.pagesPane = new JPanel(new CardLayout());
		this.navigPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		this.edit = new JButton("Edition");
		this.quit = new JButton("Quitter");
		this.select = new JButton("S�lection");
		this.save = new JButton("Enregistrer");

		// 4. Configuration des composants
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.edit.setEnabled(false);
		this.save.setEnabled(false);
		this.select.setEnabled(false);

		// 5. Cr�ation des pages de l'assistant
		this.chooserPage = new SensorChooserPane(this.barrage.getSensors());
		this.editorPage = new MeasureEditorPane();

		// 6. Installation des pages de l'assistant
		this.pagesPane.add(this.chooserPage, "Selection");
		this.pagesPane.add(this.editorPage, "Edition");

		// 7. Installation des composants
		navigPane.add(this.select);
		navigPane.add(this.edit);
		navigPane.add(Box.createHorizontalStrut(20));
		navigPane.add(this.save);
		navigPane.add(Box.createHorizontalStrut(20));
		navigPane.add(this.quit);

		panel.add(this.pagesPane, BorderLayout.CENTER);
		panel.add(this.navigPane, BorderLayout.SOUTH);

		// 8. Installation des listeners
		this.edit.addActionListener(this);
		this.quit.addActionListener(this);
		this.select.addActionListener(this);
		this.save.addActionListener(this);
		this.chooserPage.addPropertyChangeListener(this);
		this.editorPage.addPropertyChangeListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent aEvent) {
		if (this.edit == aEvent.getSource()) {
			// 1. Lecture liste des instruments/s�lectionn�s
			List<Sensor> selection = this.chooserPage.getSelection();

			if (selection.size() != 0) {
				// 1. Cr�ation liste des mesures
				List<SensorMeasures> measures = new ArrayList<SensorMeasures>();

				for (int i = 0; i < selection.size(); i++) {
					// 1. Lecture ieme instrument/variable s�lectionn�
					Sensor sensor = selection.get(i);

					// 2. Lecture liste des mesures de l'instrument/variable
					SensorMeasures iMeasures = sensor.getMeasures();

					if (iMeasures == null) {
						// 1. Cr�ation d'une liste de mesures vide
						iMeasures = new SensorMeasures(sensor);
					}

					// 3 Enregistrement mesures de l'instrument/variable
					measures.add(iMeasures);
				}

				// 3. Actualisation des controles
				this.select.setEnabled(true);

				// 2. Initialisation panneau d'edition
				this.editorPage.setMeasuresList(measures);

				// 3. Visualisation de la page d'edition
				((CardLayout) this.pagesPane.getLayout()).show(this.pagesPane,
						"Edition");
			}

		} else if (this.quit == aEvent.getSource()) {
			if (this.save.isEnabled()) {
				// 1. Construction titre de la fen�tre
				String title = CIniFileReader.getString("P0APPLNAME") + " "
						+ CIniFileReader.getString("P0APPLVERSION");

				// 2. Construction message d'information
				String message = "Enregistrer les modifications ?";

				// 3. Affichage de la fen�tre de confirmation
				int choice = JOptionPane.showConfirmDialog(null, message,
						title, JOptionPane.YES_NO_OPTION,
						JOptionPane.WARNING_MESSAGE);

				// 4. Fermeture de la fen�tre de dialogue
				this.setVisible(false);

				if (choice == JOptionPane.YES_OPTION) {
					// 1. Enregistrement des modifications
					this.editorPage.saveChanges();
				} else {
					// 1. Annulation des modifications
					this.editorPage.cancelChanges();
				}
				
				// 3. Lib�ration des ressources
				this.dispose();

			} else {
				// 1. Fermeture de la fen�tre
				this.setVisible(false);

				// 2. Annulation des modifications
				this.editorPage.cancelChanges();

				// 3. Lib�ration des ressources
				this.dispose();
			}
		} else if (this.select == aEvent.getSource()) {
			// 1. Visualisation de la page d'edition
			((CardLayout) this.pagesPane.getLayout()).show(this.pagesPane,
					"Selection");
		} else if (this.save == aEvent.getSource()) {
			// 1. Enregistrement des modifications
			this.editorPage.saveChanges();
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent aEvent) {
		if (aEvent.getPropertyName().equals("MeasureEditor.Sensors.Selection")) {

			this.edit.setEnabled((Boolean) aEvent.getNewValue());
		} else if (aEvent.getPropertyName().equals(
				"MeasureEditor.Measures.Dirty")) {
			this.select.setEnabled(!(Boolean) aEvent.getNewValue());
			this.save.setEnabled((Boolean) aEvent.getNewValue());
		}
	}
}
