package editeur;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

@SuppressWarnings("serial")
public class MeasureModePane extends JPanel implements ActionListener {
	/** Composants internes. */
	protected JRadioButton brutEdit;
	protected JRadioButton computedEdit;
	protected JCheckBox showGraph;

	/**
	 * Constructeur
	 */
	public MeasureModePane() {
		// 1. Construction de l'interface
		this.buildUI();
	}

	/**
	 * Construction de l'interface
	 */
	protected void buildUI() {
		// 1. Initialisation layout
		this.setLayout(new FlowLayout(FlowLayout.LEFT));

		// 2. Cr�ation des composants

		this.brutEdit = new JRadioButton("Mesures brutes");
		this.computedEdit = new JRadioButton("Mesures calcul�es");
		this.showGraph = new JCheckBox("Courbes d'�volution");

		// 3. Configuration des composants
		ButtonGroup group = new ButtonGroup();
		group.add(this.brutEdit);
		group.add(this.computedEdit);
		this.brutEdit.setSelected(true);

		// 4. Installation des composants
		this.add(this.brutEdit);
		this.add(this.computedEdit);
		this.add(Box.createHorizontalStrut(20));
		this.add(this.showGraph);

		// 5. Installation des listeners
		this.brutEdit.addActionListener(this);
		this.computedEdit.addActionListener(this);
		this.showGraph.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent aEvent) {
		if (this.brutEdit == aEvent.getSource()
				|| this.computedEdit == aEvent.getSource()) {
			this.firePropertyChange("brutEdit", !this.brutEdit.isSelected(),
					this.brutEdit.isSelected());
		} else {
			this.firePropertyChange("showGraph", !this.showGraph.isSelected(),
					this.showGraph.isSelected());
		}
	}
}
