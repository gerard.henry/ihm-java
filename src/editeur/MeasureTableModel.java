package editeur;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import tools.CIniFileReader;
import cs.util.AbstractBean;
import db.Sensor;
import db.SensorMeasures;
import db.Survey;

public class MeasureTableModel extends AbstractBean {
	/** Nombre de ms en pour un jour. */
	public static final long DAY_MS = 1000 * 60 * 60 * 24;


	@SuppressWarnings("serial")
	protected class DateModel extends AbstractTableModel {
		protected Calendar calendar;

		/**
		 * Constructeur
		 */
		public DateModel() {
			// 1. Initialisation des attributs
			this.calendar = Calendar.getInstance();
		}

		/**
		 * Des mesures sont-elles associ�es � cette date
		 * 
		 * @return true si au moins une mesure est associ�e � cette date
		 */
		public boolean hasMeasures(int aRow, int aColumn) {
			// 1. Lecture date associ�e � la ligne
			Date date = getDateAt(aRow);

			return (date.getTime() >= firstEditable.getTime() && date.getTime() <= lastEditable.getTime());
		}

		@Override
		public int getColumnCount() {
			return 1;
		}

		@Override
		public String getColumnName(int aColumn) {
			switch (aColumn) {
			case 0:
				return "Date";
			}
			return null;
		}

		@Override
		public Class<?> getColumnClass(int aColumn) {
			switch (aColumn) {
			case 0:
				return Date.class;
			}
			return Object.class;
		}

		@Override
		public int getRowCount() {
			return (measures == null ? 0 : editableMeasureCount);
		}

		@Override
		public Object getValueAt(int aRow, int aColumn) {
			return getDateAt(aRow);
		}
	}

	@SuppressWarnings("serial")
	protected class ValueModel extends AbstractTableModel {

		/**
		 * La valeur de la mesure est-elle valide
		 * 
		 * @return true si la valeur de la mesure est valide
		 */
		public boolean isValidMeasure(int aRow, int aColumn) {
			// 1. Lecture date associ�e � la ligne
			Date date = getDateAt(aRow);

			// 2. Lecture d�finition de la liste de mesures
			SensorMeasures meslist = measures.get(aColumn);

			if (meslist.hasMeasure(date)) {
				// 1. Lecture valeur de la mesure
				double valmes = meslist.getMeasure(date);

				return meslist.isValidMeasure(date, valmes);
			} else {
				return true;
			}
		}

		@Override
		public int getColumnCount() {
			return measures.size();
		}

		public String getColumnName(int aColumn) {
			// 1. Lecture d�finition de l'instrument/Variable
			Sensor sensor = measures.get(aColumn).getSensor();

			if (sensor.getUnit().length() == 0) {
				return sensor.getId();
			} else {
				return sensor.getId() + " (" + sensor.getUnit() + ")";
			}
		}

		@Override
		public int getRowCount() {
			return (measures == null ? 0 : editableMeasureCount);
		}

		@Override
		public Class<?> getColumnClass(int aColumn) {
			return Double.class;
		}

		@Override
		public Object getValueAt(int aRow, int aColumn) {
			// 1. Lecture date associ�e � la ligne
			Date date = getDateAt(aRow);

			// 2. Lecture d�finition de la liste de mesures
			SensorMeasures meslist = measures.get(aColumn);

			if (meslist.hasMeasure(date)) {
				// 1. Lecture valeur de la mesure
				return meslist.getMeasure(date);					
			} else {
				return null;
			}
		}

		@Override
		public boolean isCellEditable(int aRow, int aColumn) {
			// 1. Lecture d�finition des mesures de l'instrument/variable
			SensorMeasures meslist = measures.get(aColumn);

			// 2. Lecture date pour cette ligne
			Date mesdat = getDateAt(aRow);

			return meslist.isMeasureEditable(mesdat);
		}

		@Override
		public void setValueAt(Object aValue, int aRow, int aColumn) {
			// 1. Actualisation valeur de la mesure
			updateMeasure((Double) aValue, aRow, aColumn);
		}
	}

	/** Liste des mesures � editer. */
	protected List<SensorMeasures> measures;

	/** Modeles des donn�es edit�es. */
	protected DateModel dateModel;
	protected ValueModel valueModel;

	/** Dates de la premi�re/derni�re mesure editable. */
	protected Date firstEditable;
	protected Date lastEditable;

	/** Nombre de mesures editable. */
	protected int editableMeasureCount;

	/** Drapeau de modification de la liste des mesures. */
	protected boolean isDirty;

	/** Mode d'edition des mesures. */
	protected MeasureEditorPane.EditionMode editMode;
	
	/**
	 * Constructeur
	 * 
	 * @param aMeasures
	 *            Liste des mesures � editer
	 */
	public MeasureTableModel(List<SensorMeasures> aMeasures) {
		// 1. Initialisation des attributs
		this.measures = aMeasures;

		// 2. Actualisation du mod�le
		this.updateModel();

		// 3. Cr�ation mod�le des dates et valeurs de mesures
		this.dateModel = new DateModel();
		this.valueModel = new ValueModel();
		
		// 4. Initialisation du mode d'�dition
		this.editMode = MeasureEditorPane.EditionMode.BRUT;
	}

	/**
	 * Modification du mode d'�dition
	 * @param aMode Mode d'edition des mesures
	 */
	public void setEditionMode(MeasureEditorPane.EditionMode aMode) {
		// 1. Actualisation du mode d'edition
		this.editMode = aMode;
		
		// 2. Actualisation des donn�es visualis�es
		this.valueModel.fireTableDataChanged();
	}
	
	/**
	 * Lecture valeur du mode d'edition
	 * @return Valeur du mode d'�dition
	 */
	public MeasureEditorPane.EditionMode getEditionMode() {
		return this.editMode;
	}
	
	/**
	 * Initialisation du mod�le
	 */
	public void updateModel() {
		// 1. Lecture intervalle d'edition
		this.firstEditable = this.getFirstAllowedDate();
		this.lastEditable = this.getLastAllowedDate();

		// 2. Calcul nombre de jours dans l'intervalle d'edition
		this.editableMeasureCount = (int)((lastEditable.getTime()- firstEditable
				.getTime())/ DAY_MS);
	}
	
	/**
	 * Lecture liste des mesures edit�es
	 * 
	 * @return Liste des mesures edit�es
	 */
	public List<SensorMeasures> getMeasures() {
		return measures;
	}

	/**
	 * La liste de mesures a-t-elle �t� modifi�e
	 * 
	 * @return true si la liste est modifi�e
	 */
	public boolean isDirty() {
		return isDirty;
	}

	/**
	 * Actualisation drapeau de modification de la liste de mesures
	 * 
	 * @param isDirty
	 *            true si la liste est modifi�e
	 */
	public void setDirty(boolean aDirty) {
		// 1. Notification des changements
		this.firePropertyChange("MeasureEditor.Measures.Dirty", this.isDirty,
				this.isDirty = aDirty);
	}

	/**
	 * Lecture mod�le de la table des dates de mesures
	 * 
	 * @return Mod�le des dates de mesures
	 */
	public DateModel getDateModel() {
		return this.dateModel;
	}

	/**
	 * Lecture mod�le des valeurs de mesures
	 * 
	 * @return Mod�le des valeurs de mesures
	 */
	public ValueModel getValueModel() {
		return this.valueModel;
	}

	/**
	 * Lecture date pour le neme jours apr�s la premi�re date editable
	 * @param aDayCount Nombre de jours apr�s la premi�re date editable
	 * @return Valeur de la date
	 */
	public Date getDateAt(int aDayCount) {
		// 1. Lecture gestionnaire de dates
		Calendar calendar = Calendar.getInstance();
		
		// 2. Initialisation � la premi�re date editable
		calendar.setTime(firstEditable);

		// 3. Ajout du nombre de jours apr�s la premi�re editable
		calendar.add(Calendar.DATE, aDayCount);

		return calendar.getTime();
	}
	
	/**
	 * Lecture nombre de jours apr�s la premi�re date editable
	 * @param aDate Une date
	 * @return nombre de jours apr�s la premi�re date editable
	 */
	public int getDayAt(Date aDate) {
		return (int) ((aDate.getTime() - firstEditable
				.getTime()) / DAY_MS);
	}
	/**
	 * Lecture premi�re date permise
	 * 
	 * @return Premi�re date permise
	 */
	public Date getFirstAllowedDate() {
		// 1. Lecture date d'installation de l'instrument
		Date first = null;

		for (int i = 0; i < this.measures.size(); i++) {
			// 1. Lecture liste des mesures du ieme instrument/ variable
			SensorMeasures iMeasures = this.measures.get(i);

			// 2. Si l'instrument/variable n'a pas de mesures on l'ignore
			if (iMeasures == null)
				continue;

			// 3. Lecture premi�re date editable du ieme instrument/variable
			Date iDate = iMeasures.getFirstAllowedDate();

			if (first == null) {
				// 1. Initialisation premi�re date editable
				first = iDate;
			} else {
				if (iDate.before(first)) {
					// 1. Initialisation premi�re date editable
					first = iDate;
				}
			}
		}

		return first;
	}

	/**
	 * Lecture derni�re date permise
	 * 
	 * @return Derni�re date permise
	 */
	public Date getLastAllowedDate() {
		// 1. Lecture date actuelle
		Calendar now = Calendar.getInstance();

		// 2. R�initialisation heure,min,sec
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		return now.getTime();
	}


	/**
	 * Enregistrement modification d'une mesure
	 * 
	 * @param aValue
	 *            Valeur de la mesure
	 * @param aRow
	 *            Num�ro de ligne edit�e
	 * @param aColumn
	 *            Num�ro de la colonne edit�e
	 * 
	 * @return true si la mesure a �t� supprim�e/ajout�e ou modifi�e
	 */
	protected void updateMeasure(Double aValue, int aRow, int aColumn) {
		Double rawValue = null;
		
		// 1. Lecture d�finition des mesures de l'instrument/variable
		SensorMeasures meslist = measures.get(aColumn);

		// 2. Lecture date de la mesure
		Date mesdat = (Date) getDateAt(aRow);
		
		if (aValue == null) {
			// 1. Lecture ancienne valeur de la mesure
			Double oldValue = meslist.getMeasure(mesdat);

			// 2. Suppression de la mesure
			meslist.removeMeasure(mesdat);
			
			// 3. Notification des modifications
			this.firePropertyChange("MeasureEditor.Measures.Changed", oldValue, null);
		} else {
			if ( MeasureTableModel.this.editMode == MeasureEditorPane.EditionMode.BRUT) {
				// 1. Initialisation valeur brute relev�e
				rawValue = aValue;
				
				// 2. Calcul valeur de la mesure
				aValue = meslist.getComputedMeasure(rawValue);
			} 
		
			if (meslist.hasMeasure(mesdat)) {
				// 1. Lecture ancienne valeur de la mesure
				Double oldValue = meslist.getMeasure(mesdat);

				if (!oldValue.equals(aValue)) {
					if (!meslist.isValidMeasure(mesdat, aValue)) {
						if (!acceptBadMeasureValue(aValue)) {
							// 1. Annulation de l'�dition
							this.valueModel.fireTableCellUpdated(aRow, aColumn);

							// 2. Abandon du traitement
							return;
						}
					}

					// 2. Enregistrement de la mesure
					meslist.addMeasure(mesdat, aValue);
					
					// 3. Enregistrement de la valeur brute relev�e
					meslist.setRawValue(mesdat, rawValue);
					
					// 4. Notification des modifications
					this.firePropertyChange("MeasureEditor.Measures.Changed", oldValue, aValue);
				} else {
					// 1. Abandon du traitement
					return;
				}
			} else {
				if (!meslist.isValidMeasure(mesdat, aValue)) {
					if (!acceptBadMeasureValue(aValue)) {
						// 1. Annulation de l'�dition
						this.valueModel.fireTableCellUpdated(aRow, aColumn);

						// 2. Abandon du traitement
						return;
					}
				}

				// 2. Enregistrement de la mesure
				meslist.addMeasure(mesdat, aValue);

				// 3. Enregistrement de la valeur brute relev�e
				meslist.setRawValue(mesdat, rawValue);
		
				// 4. Notification des modifications
				this.firePropertyChange("MeasureEditor.Measures.Changed", null, aValue);
			}
		}

		// 3. Actualisation du mod�le
		this.updateModel();

		// 4. Notification de la modification des mesures
		this.setDirty(true);

		// 5. Notification des composants de visualisation
		this.dateModel.fireTableDataChanged();
		this.valueModel.fireTableDataChanged();
	}

	/**
	 * Demande de confirmation de la prise en compte d'une valeur de mesure hors
	 * du domaine de validit�
	 * 
	 * @return true si la valeur doit �tre prise en compte
	 */
	protected boolean acceptBadMeasureValue(Double aValue) {
		// 1. Construction titre de la fen�tre
		String title = CIniFileReader.getString("P0APPLNAME") + " "
				+ CIniFileReader.getString("P0APPLVERSION");

		// 2. Construction message d'information
		String message = "Valeur ["+Survey.format(aValue)+"] hors du domaine de validite. Confirmez-vous cette valeur ?";

		// 3. Affichage de la fen�tre de confirmation
		int choice = JOptionPane.showConfirmDialog(null, message, title,
				JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

		return (choice == JOptionPane.YES_OPTION);
	}
}
