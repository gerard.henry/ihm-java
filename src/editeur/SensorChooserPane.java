package editeur;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import db.Sensor;

@SuppressWarnings("serial")
public class SensorChooserPane extends JPanel implements ListSelectionListener {

	protected class SensorModel extends AbstractTableModel {

		/**
		 * Constructeur
		 */
		public SensorModel() {
		}

		@Override
		public String getColumnName(int aColumn) {
			switch (aColumn) {
			case 0:
				return "Num�ro";
			case 1:
				return "Identificateur";
			case 2:
				return "Titre";
			case 3:
				return "Nature";
			case 4:
				return "Type";
			case 5:
				return "Groupe de saisie";
			}

			return null;
		}

		@Override
		public int getColumnCount() {
			return 6;
		}

		@Override
		public int getRowCount() {
			return sensors.size();
		}

		@Override
		public Object getValueAt(int aRow, int aColumn) {
			switch (aColumn) {
			case 0:
				return (aRow + 1) + "/" + getRowCount();
			case 1:
				return sensors.get(aRow).getId();
			case 2:
				return sensors.get(aRow).getTitle();
			case 3:
				return sensors.get(aRow).getNature();
			case 4:
				return sensors.get(aRow).getType();
			case 5:
				return sensors.get(aRow).getGroup();

			}

			return null;
		}

	}

	protected class SelectAllListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent aEvent) {
			chooser.selectAll();
		}
	}

	protected class ClearAllListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent aEvent) {
			chooser.clearSelection();
		}
	}

	protected class PopupListener extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent aEvent) {
			if (aEvent.isPopupTrigger()) {
				menu.show(aEvent.getComponent(), aEvent.getX(), aEvent.getY());
			}
		}

		public void mouseReleased(MouseEvent aEvent) {
			if (aEvent.isPopupTrigger()) {
				menu.show(aEvent.getComponent(), aEvent.getX(), aEvent.getY());
			}
		}
	}

	/** Composants interne. */
	protected JTable chooser;
	protected JPopupMenu menu;

	/** Liste des instruments/variables s�lectionnables. */
	protected List<Sensor> sensors;

	/** Mod�le des donn�es de la table. */
	protected SensorModel sensorModel;

	/**
	 * Constructeur
	 * 
	 * @param aList
	 *            Liste des instruments/variables s�lectionnables
	 */
	public SensorChooserPane(List<Sensor> aList) {
		// 1. Initialisation des attributs
		this.sensors = aList;

		// 2. Construction de l'interface
		this.buildUI();
	}

	/**
	 * Lecture liste des instruments/variable s�lectionn�s
	 * 
	 * @return Liste des instruments variables s�lectionn�s
	 */
	public List<Sensor> getSelection() {
		// 1. Cr�ation liste des instruments/variables s�lectionn�s
		List<Sensor> selection = new ArrayList<Sensor>();

		// 2. Lecture liste des lignes s�lectionn�es
		int rows[] = chooser.getSelectedRows();

		for (int i = 0; i < rows.length; i++) {
			// 1. Conversion en index dans la liste des instruments/variables
			rows[i] = chooser.convertRowIndexToModel(rows[i]);

			// 2. Actualisation de la s�lection
			selection.add(this.sensors.get(rows[i]));
		}

		return selection;
	}

	/**
	 * Construction de l'interface
	 */
	protected void buildUI() {
		// 1. Creation layout du panneau
		this.setLayout(new BorderLayout());

		// 2. Cr�ation mod�le des donn�es de la table
		this.sensorModel = new SensorModel();

		// 3. Cr�ation des composants
		this.chooser = new JTable(this.sensorModel);
		this.menu = this.createMenu();

		// 4. Configuration des composants
		this.chooser.setRowSorter(new TableRowSorter(this.sensorModel));

		// 5. Installation des listeners
		this.chooser.addMouseListener(new PopupListener());
		this.chooser.getSelectionModel().addListSelectionListener(this);

		// 6. Installation des composants
		this.add(new JScrollPane(this.chooser), BorderLayout.CENTER);
	}

	/**
	 * Cr�ation du menu de s�lection
	 * 
	 * @return Menu de s�lection des �lements de la table
	 */
	protected JPopupMenu createMenu() {
		// 1. Cr�ation du menu
		this.menu = new JPopupMenu();

		// 2. Cr�ation des items du menu
		JMenuItem all = new JMenuItem("Tout s�lectionner");
		JMenuItem clear = new JMenuItem("Tout d�s�lectionner");

		// 3. Installation des listeners
		all.addActionListener(new SelectAllListener());
		clear.addActionListener(new ClearAllListener());

		// 4. Installation des items du menu
		this.menu.add(all);
		this.menu.add(clear);

		return menu;
	}

	@Override
	public void valueChanged(ListSelectionEvent aEvent) {
		if ( !aEvent.getValueIsAdjusting()) {
			// 1. Lecture nombre d'instruments variables s�lectionn�es
			int sensorCount = this.chooser.getSelectedRowCount();
			
			// 2. Notification du changement de la s�lection
			this.firePropertyChange("MeasureEditor.Sensors.Selection",sensorCount==0, sensorCount!=0);
		}
	}
}
