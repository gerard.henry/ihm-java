package editeur;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import db.SensorMeasures;
import db.Sensor;

@SuppressWarnings("serial")
public class MeasureInfoPane extends JPanel implements ActionListener,
		PropertyChangeListener {
	/** Composants internes. */
	protected JLabel type;
	protected JTextField title;
	protected JTextField formula;
	protected JTextField date;
	protected JTextField brute;
	protected JTextField computed;
	protected JTextField min;
	protected JTextField max;
	protected JTextField variation;
	protected JButton first;
	protected JButton last;
	protected JButton next;
	protected JButton prev;
	protected JFormattedTextField search;

	/** D�finition des mesures de l'instrument/variable courant. */
	protected SensorMeasures measures;

	/** Date de la mesure courante. */
	protected Date currentDate;

	/**
	 * Constructeur
	 */
	public MeasureInfoPane() {
		super(new GridBagLayout());

		// 1. Cr�ation des composants internes
		this.type = new JLabel("Instrument");
		this.title = new JTextField(32);
		this.formula = new JTextField(32);
		this.date = new JTextField(12);
		this.brute = new JTextField(12);
		this.computed = new JTextField(12);
		this.min = new JTextField(12);
		this.max = new JTextField(12);
		this.variation = new JTextField(12);

		// 2. Configuration des composants
		this.title.setEditable(false);
		this.formula.setEditable(false);
		this.date.setEditable(false);
		this.brute.setEditable(false);
		this.computed.setEditable(false);
		this.min.setEditable(false);
		this.max.setEditable(false);
		this.variation.setEditable(false);

		// 2. Cr�ation contraintes de positionnement
		GridBagConstraints gbc = new GridBagConstraints();

		// 3. Installation des composants
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(2, 3, 2, 3);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.LINE_START;
		gbc.weightx = 0;
		this.add(this.type, gbc);

		gbc.gridx = 1;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.weightx = 1;
		this.add(this.title, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		this.add(new JLabel("Formule"), gbc);

		gbc.gridx = 1;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.weightx = 1;
		this.add(this.formula, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		this.add(new JLabel("Date"), gbc);

		gbc.gridx = 1;
		this.add(this.date, gbc);

		gbc.gridx = 2;
		gbc.weightx = 0;
		this.add(new JLabel("Valeur brute"), gbc);

		gbc.gridx = 3;
		gbc.weightx = 1;
		this.add(this.brute, gbc);

		gbc.gridx = 4;
		gbc.weightx = 0;
		this.add(new JLabel("Valeur calcul�e"), gbc);

		gbc.gridx = 5;
		gbc.weightx = 1;
		this.add(this.computed, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		this.add(new JLabel("Min"), gbc);

		gbc.gridx = 1;
		this.add(this.min, gbc);

		gbc.gridx = 2;
		gbc.weightx = 0;
		this.add(new JLabel("Max"), gbc);

		gbc.gridx = 3;
		gbc.weightx = 1;
		this.add(this.max, gbc);

		gbc.gridx = 4;
		gbc.weightx = 0;
		this.add(new JLabel("Variation max"), gbc);

		gbc.gridx = 5;
		gbc.weightx = 1;
		this.add(this.variation, gbc);

		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 6;
		gbc.weightx = 1;
		gbc.insets = new Insets(10, 5, 5, 5);
		this.add(this.buildNavigationPane(), gbc);
	}

	/**
	 * Construction panneau de navigation
	 */
	protected JPanel buildNavigationPane() {
		// 1. Cr�ation du panneau de navigation
		JPanel navPane = new JPanel(new FlowLayout(FlowLayout.CENTER));

		// 2. Cr�ation des composants
		this.first = new JButton("Premi�re");
		this.last = new JButton("Derni�re");
		this.prev = new JButton("Pr�c�dente");
		this.next = new JButton("Suivante");
		this.search = new JFormattedTextField(MeasureRenderers.dateFormat);

		// 3. Configuration des composants
		this.first.setEnabled(false);
		this.last.setEnabled(false);
		this.next.setEnabled(false);
		this.prev.setEnabled(false);
		this.search.setColumns(10);

		// 4. Installation des composants
		navPane.add(this.first);
		navPane.add(this.prev);
		navPane.add(this.search);
		navPane.add(this.next);
		navPane.add(this.last);

		// 5. Installation des listeners
		this.first.addActionListener(this);
		this.last.addActionListener(this);
		this.prev.addActionListener(this);
		this.next.addActionListener(this);
		this.search.addPropertyChangeListener("value", this);

		return navPane;
	}

	/**
	 * Initialisation informations � afficher
	 * 
	 * @param aMeasures
	 *            Mesures de l'instrument/variable
	 * @param aDate
	 *            Date de la mesure
	 */
	public void showInfo(SensorMeasures aMeasure, Date aDate) {
		// 1. Initialisation des attributs
		this.measures = aMeasure;
		this.currentDate = aDate;

		// 2. Lecture r�f�rence � l'instrument/variable
		Sensor sensor = aMeasure.getSensor();

		// 3. Actualisation des composants
		this.first.setEnabled(true);
		this.last.setEnabled(true);
		this.next.setEnabled(true);
		this.prev.setEnabled(true);
		this.search.setValue(this.currentDate);
		this.type.setText(sensor.getId());
		this.title.setText(sensor.getTitle());
		this.formula.setText(sensor.getFormula());

		this.min.setText(MeasureRenderers.doubleFormat.format(sensor
				.getMinimumValue()));
		this.max.setText(MeasureRenderers.doubleFormat.format(sensor
				.getMaximumValue()));
		this.variation.setText(MeasureRenderers.doubleFormat.format(sensor
				.getMaximumVariation()));
		if (aMeasure.hasMeasure(aDate)) {
			// 1. Lecture valeur brute rel�v�e
			Double rawValue = aMeasure.getRawValue(aDate);

			if (rawValue != null) {
				this.brute.setText(MeasureRenderers.doubleFormat
						.format(rawValue));
			} else {
				this.brute.setText("");
			}
			this.computed.setText(MeasureRenderers.doubleFormat.format(aMeasure
					.getMeasure(aDate)));
			this.date.setText(MeasureRenderers.dateFormat.format(aDate));
		} else {
			this.brute.setText("");
			this.computed.setText("");
			this.date.setText("");
		}
	}

	@Override
	public void actionPerformed(ActionEvent aEvent) {
		Date gotoDate = null;

		if (this.first == aEvent.getSource()) {
			gotoDate = this.measures.getDateFirstMeasure();
		} else if (this.last == aEvent.getSource()) {
			gotoDate = this.measures.getDateLastMeasure();
		} else if (this.next == aEvent.getSource()) {
			gotoDate = this.measures.getDateOfNextMeasure(this.currentDate);
		} else if (this.prev == aEvent.getSource()) {
			gotoDate = this.measures.getDateOfPreviousMeasure(this.currentDate);
		}

		// 1. Notification de la mesure a afficher
		this.firePropertyChange("dateMeasure", this.currentDate, gotoDate);
	}

	@Override
	public void propertyChange(PropertyChangeEvent aEvent) {
		// 1. Lecture valeur de la date a atteindre
		Date gotoDate = (Date) aEvent.getNewValue();

		// 2. Notification de la mesure a afficher
		this.firePropertyChange("dateMeasure", this.currentDate, gotoDate);
	}
}
