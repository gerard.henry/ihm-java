package editeur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import db.SensorMeasures;

import editeur.MeasureTableModel.ValueModel;

@SuppressWarnings("serial")
public class MeasureTable extends JPanel  {
	/** Couleur de trace des dates comportant des mesures. */
	protected static final Color dateWithMeasures = new Color(180, 255, 180);

	/** Couleur de trace  des valeurs hors domaine. */
	protected static final Color badMeasureValue = new Color(255, 160, 180);

	protected class DateTable extends JTable  {
		/**
		 * Constructeur
		 * 
		 * @param aModel
		 *            Mod�le de la table
		 */
		public DateTable() {
		}

		@Override
		public void valueChanged(ListSelectionEvent e) {
			super.valueChanged(e);
			int leftIndex = dateTable.getSelectedRow();
			int rightIndex = valueTable.getSelectedRow();
			if (leftIndex != rightIndex && leftIndex != -1) {
				valueTable.setRowSelectionInterval(leftIndex, leftIndex);
			}
		}

		@Override
		public TableCellRenderer getCellRenderer(int aRow, int aColumn) {
			// 1. Lecture objet de rendu des dates
			MeasureRenderers.DateRenderer renderer = MeasureRenderers.dateRenderer;

			// 2. Initialisation mode de trace
			if (((MeasureTableModel.DateModel) this.getModel()).hasMeasures(
					aRow, aColumn)) {
				renderer.setBackground(dateWithMeasures);
			} else {
				renderer.setBackground(this.getBackground());
			}

			return renderer;
		}
	}

	protected class ValueTable extends JTable  {
		/**
		 * Constructeur
		 */
		public ValueTable() {
			// 1. Initialisation des modes de selection
			this.setCellSelectionEnabled(true);
			this.setColumnSelectionAllowed(false);
			this.setRowSelectionAllowed(false);
		}

		@Override
		public void valueChanged(ListSelectionEvent e) {
			super.valueChanged(e);
			int leftIndex = dateTable.getSelectedRow();
			int rightIndex = valueTable.getSelectedRow();
			if (leftIndex != rightIndex && rightIndex != -1) {
				dateTable.setRowSelectionInterval(rightIndex, rightIndex);
			}
		}

		@Override
		public TableCellRenderer getCellRenderer(int aRow, int aColumn) {
			// 1. Lecture objet de rendu des dates
			MeasureRenderers.NumberRenderer renderer = MeasureRenderers.numberRenderer;

			// 2. Initialisation mode de trace
			if (!this.getModel().isCellEditable(aRow, aColumn)) {
				renderer.setBackground(Color.lightGray);
			} else {
				if ( ((MeasureTableModel.ValueModel)this.getModel()).isValidMeasure(aRow, aColumn) ) {
					renderer.setBackground(this.getBackground());
					
				} else {
					renderer.setBackground(badMeasureValue);
				}
			}

			return renderer;
		}
		
		@Override
		public TableCellEditor getCellEditor(int aRow,int aColumn) {
			// 1. Lecture mesures de l'instrument/variable � editer
			SensorMeasures measures= MeasureTable.this.model.getMeasures().get(aColumn);
			
			// 2. Lecture date de la mesure
			Date mesdat = MeasureTable.this.model.getDateAt(aRow);

			// 3. Lecture du mode d'�dition courant
			MeasureEditorPane.EditionMode editMode = MeasureTable.this.model.getEditionMode();
			
			// 4. Initialisation contexte d'edition
			MeasureEditors.measureEditor.setEditionContext(measures,mesdat,editMode);
			
			return MeasureEditors.measureEditor;
		} 
	}

	/** Mod�le de la table de mesure. */
	protected MeasureTableModel model;

	/** Tables d'�dition des dates et valeurs de mesures. */
	protected DateTable dateTable;
	protected ValueTable valueTable;

	/**
	 * Constructeur
	 * 
	 * @param aModel
	 *            Mod�le de la table des mesures
	 */
	public MeasureTable() {
		super(new BorderLayout());

		// 1. Cr�ation des tables d'�dition des dates et valeurs de mesures
		this.dateTable = new DateTable();
		this.valueTable = new ValueTable();

		// 2. Cr�ation conteneurs des tables
		JScrollPane scrollPane = new JScrollPane(this.valueTable);
		JViewport viewport = new JViewport();

		// 3. Configuration des composants
		valueTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		dateTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		valueTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		dateTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		valueTable.getTableHeader().setReorderingAllowed(false);
		dateTable.getTableHeader().setReorderingAllowed(false);
		dateTable.getTableHeader().setResizingAllowed(false);
		valueTable.getTableHeader().setDefaultRenderer(MeasureRenderers.headerRenderer);
		
		// 4. Installation des composants
		viewport.setView(this.dateTable);
		viewport.setPreferredSize(new Dimension(10, 10));
		scrollPane.setRowHeaderView(viewport);
		scrollPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, this.dateTable
				.getTableHeader());

		this.add(scrollPane, BorderLayout.CENTER);
	}

	/**
	 * Lecture composant d'affichage des dates de mesures
	 * @return composant d'affichage des dates de mesures
	 */
	public DateTable getDateTable() {
		return dateTable;
	}

	/**
	 * Lecture composant d'edition des mesures
	 * @return composant  d'edition des mesures
	 */
	public ValueTable getValueTable() {
		return valueTable;
	}

	/**
	 * Initialisation mod�le des mesures
	 * 
	 * @param aModel
	 *            Mod�le des mesures
	 */
	public void setModel(MeasureTableModel aModel) {
		// 1. Initialisation des attributs
		this.model = aModel;

		// 2. Initialisation mod�les des tables
		this.dateTable.setModel(this.model.getDateModel());
		this.valueTable.setModel(this.model.getValueModel());

		// 3. Initialisation dimension de la table des dates
		((JViewport) this.dateTable.getParent())
				.setPreferredSize(this.dateTable.getPreferredSize());
	}

	/**
	 * Positionnement de la table sur la date
	 * 
	 * @param aDate
	 *            Une date
	 */
	public void setDateVisible(Date aDate) {
		if ( this.model != null ) {
			// 1. Lecture index de la date
			int absIndex = this.model.getDayAt(aDate);

			this.valueTable.setRowSelectionInterval(absIndex, absIndex);
			this.valueTable.scrollRectToVisible(this.valueTable
					.getCellRect(absIndex, 0, true));
		}
	}
}
