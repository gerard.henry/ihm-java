package editeur;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import db.SensorMeasures;
import db.Survey;

@SuppressWarnings("serial")
public class MeasureEditorPane extends JPanel implements
		PropertyChangeListener, ListSelectionListener {
	/** Composants internes. */
	protected MeasureTable editTable;
	protected MeasureInfoPane infoPane;
	protected MeasureModePane modePane;
	protected MeasureGraphPane graphPane;
	protected JSplitPane splitPane;

	/** Mod�le de la table des mesures. */
	protected MeasureTableModel modelTable;

	/** Mode d'�dition des mesures. */
	public enum EditionMode {
		BRUT, COMPUTED
	}
	
	/**
	 * Constructeur
	 */
	public MeasureEditorPane() {
		// 1. Construction de l'interface
		this.buildUI();
	}

	/**
	 * Initialisation liste des mesures � editer
	 * 
	 * @param aMeasures
	 *            Liste des mesures � editer
	 */
	public void setMeasuresList(List<SensorMeasures> aMeasures) {
		if ( this.modelTable != null ){
			// 1. D�sinstallation des listeners
			this.modelTable.removePropertyChangeListener(this);
		}
		
		// 1. Cr�ation mod�le de la table des mesures
		this.modelTable = new MeasureTableModel(aMeasures);

		// 2. Initialisation mod�le des mesures
		this.editTable.setModel(this.modelTable);
		
		// 3. Installation des listeners
		this.modelTable.addPropertyChangeListener(this);
	}

	/**
	 * lecture liste des mesures edit�es
	 * 
	 * @return 
	 *            Liste des mesures edit�es
	 */
	public List<SensorMeasures> getMeasuresList() {
		return this.modelTable.getMeasures();
	}
	
	/**
	 * Enregistrement des modifications
	 */
	public void saveChanges() {
		// 1. Lecture liste des mesures edit�es
		List<SensorMeasures> measures = this.getMeasuresList();
		
		// 2. Actualisation d�finition des mesures
		for ( int i = 0 ; i < measures.size(); i++) {
			Survey.getInstance().updateSensorMeasures(measures.get(i), false);
		}
		
		// 3. Enregistrement des modifications
		Survey.getInstance().applyChanges();
				
		// 4. Actualisation drapeau de modification
		this.modelTable.setDirty(false);
	}
	
	/**
	 * Annulation des modifications
	 */
	public void cancelChanges() {
		// 1. Annulation des modifications
		Survey.getInstance().cancelChanges();
		
		// 2. Actualisation drapeau de modification
		this.modelTable.setDirty(false);
	}
	
	/**
	 * Construction de l'interface
	 */
	protected void buildUI() {
		// 1. Initialisation layout du panneau
		this.setLayout(new BorderLayout());

		// 2. Cr�ation des composants
		this.infoPane = new MeasureInfoPane();
		this.splitPane = new JSplitPane();

		// 3. Configuration des composants
		this.splitPane.setOneTouchExpandable(true);
		this.splitPane.setEnabled(false);

		// 4. Installation des composants
		this.splitPane.setLeftComponent(this.buildEditionPane());
		this.splitPane.setRightComponent(this.buildGraphPane());
		this.add(this.splitPane, BorderLayout.CENTER);
		this.add(this.infoPane, BorderLayout.NORTH);

		// 6. Installation des listeners
		this.infoPane.addPropertyChangeListener("dateMeasure", this);
	}

	/**
	 * Construction du panneau d'edition
	 */
	protected JPanel buildEditionPane() {
		// 1. Cr�ation du panneau d'edition
		JPanel editPane = new JPanel(new BorderLayout());

		// 2. Cr�ation des composants
		this.editTable = new MeasureTable();
		this.modePane = new MeasureModePane();

		// 3. Installation des composants
		editPane.add(this.editTable, BorderLayout.CENTER);
		editPane.add(this.modePane, BorderLayout.SOUTH);

		// 6. Installation des listeners
		this.editTable.getValueTable().getSelectionModel()
				.addListSelectionListener(this);
		this.editTable.getValueTable().getColumnModel().getSelectionModel()
				.addListSelectionListener(this);
		this.modePane.addPropertyChangeListener(this);

		// 7. Initialisation taille minimale du panneau d'edition
		editPane.setMinimumSize(new Dimension(400, 400));

		return editPane;
	}

	/**
	 * Construction du panneau d'affichage des graphes
	 */
	protected JPanel buildGraphPane() {
		// 1. Cr�ation panneau de visualisation des graphes
		this.graphPane = new MeasureGraphPane();

		// 2. Configuration des composants
		this.graphPane.setVisible(false);

		return graphPane;
	}

	@Override
	public void propertyChange(PropertyChangeEvent aEvent) {
		if (aEvent.getPropertyName().equals("dateMeasure")) {
			// 1. Lecture valeur de la date � rechercher
			Date aDate = (Date) aEvent.getNewValue();

			// 2. Affichage de la date
			if (aDate != null)
				this.editTable.setDateVisible(aDate);
		} else if (aEvent.getPropertyName().equals("brutEdit")) {
			if ((Boolean) aEvent.getNewValue()) {
				this.modelTable.setEditionMode(MeasureEditorPane.EditionMode.BRUT);
			} else {
				this.modelTable.setEditionMode(MeasureEditorPane.EditionMode.COMPUTED);
			}
		} else if (aEvent.getPropertyName().equals("showGraph")) {
			if ((Boolean) aEvent.getNewValue()) {
				// 1. Affichage du panneau des graphes
				splitPane.setEnabled(true);
				splitPane.getRightComponent().setVisible(true);
				splitPane.resetToPreferredSizes();

				// 2. Lecture colonne s�lectionn�e
				int column = this.editTable.getValueTable().getSelectedColumn();

				if (column != -1) {
					// 1. Lecture d�finition de la liste de mesures
					SensorMeasures meslist = this.modelTable.getMeasures().get(
							column);

					// 2. Affichage du graphe des mesures
					this.graphPane.showMeasures(meslist);
				}
			} else {
				// 1. On masque le panneau des graphes
				splitPane.setEnabled(false);
				splitPane.getRightComponent().setVisible(false);
				splitPane.resetToPreferredSizes();
			}
		} else if (aEvent.getPropertyName().equals(
				"MeasureEditor.Measures.Dirty")) {
			// 1. Propagation de la notification
			this.firePropertyChange("MeasureEditor.Measures.Dirty", aEvent
					.getOldValue(), aEvent.getNewValue());
		}else if (aEvent.getPropertyName().equals(
				"MeasureEditor.Measures.Changed")) {
			// 1. Lecture cellule s�lectionn�e
			int column = this.editTable.getValueTable().getSelectedColumn();
			int row = this.editTable.getValueTable().getSelectedRow();

			if (column != -1 && row != -1) {
				// 1. Lecture d�finition de la liste de mesures
				SensorMeasures measures = this.modelTable.getMeasures().get(column);
				
				// 2. Actualisation panneau d'affichage de l'�volution de la mesure
				if (this.graphPane.isVisible()) {
					this.graphPane.showMeasures(measures);
				}
			}
		}

	}

	@Override
	public void valueChanged(ListSelectionEvent aEvent) {

		// 1. Lecture cellule s�lectionn�e
		int column = this.editTable.getValueTable().getSelectedColumn();
		int row = this.editTable.getValueTable().getSelectedRow();

		if (column != -1 && row != -1) {
			// 1. Lecture d�finition de la liste de mesures
			SensorMeasures measures = this.modelTable.getMeasures().get(column);

			// 2. Lecture date de la mesure
			Date datmes = (Date) this.modelTable.getDateModel().getValueAt(row,
					0);

			// 3. Actualisation information sur la mesure
			this.infoPane.showInfo(measures, datmes);

			// 4. Actualisation panneau d'affichage de l'�volution de la mesure
			if (this.graphPane.isVisible()) {
				this.graphPane.showMeasures(measures);
			}
		}

		// 2. Actualisation entete de la table (Affichage de la colonne
		// s�lectionn�e)
		this.editTable.getValueTable().getTableHeader().repaint();
	}
}
