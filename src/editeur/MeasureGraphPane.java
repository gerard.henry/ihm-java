package editeur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

import controls.BaseView;
import db.Sensor;
import db.SensorMeasures;

@SuppressWarnings("serial")
public class MeasureGraphPane extends BaseView {
	/** Composants internes. */
	protected ChartPanel chartPane;

	/** Liste de mesures viaualis�es. */
	protected SensorMeasures measures;
	
	/**
	 * Constructeur
	 */
	public MeasureGraphPane() {
		this.setLayout(new BorderLayout());
	}

	
	/**
	 * Visualisation des mesures de l'instrument/Variable
	 * 
	 * @param aMeasures
	 *            Mesures de l'instrument/Variable
	 */
	public void showMeasures(SensorMeasures aMeasures) {
		// 1. Actualisation liste de mesures visualis�es
		this.measures = aMeasures;
		
		// 2. Cr�ation du graphe � visualiset
		JFreeChart chart = this.createChart(aMeasures);

		// 3. Affichage du graphe
		if (this.chartPane == null) {
			// 1. Cr�ation panneau 'affichage du graphe
			this.chartPane = new ChartPanel(chart);

			// 2. Configuration du panneau
			this.chartPane.setPreferredSize(new Dimension(500, 300));
			chartPane.setMouseZoomable(true, false);

			// 2. Installation du panneau
			this.add(this.chartPane);
		} else {
			this.chartPane.setChart(chart);
		}
	}

	/**
	 * Cr�ation du graphe � visualiser
	 * 
	 * @param aMeasures
	 *            D�finition de l'instrument/variable
	 */
	protected JFreeChart createChart(SensorMeasures aMeasures) {
		double maxValue,minValue;
		
		// 1. Cr�ation des s�ries de donn�es � visualiser
		XYDataset dataset = new TimeSeriesCollection(aMeasures.getTimeSeries());

		// 2. Lecture d�finition de l'instrument variable
		Sensor aSensor = aMeasures.getSensor();
		
		// 2. Initialisation labels du graphe
		String xLabel = "Temps";
		String yLabel = (aSensor.getUnit().length() == 0 ? "Mesure"
				: "Mesure (" + aSensor.getUnit() + ")");
		String title = aSensor.getBarrage().getId() + "-" + aSensor.getTitle();

		// 2. Cr�ation du graphe
		JFreeChart chart = ChartFactory.createTimeSeriesChart(title, xLabel,
				yLabel, dataset, true, true, false);

		// 2. Lecture r�f�rence � la zone de trace
		final XYPlot plot = chart.getXYPlot();

		// 3. Lecture valeurs macimale et minimale de la mesure
		minValue = aSensor.getMinimumValue();
		maxValue = aSensor.getMaximumValue();
		
		// 3. Ajout marge pour l'affichage des marqueurs
		ValueAxis domainAxis = plot.getDomainAxis();
		ValueAxis rangeAxis = plot.getRangeAxis();
		domainAxis.setUpperMargin(0.50);
		rangeAxis.setUpperMargin(0.30);
		rangeAxis.setLowerMargin(0.50);

		// 3. Affichage valeur maximale (Autoris�e) de la mesure
		Marker start = new ValueMarker(maxValue);
		start.setPaint(Color.red);
		start.setLabel("Max");
		start.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
		start.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
		plot.addRangeMarker(start);

		// 3. Affichage valeur minimale (Autoris�e) de la mesure
		Marker target = new ValueMarker(minValue);
		target.setPaint(Color.green);
		target.setLabel("Min");
		target.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
		target.setLabelTextAnchor(TextAnchor.BOTTOM_RIGHT);
		plot.addRangeMarker(target);

		return chart;
	}
}
