package gui;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;

import tools.CIniFileReader;
import controls.BaseExplorer;
import controls.BaseViewFactory;
import db.Barrage;
import db.Sensor;

/**
 * Fen�tre principale de l'application.
 * 
 * @author : soci�t� CS SI
 * @version : 1.0 du 09/11/09
 */
public abstract class CSurveyGui extends javax.swing.JFrame implements BaseViewFactory {
	private static final long serialVersionUID = 1L;

	private static final int MAIN_WINDOW_WIDTH = 800;
	private static final int MAIN_WINDOW_HEIGHT = 600;

	/** Composants internes. */
	protected BaseExplorer explorer;

	// Barre de menus.
	protected JMenuBar mMenuBar;

	// Menu "Syst�me".
	protected JMenu mSystemMenu;
	protected JMenuItem mImportDataMenuItem;
	protected JSeparator mSystemMenuSeparator;
	protected JMenuItem mQuitMenuItem;

	// Menu "Divers".
	protected JMenu mMiscellaneousMenu;
	protected JMenuItem mDamExportMenuItem;
	protected JMenuItem mDamImportMenuItem;
	protected JMenuItem mDamUpdateMenuItem;
	protected JMenuItem mDamExcelImportMenuItem;

	// Menu "Gestion des barrages".
	protected JMenu mDamsManagementMenu;
	protected JMenuItem mCreateDamMenuItem;
	protected JMenuItem mModifyDamMenuItem;
	protected JMenuItem mDeleteDamMenuItem;

	// Menu "Barrage".
	protected JMenu mDamMenu;
	protected JMenu mStructureManagementSubMenu;
	protected JMenuItem mGeneralInformationsMenuItem;
	//protected JMenuItem mSensorsAndVariablesMenuItem;
	protected JMenuItem mSummaryMenuItem;
	protected JMenu mDataManagementSubMenu;
	protected JMenuItem mEditorMenuItem;
	protected JMenuItem mExportMenuItem;
	protected JMenu mUseOfDataSubMenu;
	protected JMenuItem mAnalyseSensorMenuItem;
	protected JMenuItem mUseOfResultsMenuItem;

	// Menu "Instruments et variables".
	protected JMenu mSensorsAndVariables;
	protected JMenuItem mCreateSensorOrVariableMenuItem;
	protected JMenuItem mModifySensorOrVariableMenuItem;
	protected JMenuItem mDeleteSensorOrVariableMenuItem;

	protected JPanel mMainPanel;

	// Le nom de l'�cran actif.
	protected String mActiveScreen;

	/**
	 * Constructeur sans argument.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 09/11/09
	 */
	protected CSurveyGui() {
		mActiveScreen = "";
		initGUI();

	}

	/**
	 * Lecture r�f�rence au barrage s�lectionn�
	 * 
	 * @return Barrage s�lectionn�
	 */
	public Barrage getBarrage() {
		return explorer.getBarrage();
	}

	/**
	 * Lecture r�f�rence � l'instrument/variable s�lectionn�
	 * 
	 * @return Instrument/variable s�lectionn�
	 */
	public Sensor getSensor() {
		return explorer.getSensor();
	}

	/**
	 * M�thode de construction de l'IHM.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 09/11/09
	 */
	private void initGUI() {
		try {

			setDefaultLookAndFeelDecorated(true);
			javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager
					.getSystemLookAndFeelClassName());
			setResizable(true);
//			setIconImage(new ImageIcon(getClass().getClassLoader().getResource(
//					"/config/CemagrefLogo.png")).getImage());
			setIconImage(new ImageIcon("config/CemagrefLogo.png").getImage());
			{
				// 1. Cr�ation explorateur de base
				this.explorer = new BaseExplorer(this);

				// 2. Installation du composant
				this.getContentPane().add(this.explorer, BorderLayout.CENTER);

			}
			setSize(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
			{
				mMenuBar = new JMenuBar();
				setJMenuBar(mMenuBar);
				{
					mSystemMenu = new JMenu();
					mMenuBar.add(mSystemMenu);
					mSystemMenu.setText(CIniFileReader.getString("M1T"));
					mSystemMenu.setToolTipText(CIniFileReader
							.getString("M1TTT"));
					{
						mSystemMenuSeparator = new JSeparator();
						mSystemMenu.add(mSystemMenuSeparator);
					}
					{
						mImportDataMenuItem = new JMenuItem();
						mSystemMenu.add(mImportDataMenuItem);
						mImportDataMenuItem.setText(CIniFileReader
								.getString("M1I1T"));
						mImportDataMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M1I1A")));
						mImportDataMenuItem.setToolTipText(CIniFileReader
								.getString("M1I1TTT"));
					}
					{
						mQuitMenuItem = new JMenuItem();
						mSystemMenu.add(mQuitMenuItem);
						mQuitMenuItem
								.setText(CIniFileReader.getString("M1I3T"));
						mQuitMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M1I3A")));
						mQuitMenuItem.setToolTipText(CIniFileReader
								.getString("M1I3TTT"));
					}
				}
				{
					mMiscellaneousMenu = new JMenu();
					mMenuBar.add(mMiscellaneousMenu);
					mMiscellaneousMenu.setText(CIniFileReader.getString("M3T"));
					mMiscellaneousMenu.setToolTipText(CIniFileReader
							.getString("M3TTT"));
					{
						mDamExportMenuItem = new JMenuItem();
						mMiscellaneousMenu.add(mDamExportMenuItem);
						mDamExportMenuItem.setText(CIniFileReader
								.getString("M3I1T"));
						mDamExportMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M3I1A")));
						mDamExportMenuItem.setToolTipText(CIniFileReader
								.getString("M3I1TTT"));
					}
					{
						mDamImportMenuItem = new JMenuItem();
						mMiscellaneousMenu.add(mDamImportMenuItem);
						mDamImportMenuItem.setText(CIniFileReader
								.getString("M3I2T"));
						mDamImportMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M3I2A")));
						mDamImportMenuItem.setToolTipText(CIniFileReader
								.getString("M3I2TTT"));
					}
					{
						mDamUpdateMenuItem = new JMenuItem();
						mMiscellaneousMenu.add(mDamUpdateMenuItem);
						mDamUpdateMenuItem.setText(CIniFileReader
								.getString("M3I3T"));
						mDamUpdateMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M3I3A")));
						mDamUpdateMenuItem.setToolTipText(CIniFileReader
								.getString("M3I3TTT"));
					}
					{
						mDamExcelImportMenuItem = new JMenuItem();
						mMiscellaneousMenu.add(mDamExcelImportMenuItem);
						mDamExcelImportMenuItem.setText(CIniFileReader
								.getString("M3I4T"));
						mDamExcelImportMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M3I4A")));
						mDamExcelImportMenuItem.setToolTipText(CIniFileReader
								.getString("M3I4TTT"));
					}
				}
				{
					mDamsManagementMenu = new JMenu();
					mMenuBar.add(mDamsManagementMenu);
					mDamsManagementMenu
							.setText(CIniFileReader.getString("M4T"));
					mDamsManagementMenu.setToolTipText(CIniFileReader
							.getString("M4TTT"));
					{
						mCreateDamMenuItem = new JMenuItem();
						mDamsManagementMenu.add(mCreateDamMenuItem);
						mCreateDamMenuItem.setText(CIniFileReader
								.getString("M4I1T"));
						mCreateDamMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M4I1A")));
						mCreateDamMenuItem.setToolTipText(CIniFileReader
								.getString("M4I1TTT"));
					}
					{
						mModifyDamMenuItem = new JMenuItem();
						mDamsManagementMenu.add(mModifyDamMenuItem);
						mModifyDamMenuItem.setText(CIniFileReader
								.getString("M4I2T"));
						mModifyDamMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M4I2A")));
						mModifyDamMenuItem.setToolTipText(CIniFileReader
								.getString("M4I2TTT"));
					}
					{
						mDeleteDamMenuItem = new JMenuItem();
						mDamsManagementMenu.add(mDeleteDamMenuItem);
						mDeleteDamMenuItem.setText(CIniFileReader
								.getString("M4I3T"));
						mDeleteDamMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M4I3A")));
						mDeleteDamMenuItem.setToolTipText(CIniFileReader
								.getString("M4I3TTT"));
					}
				}
				{
					mDamMenu = new JMenu();
					mMenuBar.add(mDamMenu);
					mDamMenu.setText(CIniFileReader.getString("M5T"));
					mDamMenu.setToolTipText(CIniFileReader.getString("M5TTT"));
					{
						mStructureManagementSubMenu = new JMenu();
						mDamMenu.add(mStructureManagementSubMenu);
						mStructureManagementSubMenu.setText(CIniFileReader
								.getString("M5SM1T"));
						mStructureManagementSubMenu
								.setToolTipText(CIniFileReader
										.getString("M5SM1TTT"));

						{
							mGeneralInformationsMenuItem = new JMenuItem();
							mStructureManagementSubMenu
									.add(mGeneralInformationsMenuItem);
							mGeneralInformationsMenuItem.setText(CIniFileReader
									.getString("M5SM1I1T"));
							mGeneralInformationsMenuItem
									.setAccelerator(KeyStroke
											.getKeyStroke(CIniFileReader
													.getString("M5SM1I1A")));
							mGeneralInformationsMenuItem
									.setToolTipText(CIniFileReader
											.getString("M5SM1I1TTT"));
						}
						{
							/*
							mSensorsAndVariablesMenuItem = new JMenuItem();
							mStructureManagementSubMenu
									.add(mSensorsAndVariablesMenuItem);
							mSensorsAndVariablesMenuItem.setText(CIniFileReader
									.getString("M5SM1I2T"));
							mSensorsAndVariablesMenuItem
									.setAccelerator(KeyStroke
											.getKeyStroke(CIniFileReader
													.getString("M5SM1I2A")));
							mSensorsAndVariablesMenuItem
									.setToolTipText(CIniFileReader
											.getString("M5SM1I2TTT"));
							*/
						}
						{
							mSummaryMenuItem = new JMenuItem();
							mStructureManagementSubMenu.add(mSummaryMenuItem);
							mSummaryMenuItem.setText(CIniFileReader
									.getString("M5SM1I3T"));
							mSummaryMenuItem.setAccelerator(KeyStroke
									.getKeyStroke(CIniFileReader
											.getString("M5SM1I3A")));
							mSummaryMenuItem.setToolTipText(CIniFileReader
									.getString("M5SM1I3TTT"));
						}
					}
					{
						mDataManagementSubMenu = new JMenu();
						mDamMenu.add(mDataManagementSubMenu);
						mDataManagementSubMenu.setText(CIniFileReader
								.getString("M5SM2T"));
						mDataManagementSubMenu.setToolTipText(CIniFileReader
								.getString("M5SM2TTT"));
						{
							mEditorMenuItem = new JMenuItem();
							mDataManagementSubMenu.add(mEditorMenuItem);
							mEditorMenuItem.setText(CIniFileReader
									.getString("M5SM2I1T"));
							mEditorMenuItem.setBounds(0, 19, 195, 19);
							mEditorMenuItem.setAccelerator(KeyStroke
									.getKeyStroke(CIniFileReader
											.getString("M5SM2I1A")));
							mEditorMenuItem.setToolTipText(CIniFileReader
									.getString("M5SM2I1TTT"));
						}
						{
							mExportMenuItem = new JMenuItem();
							mDataManagementSubMenu.add(mExportMenuItem);
							mExportMenuItem.setText(CIniFileReader
									.getString("M5SM2I2T"));
							mExportMenuItem.setAccelerator(KeyStroke
									.getKeyStroke(CIniFileReader
											.getString("M5SM2I2A")));
							mExportMenuItem.setToolTipText(CIniFileReader
									.getString("M5SM2I2TTT"));
						}
					}
					{
						mUseOfDataSubMenu = new JMenu();
						mDamMenu.add(mUseOfDataSubMenu);
						mUseOfDataSubMenu.setText(CIniFileReader
								.getString("M5SM3T"));
						mUseOfDataSubMenu.setToolTipText(CIniFileReader
								.getString("M5SM3TTT"));
						{
							mAnalyseSensorMenuItem = new JMenuItem();
							mUseOfDataSubMenu.add(mAnalyseSensorMenuItem);
							mAnalyseSensorMenuItem.setText(CIniFileReader
									.getString("M5SM3I1T"));
							mAnalyseSensorMenuItem.setAccelerator(KeyStroke
									.getKeyStroke(CIniFileReader
											.getString("M5SM3I1A")));
							mAnalyseSensorMenuItem
									.setToolTipText(CIniFileReader
											.getString("M5SM3I1TTT"));
						}
						{
							mUseOfResultsMenuItem = new JMenuItem();
							mUseOfDataSubMenu.add(mUseOfResultsMenuItem);
							mUseOfResultsMenuItem.setText(CIniFileReader
									.getString("M5SM3I2T"));
							mUseOfResultsMenuItem.setAccelerator(KeyStroke
									.getKeyStroke(CIniFileReader
											.getString("M5SM3I2A")));
							mUseOfResultsMenuItem.setToolTipText(CIniFileReader
									.getString("M5SM3I2TTT"));
						}
					}
				}
				{
					mSensorsAndVariables = new JMenu();
					mMenuBar.add(mSensorsAndVariables);
					mSensorsAndVariables.setText(CIniFileReader
							.getString("M6T"));
					mSensorsAndVariables.setToolTipText(CIniFileReader
							.getString("M6TTT"));
					{
						mCreateSensorOrVariableMenuItem = new JMenuItem();
						mSensorsAndVariables
								.add(mCreateSensorOrVariableMenuItem);
						mCreateSensorOrVariableMenuItem.setText(CIniFileReader
								.getString("M6I1T"));
						mCreateSensorOrVariableMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M6I1A")));
						mCreateSensorOrVariableMenuItem
								.setToolTipText(CIniFileReader
										.getString("M6I1TTT"));
					}
					{
						mModifySensorOrVariableMenuItem = new JMenuItem();
						mSensorsAndVariables
								.add(mModifySensorOrVariableMenuItem);
						mModifySensorOrVariableMenuItem.setText(CIniFileReader
								.getString("M6I2T"));
						mModifySensorOrVariableMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M6I2A")));
						mModifySensorOrVariableMenuItem
								.setToolTipText(CIniFileReader
										.getString("M6I2TTT"));
					}
					{
						mDeleteSensorOrVariableMenuItem = new JMenuItem();
						mSensorsAndVariables
								.add(mDeleteSensorOrVariableMenuItem);
						mDeleteSensorOrVariableMenuItem.setText(CIniFileReader
								.getString("M6I3T"));
						mDeleteSensorOrVariableMenuItem
								.setAccelerator(KeyStroke
										.getKeyStroke(CIniFileReader
												.getString("M6I3A")));
						mDeleteSensorOrVariableMenuItem
								.setToolTipText(CIniFileReader
										.getString("M6I3TTT"));
					}
				}
			}

			displayTitle();
		} catch (Exception lException) {
			lException.printStackTrace();
		}
	}

	/**
	 * M�thode permettant d'afficher dans la barre de titre, le nom de
	 * l'application, le num�ro de version, le titre de la fen�tre active et le
	 * nom du barrage actif.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 10/12/09
	 */
	public void displayTitle() {
		String lTitle = CIniFileReader.getString("P0APPLNAME") + " "
				+ CIniFileReader.getString("P0APPLVERSION");

		if (mActiveScreen.isEmpty() == false) {
			lTitle += " - " + mActiveScreen;
		}
		if (getBarrage() != null) {
			lTitle += " - " + CIniFileReader.getString("INFMSG1") + " \""
					+ getBarrage().getId() + "\"";
		}

		setTitle(lTitle);
	}
}
