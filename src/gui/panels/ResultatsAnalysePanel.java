package gui.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import controls.BaseView;

import tools.CIniFileReader;
import cs.ui.DateField;
import cs.ui.DoubleField;
import cs.ui.IntegerField;
import cs.ui.StringField;
import db.SensorResultats;
import db.Survey;

/**
 * Panneau d'utilisation des r�sultats.
 * 
 * @author : soci�t� CS SI
 * @version : 1.0 du 19/01/10
 */
public class ResultatsAnalysePanel extends BaseView {
	private static final long serialVersionUID = 1L;
	protected IntegerField mRegressionTypeField;
	protected StringField mSensor;
	protected StringField mTitleTextField;
	protected DateField mAnalyseDateTextField;
	protected IntegerField mValuesCountTextField;
	protected DateField mBeginDateTextField;
	protected DateField mEndDateTextField;
	protected IntegerField mDurationTextField;
	protected DoubleField mR2TextField;
	protected DoubleField mMeanCoteTextField;
	protected DoubleField mSigCoteTextField;
	protected DoubleField mConstantTermTextField;
	protected DoubleField mTTextField;
	protected DoubleField mExpTTextField;
	protected DoubleField mExpMTTextField;
	protected DoubleField mSinTextField;
	protected DoubleField mCosTextField;
	protected DoubleField mSicoTextField;
	protected DoubleField mSin2TextField;
	protected DoubleField mZTextField;
	protected DoubleField mZ2TextField;
	protected DoubleField mZ3TextField;
	protected DoubleField mZ4TextField;
	protected DoubleField mP1TextField;
	protected DoubleField mP2TextField;
	protected DoubleField mP3TextField;
	protected DoubleField mP4TextField;
	protected DoubleField mP56TextField;
	protected DoubleField mP78TextField;

	/**
	 * Constructeur sans argument.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 19/01/10
	 */
	public ResultatsAnalysePanel() {
		initGUI();
	}

	/**
	 * Initialisation r�sultats de l'analyse � visualiser
	 * @param aResultats R�sultats de l'analyse d'un instrument/variable
	 */
	public void setResultats(SensorResultats aResultats) {
		mRegressionTypeField.setValue(aResultats.getLNasous());
		mSensor.setValue(aResultats.getId());
		mTitleTextField.setValue(aResultats.getSensor().getTitle());
		mAnalyseDateTextField.setValue(aResultats.getSensor().getRegressionDate());
		mValuesCountTextField.setValue(aResultats.getLValuesCount());
		mBeginDateTextField.setValue(aResultats.getLAnalyseBeginDate());
		mEndDateTextField.setValue(aResultats.getLAnalyseEndDate());
		mDurationTextField.setValue(aResultats.getLDuration());
		mR2TextField.setValue(aResultats.getLR2());
		mMeanCoteTextField.setValue(aResultats.getLMeanCote());
		mSigCoteTextField.setValue(aResultats.getLSigCote());
		mConstantTermTextField.setValue(aResultats.getLConstantTerm());
		mTTextField.setValue(aResultats.getLT());
		mExpTTextField.setValue(aResultats.getLExpT());
		mExpMTTextField.setValue(aResultats.getLExpMT());
		mSinTextField.setValue(aResultats.getLSin());
		mCosTextField.setValue(aResultats.getLCos());
		mSicoTextField.setValue(aResultats.getLSico());
		mSin2TextField.setValue(aResultats.getLSin2());
		mZTextField.setValue(aResultats.getLZ());
		mZ2TextField.setValue(aResultats.getLZ2());
		mZ3TextField.setValue(aResultats.getLZ3());
		mZ4TextField.setValue(aResultats.getLZ4());
		mP1TextField.setValue(aResultats.getLP1());
		mP2TextField.setValue(aResultats.getLP2());
		mP3TextField.setValue(aResultats.getLP3());
		mP4TextField.setValue(aResultats.getLP4());
		mP56TextField.setValue(aResultats.getLP56());
		 mP78TextField.setValue(aResultats.getLP78());
	}
	
	/**
	 * M�thode de construction de l'IHM.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 19/01/10
	 */
	private void initGUI() {
		try {
			setPreferredSize(new java.awt.Dimension(561, 449));
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.columnWidths = new int[] { 92, 90, 10, 90, 20, 90, 20,
					104, 20, 90, 20, 20 };
			thisLayout.rowHeights = new int[] { 7, 8, 20, 8, 20, 20, 20, 20,
					20, 8, 20, 20, 20, 20, 20, 20, 20, 20, 7, 20 };
			thisLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.1,
					0.0, 0.1, 0.0, 0.1, 0.1, 0.1, 0.1 };
			thisLayout.rowWeights = new double[] { 0.1, 0.0, 0.1, 0.0, 0.1,
					0.1, 0.1, 0.1, 0.1, 0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1,
					0.1, 0.0, 0.1 };
			this.setLayout(thisLayout);

			{
				JLabel mBeginDateLabel = new JLabel();
				this.add(mBeginDateLabel, new GridBagConstraints(0, 5, 1, 1,
						0.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4),
						0, 0));
				mBeginDateLabel.setText(CIniFileReader.getString("P4L7"));
				mBeginDateLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				JLabel mEndDateLabel = new JLabel();
				this.add(mEndDateLabel, new GridBagConstraints(2, 5, 3, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mEndDateLabel.setText(CIniFileReader.getString("P4L8"));
				mEndDateLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mBeginDateTextField = new DateField(Survey.dateFormat);
				this.add(mBeginDateTextField, new GridBagConstraints(1, 5, 1,
						1, 0.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mBeginDateTextField
						.setHorizontalAlignment(SwingConstants.CENTER);
				mBeginDateTextField.setEnabled(false);
				mBeginDateTextField.setNullAllowed(true);
				mBeginDateTextField.setToolTipText(CIniFileReader
						.getString("P4L7TTT"));
			}
			{
				mEndDateTextField = new DateField(Survey.dateFormat);
				this.add(mEndDateTextField, new GridBagConstraints(5, 5, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mEndDateTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mEndDateTextField.setEnabled(false);
				mEndDateTextField.setNullAllowed(true);
				mEndDateTextField.setToolTipText(CIniFileReader
						.getString("P4L8TTT"));
			}
			{
				JLabel mCoteLabel = new JLabel();
				this.add(mCoteLabel, new GridBagConstraints(0, 8, 1, 1, 0.0,
						0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4),
						0, 0));
				mCoteLabel.setText(CIniFileReader.getString("P4L14"));
				mCoteLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}

			{
				JLabel mRegressionTypeLabel = new JLabel();
				this.add(mRegressionTypeLabel, new GridBagConstraints(0, 0, 2,
						1, 0.0, 0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4),
						0, 0));
				mRegressionTypeLabel.setText(CIniFileReader.getString("P4L1"));
				mRegressionTypeLabel
						.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				JLabel mSensorLabel = new JLabel();
				this.add(mSensorLabel, new GridBagConstraints(0, 2, 1, 1, 0.0,
						0.0, GridBagConstraints.WEST,
						GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4),
						0, 0));
				mSensorLabel.setText(CIniFileReader.getString("P4L2"));
				mSensorLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				JLabel mR2Label = new JLabel();
				this.add(mR2Label, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
						GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
						new Insets(0, 8, 0, 4), 0, 0));
				mR2Label.setText(CIniFileReader.getString("P4L11"));
				mR2Label.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mR2TextField = new DoubleField(4);
				this.add(mR2TextField, new GridBagConstraints(1, 6, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mR2TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mR2TextField.setEnabled(false);
				mR2TextField.setToolTipText(CIniFileReader
						.getString("P4L11TTT"));
			}
			{
				JLabel mMeanCoteLabel = new JLabel();
				this.add(mMeanCoteLabel, new GridBagConstraints(1, 7, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mMeanCoteLabel.setText(CIniFileReader.getString("P4L12"));
				mMeanCoteLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				JLabel mSigCoteLabel = new JLabel();
				this.add(mSigCoteLabel, new GridBagConstraints(5, 7, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mSigCoteLabel.setText(CIniFileReader.getString("P4L13"));
				mSigCoteLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mMeanCoteTextField = new DoubleField(4);
				this.add(mMeanCoteTextField, new GridBagConstraints(1, 8, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mMeanCoteTextField
						.setHorizontalAlignment(SwingConstants.CENTER);
				mMeanCoteTextField.setEnabled(false);
				mMeanCoteTextField.setToolTipText(CIniFileReader
						.getString("P4L15TTT"));
			}
			{
				JLabel mChoosenSensorLabel = new JLabel();
				this.add(mChoosenSensorLabel, new GridBagConstraints(0, 11, 1,
						1, 0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4),
						0, 0));
				mChoosenSensorLabel.setText("");
				mChoosenSensorLabel
						.setHorizontalAlignment(SwingConstants.CENTER);
				mChoosenSensorLabel.setFont(new java.awt.Font(
						"Bitstream Vera Sans", 1, 10));
			}
			{
				JLabel mEquationTitleLabel = new JLabel();
				this.add(mEquationTitleLabel, new GridBagConstraints(0, 10, 12,
						1, 0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mEquationTitleLabel.setText(CIniFileReader.getString("P4L17"));
				mEquationTitleLabel
						.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				JLabel mLabel6 = new JLabel();
				this.add(mLabel6, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0,
						GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4),
						0, 0));
				mLabel6.setText("+");
				mLabel6.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel7 = new JLabel();
				this.add(mLabel7, new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0,
						GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4),
						0, 0));
				mLabel7.setText("+");
				mLabel7.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel10 = new JLabel();
				this.add(mLabel10, new GridBagConstraints(3, 12, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel10.setText("T");
				mLabel10.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel10.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				JLabel mLabel11 = new JLabel();
				this.add(mLabel11, new GridBagConstraints(7, 12, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel11.setText("EXP(T)");
				mLabel11.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel11.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				JLabel mLabel12 = new JLabel();
				this.add(mLabel12, new GridBagConstraints(3, 16, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel12.setText("Z2");
				mLabel12.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel12.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				JLabel mLabel13 = new JLabel();
				this.add(mLabel13, new GridBagConstraints(7, 16, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel13.setText("P3");
				mLabel13.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel13.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				mConstantTermTextField = new DoubleField(4);;
				this.add(mConstantTermTextField, new GridBagConstraints(1, 11,
						1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mConstantTermTextField
						.setHorizontalAlignment(SwingConstants.CENTER);
				mConstantTermTextField.setEnabled(false);
			}
			{
				mTTextField = new DoubleField(4);
				this.add(mTTextField, new GridBagConstraints(1, 12, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mTTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mTTextField.setEnabled(false);
			}
			{
				mP2TextField =new DoubleField(4);
				this.add(mP2TextField, new GridBagConstraints(1, 16, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mP2TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mP2TextField.setEnabled(false);
			}
			{
				mExpTTextField = new DoubleField(4);
				this.add(mExpTTextField, new GridBagConstraints(5, 12, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mExpTTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mExpTTextField.setEnabled(false);
			}
			{
				mP3TextField = new DoubleField(4);
				this.add(mP3TextField, new GridBagConstraints(5, 16, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mP3TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mP3TextField.setEnabled(false);
			}
			{
				mExpMTTextField = new DoubleField(4);
				this.add(mExpMTTextField, new GridBagConstraints(9, 12, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mExpMTTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mExpMTTextField.setEnabled(false);
			}
			{
				mP4TextField = new DoubleField(4);
				this.add(mP4TextField, new GridBagConstraints(9, 16, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mP4TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mP4TextField.setEnabled(false);
			}
			{
				JLabel mLabel14 = new JLabel();
				this.add(mLabel14, new GridBagConstraints(0, 17, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4),
						0, 0));
				mLabel14.setText("+");
				mLabel14.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				mRegressionTypeField = new IntegerField();
				this.add(mRegressionTypeField, new GridBagConstraints(3, 0, 1,
						1, 0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mRegressionTypeField
						.setHorizontalAlignment(SwingConstants.CENTER);
				mRegressionTypeField.setEnabled(false);
				mRegressionTypeField.setNullAllowed(true);
				mRegressionTypeField.setToolTipText(CIniFileReader
						.getString("P4L1TTT"));
			}
			{
				mSensor = new StringField();
				this.add(mSensor, new GridBagConstraints(1, 2, 3, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mSensor.setEnabled(false);
				mSensor.setToolTipText(CIniFileReader
						.getString("P4L2TTT"));
			}
			{
				JLabel mTitleLabel = new JLabel();
				this.add(mTitleLabel, new GridBagConstraints(5, 2, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.NONE, new Insets(0, 4, 0, 4), 0, 0));
				mTitleLabel.setText(CIniFileReader.getString("P4L3"));
			}
			{
				mTitleTextField = new StringField();
				this.add(mTitleTextField, new GridBagConstraints(7, 2, 5, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8),
						0, 0));
				mTitleTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mTitleTextField.setEnabled(false);
				mTitleTextField.setToolTipText(CIniFileReader
						.getString("P4L3TTT"));
			}
			{
				JLabel mAnalyseDateLabel = new JLabel();
				this.add(mAnalyseDateLabel, new GridBagConstraints(0, 4, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.NONE, new Insets(0, 8, 0, 4), 0, 0));
				mAnalyseDateLabel.setText(CIniFileReader.getString("P4L4"));
			}
			{
				JLabel mValuesCountLabel = new JLabel();
				this.add(mValuesCountLabel, new GridBagConstraints(2, 4, 3, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mValuesCountLabel.setText(CIniFileReader.getString("P4L5"));
				mValuesCountLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				JLabel mValuesLabel = new JLabel();
				this.add(mValuesLabel, new GridBagConstraints(7, 4, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mValuesLabel.setText(CIniFileReader.getString("P4L6"));
				mValuesLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mAnalyseDateTextField = new DateField(Survey.dateFormat);
				this.add(mAnalyseDateTextField, new GridBagConstraints(1, 4, 1,
						1, 0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mAnalyseDateTextField
						.setHorizontalAlignment(SwingConstants.CENTER);
				mAnalyseDateTextField.setEnabled(false);
				mEndDateTextField.setNullAllowed(true);
				mAnalyseDateTextField.setToolTipText(CIniFileReader
						.getString("P4L4TTT"));
			}
			{
				mValuesCountTextField = new IntegerField();
				this.add(mValuesCountTextField, new GridBagConstraints(5, 4, 1,
						1, 0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mValuesCountTextField
						.setHorizontalAlignment(SwingConstants.CENTER);
				mValuesCountTextField.setEnabled(false);
				mValuesCountTextField.setToolTipText(CIniFileReader
						.getString("P4L5TTT"));
			}
			{
				JLabel mDurationLabel = new JLabel();
				this.add(mDurationLabel, new GridBagConstraints(7, 5, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mDurationLabel.setText(CIniFileReader.getString("P4L9"));
				mDurationLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mDurationTextField = new IntegerField();
				this.add(mDurationTextField, new GridBagConstraints(9, 5, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mDurationTextField.setIgnoreRepaint(true);
				mDurationTextField
						.setHorizontalAlignment(SwingConstants.CENTER);
				mDurationTextField.setEnabled(false);
				mDurationTextField.setToolTipText(CIniFileReader
						.getString("P4L9TTT"));
			}
			{
				mSigCoteTextField = new DoubleField(4);
				this.add(mSigCoteTextField, new GridBagConstraints(5, 8, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mSigCoteTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mSigCoteTextField.setEnabled(false);
				mSigCoteTextField.setToolTipText(CIniFileReader
						.getString("P4L16TTT"));
			}
			{
				JLabel mMeterUnitLabel = new JLabel();
				this.add(mMeterUnitLabel, new GridBagConstraints(6, 8, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mMeterUnitLabel.setText("(m)");
				mMeterUnitLabel.setHorizontalAlignment(SwingConstants.LEFT);
				mMeterUnitLabel.setFocusTraversalPolicyProvider(true);
			}
			{
				JLabel mLabel8 = new JLabel();
				this.add(mLabel8, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0,
						GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4),
						0, 0));
				mLabel8.setText("+");
				mLabel8.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel9 = new JLabel();
				this.add(mLabel9, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0,
						GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4),
						0, 0));
				mLabel9.setText("+");
				mLabel9.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel21 = new JLabel();
				this.add(mLabel21, new GridBagConstraints(0, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4),
						0, 0));
				mLabel21.setText("+");
				mLabel21.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				mSinTextField = new DoubleField(4);
				this.add(mSinTextField, new GridBagConstraints(1, 13, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mSinTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mSinTextField.setEnabled(false);
			}
			{
				mSin2TextField = new DoubleField(4);
				this.add(mSin2TextField, new GridBagConstraints(1, 14, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mSin2TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mSin2TextField.setEnabled(false);
			}
			{
				mZ3TextField = new DoubleField(4);
				this.add(mZ3TextField, new GridBagConstraints(1, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mZ3TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mZ3TextField.setEnabled(false);
			}
			{
				mP56TextField = new DoubleField(4);
				this.add(mP56TextField, new GridBagConstraints(1, 17, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mP56TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mP56TextField.setEnabled(false);
			}
			{
				JLabel mLabel22 = new JLabel();
				this.add(mLabel22, new GridBagConstraints(3, 13, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel22.setText("SIN");
				mLabel22.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel22.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				JLabel mLabel23 = new JLabel();
				this.add(mLabel23, new GridBagConstraints(3, 14, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel23.setText("SIN2");
				mLabel23.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel23.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				JLabel mLabel24 = new JLabel();
				this.add(mLabel24, new GridBagConstraints(3, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel24.setText("Z3");
				mLabel24.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel24.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				JLabel mLabel25 = new JLabel();
				this.add(mLabel25, new GridBagConstraints(3, 17, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel25.setText("P56");
				mLabel25.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel25.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				mCosTextField = new DoubleField(4);
				this.add(mCosTextField, new GridBagConstraints(5, 13, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mCosTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mCosTextField.setEnabled(false);
			}
			{
				mZTextField = new DoubleField(4);
				this.add(mZTextField, new GridBagConstraints(5, 14, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mZTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mZTextField.setEnabled(false);
			}
			{
				mZ4TextField = new DoubleField(4);
				this.add(mZ4TextField, new GridBagConstraints(5, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mZ4TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mZ4TextField.setEnabled(false);
			}
			{
				mP78TextField = new DoubleField(4);
				this.add(mP78TextField, new GridBagConstraints(5, 17, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mP78TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mP78TextField.setEnabled(false);
			}
			{
				JLabel mLabel26 = new JLabel();
				this.add(mLabel26, new GridBagConstraints(7, 13, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel26.setText("COS");
				mLabel26.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel26.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				JLabel mLabel27 = new JLabel();
				this.add(mLabel27, new GridBagConstraints(7, 14, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel27.setText("Z");
				mLabel27.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel27.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				JLabel mLabel28 = new JLabel();
				this.add(mLabel28, new GridBagConstraints(7, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel28.setText("Z4");
				mLabel28.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel28.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				JLabel mLabel29 = new JLabel();
				this.add(mLabel29, new GridBagConstraints(7, 17, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel29.setText("P78");
				mLabel29.setHorizontalAlignment(SwingConstants.CENTER);
				mLabel29.setHorizontalTextPosition(SwingConstants.CENTER);
			}
			{
				mSicoTextField =new DoubleField(4);
				this.add(mSicoTextField, new GridBagConstraints(9, 13, 1, 1,
						0.0, 0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mSicoTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mSicoTextField.setEnabled(false);
			}
			{
				mZ2TextField = new DoubleField(4);
				this.add(mZ2TextField, new GridBagConstraints(9, 14, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mZ2TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mZ2TextField.setEnabled(false);
			}
			{
				mP1TextField = new DoubleField(4);
				this.add(mP1TextField, new GridBagConstraints(9, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mP1TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mP1TextField.setEnabled(false);
			}
			{
				JLabel mLabel30 = new JLabel();
				this.add(mLabel30, new GridBagConstraints(11, 12, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8),
						0, 0));
				mLabel30.setText("EXP(-T)");
				mLabel30.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel31 = new JLabel();
				this.add(mLabel31, new GridBagConstraints(11, 13, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8),
						0, 0));
				mLabel31.setText("SICO");
				mLabel31.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel32 = new JLabel();
				this.add(mLabel32, new GridBagConstraints(11, 14, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8),
						0, 0));
				mLabel32.setText("Z2");
				mLabel32.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel33 = new JLabel();
				this.add(mLabel33, new GridBagConstraints(11, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8),
						0, 0));
				mLabel33.setText("P1");
				mLabel33.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel34 = new JLabel();
				this.add(mLabel34, new GridBagConstraints(11, 16, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8),
						0, 0));
				mLabel34.setText("P4");
				mLabel34.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mDaysLabel = new JLabel();
				this.add(mDaysLabel, new GridBagConstraints(11, 5, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8),
						0, 0));
				mDaysLabel.setText(CIniFileReader.getString("P4L10"));
			}
			{
				JLabel mLabel37 = new JLabel();
				this.add(mLabel37, new GridBagConstraints(2, 12, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel37.setText("x");
			}
			{
				JLabel mLabel38 = new JLabel();
				this.add(mLabel38, new GridBagConstraints(2, 13, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel38.setText("x");
			}
			{
				JLabel mLabel39 = new JLabel();
				this.add(mLabel39, new GridBagConstraints(2, 14, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel39.setText("x");
			}
			{
				JLabel mLabel40 = new JLabel();
				this.add(mLabel40, new GridBagConstraints(2, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel40.setText("x");
			}
			{
				JLabel mLabel41 = new JLabel();
				this.add(mLabel41, new GridBagConstraints(2, 16, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel41.setText("x");
			}
			{
				JLabel mLabel42 = new JLabel();
				this.add(mLabel42, new GridBagConstraints(2, 17, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel42.setText("x");
			}
			{
				JLabel mLabel43 = new JLabel();
				this.add(mLabel43, new GridBagConstraints(4, 12, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel43.setText("+");
				mLabel43.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel44 = new JLabel();
				this.add(mLabel44, new GridBagConstraints(4, 13, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel44.setText("+");
				mLabel44.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel45 = new JLabel();
				this.add(mLabel45, new GridBagConstraints(4, 14, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel45.setText("+");
				mLabel45.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel46 = new JLabel();
				this.add(mLabel46, new GridBagConstraints(4, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel46.setText("+");
				mLabel46.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel47 = new JLabel();
				this.add(mLabel47, new GridBagConstraints(4, 16, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel47.setText("+");
				mLabel47.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel48 = new JLabel();
				this.add(mLabel48, new GridBagConstraints(4, 17, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel48.setText("+");
				mLabel48.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel49 = new JLabel();
				this.add(mLabel49, new GridBagConstraints(6, 12, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel49.setText("x");
				mLabel49.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel50 = new JLabel();
				this.add(mLabel50, new GridBagConstraints(6, 13, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel50.setText("x");
				mLabel50.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel51 = new JLabel();
				this.add(mLabel51, new GridBagConstraints(6, 14, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel51.setText("x");
				mLabel51.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel52 = new JLabel();
				this.add(mLabel52, new GridBagConstraints(6, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel52.setText("x");
				mLabel52.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel53 = new JLabel();
				this.add(mLabel53, new GridBagConstraints(6, 16, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel53.setText("x");
				mLabel53.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel54 = new JLabel();
				this.add(mLabel54, new GridBagConstraints(6, 17, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel54.setText("x");
				mLabel54.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel55 = new JLabel();
				this.add(mLabel55, new GridBagConstraints(8, 12, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel55.setText("+");
				mLabel55.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel56 = new JLabel();
				this.add(mLabel56, new GridBagConstraints(8, 13, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel56.setText("+");
				mLabel56.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel57 = new JLabel();
				this.add(mLabel57, new GridBagConstraints(8, 14, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel57.setText("+");
				mLabel57.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel58 = new JLabel();
				this.add(mLabel58, new GridBagConstraints(8, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel58.setText("+");
				mLabel58.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel59 = new JLabel();
				this.add(mLabel59, new GridBagConstraints(8, 16, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel59.setText("+");
				mLabel59.setHorizontalAlignment(SwingConstants.RIGHT);
			}
			{
				JLabel mLabel60 = new JLabel();
				this.add(mLabel60, new GridBagConstraints(10, 12, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel60.setText("x");
				mLabel60.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel61 = new JLabel();
				this.add(mLabel61, new GridBagConstraints(10, 13, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel61.setText("x");
				mLabel61.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel62 = new JLabel();
				this.add(mLabel62, new GridBagConstraints(10, 14, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel62.setText("x");
				mLabel62.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel63 = new JLabel();
				this.add(mLabel63, new GridBagConstraints(10, 15, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel63.setText("x");
				mLabel63.setHorizontalAlignment(SwingConstants.LEFT);
			}
			{
				JLabel mLabel64 = new JLabel();
				this.add(mLabel64, new GridBagConstraints(10, 16, 1, 1, 0.0,
						0.0, GridBagConstraints.CENTER,
						GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4),
						0, 0));
				mLabel64.setText("x");
				mLabel64.setHorizontalAlignment(SwingConstants.LEFT);
			}
		} catch (Exception lException) {
			lException.printStackTrace();
		}
	}
}