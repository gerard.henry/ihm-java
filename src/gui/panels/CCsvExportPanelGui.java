package gui.panels;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;

import tools.CIniFileReader;
import controls.BaseView;
import controls.CSurveyControl;
import db.Sensor;
import db.SensorMeasures;
import db.Survey;

/**
 * Panel d'exportation des donn�es Excel.
 * 
 * @author  :  soci�t� CS SI
 * @version : 1.0 du 03/02/10
 */
public class CCsvExportPanelGui extends BaseView
{
	@SuppressWarnings("serial")
	protected class SensorModel extends AbstractTableModel {
		/** Liste des instruments/variables. */
		protected List<Sensor> sensors;
		
		/**
		 * Constructeur
		 * @param aSensors Liste des instruments/variables
		 */
		public SensorModel(List<Sensor> aSensors) {
			// 1. Initialisation des attributs
			this.sensors = aSensors;
		}

		@Override
		public String getColumnName(int aColumn) {
			switch (aColumn) {
			case 0:
				return "Identificateur";
			case 1:
				return "Premi�re mesure";
			case 2:
				return "Derni�re mesure";
			case 3:
				return "Nb mesures";
			}

			return null;
		}

		@Override
		public int getColumnCount() {
			return 4;
		}

		@Override
		public int getRowCount() {
			return sensors.size();
		}

		@Override
		public Object getValueAt(int aRow, int aColumn) {
			// 1. Lecture d�finition de l'instrument/variable
			Sensor sensor = sensors.get(aRow);
			
			// 2. Lecture liste des mesures de l'instrument/variable
			SensorMeasures measures = sensor.getMeasures();
			
			switch (aColumn) {
			case 0:
				return sensor.getId();
			case 1:
				return (measures != null ? Survey.format(measures.getDateFirstMeasure()) : "");
			case 2:
				return (measures != null ? Survey.format(measures.getDateLastMeasure()): "");
			case 3:
				return (measures != null ? measures.getMeasureCount() : "0");
			}

			return null;
		}
	}

	private static final long serialVersionUID = 1L;

	private   JLabel          mSensorsLabel;

	protected JButton         mExportButton;
	protected JFormattedTextField mEndDateTextField;
	protected JTextField mVoidValueSymbolTextField;
	protected JComboBox mDateTriggerCombobox;
	protected JComboBox mDatesFormatCombobox;
	private JLabel mVoidValueSymbolLabel;
	private JLabel mDateTriggerLabel;
	private JLabel mDatesFormatLabel;
	protected JCheckBox mColumnsHeaders;
	protected JFormattedTextField mBeginDateTextField;
	private JLabel mEndDateLabel;
	private JLabel mBeginDateLabel;
	protected JTable mSensorsTable;
	protected JButton         mCloseButton;
	
	// La frame m�re.
	protected CSurveyControl    mParent;

	
	/**
	 * Constructeur sans argument.
	 * 
	 * @author  :  soci�t� CS SI
	 * @version : 1.0 du 03/02/10
	 * 
	 * @param pParent la frame m�re.
	 */
	protected CCsvExportPanelGui(CSurveyControl pParent)
	{
		super();

        mParent = pParent;

		initGUI();
	}
	
	/**
	 * M�thode de construction de l'IHM.
	 * 
	 * @author  :  soci�t� CS SI
	 * @version : 1.0 du 27/01/10
	 */
	private void initGUI()
	{
		try
		{
			this.setPreferredSize(new java.awt.Dimension(561,449));
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.columnWidths = new int[] {236, 138, 140, 105};
			thisLayout.rowHeights = new int[] {7, 32, 7, 36, 20, 20, 8, 20, 8, 20, 20, 20, 20, 8, 20, 20, 20};
			thisLayout.columnWeights = new double[] {0.0, 0.0, 0.0, 0.0};
			thisLayout.rowWeights = new double[] {0.1, 0.0, 0.1, 0.0, 0.1, 0.1, 0.0, 0.1, 0.0, 0.1, 0.1, 0.1, 0.1, 0.0, 0.1, 0.1, 0.1};
			this.setLayout(thisLayout);
			{
				mSensorsLabel = new JLabel();
				this.add(mSensorsLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 8, 0, 4), 0, 0));
				mSensorsLabel.setText(CIniFileReader.getString("P6B1"));
				mSensorsLabel.setHorizontalAlignment(SwingConstants.CENTER);
				mSensorsLabel.setToolTipText(CIniFileReader.getString("P6B1TTT"));
			}
			{
				mExportButton = new JButton();
				this.add(mExportButton, new GridBagConstraints(2, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mExportButton.setText(CIniFileReader.getString("P6B1"));
				mExportButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mExportButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mExportButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mExportButton.setSize(90, 23);
				mExportButton.setToolTipText(CIniFileReader.getString("P6B1TTT"));
				mExportButton.setEnabled(true);
			}
			{
				mCloseButton = new JButton();
				this.add(mCloseButton, new GridBagConstraints(3, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mCloseButton.setText(CIniFileReader.getString("P6B2"));
				mCloseButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mCloseButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mCloseButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mCloseButton.setSize(90, 23);
				mCloseButton.setToolTipText(CIniFileReader.getString("P6B2TTT"));
			}
			{
				this.add(new JScrollPane(getMSensorsTable()), new GridBagConstraints(1, 1, 3, 5, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
				this.add(getMBeginDateLabel(), new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				this.add(getMEndDateLabel(), new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				this.add(getMBeginDateTextField(), new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
				this.add(getMEndDateTextField(), new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
				this.add(getMColumnsHeaders(), new GridBagConstraints(1, 9, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
				this.add(getMDatesFormatLabel(), new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				this.add(getMDateTriggerLabel(), new GridBagConstraints(2, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				this.add(getMVoidValueSymbolLabel(), new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				this.add(getMDatesFormatCombobox(), new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
				this.add(getMDateTriggerCombobox(), new GridBagConstraints(3, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
				this.add(getMVoidValueSymbolTextField(), new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
			}
		}
		catch (Exception lException)
		{
			lException.printStackTrace();
		}
	}
	

	 
	 private JTable getMSensorsTable() {
		 if(mSensorsTable == null) {
			 mSensorsTable = new JTable();
		 }
		 return mSensorsTable;
	 }
	 
	 private JLabel getMBeginDateLabel() {
		 if(mBeginDateLabel == null) {
			 mBeginDateLabel = new JLabel();
			 mBeginDateLabel.setText(CIniFileReader.getString("P6L2"));
		 }
		 return mBeginDateLabel;
	 }
	 
	 private JLabel getMEndDateLabel() {
		 if(mEndDateLabel == null) {
			 mEndDateLabel = new JLabel();
			 mEndDateLabel.setText(CIniFileReader.getString("P6L3"));
		 }
		 return mEndDateLabel;
	 }
	 
	 private JTextField getMBeginDateTextField() {
		 if(mBeginDateTextField == null) {
			 mBeginDateTextField = new JFormattedTextField(Survey.dateFormat);
			 mBeginDateTextField.setToolTipText(CIniFileReader.getString("P6L2TTT"));
		 }
		 return mBeginDateTextField;
	 }
	 
	 private JTextField getMEndDateTextField() {
		 if(mEndDateTextField == null) {
			 mEndDateTextField = new JFormattedTextField(Survey.dateFormat);
			 mEndDateTextField.setToolTipText(CIniFileReader.getString("P6L3TTT"));
		 }
		 return mEndDateTextField;
	 }
	 
	 private JCheckBox getMColumnsHeaders() {
		 if(mColumnsHeaders == null) {
			 mColumnsHeaders = new JCheckBox();
			 mColumnsHeaders.setText(CIniFileReader.getString("P6L4"));
			 mColumnsHeaders.setToolTipText(CIniFileReader.getString("P6L4TTT"));
		 }
		 return mColumnsHeaders;
	 }
	 
	 private JLabel getMDatesFormatLabel() {
		 if(mDatesFormatLabel == null) {
			 mDatesFormatLabel = new JLabel();
			 mDatesFormatLabel.setText(CIniFileReader.getString("P6L5"));
		 }
		 return mDatesFormatLabel;
	 }
	 
	 private JLabel getMDateTriggerLabel() {
		 if(mDateTriggerLabel == null) {
			 mDateTriggerLabel = new JLabel();
			 mDateTriggerLabel.setText(CIniFileReader.getString("P6L6"));
		 }
		 return mDateTriggerLabel;
	 }
	 
	 private JLabel getMVoidValueSymbolLabel() {
		 if(mVoidValueSymbolLabel == null) {
			 mVoidValueSymbolLabel = new JLabel();
			 mVoidValueSymbolLabel.setText(CIniFileReader.getString("P6L7"));
		 }
		 return mVoidValueSymbolLabel;
	 }
	 
	 private JComboBox getMDatesFormatCombobox() {
		 if(mDatesFormatCombobox == null)
		 {
			 ArrayList<String> lStringsList = new ArrayList<String>();
			
			
			 lStringsList.add(CIniFileReader.getString("P6L5FORMAT1"));
			 lStringsList.add(CIniFileReader.getString("P6L5FORMAT2"));
			 lStringsList.add(CIniFileReader.getString("P6L5FORMAT3"));
			 lStringsList.add(CIniFileReader.getString("P6L5FORMAT4"));
			 lStringsList.add(CIniFileReader.getString("P6L5FORMAT5"));
				
			 ComboBoxModel mDatesFormatComboboxModel = 
				 new DefaultComboBoxModel(lStringsList.toArray());
			 mDatesFormatCombobox = new JComboBox();
			 mDatesFormatCombobox.setModel(mDatesFormatComboboxModel);
			 mDatesFormatCombobox.setToolTipText(CIniFileReader.getString("P6L5TTT"));
		 }
		 return mDatesFormatCombobox;
	 }
	 
	 private JComboBox getMDateTriggerCombobox() {
		 if(mDateTriggerCombobox == null)
		 {
			 ArrayList<String> lStringsList = new ArrayList<String>();
				
				
			 lStringsList.add(CIniFileReader.getString("P6L6TRIGGER1"));
			 lStringsList.add(CIniFileReader.getString("P6L6TRIGGER2"));
			 lStringsList.add(CIniFileReader.getString("P6L6TRIGGER3"));
			 lStringsList.add(CIniFileReader.getString("P6L6TRIGGER4"));
			 lStringsList.add(CIniFileReader.getString("P6L6TRIGGER5"));
				
			 ComboBoxModel mDateTriggerComboboxModel = 
				 new DefaultComboBoxModel(lStringsList.toArray());
			 mDateTriggerCombobox = new JComboBox();
			 mDateTriggerCombobox.setModel(mDateTriggerComboboxModel);
			 mDateTriggerCombobox.setToolTipText(CIniFileReader.getString("P6L6TTT"));
		 }
		 return mDateTriggerCombobox;
	 }
	 
	 private JTextField getMVoidValueSymbolTextField() {
		 if(mVoidValueSymbolTextField == null) {
			 mVoidValueSymbolTextField = new JTextField();
			 mVoidValueSymbolTextField.setToolTipText(CIniFileReader.getString("P6L7TTT"));
		 }
		 return mVoidValueSymbolTextField;
	 }
}
