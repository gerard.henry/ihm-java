package gui.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import tools.CIniFileReader;
import controls.BaseView;
import cs.ui.DateField;
import cs.ui.DoubleField;
import cs.ui.IntegerField;
import cs.ui.StringField;
import db.Survey;




/**
 * Panel de gestion des senseurs et variables.
 * 
 * @author  :  soci�t� CS SI
 * @version : 1.0 du 17/12/09
 */
public class SensorsGui extends BaseView
{
	private static final long serialVersionUID = 1L;

	private   JLabel     mIdLabel;
	protected StringField mIdTextField;
	
	private   JLabel     mNatureLabel;
	protected JTextField mNatureTextField;

	private   JLabel     mTypeLabel;
	protected StringField mTypeTextField;

	private   JLabel     mGroupLabel;
	protected StringField mGroupTextField;

	private   JLabel     mDescriptionLabel;
	protected JTextArea  mDescriptionTextArea;

	private   JLabel     mStateLabel;
	protected JTextField mStateTextField;

	private   JLabel     mUnitLabel;
	protected StringField mUnitTextField;

	private   JLabel     mInstallationDateLabel;
	protected DateField mInstallationDateTextField;

	private   JLabel     mModificationDateLabel;
	protected DateField mModificationDateTextField;

	private   JLabel     mRegressionDateLabel;
	protected DateField mRegressionDateTextField;

	private   JLabel     mReferenceValueLabel;
	protected DoubleField mReferenceValueTextField;

	private   JLabel     mMinimumValueLabel;
	protected DoubleField mMinimumValueTextField;

	private   JLabel     mMaximumValueLabel;
	protected DoubleField mMaximumValueTextField;

	private   JLabel     mMaximumVariationLabel;
	protected DoubleField mMaximumVariationTextField;

	private   JLabel     mMeasuresCountLabel;
	protected IntegerField mMeasuresCountTextField;

	private   JLabel     mV0Label;
	
	private   JLabel     mV1Label;
	protected DoubleField mV1TextField;
	
	private   JLabel     mV2Label;
	protected DoubleField mV2TextField;
	
	private   JLabel     mV3Label;
	protected DoubleField mV3TextField;
	
	private   JLabel     mV4Label;
	protected DoubleField mV4TextField;
	
	private   JLabel     mV5Label;
	protected DoubleField mV5TextField;
	
	private   JLabel     mV6Label;
	protected DoubleField mV6TextField;
	
	private   JLabel     mV7Label;
	protected DoubleField mV7TextField;
	
	private   JLabel     mV8Label;
	protected DoubleField mV8TextField;
	
	private   JLabel     mV9Label;
	protected DoubleField mV9TextField;

	private   JLabel     mFormulaLabel;
	protected JTextField mFormulaTextField;
	
	protected JButton    mModifyButton;
	protected JButton    mAddButton;
	protected JButton    mSaveButton;
	protected JButton    mDeleteButton;
	protected JButton    mCancelButton;
	protected JButton    mSearchButton;
	protected JButton    mPrintButton;
	protected JButton    mCloseButton;

	
	/**
	 * Constructeur sans argument.
	 * 
	 * @author  :  soci�t� CS SI
	 * @version : 1.0 du 17/12/09
	 */
	public SensorsGui()
	{
		super();
		initGUI();
	}
	
	/**
	 * M�thode de construction de l'IHM.
	 * 
	 * @author  :  soci�t� CS SI
	 * @version : 1.0 du 17/12/09
	 */
	private void initGUI()
	{
		try {
			this.setPreferredSize(new java.awt.Dimension(561,449));
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.columnWidths = new int[] {175, 7, 138, 7, 104, 20};
			thisLayout.rowHeights = new int[] {7, 20, 20, 8, 106, 8, 20, 8, 20, 20, 20, 20, 8, 20, 20, 20, 20, 20, 7, 20, 20};
			thisLayout.columnWeights = new double[] {0.0, 0.1, 0.0, 0.1, 0.0, 0.1};
			thisLayout.rowWeights = new double[] {0.1, 0.1, 0.1, 0.0, 0.0, 0.0, 0.1, 0.0, 0.1, 0.1, 0.1, 0.1, 0.0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.0, 0.1, 0.1};
			this.setLayout(thisLayout);
			{
				mDescriptionLabel = new JLabel();
				this.add(mDescriptionLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mDescriptionLabel.setText(CIniFileReader.getString("P2L7"));
				mDescriptionLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mDescriptionTextArea = new JTextArea();
				this.add(mDescriptionTextArea, new GridBagConstraints(1, 4, 5, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 4, 0, 8), 0, 0));
				mDescriptionTextArea.setFont(new java.awt.Font("Monospaced",0,11));
				mDescriptionTextArea.setMinimumSize(new java.awt.Dimension(364,136));
				mDescriptionTextArea.setEnabled(false);
				mDescriptionTextArea.setToolTipText(CIniFileReader.getString("P2L7TTT"));
			}
			{
				mInstallationDateTextField = new DateField(Survey.dateFormat);
				this.add(mInstallationDateTextField, new GridBagConstraints(5, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8), 0, 0));
				mInstallationDateTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mInstallationDateTextField.setEnabled(false);
				mInstallationDateTextField.setNullAllowed(true);
				mInstallationDateTextField.setToolTipText(CIniFileReader.getString("P2L10TTT"));
			}
			{
				mModificationDateLabel = new JLabel();
				this.add(mModificationDateLabel, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mModificationDateLabel.setText(CIniFileReader.getString("P2L11"));
				mModificationDateLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mRegressionDateLabel = new JLabel();
				this.add(mRegressionDateLabel, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mRegressionDateLabel.setText(CIniFileReader.getString("P2L12"));
				mRegressionDateLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mModificationDateTextField = new DateField(Survey.dateFormat);
				this.add(mModificationDateTextField, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mModificationDateTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mModificationDateTextField.setEnabled(false);
				mModificationDateTextField.setNullAllowed(true);
				mModificationDateTextField.setToolTipText(CIniFileReader.getString("P2L11TTT"));
			}
			{
				mRegressionDateTextField = new DateField(Survey.dateFormat);
				mRegressionDateTextField.setNullAllowed(true);
				this.add(mRegressionDateTextField, new GridBagConstraints(3, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mRegressionDateTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mRegressionDateTextField.setEnabled(false);
				mRegressionDateTextField.setNullAllowed(true);
				mRegressionDateTextField.setToolTipText(CIniFileReader.getString("P2L12TTT"));
			}
			{
				mMeasuresCountLabel = new JLabel();
				this.add(mMeasuresCountLabel, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mMeasuresCountLabel.setText(CIniFileReader.getString("P2L17"));
				mMeasuresCountLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mIdTextField = new StringField();
				this.add(mIdTextField, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mIdTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mIdTextField.setEnabled(false);
				mIdTextField.setMaxValue(8);
				mIdTextField.setToolTipText(CIniFileReader.getString("P2L1TTT"));
            }
			{
				mNatureLabel = new JLabel();
				this.add(mNatureLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mNatureLabel.setText(CIniFileReader.getString("P2L3"));
				mNatureLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mTypeLabel = new JLabel();
				this.add(mTypeLabel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mTypeLabel.setText(CIniFileReader.getString("P2L4"));
				mTypeLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mGroupLabel = new JLabel();
				this.add(mGroupLabel, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mGroupLabel.setText(CIniFileReader.getString("P2L5"));
				mGroupLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mNatureTextField = new JTextField();
				this.add(mNatureTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mNatureTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mNatureTextField.setText("");
				mNatureTextField.setEnabled(false);
				mNatureTextField.setToolTipText(CIniFileReader.getString("P2L3TTT"));
			}
			{
				mTypeTextField = new StringField();
				this.add(mTypeTextField, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mTypeTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mTypeTextField.setEnabled(false);
				mTypeTextField.setMaxValue(2);
				mTypeTextField.setToolTipText(CIniFileReader.getString("P2L4TTT"));
			}
			{
				mGroupTextField = new StringField();
				this.add(mGroupTextField, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8), 0, 0));
				mGroupTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mGroupTextField.setEnabled(false);
				mGroupTextField.setMaxValue(2);
				mGroupTextField.setToolTipText(CIniFileReader.getString("P2L5TTT"));
			}
			{
				mIdLabel = new JLabel();
				this.add(mIdLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mIdLabel.setText(CIniFileReader.getString("P2L1"));
				mIdLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mInstallationDateLabel = new JLabel();
				this.add(mInstallationDateLabel, new GridBagConstraints(4, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mInstallationDateLabel.setText(CIniFileReader.getString("P2L10"));
				mInstallationDateLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mUnitLabel = new JLabel();
				this.add(mUnitLabel, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mUnitLabel.setText(CIniFileReader.getString("P2L9"));
				mUnitLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mUnitTextField = new StringField();
				this.add(mUnitTextField, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mUnitTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mUnitTextField.setEnabled(false);
				mUnitTextField.setMaxValue(6);
				mUnitTextField.setToolTipText(CIniFileReader.getString("P2L9TTT"));
			}
			{
				mStateLabel = new JLabel();
				this.add(mStateLabel, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mStateLabel.setText(CIniFileReader.getString("P2L8"));
				mStateLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mStateTextField = new JTextField();
				this.add(mStateTextField, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mStateTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mStateTextField.setEnabled(false);
				mStateTextField.setToolTipText(CIniFileReader.getString("P2L8TTT"));
			}
			{
				mReferenceValueLabel = new JLabel();
				this.add(mReferenceValueLabel, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mReferenceValueLabel.setText(CIniFileReader.getString("P2L13"));
				mReferenceValueLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mReferenceValueTextField = new DoubleField(3);
				this.add(mReferenceValueTextField, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mReferenceValueTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mReferenceValueTextField.setEnabled(false);
				mReferenceValueTextField.setToolTipText(CIniFileReader.getString("P2L13TTT"));
			}
			{
				mMinimumValueLabel = new JLabel();
				this.add(mMinimumValueLabel, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mMinimumValueLabel.setText(CIniFileReader.getString("P2L14"));
				mMinimumValueLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mMaximumValueLabel = new JLabel();
				this.add(mMaximumValueLabel, new GridBagConstraints(2, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mMaximumValueLabel.setText(CIniFileReader.getString("P2L15"));
				mMaximumValueLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mMaximumVariationLabel = new JLabel();
				this.add(mMaximumVariationLabel, new GridBagConstraints(4, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mMaximumVariationLabel.setText(CIniFileReader.getString("P2L16"));
				mMaximumVariationLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mMinimumValueTextField = new DoubleField(3);;
				this.add(mMinimumValueTextField, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mMinimumValueTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mMinimumValueTextField.setEnabled(false);
				mMinimumValueTextField.setMinValue(0.0);
				mMinimumValueTextField.setToolTipText(CIniFileReader.getString("P2L14TTT"));
			}
			{
				mMaximumValueTextField = new DoubleField(3);
				this.add(mMaximumValueTextField, new GridBagConstraints(3, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mMaximumValueTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mMaximumValueTextField.setEnabled(false);
				mMaximumValueTextField.setMinValue(0.0);
				mMaximumValueTextField.setToolTipText(CIniFileReader.getString("P2L15TTT"));
			}
			{
				mMaximumVariationTextField = new DoubleField(3);
				this.add(mMaximumVariationTextField, new GridBagConstraints(5, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8), 0, 0));
				mMaximumVariationTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mMaximumVariationTextField.setEnabled(false);
				mMaximumVariationTextField.setMinValue(0.0);
				mMaximumVariationTextField.setToolTipText(CIniFileReader.getString("P2L16TTT"));
			}
			{
				mMeasuresCountTextField = new IntegerField();
				this.add(mMeasuresCountTextField, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mMeasuresCountTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mMeasuresCountTextField.setEnabled(false);
				mMeasuresCountTextField.setToolTipText(CIniFileReader.getString("P2L17TTT"));
			}
			{
				mV1Label = new JLabel();
				this.add(mV1Label, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mV1Label.setText(CIniFileReader.getString("P2L19"));
				mV1Label.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mV0Label = new JLabel();
				this.add(mV0Label, new GridBagConstraints(0, 13, 6, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
				mV0Label.setText(CIniFileReader.getString("P2L18"));
				mV0Label.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mV4Label = new JLabel();
				this.add(mV4Label, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mV4Label.setText(CIniFileReader.getString("P2L22"));
				mV4Label.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mV7Label = new JLabel();
				this.add(mV7Label, new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mV7Label.setText(CIniFileReader.getString("P2L25"));
				mV7Label.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mV2Label = new JLabel();
				this.add(mV2Label, new GridBagConstraints(2, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV2Label.setText(CIniFileReader.getString("P2L20"));
				mV2Label.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mV3Label = new JLabel();
				this.add(mV3Label, new GridBagConstraints(4, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV3Label.setText(CIniFileReader.getString("P2L21"));
				mV3Label.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mV5Label = new JLabel();
				this.add(mV5Label, new GridBagConstraints(2, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV5Label.setText(CIniFileReader.getString("P2L23"));
				mV5Label.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mV6Label = new JLabel();
				this.add(mV6Label, new GridBagConstraints(4, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV6Label.setText(CIniFileReader.getString("P2L24"));
				mV6Label.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mV8Label = new JLabel();
				this.add(mV8Label, new GridBagConstraints(2, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV8Label.setText(CIniFileReader.getString("P2L26"));
				mV8Label.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mV9Label = new JLabel();
				this.add(mV9Label, new GridBagConstraints(4, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV9Label.setText(CIniFileReader.getString("P2L27"));
				mV9Label.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mV1TextField = new DoubleField(3);
				this.add(mV1TextField, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV1TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mV1TextField.setEnabled(false);
				mV1TextField.setToolTipText(CIniFileReader.getString("P2L19TTT"));
			}
			{
				mV4TextField = new DoubleField(3);
				this.add(mV4TextField, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV4TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mV4TextField.setEnabled(false);
				mV4TextField.setToolTipText(CIniFileReader.getString("P2L22TTT"));
			}
			{
				mV7TextField = new DoubleField(3);
				this.add(mV7TextField, new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV7TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mV7TextField.setEnabled(false);
				mV7TextField.setToolTipText(CIniFileReader.getString("P2L25TTT"));
			}
			{
				mV2TextField = new DoubleField(3);
				this.add(mV2TextField, new GridBagConstraints(3, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV2TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mV2TextField.setEnabled(false);
				mV2TextField.setToolTipText(CIniFileReader.getString("P2L20TTT"));
			}
			{
				mV5TextField = new DoubleField(3);
				this.add(mV5TextField, new GridBagConstraints(3, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV5TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mV5TextField.setEnabled(false);
				mV5TextField.setToolTipText(CIniFileReader.getString("P2L23TTT"));
			}
			{
				mV8TextField = new DoubleField(3);
				this.add(mV8TextField, new GridBagConstraints(3, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mV8TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mV8TextField.setEnabled(false);
				mV8TextField.setToolTipText(CIniFileReader.getString("P2L26TTT"));
			}
			{
				mV3TextField = new DoubleField(3);
				this.add(mV3TextField, new GridBagConstraints(5, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8), 0, 0));
				mV3TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mV3TextField.setEnabled(false);
				mV3TextField.setToolTipText(CIniFileReader.getString("P2L21TTT"));
			}
			{
				mV6TextField = new DoubleField(3);
				this.add(mV6TextField, new GridBagConstraints(5, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8), 0, 0));
				mV6TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mV6TextField.setEnabled(false);
				mV6TextField.setToolTipText(CIniFileReader.getString("P2L24TTT"));
			}
			{
				mV9TextField = new DoubleField(3);
				this.add(mV9TextField, new GridBagConstraints(5, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8), 0, 0));
				mV9TextField.setHorizontalAlignment(SwingConstants.CENTER);
				mV9TextField.setEnabled(false);
				mV9TextField.setToolTipText(CIniFileReader.getString("P2L27TTT"));
			}
			{
				mFormulaLabel = new JLabel();
				this.add(mFormulaLabel, new GridBagConstraints(0, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mFormulaLabel.setText(CIniFileReader.getString("P2L28"));
				mFormulaLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mFormulaTextField = new JTextField();
				this.add(mFormulaTextField, new GridBagConstraints(1, 17, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8), 0, 0));
				mFormulaTextField.setText("");
				mFormulaTextField.setEnabled(false);
				mFormulaTextField.setToolTipText(CIniFileReader.getString("P2L28TTT"));
			}
			{
				mModifyButton = new JButton();
				this.add(mModifyButton, new GridBagConstraints(1, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mModifyButton.setText(CIniFileReader.getString("P2B1"));
				mModifyButton.setToolTipText(CIniFileReader.getString("P2B1TTT"));
				mModifyButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mModifyButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mModifyButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mModifyButton.setSize(90, 23);
			}
			{
				mAddButton = new JButton();
				this.add(mAddButton, new GridBagConstraints(2, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mAddButton.setText(CIniFileReader.getString("P2B2"));
				mAddButton.setToolTipText(CIniFileReader.getString("P2B2TTT"));
				mAddButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mAddButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mAddButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mAddButton.setSize(90, 23);
			}
			{
				mSaveButton = new JButton();
				this.add(mSaveButton, new GridBagConstraints(3, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mSaveButton.setText(CIniFileReader.getString("P2B3"));
				mSaveButton.setToolTipText(CIniFileReader.getString("P2B3TTT"));
				mSaveButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mSaveButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mSaveButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mSaveButton.setSize(90, 23);
			}
			{
				mDeleteButton = new JButton();
				this.add(mDeleteButton, new GridBagConstraints(4, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mDeleteButton.setText(CIniFileReader.getString("P2B4"));
				mDeleteButton.setToolTipText(CIniFileReader.getString("P2B4TTT"));
				mDeleteButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mDeleteButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mDeleteButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mDeleteButton.setSize(90, 23);
			}
			{
				mCancelButton = new JButton();
				this.add(mCancelButton, new GridBagConstraints(5, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mCancelButton.setText(CIniFileReader.getString("P2B5"));
				mCancelButton.setToolTipText(CIniFileReader.getString("P2B5TTT"));
				mCancelButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mCancelButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mCancelButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mCancelButton.setSize(90, 23);
			}
			{
				mSearchButton = new JButton();
				this.add(mSearchButton, new GridBagConstraints(3, 20, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mSearchButton.setText(CIniFileReader.getString("P2B6"));
				mSearchButton.setToolTipText(CIniFileReader.getString("P2B6TTT"));
				mSearchButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mSearchButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mSearchButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mSearchButton.setSize(90, 23);
			}
			{
				mPrintButton = new JButton();
				this.add(mPrintButton, new GridBagConstraints(4, 20, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mPrintButton.setText(CIniFileReader.getString("P2B7"));
				mPrintButton.setToolTipText(CIniFileReader.getString("P2B7TTT"));
				mPrintButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mPrintButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mPrintButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mPrintButton.setSize(90, 23);
			}
			{
				mCloseButton = new JButton();
				this.add(mCloseButton, new GridBagConstraints(5, 20, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 8), 0, 0));
				mCloseButton.setText(CIniFileReader.getString("P2B8"));
				mCloseButton.setToolTipText(CIniFileReader.getString("P2B8TTT"));
				mCloseButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mCloseButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mCloseButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mCloseButton.setSize(90, 23);
			}
		}
		catch (Exception lException)
		{
			lException.printStackTrace();
		}
	}
}
