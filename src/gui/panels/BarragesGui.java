package gui.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import tools.CIniFileReader;
import controls.BaseView;
import cs.ui.DoubleField;
import db.Survey;


/**
 * Panel de gestion d'un barrage.
 * 
 * @author  :  soci�t� CS SI
 * @version : 1.0 du 04/12/09
 */
public class BarragesGui extends BaseView
{
	private static final long serialVersionUID = 1L;

	private JLabel       mIdLabel;
	protected JTextField mIdTextField;

	private JLabel       mTitleLabel;
	protected JTextArea  mTitleTextArea;

	private JLabel       mDescriptionLabel;
	protected JTextArea  mDescriptionTextArea;

	private JLabel       mLaunchingDateLabel;
	protected JFormattedTextField mLaunchingDateTextField;

	private JLabel       mSensorsCountLabel;
	protected JTextField mSensorsCountTextField;

	private JLabel       mStockedVariablesCountLabel;
	protected JTextField mStockedVariablesCountTextField;

	private JLabel       mUpdateDateLabel;
	protected JFormattedTextField mUpdateDateTextField;

	private JLabel       mSaveDateLabel;
	protected JFormattedTextField mSaveDateTextField;
	
	private JLabel       mMeasuresCountLabel;
	protected JTextField mMeasuresCountTextField;

	private JLabel       mWaterMinLevelLabel;
	protected DoubleField mWaterMinLevelTextField;
	private JLabel       mWaterMinLevelUnitLabel;

	private JLabel       mWaterNominalLevelLabel;
	protected DoubleField mWaterNominalLevelTextField;
	private JLabel       mWaterNominalLevelUnitLabel;
	
	private JLabel       mWaterMaxLevelLabel;
	protected DoubleField mWaterMaxLevelTextField;
	private JLabel       mWaterMaxLevelUnitLabel;

	protected JButton    mModifyButton;
	protected JButton    mSaveButton;
	protected JButton    mDeleteButton;
	protected JButton    mCancelButton;
	protected JButton    mPrintButton;
	protected JButton    mCloseButton;
	
	
	/**
	 * Constructeur sans argument.
	 * 
	 * @author  :  soci�t� CS SI
	 * @version : 1.0 du 04/12/09
	 */
	protected BarragesGui()
	{
		super();
		initGUI();
	}
	
	/**
	 * M�thode de construction de l'IHM.
	 * 
	 * @author  :  soci�t� CS SI
	 * @version : 1.0 du 04/12/09
	 */
	private void initGUI()
	{
		try
		{
			this.setPreferredSize(new java.awt.Dimension(561,449));
			GridBagLayout thisLayout = new GridBagLayout();
			thisLayout.columnWidths = new int[] {190, 90, 180, 7};
			thisLayout.rowHeights = new int[] {7, 7, 7, 7, 20, 20, 20, 20, 7, 20, 20, 7, 7, 20, 20};
			thisLayout.columnWeights = new double[] {0.0, 0.0, 0.0, 0.1};
			thisLayout.rowWeights = new double[] {0.1, 0.1, 0.1, 0.0, 0.1, 0.1, 0.1, 0.1, 0.0, 0.1, 0.1, 0.1, 0.0, 0.1, 0.1};
			this.setLayout(thisLayout);
			{
				mIdLabel = new JLabel();
				this.add(mIdLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mIdLabel.setText(CIniFileReader.getString("P1L1"));
				mIdLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mIdTextField = new JTextField();
				this.add(mIdTextField, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 8), 0, 0));
				mIdTextField.setFont(new java.awt.Font("Tahoma",1,11));
				mIdTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mIdTextField.setText("");
				mIdTextField.setToolTipText(CIniFileReader.getString("P1L1TTT"));
			}
			{
				mDescriptionLabel = new JLabel();
				this.add(mDescriptionLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mDescriptionLabel.setText(CIniFileReader.getString("P1L3"));
				mDescriptionLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mDescriptionTextArea = new JTextArea();
				this.add(mDescriptionTextArea, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(4, 0, 0, 8), 0, 0));
				mDescriptionTextArea.setFont(new java.awt.Font("Monospaced",0,11));
				mDescriptionTextArea.setMinimumSize(new java.awt.Dimension(364,136));
				mDescriptionTextArea.setToolTipText(CIniFileReader.getString("P1L3TTT"));
			}
			{
				mTitleLabel = new JLabel();
				this.add(mTitleLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mTitleLabel.setText(CIniFileReader.getString("P1L2"));
				mTitleLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mTitleTextArea = new JTextArea();
				this.add(mTitleTextArea, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 4, 8), 0, 0));
				mTitleTextArea.setFont(new java.awt.Font("Monospaced",0,11));
				mTitleTextArea.setMinimumSize(new java.awt.Dimension(364,136));
				mTitleTextArea.setToolTipText(CIniFileReader.getString("P1L2TTT"));
			}
			{
				mLaunchingDateLabel = new JLabel();
				this.add(mLaunchingDateLabel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mLaunchingDateLabel.setText(CIniFileReader.getString("P1L4"));
				mLaunchingDateLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mLaunchingDateTextField = new JFormattedTextField(Survey.dateFormat);
				this.add(mLaunchingDateTextField, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mLaunchingDateTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mLaunchingDateTextField.setToolTipText(CIniFileReader.getString("P1L4TTT"));
			}
			{
				mSensorsCountLabel = new JLabel();
				this.add(mSensorsCountLabel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mSensorsCountLabel.setText(CIniFileReader.getString("P1L5"));
				mSensorsCountLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mSensorsCountTextField = new JTextField();
				this.add(mSensorsCountTextField, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mSensorsCountTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mSensorsCountTextField.setToolTipText(CIniFileReader.getString("P1L5TTT"));
			}
			{
				mStockedVariablesCountLabel = new JLabel();
				this.add(mStockedVariablesCountLabel, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mStockedVariablesCountLabel.setText(CIniFileReader.getString("P1L6"));
				mStockedVariablesCountLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mStockedVariablesCountTextField = new JTextField();
				this.add(mStockedVariablesCountTextField, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8), 0, 0));
				mStockedVariablesCountTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mStockedVariablesCountTextField.setToolTipText(CIniFileReader.getString("P1L6TTT"));
			}
			{
				mUpdateDateLabel = new JLabel();
				this.add(mUpdateDateLabel, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mUpdateDateLabel.setText(CIniFileReader.getString("P1L7"));
				mUpdateDateLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mSaveDateLabel = new JLabel();
				this.add(mSaveDateLabel, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mSaveDateLabel.setText(CIniFileReader.getString("P1L8"));
				mSaveDateLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mUpdateDateTextField = new JFormattedTextField(Survey.dateFormat);
				this.add(mUpdateDateTextField, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mUpdateDateTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mUpdateDateTextField.setToolTipText(CIniFileReader.getString("P1L7TTT"));
			}
			{
				mSaveDateTextField = new JFormattedTextField(Survey.dateFormat);
				this.add(mSaveDateTextField, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 8), 0, 0));
				mSaveDateTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mSaveDateTextField.setToolTipText(CIniFileReader.getString("P1L8TTT"));
			}
			{
				mMeasuresCountTextField = new JTextField();
				this.add(mMeasuresCountTextField, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mMeasuresCountTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mMeasuresCountTextField.setToolTipText(CIniFileReader.getString("P1L9TTT"));
			}
			{
				mMeasuresCountLabel = new JLabel();
				this.add(mMeasuresCountLabel, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mMeasuresCountLabel.setText(CIniFileReader.getString("P1L9"));
				mMeasuresCountLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mWaterMinLevelLabel = new JLabel();
				this.add(mWaterMinLevelLabel, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mWaterMinLevelLabel.setText(CIniFileReader.getString("P1L10"));
				mWaterMinLevelLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mWaterNominalLevelLabel = new JLabel();
				this.add(mWaterNominalLevelLabel, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mWaterNominalLevelLabel.setText(CIniFileReader.getString("P1L11"));
				mWaterNominalLevelLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mWaterMaxLevelLabel = new JLabel();
				this.add(mWaterMaxLevelLabel, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 8, 0, 4), 0, 0));
				mWaterMaxLevelLabel.setText(CIniFileReader.getString("P1L12"));
				mWaterMaxLevelLabel.setHorizontalAlignment(SwingConstants.CENTER);
			}
			{
				mWaterMinLevelUnitLabel = new JLabel();
				this.add(mWaterMinLevelUnitLabel, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mWaterMinLevelUnitLabel.setText("  (" + CIniFileReader.getString("P1L13") + ")");
			}
			{
				mWaterNominalLevelUnitLabel = new JLabel();
				this.add(mWaterNominalLevelUnitLabel, new GridBagConstraints(2, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mWaterNominalLevelUnitLabel.setText("  (" + CIniFileReader.getString("P1L14") + ")");
			}
			{
				mWaterMaxLevelUnitLabel = new JLabel();
				this.add(mWaterMaxLevelUnitLabel, new GridBagConstraints(2, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mWaterMaxLevelUnitLabel.setText("  (" + CIniFileReader.getString("P1L15") + ")");
			}
			{
				mWaterMinLevelTextField = new DoubleField(3);
				this.add(mWaterMinLevelTextField, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mWaterMinLevelTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mWaterMinLevelTextField.setToolTipText(CIniFileReader.getString("P1L10TTT"));
			}
			{
				mWaterNominalLevelTextField = new DoubleField(3);
				this.add(mWaterNominalLevelTextField, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mWaterNominalLevelTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mWaterNominalLevelTextField.setToolTipText(CIniFileReader.getString("P1L11TTT"));
			}
			{
				mWaterMaxLevelTextField = new DoubleField(3);
				this.add(mWaterMaxLevelTextField, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(0, 4, 0, 4), 0, 0));
				mWaterMaxLevelTextField.setHorizontalAlignment(SwingConstants.CENTER);
				mWaterMaxLevelTextField.setToolTipText(CIniFileReader.getString("P1L12TTT"));
			}
			{
				mModifyButton = new JButton();
				this.add(mModifyButton, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mModifyButton.setText(CIniFileReader.getString("P1B1"));
				mModifyButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mModifyButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mModifyButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mModifyButton.setSize(90, 23);
				mModifyButton.setToolTipText(CIniFileReader.getString("P1B1TTT"));
			}
			{
				mSaveButton = new JButton();
				this.add(mSaveButton, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mSaveButton.setText(CIniFileReader.getString("P1B2"));
				mSaveButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mSaveButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mSaveButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mSaveButton.setSize(90, 23);
				mSaveButton.setToolTipText(CIniFileReader.getString("P1B2TTT"));
			}
			{
				mDeleteButton = new JButton();
				this.add(mDeleteButton, new GridBagConstraints(2, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mDeleteButton.setText(CIniFileReader.getString("P1B3"));
				mDeleteButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mDeleteButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mDeleteButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mDeleteButton.setSize(90, 23);
				mDeleteButton.setToolTipText(CIniFileReader.getString("P1B3TTT"));
			}
			{
				mCancelButton = new JButton();
				this.add(mCancelButton, new GridBagConstraints(3, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mCancelButton.setText(CIniFileReader.getString("P1B4"));
				mCancelButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mCancelButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mCancelButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mCancelButton.setSize(90, 23);
				mCancelButton.setToolTipText(CIniFileReader.getString("P1B4TTT"));
			}
			{
				mPrintButton = new JButton();
				this.add(mPrintButton, new GridBagConstraints(2, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mPrintButton.setText(CIniFileReader.getString("P1B5"));
				mPrintButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mPrintButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mPrintButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mPrintButton.setSize(90, 23);
				mPrintButton.setToolTipText(CIniFileReader.getString("P1B5TTT"));
			}
			{
				mCloseButton = new JButton();
				this.add(mCloseButton, new GridBagConstraints(3, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
				mCloseButton.setText(CIniFileReader.getString("P1B6"));
				mCloseButton.setMinimumSize(new java.awt.Dimension(90, 23));
				mCloseButton.setMaximumSize(new java.awt.Dimension(90, 23));
				mCloseButton.setPreferredSize(new java.awt.Dimension(90, 23));
				mCloseButton.setSize(90, 23);
				mCloseButton.setToolTipText(CIniFileReader.getString("P1B6TTT"));
			}
		}
		catch (Exception lException)
		{
			lException.printStackTrace();
		}
	}
}
