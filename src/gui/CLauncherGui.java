package gui;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import tools.CIniFileReader;


/**
 * Fen�tre de lancement de l'application.
 * 
 * @author          :  soci�t� CS SI
 * @version         : 1.0 du 14/11/09
 */
public class CLauncherGui extends javax.swing.JFrame
{
	private static final long   serialVersionUID    = 1L;
	
	private   JLabel  mCemagrefImageLabel;
	private   JLabel  mCopyrightLabel;
	protected JButton mOkButton;

	
/**
 * Constructeur sans argument.
 * 
 * @author  :  soci�t� CS SI
 * @version : 1.0 du 14/11/09
 */
	public CLauncherGui()
	{
		super();
		initGUI();
	}
	
	/**
	 * M�thode de construction de l'IHM.
	 * 
	 * @author  :  soci�t� CS SI
	 * @version : 1.0 du 14/11/09
	 */
	private void initGUI()
	{
		try
		{
			{
		this.setTitle(
				CIniFileReader.getString("P0APPLNAME")
				+ " "
				+ CIniFileReader.getString("P0APPLVERSION"));
		
				setDefaultLookAndFeelDecorated(true);
				javax.swing.UIManager.setLookAndFeel(
						javax.swing.UIManager.getSystemLookAndFeelClassName());
				setResizable(true);
				//this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("config/CemagrefLogo.png")).getImage());
				this.setIconImage(new ImageIcon("config/CemagrefLogo.png").getImage());
				GridBagLayout thisLayout = new GridBagLayout();
				this.setMinimumSize(new java.awt.Dimension(400, 300));
				thisLayout.rowWeights = new double[] {0.1, 0.1, 0.1, 0.0, 0.0, 0.0, 0.0, 0.0};
				thisLayout.rowHeights = new int[] {7, 7, 7, 4, 23, 4, 34, 4};
				thisLayout.columnWeights = new double[] {0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
				thisLayout.columnWidths = new int[] {7, 20, 7, 7, 20, 7};
				getContentPane().setLayout(thisLayout);
				{
					mOkButton = new JButton();
					getContentPane().add(mOkButton, new GridBagConstraints(2, 6, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
					mOkButton.setText(CIniFileReader.getString("P0B1"));
					mOkButton.setSize(80, 23);
				}
				{
					mCopyrightLabel = new JLabel();
					getContentPane().add(mCopyrightLabel, new GridBagConstraints(0, 4, 6, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
					mCopyrightLabel.setText(CIniFileReader.getString("P0COPYRIGHT"));
					mCopyrightLabel.setHorizontalTextPosition(SwingConstants.CENTER);
					mCopyrightLabel.setHorizontalAlignment(SwingConstants.CENTER);
				}
				{
					mCemagrefImageLabel = new JLabel();
					getContentPane().add(mCemagrefImageLabel, new GridBagConstraints(0, 0, 6, 3, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(8, 8, 0, 8), 0, 0));
					mCemagrefImageLabel.setIcon(new ImageIcon("config/CemagrefLogo.jpg"));
				}
			}
			setResizable(false);
			pack();
		}
		catch (Exception lException)
		{
			lException.printStackTrace();
		}
	}
}