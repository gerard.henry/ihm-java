package controls;

import gui.CSurveyGui;
import gui.panels.ResultatsAnalysePanel;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import tools.CIniFileReader;
import controls.panels.BarrageExportDialog;
import controls.panels.BarragesControl;
import controls.panels.CCsvExportPanelControl;
import controls.panels.CorrectionAnalyseDialog;
import controls.panels.FusionBarragesDialog;
import controls.panels.SensorsControl;
import controls.panels.sensorAnalysis.CSensorAnalysControl;
import cs.ui.Dialogs;
import db.Barrage;
import db.Sensor;
import db.SensorMeasures;
import db.SensorResultats;
import db.Survey;
import editeur.MeasureEditor;
import editeur.MeasureGraphPane;

/**
 * Contr�les de la fen�tre principale de l'application.
 * 
 * @author : soci�t� CS SI
 * @version : 1.0 du 01/12/09
 */
@SuppressWarnings("serial")
public class CSurveyControl extends CSurveyGui {
	public static final String PRINT_DIRECTORY = "db/print/";

	// R�pertoire d'exportation des donn�es des barrages.
	public static final String EXPORT_DIRECTORY = "db/export/";

	/**
	 * Constructeur sans argument.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 09/11/09
	 */
	protected CSurveyControl() {
		connectListeners();
	}

	/**
	 * Permet de connecter les composants � leurs �couteurs.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 10/12/09
	 */
	private void connectListeners() {
		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de demander confirmation de
		// fermeture de l'application lorsque l'utilisateur clique sur la croix
		// de la fen�tre.
		// ---------------------------------------------------------------------
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent lEvent) {
				if (Dialogs.showConfirmDialog(CIniFileReader
						.getString("CONFMSG3"), CIniFileReader
						.getString("CONFMSG4")) == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});

		// Connextion des rubriques des menus � leurs �couteurs.
		connectSystemMenuListeners();
		connectMiscellaneousMenuListeners();
		connectDamManagementMenuListeners();
		connectDamMenuListeners();
		connectSensorsAndVariablesMenuListeners();
	}

	/**
	 * Permet de connecter les rubriques du menu "Syst�me" � leurs �couteurs.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 14/12/09
	 */
	private void connectSystemMenuListeners() {
		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'importer une liste de barrages
		// ainsi que les donn�es associ�es.
		// ---------------------------------------------------------------------
		mImportDataMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				if (Dialogs.showConfirmDialog(CIniFileReader
						.getString("CONFMSG7"), CIniFileReader
						.getString("CONFMSG8")) == JOptionPane.YES_OPTION) {
					// 1. Importation des fichiers de donn�es au format DBase
					Survey.getInstance().importDB("convert");
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de demander confirmation de
		// fermeture de l'application lorsque l'utilisateur clique sur la
		// rubrique "Quitter" dans le menu "Syst�me".
		// ---------------------------------------------------------------------
		mQuitMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				if (Dialogs.showConfirmDialog(CIniFileReader
						.getString("CONFMSG3"), CIniFileReader
						.getString("CONFMSG4")) == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});
	}

	/**
	 * Permet de connecter les rubriques du menu "Gestion des barrages" � leurs
	 * �couteurs.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 14/12/09
	 */
	private void connectDamManagementMenuListeners() {
		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de cr�er un barrage.
		// ---------------------------------------------------------------------
		mCreateDamMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				openView(CIniFileReader.getString("P1NAME"),
						new BarragesControl(CSurveyControl.this,null, BarragesControl.CREATE_MODE));
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de modifier un barrage.
		// ---------------------------------------------------------------------
		mModifyDamMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				// 1. Lecture r�f�rence au barrage s�lectionn�
				Barrage barrage = getBarrage();

				if (barrage != null) {
					openView(CIniFileReader.getString("P1NAME"),
							new BarragesControl(CSurveyControl.this,barrage,
									BarragesControl.MODIFY_MODE));
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de supprimer un barrage.
		// ---------------------------------------------------------------------
		mDeleteDamMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				// 1. Lectur er�f�rence au barrage � supprimer
				Barrage barrage = getBarrage();
				
				if (barrage != null) {
					if (Dialogs.showConfirmDialog(CIniFileReader
							.getString("CONFMSG9"), CIniFileReader
							.getString("CONFMSG10")) == JOptionPane.YES_OPTION) {
						// 1. Cr�ation format de date
						SimpleDateFormat fmt = new SimpleDateFormat("-dd-MM-yyyy");
						
						// 2. Cr�ation du fichier de backup du barrage
						File backup = new File("db/backup",barrage.getId()+fmt.format(new Date())+".xml");
						
						// 3. Sauvegarde syst�matique du barrage avant suppression
						Survey.getInstance().exportBarrage(barrage,backup.getAbsolutePath(),null,null);

						// 4. Suppression du barrage
						Survey.getInstance()
								.removeBarrage(barrage.getId());
						
						// 5. Affichage d'um message d'information
						Dialogs.showInformationDialog("Une copie de sauvegarde du barrage a �t� cr��e dans "+backup.getAbsolutePath());
					}
				}
			}
		});
	}

	/**
	 * Permet de connecter les rubriques du menu "Divers" � leurs �couteurs.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 14/12/09
	 */
	private void connectMiscellaneousMenuListeners() {
		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'exporter un barrage.
		// ---------------------------------------------------------------------
		mDamExportMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				// 1. Lecture r�f�rence au barrage s�lectionn�
				Barrage barrage = getBarrage();

				if (barrage != null) {
					// 2. Cr�ation dialogie d'exportation du barrage
					BarrageExportDialog dialog = new BarrageExportDialog(
							barrage);

					// 3. Positionnement du dialogue
					dialog
							.setLocationRelativeTo((Component) lEvent
									.getSource());

					// 4. Visualisation du dialogue
					dialog.setVisible(true);
				}

			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'importer un barrage.
		// ---------------------------------------------------------------------
		mDamImportMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				importActiveDam();

			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de mettre � jour un barrage.
		// ---------------------------------------------------------------------
		mDamUpdateMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				// 1. Cr�ation dialogue de fusion des donn�es de deux barrages
				FusionBarragesDialog dialog = new FusionBarragesDialog();

				// 2. R�solution des contraintes g�om�triques
				dialog.pack();

				// 3. Positionnement du dialogue
				dialog.setLocationRelativeTo((Component) lEvent.getSource());

				// 4. Visualisation du dialogue
				dialog.setVisible(true);

			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'importer un fichier de donn�es
		// Excel d'un barrage.
		// ---------------------------------------------------------------------
		mDamExcelImportMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				// 1. Cr�ation dialogue d'importation du barrage
				BarrageImportDialog dialog = new BarrageImportDialog();

				// 2. Positionnement du dialogue
				dialog.setLocationRelativeTo((Component) lEvent.getSource());

				// 3. Application contraintes g�om�triques
				dialog.pack();

				// 4. Visualisation du dialogue()
				dialog.setVisible(true);
			}
		});
	}

	/**
	 * Permet de connecter les rubriques du menu "Barrage" � leurs �couteurs.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 14/12/09
	 */
	private void connectDamMenuListeners() {
		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'afficher les informations
		// g�n�rales du barrage actif.
		// ---------------------------------------------------------------------
		mGeneralInformationsMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				// 1. Lecture r�f�rence au barrage actif
				Barrage barrage = getBarrage();

				if (barrage != null) {
					openView(CIniFileReader.getString("P1NAME"),
							new BarragesControl(CSurveyControl.this,barrage,
									BarragesControl.INFORMATION_MODE));
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'imprimer l'ensemble des
		// informations des instruments du barrage courant.
		// ---------------------------------------------------------------------
		mSummaryMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {

				// 1. Lecture d�finition du barrage s�lectionn�
				Barrage barrage = getBarrage();

				if (barrage != null) {
					// 2. Impression des instruments/variables du barrage
					printAllSensorsOfTheActiveDam(barrage);
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'editer les mesures
		// ---------------------------------------------------------------------
		this.mEditorMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				// 1. Lecture r�f�rence au barrage s�lectionn�
				Barrage barrage = getBarrage();

				if (barrage != null) {
					// 2. Cr�ation editeur des mesures
					MeasureEditor editor = new MeasureEditor(barrage);

					// 3. Visualisation de l'editeur
					editor.setModal(true);
					editor.pack();
					editor.setVisible(true);
				}
			}

		});
		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'exporter les donn�es sous la
		// forme d'un fichier Excel.
		// ---------------------------------------------------------------------
		this.mExportMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				if (getBarrage() != null) {
					exportCsvFile();
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'analyser un instrument.
		// ---------------------------------------------------------------------
		mAnalyseSensorMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				// 1. Lecture r�f�rence au barrage s�lectionn�
				Barrage barrage = getBarrage();

				if (barrage != null) {
					// 1. COnstruction titre du dialogue
					String title = Survey.getTitle("PANALYSISNAME", barrage
							.getId());

					// 2. Cr�ation dialogue d'ex�cution des calculs d'analyse
					JDialog dialog = new JDialog((Frame) null, title, true);

					// 3. Configuration de la boite de dialogue
					dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
					
					// 4. Construction du panneau d'edition
					CSensorAnalysControl panel = new CSensorAnalysControl(
							barrage);
					
					// 5. Enregistrement du panneau d'edition
					dialog.getContentPane().add(panel);

					// 6. Application des contraintes geom�triques
					dialog.pack();

					// 6 Positionnement du dialogue
					dialog
							.setLocationRelativeTo((Component) lEvent
									.getSource());

					// 7. Visualisation du dialogue
					dialog.setVisible(true);

				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'utiliser les r�sultats entre
		// deux analyses.
		// ---------------------------------------------------------------------
		mUseOfResultsMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				// 1. Lecture r�f�rence au barrage s�lectionn�
				Barrage barrage = getBarrage();

				if (barrage != null) {
					// 1. Cr�ation dialogue de correction des r�sultats
					// d'analyse
					CorrectionAnalyseDialog dialog = new CorrectionAnalyseDialog(
							barrage);

					// 2. R�solution des contraintes de dimensionnement
					dialog.pack();

					// 3. Positionnement du dialogue
					dialog
							.setLocationRelativeTo((Component) lEvent
									.getSource());

					// 4. Visualisation du dialogue
					dialog.setVisible(true);
				}
			}
		});
	}

	/**
	 * Permet de connecter les rubriques du menu "Instruments et variables" �
	 * leurs �couteurs.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 15/12/09
	 */
	private void connectSensorsAndVariablesMenuListeners() {
		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de cr�er un �l�ment.
		// ---------------------------------------------------------------------
		mCreateSensorOrVariableMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				// 1. Lecture r�f�rence au barrage s�lectionn�
				Barrage barrage = getBarrage();

				if (barrage != null) {
					// Affichage du panneau de gestion d'un instrument
					// ou d'une variable en mode "cr�ation".
					openView(CIniFileReader.getString("P2NAME"),
							new SensorsControl(CSurveyControl.this,barrage, null,
									SensorsControl.CREATE_MODE));
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de modifier un �l�ment.
		// ---------------------------------------------------------------------
		mModifySensorOrVariableMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				// 1. Lecture r�f�rence l 'instrument/variable s�lectionn�
				Sensor sensor = getSensor();

				if (sensor != null) {
					openView(CIniFileReader.getString("P2NAME"),
							new SensorsControl(CSurveyControl.this,sensor.getBarrage(), sensor
									.getId(), SensorsControl.MODIFY_MODE));
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de supprimer un �l�ment.
		// ---------------------------------------------------------------------
		mDeleteSensorOrVariableMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				if (Dialogs.showConfirmDialog(CIniFileReader
						.getString("CONFMSG13"), CIniFileReader
						.getString("CONFMSG14")) == JOptionPane.YES_OPTION) {

					// 1. Lecture r�f�rence a l'instrument/variable s�lectionn�
					Sensor sensor = getSensor();

					if (sensor != null) {
						// 1. Suppression de l'instrument/variable
						Survey.getInstance().removeSensor(sensor);
					}
				}
			}
		});
	}

	/**
	 * Fermeture de la vue sur les donn�es
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 14/12/09
	 */
	public void closeView() {
		// 1. Fermeture de la vue
		this.explorer.setCurrentView(null);

		// 2. R�initialisation nom de la vue courante
		mActiveScreen = "";

		// 3. Affichage du titre
		displayTitle();
	}

	/**
	 * Ouverture d'une nouvelle vue
	 * 
	 * @param aName
	 *            Nom de la vue
	 * @param aView
	 *            Vue � afficher
	 */
	public void openView(String aName, BaseView aView) {
		// 1. Actualisation nom de la vue courante
		mActiveScreen = aName;

		// 2. Affichage de la vue
		this.explorer.setCurrentView(aView);

		// 3. Actualisation du titre
		this.displayTitle();
	}

	/**
	 * Permet d'importer les donn�es du barrage actif.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 26/01/10
	 */
	private void importActiveDam() {
		// 1. Cr�ation d'un s�lecteur de fichier
		JFileChooser lChooser = new JFileChooser();

		// 2. Cr�ation r�pertoire d'importation par defaut
		File importDir = new File("db/export");
		
		// 3. Initialisation selecteur de fichier
		lChooser.setCurrentDirectory(importDir);
		
		if (lChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			// 1. Lecture chemin du fichier � importer
			String filePath = lChooser.getSelectedFile().getAbsolutePath();

			// 2. Importation d�finition du barrage
			Barrage barrage = Survey.getInstance().importBarrage(filePath);

			// 3. Lecture identifiant du barrage
			String barrageId = barrage.getId();

			if (Survey.getInstance().getBarrage(barrageId) != null) {
				// 1. Saisie nom � affecter au barrage
				String newBarrageId = JOptionPane
						.showInputDialog(CIniFileReader.getString("INFMSG7"));

				if (newBarrageId != null) {
					// 1. Modification identificateur du barrage
					barrage.setId(newBarrageId);

					// 2. Ajout � la liste des barrages
					Survey.getInstance().addBarrage(barrage, false);

					// 3. Sauvegarde de la base
					Survey.getInstance().applyChanges();

					// 4. Notifier l'utilisateur que l'importation a �t�
					// r�alis�e.
					Dialogs.showInformationDialog(CIniFileReader
							.getString("INFMSG6"));
				}
			} else {
				// 1. Ajout � la liste des barrages
				Survey.getInstance().addBarrage(barrage, false);

				// 2. Sauvegarde de la base
				Survey.getInstance().applyChanges();

				// 2. Notifier l'utilisateur que l'importation a �t� r�alis�e.
				Dialogs.showInformationDialog(CIniFileReader
						.getString("INFMSG6"));
			}
		}
	}

	/**
	 * Permet d'imprimer les informations des instruments du barrage actif.
	 * 
	 * @param aBarrage
	 *            D�finition d'un barrage
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 29/01/10
	 */
	private void printAllSensorsOfTheActiveDam(Barrage aBarrage) {
		try {
			// 1. Initialisation nom du fichier d'impression
			String lFileName = CSurveyControl.PRINT_DIRECTORY
					+ aBarrage.getId() + "_" + "global_summary" + ".txt";

			// 2. Ouverture flux d'�criture dans le fichier d'impression
			PrintWriter lPrintWriter = new PrintWriter(lFileName);

			// 3. Impression nom du barrage
			lPrintWriter.print(CIniFileReader.getString("P1L1") + " "
					+ aBarrage.getId() + "\n\n");

			// 3. Lecture liste des instruments/variables du barrage
			List<Sensor> allSensors = aBarrage.getSensors();

			for (int i = 0; i < allSensors.size(); i++) {
				// 1. Lecture d�finition du ieme imstrument/variable
				Sensor iSensor = allSensors.get(i);

				// 2. Ecriture des caract�ristiques de l'instrument/variable
				lPrintWriter.print("  " + CIniFileReader.getString("P2L1")
						+ " " + iSensor.getId() + "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L2")
						+ " " + iSensor.getInternalCode() + "\n");

				lPrintWriter.print("  " + CIniFileReader.getString("P2L3")
						+ " " + iSensor.getNature() + "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L4")
						+ " " + iSensor.getType() + "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L5")
						+ " " + iSensor.getGroup() + "\n");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L6")
						+ " " + i + "\n\n");

				lPrintWriter.print("  " + CIniFileReader.getString("P2L7")
						+ " " + iSensor.getDescription() + "\n\n");

				lPrintWriter.print("  " + CIniFileReader.getString("P2L8")
						+ " " + iSensor.getState() + "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L9")
						+ " " + iSensor.getUnit() + "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L10")
						+ " " + Survey.format(iSensor.getInstallationDate())
						+ "\n\n");

				lPrintWriter
						.print("  " + CIniFileReader.getString("P2L11") + " "
								+ Survey.format(iSensor.getUpdateDate())
								+ "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L12")
						+ " " + Survey.format(iSensor.getRegressionDate())
						+ "\n");

				lPrintWriter.print("  " + CIniFileReader.getString("P2L13")
						+ " " + Survey.format(iSensor.getRefValue()) + "\n");

				lPrintWriter
						.print("  " + CIniFileReader.getString("P2L14") + " "
								+ Survey.format(iSensor.getMinimumValue())
								+ "\t");
				lPrintWriter
						.print("  " + CIniFileReader.getString("P2L15") + " "
								+ Survey.format(iSensor.getMaximumValue())
								+ "\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L16")
						+ " " + Survey.format(iSensor.getMaximumVariation())
						+ "\n");

				lPrintWriter.print("  " + CIniFileReader.getString("P2L17")
						+ " " + iSensor.getMeasuresCount() + "\n\n");

				lPrintWriter.print("  " + CIniFileReader.getString("P2L29")
						+ "\n");

				lPrintWriter.print("  " + CIniFileReader.getString("P2L19")
						+ " " + Survey.format(iSensor.getV1()) + "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L20")
						+ " " + Survey.format(iSensor.getV2()) + "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L21")
						+ " " + Survey.format(iSensor.getV3()) + "\n");

				lPrintWriter.print("  " + CIniFileReader.getString("P2L22")
						+ " " + Survey.format(iSensor.getV4()) + "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L23")
						+ " " + Survey.format(iSensor.getV5()) + "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L24")
						+ " " + Survey.format(iSensor.getV6()) + "\n");

				lPrintWriter.print("  " + CIniFileReader.getString("P2L25")
						+ " " + Survey.format(iSensor.getV7()) + "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L26")
						+ " " + Survey.format(iSensor.getV8()) + "\t\t");
				lPrintWriter.print("  " + CIniFileReader.getString("P2L27")
						+ " " + Survey.format(iSensor.getV9()) + "\n");

				lPrintWriter.print("  " + CIniFileReader.getString("P2L28")
						+ " " + iSensor.getFormula() + "\n");

				lPrintWriter
						.print("------------------------------------------------------------------------------\n\n");

			}

			lPrintWriter.close();

			Dialogs.showInformationDialog(CIniFileReader.getString("INFMSG10")
					+ lFileName);
		}

		catch (IOException lException2) {
			lException2.printStackTrace();
		}
	}



	/**
	 * Permet d'exporter des donn�es du barrage actif au format CSV.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 03/02/10
	 */
	private void exportCsvFile() {
		openView(CIniFileReader.getString("PCSVEXPORTNAME"),
				new CCsvExportPanelControl(CSurveyControl.this));
	}

	@Override
	public BaseView getBarrageInfoView(Barrage aBarrage) {
		mActiveScreen = CIniFileReader.getString("P1NAME");

		displayTitle();

		return new BarragesControl(this,aBarrage, BarragesControl.INFORMATION_MODE);

	}

	@Override
	public BaseView getSensorInfoView(Sensor aSensor) {
		mActiveScreen = CIniFileReader.getString("P2NAME");

		displayTitle();

		return new SensorsControl(CSurveyControl.this,aSensor.getBarrage(), aSensor.getId(),
				SensorsControl.INFORMATION_MODE);
	}

	@Override
	public BaseView getSensorMeasuresView(SensorMeasures aMeasures) {
		mActiveScreen = CIniFileReader.getString("P2NAME");

		// 1. Cr�ation de la vue
		MeasureGraphPane view = new MeasureGraphPane();

		// 2. Initialisation liste des mesures � visualiser
		view.showMeasures(aMeasures);

		displayTitle();

		// 3. Initialisation de la vue active
		return view;
	}

	@Override
	public BaseView getSensorResultatsView(SensorResultats aResultat) {
		mActiveScreen = CIniFileReader.getString("P2NAME");

		// 1. Cr�ation panneau d'affichage des r�sultats d'analyse
		ResultatsAnalysePanel view = new ResultatsAnalysePanel();

		// 2 . Initialisation du panneau
		view.setResultats(aResultat);

		displayTitle();

		return view;
	}
}
