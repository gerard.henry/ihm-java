package controls;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import db.Barrage;
import db.Sensor;
import db.SensorMeasures;
import db.SensorResultats;
import db.Survey;
import db.SurveyBase;
import db.SurveyChangeEvent;
import db.SurveyListener;

@SuppressWarnings("serial")
public class BaseExplorer extends JPanel implements TreeWillExpandListener,
		ActionListener, TreeSelectionListener, SurveyListener {
	/** Composant repr�sentant une vue vide. */
	public static final Component EMPTY_VIEW = Box
			.createRigidArea(new Dimension(500, 600));

	/** Codification mode d'affichage de la base. */
	private enum Mode {
		INFORMATION, MEASURE, RESULTAT
	}

	/** Composant internes. */
	protected JTree tree;
	protected JRadioButton modeInfo;
	protected JRadioButton modeMeasure;
	protected JRadioButton modeResultat;
	protected JSplitPane splitPane;

	/** Base des barrages/instruments/variables/r�sultats.... */
	protected Survey base;

	/** Mode d'affichage courant. */
	protected Mode mode;

	/** Barrage s�lectionn�. */
	protected Barrage barrage;

	/** Instrument/variable s�lectionn�. */
	protected Sensor sensor;

	/** Fabrique des vues sur les objets de la base. */
	protected BaseViewFactory factory;
	

	/**
	 * Constructeur
	 * 
	 * @param aFactory
	 *            Fabrique des vues sur les objets de la base
	 */
	public BaseExplorer(BaseViewFactory aFactory) {
		// 1. Initialisation des attributs
		this.base = Survey.getInstance();
		this.factory = aFactory;
		this.mode = Mode.INFORMATION;
		this.barrage = null;
		this.sensor = null;
		
		// 2. Construction de l'interface
		this.buildUI();
	}

	/**
	 * Lecture du mode d'affichage courant
	 * 
	 * @return Mode d'affichage courant
	 */
	public Mode getMode() {
		return mode;
	}

	/**
	 * Lecture r�f�rence au barrage s�lectionn�
	 * 
	 * @return Barrage s�lectionn�
	 */
	public Barrage getBarrage() {
		return barrage;
	}

	/**
	 * Lecture r�f�rence � l'instrument/variable s�lectionn�
	 * 
	 * @return Instrument/variable s�lectionn�
	 */
	public Sensor getSensor() {
		return sensor;
	}

	/**
	 * R�initialisation de la s�lection
	 */
	public void clearSelection() {
		this.tree.clearSelection();
	}

	/**
	 * Construction de l'interface
	 */
	protected void buildUI() {
		// 1. Initialisation layout du panneau
		this.setLayout(new BorderLayout());

		// 2. Cr�ation des noeuds de niveau 1 et 2 de la hi�rarchie
		DefaultMutableTreeNode root = this.createNodes();

		// 3. Cr�ation des composants
		this.tree = new JTree(new DefaultTreeModel(root));
		this.splitPane = new JSplitPane();
		this.modeInfo = new JRadioButton("Informations g�n�rales");
		this.modeMeasure = new JRadioButton("Mesures");
		this.modeResultat = new JRadioButton("R�sultats analyse");

		// 4. Cr�ation des composants interm�diaire
		JScrollPane scrollPane = new JScrollPane(this.tree);
		JPanel treePanel = new JPanel(new BorderLayout());
		JPanel modePanel = new JPanel(new GridLayout(3, 1));
		ButtonGroup group = new ButtonGroup();
		modePanel.setBorder(BorderFactory
				.createTitledBorder("Mode d'affichage"));

		// 5. Configuration des composants
		group.add(this.modeInfo);
		group.add(this.modeMeasure);
		group.add(this.modeResultat);
		scrollPane.setMinimumSize(new Dimension(150, 600));
		this.modeInfo.setSelected(true);
		this.tree.getSelectionModel().setSelectionMode(
				TreeSelectionModel.SINGLE_TREE_SELECTION);

		// 5. Installation des composants
		modePanel.add(this.modeInfo);
		modePanel.add(this.modeMeasure);
		modePanel.add(this.modeResultat);
		treePanel.add(modePanel, BorderLayout.NORTH);
		treePanel.add(scrollPane, BorderLayout.CENTER);
		splitPane.setLeftComponent(treePanel);
		splitPane.setRightComponent(EMPTY_VIEW);
		this.add(this.splitPane);

		// 6. Installation des listeners
		this.tree.addTreeWillExpandListener(this);
		this.tree.getSelectionModel().addTreeSelectionListener(this);
		this.modeInfo.addActionListener(this);
		this.modeMeasure.addActionListener(this);
		this.modeResultat.addActionListener(this);
		this.base.addSurveyListener(this);
	}

	@Override
	public void treeWillCollapse(TreeExpansionEvent aEvent)
			throws ExpandVetoException {
		// 1. Lecture chemin du noeud � fermer
		TreePath path = aEvent.getPath();

		// 2. Lecture r�f�rence au noeud s�lectionn�
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) path
				.getLastPathComponent();

		// 3. Lecture mod�le de la hi�rarchie
		DefaultTreeModel model = (DefaultTreeModel) this.tree.getModel();

		switch (path.getPathCount()) {
		case 2:
			// 1. Suppression de tous les noeud fils
			node.removeAllChildren();

			// 2. Cr�ation noeud temporaire
			node.add(new DefaultMutableTreeNode("(vide)"));

			// 3. Actualisation de la hierarchie
			model.reload();
		}
	}

	@Override
	public void treeWillExpand(TreeExpansionEvent aEvent)
			throws ExpandVetoException {
		// 1. Lecture chemin du noeud � d�velopper
		TreePath path = aEvent.getPath();

		// 2. Lecture r�f�rence au noeud s�lectionn�
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) path
				.getLastPathComponent();

		// 3. Lecture mod�le de la hi�rarchie
		DefaultTreeModel model = (DefaultTreeModel) this.tree.getModel();

		switch (path.getPathCount()) {
		case 2:
			// 1. D�veloppement d'un barrage
			String name = (String) node.getUserObject();

			// 2. Lecture d�finition du barrage
			Barrage barrage = this.base.getBarrage(name);

			// 3. Lecture liste des noms d'instruments/ variables
			List<String> names = barrage.getSensorsNames();

			// 4. Suppression noeud temporaire
			node.remove(0);

			if (names.size() != 0) {
				for (int i = 0; i < names.size(); i++) {
					// 1. Lecture nom de l'instrument/variable
					String iname = names.get(i);

					// 2. Cr�ation noued repr�sentant l'instrument/variable
					node.add(new DefaultMutableTreeNode(iname));
				}
			} else {
				// 1. Cr�ation noeud repr�sentant une liste vide
				node.add(new DefaultMutableTreeNode("(vide)"));
			}

			// 5. Actualisation de la hierarchie
			model.reload();
		}
	}

	/**
	 * Cr�ation de la hi�rarchie
	 */
	protected DefaultMutableTreeNode createNodes() {
		// 1. Cr�ation du noeud racine
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Barrages");

		// 2. Lecture liste ordonn�es des noms de barrages
		List<String> names = this.base.getBarragesNames();

		for (int i = 0; i < names.size(); i++) {
			// 1. Cr�ation noeud repr�sentant le ieme barrage
			DefaultMutableTreeNode barrage = new DefaultMutableTreeNode(names
					.get(i));

			// 2. Cr�ation noeud temporaire
			barrage.add(new DefaultMutableTreeNode("(vide)"));

			// 3. Ajout au noeud racine
			root.add(barrage);
		}

		return root;
	}

	@Override
	public void actionPerformed(ActionEvent aEvent) {
		// 1. Actualisation mode d'affichage de la hi�rarchie
		if (this.modeInfo.isSelected()) {
			this.mode = Mode.INFORMATION;
		} else if (this.modeMeasure.isSelected()) {
			this.mode = Mode.MEASURE;
		} else if (this.modeResultat.isSelected()) {
			this.mode = Mode.RESULTAT;
		}

		// 2. Actualisation vue repr�sentant la donn�e s�lectionn�e
		this.updateView();
	}

	@Override
	public void valueChanged(TreeSelectionEvent aEvent) {
		// 1. Lecture chemin de la s�lection
		TreePath path = aEvent.getPath();

		if (path != null && aEvent.isAddedPath()) {
			// 1. Lecture liste des noms de la s�lection
			Object names[] = ((DefaultMutableTreeNode) path
					.getLastPathComponent()).getUserObjectPath();

			switch (path.getPathCount()) {
			case 1:
				// 1. R�initialisation de la s�lection
				this.barrage = null;
				this.sensor = null;

				break;
			case 2:
				// 1. Actualisation de la s�lection
				this.barrage = base.getBarrage((String) names[1]);
				this.sensor = null;

				break;
			case 3:
				// 1. Actualisation de la s�lection
				this.barrage = base.getBarrage((String) names[1]);
				this.sensor = this.barrage.getSensor((String) names[2]);

				break;
			}
		} else {
			// 1. R�initialisation de la s�lection
			this.barrage = null;
			this.sensor = null;
		}

		// 1. Actualisation repr�sentant la donn�e s�lectionn�e
		this.updateView();
	}

	/**
	 * Actualisation de la vue sur la donn�e s�lectionn�e
	 */
	protected void updateView() {
		if (this.sensor != null) {
			if (this.mode == Mode.INFORMATION) {
				// 1. Affichage des informations g�n�rales de
				// l'instrument/variable
				this.showSensorInfo(this.sensor);
			} else if (this.mode == Mode.MEASURE) {
				if (this.sensor.getMeasuresCount() != 0) {
					// 1. Affichage des mesures de l'instrument/variable
					this.showSensorMeasures(this.barrage
							.getSensorMeasures(sensor.getId()));
				} else {
					this.setCurrentView(null);
				}
			} else if (this.mode == Mode.RESULTAT) {
				if (this.sensor.getRegressionDate() != null) {
					// 1. Affichage des mesures de l'instrument/variable
					this.showSensorResultats(this.barrage
							.getSensorResultats(sensor.getId()));
				} else {
					this.setCurrentView(null);
				}
			}
		} else if (this.barrage != null) {
			// 1. Affichage des informations g�n�rales du barrage
			this.showBarrageInfo(this.barrage);
		} else {
			this.setCurrentView(null);
		}
	}

	/**
	 * Affichage vue d'edition des informations g�n�rales d'un barrage
	 * 
	 * @param aBarrage
	 *            caract�ristiques d'un barrage
	 */
	protected void showBarrageInfo(Barrage aBarrage) {
		this.setCurrentView(factory.getBarrageInfoView(aBarrage));
	}

	/**
	 * Affichage vue d'edition des r�sultats d'analyse d'un instrument/variable
	 * 
	 * @param aResultat
	 *            R�sultats d'une analyse
	 */
	protected void showSensorResultats(SensorResultats aResultat) {
		this.setCurrentView(factory.getSensorResultatsView(aResultat));
	}

	/**
	 * Affichage vue d'edition des mesures d'un instrument/variable
	 * 
	 * @param aMeasures
	 *            Mesures d'un instrument/variable
	 */
	protected void showSensorMeasures(SensorMeasures aMeasures) {
		// 3. Initialisation de la vue active
		this.setCurrentView(factory.getSensorMeasuresView(aMeasures));
	}

	/**
	 * Affichage vue d'edition des informations g�n�rales d'un
	 * instrument/variable
	 * 
	 * @param aSensor
	 *            caract�ristiques d'un instrument/variable
	 */
	protected void showSensorInfo(Sensor aSensor) {
		this.setCurrentView(factory.getSensorInfoView(aSensor));
	}

	/**
	 * Affichage d'une vue particuli�re
	 * 
	 * @param aView
	 *            La vue � afficher
	 */
	public void setCurrentView(JPanel aView) {
		if (aView == null) {
			// 1. Affichage de la vue par defaut
			this.splitPane.setRightComponent(EMPTY_VIEW);
		} else {
			// 1. Installation de la vue
			this.splitPane.setRightComponent(aView);
		}
	}
 
	@Override
	public void baseChanged(SurveyChangeEvent aEvent) {
		// 1. Initialisation drapeau de modification 
		boolean hasChanged = false;
				
		for ( int i = 0 ; i < aEvent.getPendingChangesCount() && !hasChanged ; i++) {
			// 1. Lecture ieme changement
			SurveyChangeEvent.Change change = aEvent.getPendingChangesAt(i);
			
			// 2. Lecture objet modifi� de la base
			Object target = change.getTarget();
			
			if ( target instanceof Barrage || target instanceof Sensor || target instanceof SurveyBase ) {
				hasChanged = true;
			}
		}
		
		if ( hasChanged ) {			
			// 1. Lecture de la s�lection courante
			TreePath selectionPath = this.tree.getSelectionPath();
			
			// 2. Reconstruction modele de la base
			this.tree.setModel(new DefaultTreeModel(this.createNodes()));
			
			// 3. R�initialisation de la s�lection
			this.barrage = null;
			this.sensor = null;
			
			// 4. Actualisation de la s�lection
			this.tree.setSelectionPath(selectionPath);
		}
	}
}
