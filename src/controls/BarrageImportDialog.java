package controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import tools.csv.CSVHandlerAdapter;
import tools.csv.CSVReader;
import db.Barrage;
import db.Sensor;
import db.SensorMeasures;
import db.Survey;

@SuppressWarnings("serial")
public class BarrageImportDialog extends JDialog implements ActionListener,
		DocumentListener {
	/** Enregistreur des traces d'ex�cution. */
	protected  Logger logger = Logger
			.getLogger(BarrageImportDialog.class.getName());

	public class ImportLogHandler extends Handler {

		@Override
		public void close() throws SecurityException {
		}

		@Override
		public void flush() {
		}

		@Override
		public void publish(final LogRecord aRecord) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					// 1. Lecture r�f�rence au document
					StyledDocument document = console.getStyledDocument();

					// 2. Lecture position dans le document
					int start = document.getLength();
					try {
						// 1. Enregistrement du message de trace
						document.insertString(start, aRecord.getMessage()
								+ "\n", document.getStyle(aRecord.getLevel()
								.getName()));
					} catch (BadLocationException e) {
					}
				}
			});
		}
	}

	public class BarrageParser extends CSVHandlerAdapter {
		@Override
		public void fieldsLine(CSVReader reader, String[] fields)
				throws IOException {
			if (reader.getLineNumber() == 2) {
				if (fields.length >= 5) {
					try {
						// 1. Lecture valeurs des champs
						String id = fields[0];
						String title = fields[1];
						double zm = (fields[2].length() != 0 ? Double
								.parseDouble(fields[2].replace(',', '.')) : 0.);
						double zn = (fields[3].length() != 0 ? Double
								.parseDouble(fields[3].replace(',', '.')) : 0.);
						double zh = (fields[4].length() != 0 ? Double
								.parseDouble(fields[4].replace(',', '.')) : 0.);

						// 2. Importation d�finition du barrage
						importBarrageInfo(id, title, zm, zn, zh);
					} catch (NumberFormatException nfex) {
						// 1. Propagation d'une exception
						throw new IOException(
								"Barrage  : Valeur champ incorrecte");
					}
				} else {
					// 1. Propagation d'une exception
					throw new IOException(
							"Barrage : Le nombre de champs est incorrect");
				}
			}
		}

		@Override
		public boolean isEndOfDocument(CSVReader aReader) {
			if (aReader.getLineNumber() == 3) {
				return true;
			} else {
				return false;
			}
		}
	}

	public class SensorParser extends CSVHandlerAdapter {
		/** Drapeau de fin de lecture du bloc de donn�es. */
		protected boolean isLastLine = false;

		@Override
		public void fieldsLine(CSVReader reader, String[] fields)
				throws IOException {
			if ( fields.length == 0 ) {
				// 1. Arret sur la premi�re ligne vide
				isLastLine = true;
				
				return;
			}
			if (fields.length >= 3) {
				try {
					// 1. Importation de l'instrument
					importSensorInfo(fields[0], fields[1],
							(fields[2].length() != 0 ? Double
									.parseDouble(fields[2].replace(',', '.'))
									: 0.));
				} catch (NumberFormatException nfex) {
					// 1. Propagation d'une exception
					throw new IOException(
							"Instrument : Valeur champ incorrecte");
				}
			} else {
				// 1. Propagation d'une exception
				throw new IOException(
						"Instrument : Le nombre de champs est incorrect");
			}
		}

		@Override
		public void emptyLine(CSVReader aReader) throws IOException {
			isLastLine = true;
		}

		@Override
		public boolean isEndOfDocument(CSVReader aReader) {
			return isLastLine;
		}
	}

	public class MeasureParser extends CSVHandlerAdapter {
		/** Drapeau de fin de lecture du bloc de donn�es. */
		protected boolean isLastLine = false;

		/** Buffer des valeurs de mesures. */
		protected Double values[];

		/**
		 * Constructeur
		 */
		public MeasureParser() {
			// 1. Initialisation des attributs
			this.values = new Double[sensors.size()];
		}

		@Override
		public void fieldsLine(CSVReader reader, String[] fields)
				throws IOException {
			if ( fields.length == 0 ) {
				// 1. Arret sur la premi�re ligne vide
				isLastLine = true;
				
				return;
			}
			
			if (fields.length >=  2) {
				try {
					// 1. Lecture date de la mesure
					Date date = Survey.dateFormat.parse(fields[0]);

					// 2. Lecture valeur de la cote en Z
					Double z = (fields[1].length() != 0 ? Double
							.parseDouble(fields[1].replace(',', '.')) : null);

					// 3. Initialisation valeurs des mesures
					for (int i = 0; i < values.length; i++) {
						values[i] = null;
					}
					
					// 4. Lecture des valeurs des mesures
					for (int i = 0; i < Math.min(values.length,fields.length-2); i++) {
						values[i] = (fields[2 + i].length() != 0 ? Double
								.parseDouble(fields[2 + i].replace(',', '.'))
								: null);
					}

					// 4. Importation des mesures
					importMeasures(date, z, values);
				} catch (NumberFormatException nfex) {
					// 1. Propagation d'une exception
					throw new IOException("Mesure : Valeur champ incorrecte");
				} catch (ParseException pex) {
					// 1. Propagation d'une exception
					throw new IOException("Mesure : Date incorrecte");
				}

			} else {
				// 1. Propagation d'une exception
				throw new IOException(
						"Mesure : Le nombre de champs est incorrect");
			}
		}

		@Override
		public void emptyLine(CSVReader aReader) throws IOException {
			isLastLine = true;
		}

		@Override
		public boolean isEndOfDocument(CSVReader aReader) {
			return isLastLine;
		}
	}

	public class RainParser extends CSVHandlerAdapter {
		/** Drapeau de fin de lecture du bloc de donn�es. */
		protected boolean isLastLine = false;

		/**
		 * Constructeur
		 */
		public RainParser() {
		}

		@Override
		public void fieldsLine(CSVReader reader, String[] fields)
				throws IOException {
			if ( fields.length == 0 ) {
				// 1. Arret sur la premi�re ligne vide
				isLastLine = true;
				
				return;
			}
			else if (fields.length >= 1) {
				try {
					// 1. Lecture date de la mesure
					Date date = Survey.dateFormat.parse(fields[0]);

					if ( fields.length == 2) {
					// 1. Lecture valeur de la pluiviom�trie
					Double pluie = (fields[1].length() != 0 ? Double
							.parseDouble(fields[1].replace(',', '.')) : null);

					// 2. Importation de la pluviom�trie
					importRainMeasure(date, pluie);
					} else {
						// 1. Importation de la pluviom�trie
						importRainMeasure(date, null);						
					}
				} catch (NumberFormatException nfex) {
					// 1. Propagation d'une exception
					throw new IOException("Pluie : Valeur champ incorrecte");
				} catch (ParseException pex) {
					// 1. Propagation d'une exception
					throw new IOException("Pluie : Date incorrecte");
				}

			} else {
				// 1. Propagation d'une exception
				throw new IOException(
						"Pluie : Le nombre de champs est incorrect");
			}
		}

		@Override
		public void emptyLine(CSVReader aReader) throws IOException {
			isLastLine = true;
		}

		@Override
		public boolean isEndOfDocument(CSVReader aReader) {
			return isLastLine;
		}
	}

	/** Composants internes. */
	protected JTextField damFilePath;
	protected JButton damFileBrowse;
	protected JTextField rainFilePath;
	protected JButton rainFileBrowse;
	protected JButton apply;
	protected JButton close;
	protected JButton run;
	protected JTextPane console;

	/** R�f�rence au barrage en cours d'importation. */
	protected Barrage barrage;

	/** Mesures des variables Z et PLUIE. */
	protected SensorMeasures z, pluie;

	/** Mesures des instruments import�s. */
	protected List<SensorMeasures> sensors;

	/** Handler d'affichage des traces d'ex�cution. */
	protected ImportLogHandler importHandler;

	/**
	 * Constructeur
	 */
	public BarrageImportDialog() {
		// 1. Initialisation des attributs
		this.importHandler = new ImportLogHandler();

		// 1. Construction de l'interface
		this.buildUI();
	}

	/**
	 * Construction de l'interface
	 */
	protected void buildUI() {
		GridBagConstraints gbc = new GridBagConstraints();

		// 1. Lectur er�f�rence au panneau principal
		JPanel mainPane = (JPanel) this.getContentPane();

		// 2. Cr�ation des composants
		this.damFilePath = new JTextField(32);
		this.rainFilePath = new JTextField(32);
		this.damFileBrowse = new JButton("Parcourir...");
		this.rainFileBrowse = new JButton("Parcourir...");
		this.console = new JTextPane();
		this.apply = new JButton("Enregistrer");
		this.close = new JButton("Fermer");
		this.run = new JButton("Importer");

		// 3. Cr�ation des panneaux interm�diaires
		JPanel selPane = new JPanel(new GridBagLayout());
		JPanel butPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JScrollPane scrPane = new JScrollPane(this.console);

		// 4. Configuration des composants
		console.setEditable(false);
		scrPane.setPreferredSize(new Dimension(400, 200));
		this.installConsoleStyles(this.console.getStyledDocument());
		this.run.setEnabled(false);
		this.apply.setEnabled(false);

		// 5. Installation des composants
		butPane.add(this.run);
		butPane.add(this.apply);
		butPane.add(this.close);

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 3, 10, 3);
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.fill = GridBagConstraints.NONE;
		selPane.add(new JLabel("Fichier duplis"), gbc);
		gbc.gridx = 1;
		gbc.weightx = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		selPane.add(this.damFilePath, gbc);
		gbc.gridx = 2;
		gbc.weightx = 0;
		gbc.fill = GridBagConstraints.NONE;
		selPane.add(this.damFileBrowse, gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.fill = GridBagConstraints.NONE;
		selPane.add(new JLabel("Fichier pluie"), gbc);
		gbc.gridx = 1;
		gbc.weightx = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		selPane.add(this.rainFilePath, gbc);
		gbc.gridx = 2;
		gbc.weightx = 0;
		gbc.fill = GridBagConstraints.NONE;
		selPane.add(this.rainFileBrowse, gbc);

		mainPane.add(selPane, BorderLayout.NORTH);
		mainPane.add(scrPane, BorderLayout.CENTER);
		mainPane.add(butPane, BorderLayout.SOUTH);

		// 6. Installation des listeners
		this.damFileBrowse.addActionListener(this);
		this.rainFileBrowse.addActionListener(this);
		this.damFilePath.getDocument().addDocumentListener(this);
		this.rainFilePath.getDocument().addDocumentListener(this);
		this.apply.addActionListener(this);
		this.close.addActionListener(this);
		this.run.addActionListener(this);
		this.logger.addHandler(importHandler);

		// 7. Configuration du dialogue
		this.setModal(true);
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.setTitle(Survey.getApplicationName() + "- Importation barrage");
	}

	/**
	 * Importation des donn�es en base
	 */
	protected void doImport() {
		// 1. R�initialisation de la console
		this.console.setText("");

		try {
			// 1. Importation du barrage
			this.importBarrage(damFilePath.getText());

			if (rainFilePath.getText().length() != 0) {
				// 1. Importation fichier de pluiviom�trie
				this.importRainFile(rainFilePath.getText());
			}

	
			// 2. Actualisation des nombres de mesures
			this.barrage.updateMeasureCount();
			
			// 3. Actualisation d�finition du barrage
			Survey.getInstance().updateBarrage(this.barrage,false);
			
			// 4. Activation autorisation de sauvegarde
			this.apply.setEnabled(true);
		} catch (IOException ioex) {
			// 1. Enregistrement trace d'ex�cution
			logger.log(Level.SEVERE, ioex.getMessage(), ioex);
		}
	}

	/**
	 * Importation des donn�es de d�finition du barrage
	 * 
	 * @param aFile
	 *            Nom du fichier au format csv
	 */
	protected void importBarrage(String aFile) throws IOException {
		FileInputStream in = null;

		// 1. Initialisation contexte d'importation
		this.barrage = null;
		this.z = null;
		this.pluie = null;
		this.sensors = new ArrayList<SensorMeasures>();

		try {
			// 1. Cr�ation flux de lecture du fichier � importer
			in = new FileInputStream(aFile);

			// 2. Cr�ation d'un lecteur au format CSV
			CSVReader reader = new CSVReader(in);

			// 3. La premi�re ligne est ignor�e
			reader.setSkippedLines(1);

			// 4. Lecture information sur le barrage
			reader.parse(new BarrageParser());

			// 5. La ligne suivante est ignor�e
			reader.setSkippedLines(1);

			// 6. Lecture des informations sur les instruments
			reader.parse(new SensorParser());

			// 7. La ligne suivante est ignor�e
			reader.setSkippedLines(1);

			// 8. Lecture des mesures sur les instruments
			reader.parse(new MeasureParser());
		} finally {
			if (in != null) {
				try {
					// 1. Fermeture du flux de lecture
					in.close();
				} catch (IOException ign) {
				}
			}
		}
	}

	/**
	 * Importation du fichier pluie
	 * 
	 * @param aFile
	 *            Chemin du fichier pluie
	 */
	protected void importRainFile(String aFile) throws IOException {
		FileInputStream in = null;

		try {
			// 1. Cr�ation flux de lecture du fichier � importer
			in = new FileInputStream(aFile);

			// 2. Cr�ation d'un lecteur au format CSV
			CSVReader reader = new CSVReader(in);

			// 3. Lecture mesures de la pluviom�trie
			reader.parse(new RainParser());

		} finally {
			if (in != null) {
				try {
					// 1. Fermeture du flux de lecture
					in.close();
				} catch (IOException ign) {
				}
			}
		}
	}

	/**
	 * Importation du barrage
	 * 
	 * @param aId
	 *            Identificateur du barrage
	 * @param aTitle
	 *            Titre du barrage
	 * @param aZm
	 *            Cote minimale du barrage
	 * @param aZn
	 *            Cote nominale du barrage
	 * @param @Zh Cote maximale du barrage
	 */
	protected void importBarrageInfo(String aId, String aTitle, double aZm,
			double aZn, double aZh) {
		// 1. Recherche d�finition du barrage
		this.barrage = Survey.getInstance().getBarrage(aId);

		// 2. Lecture de la date de cr�ation
		Date now = new Date();

		if (this.barrage == null) {
			// 1. Cr�ation d'un nouveau barrage
			this.barrage = new Barrage();

			// 2. Initialisation des caract�ristiques du barrage
			this.barrage.setId(aId);
			this.barrage.setTitle(aTitle);
			this.barrage.setWaterMinLevel(aZm);
			this.barrage.setWaterNominalLevel(aZn);
			this.barrage.setWaterMaxLevel(aZh);
			this.barrage.setUpdateDate(now);
			this.barrage.setLaunchingDate(now);

			// 3. Enregistrement trace d'ex�cution
			logger.log(Level.INFO, "Cr�ation barrage <" + aId + "> " + aTitle);
			logger.log(Level.INFO, "... Cote minimale " + Survey.format(aZm));
			logger.log(Level.INFO, "... Cote nominale " + Survey.format(aZn));
			logger.log(Level.INFO, "... Cote maximale " + Survey.format(aZh));

			// 4. Enregistrement d�finition du barrage
			Survey.getInstance().addBarrage(this.barrage, false);
		}

		// 2. Recherche d�finition de la variable Z
		Sensor sensor = this.barrage.getSensor("Z");

		if (sensor == null) {
			// 1. Cr�ation de la variable Z
			sensor = new Sensor();

			// 2. Initialisation des caract�ristiques de la variable
			sensor.setBarrage(this.barrage);
			sensor.setId("Z");
			sensor.setTitle("Cote de l'eau");
			sensor.setNature("V");
			sensor.setState("ON");
			sensor.setUpdateDate(now);

			// 3. Enregistrement trace d'ex�cution
			logger.log(Level.INFO, "... Cr�ation variable <" + sensor.getId()
					+ "> " + sensor.getTitle());

			// 4. Enregistrement d�finition de la variable
			Survey.getInstance().addSensor(sensor, false);
		}

		// 3. Lecture liste des mesures de la cote Z
		this.z = sensor.getMeasures();

		if (this.z == null) {
			// 1. Cr�ation mesures de la cote
			this.z = new SensorMeasures(sensor);
		}
		
		// 4. Enregistrement des mesures de la variable
		Survey.getInstance().updateSensorMeasures(this.z, false);

		if (this.rainFilePath.getText().length() != 0) {
			// 1. Recherche d�finition de la variable pluie
			sensor = this.barrage.getSensor("PLUIE");

			if (sensor == null) {
				// 1. Cr�ation de la variable PLUIE
				sensor = new Sensor();

				// 2. Initialisation des caract�ristiques de la variable
				sensor.setBarrage(this.barrage);
				sensor.setId("PLUIE");
				sensor.setTitle("Pluie journali�re");
				sensor.setNature("V");
				sensor.setState("ON");
				sensor.setUpdateDate(now);

				// 3. Enregistrement trace d'ex�cution
				logger.log(Level.INFO, "... Cr�ation variable <"
						+ sensor.getId() + "> " + sensor.getTitle());

				// 4. Enregistrement d�finition de la variable
				Survey.getInstance().addSensor(sensor, false);
			}

			// 1. Lecture liste des mesures de la pluie
			this.pluie = sensor.getMeasures();

			if (this.pluie == null) {
				// 1. Cr�ation mesures de la pluie
				this.pluie = new SensorMeasures(sensor);
			}
			
			// 2. Enregistrement des mesures de la variable
			Survey.getInstance().updateSensorMeasures(this.pluie, false);
		}
	}

	/**
	 * Importation d'un instrument
	 * 
	 * @param aId
	 *            Identificateur de l'instrument
	 * @param aTitle
	 *            Titre de l'instrument
	 * @param aVref
	 *            Valeur de r�f�rence de l'instrument
	 */
	protected void importSensorInfo(String aId, String aTitle, double aVref) {
		// 1. Recherche d�finition de l'instrument
		Sensor sensor = this.barrage.getSensor(aId);

		if (sensor == null) {
			// 1. Cr�ation de l'instrument
			sensor = new Sensor();

			// 2. Initialisation des caract�ristiques de la variable
			sensor.setBarrage(this.barrage);
			sensor.setId(aId);
			sensor.setTitle(aTitle);
			sensor.setNature("I");
			sensor.setState("OO");
			sensor.setUpdateDate(new Date());
			sensor.setRefValue(aVref);

			// 3. Enregistrement trace d'ex�cution
			logger.log(Level.INFO, "... Cr�ation instrument <" + sensor.getId()
					+ "> " + sensor.getTitle());

			// 4. Enregistrement d�finition de la variable
			Survey.getInstance().addSensor(sensor, false);
		}

		// 2. Lecture liste des mesures de l'instrument
		SensorMeasures measures = sensor.getMeasures();

		if (measures == null) {
			// 1. Cr�ation liste des mesures de l'instrument
			measures = new SensorMeasures(sensor);
		}
		
		// 3. Enregistrement des mesures de l'instrument
		Survey.getInstance().updateSensorMeasures(measures, false);
		

		// 4. Actualisation liste des mesures � importer
		this.sensors.add(sensor.getMeasures());
	}

	/**
	 * Importation des mesures
	 * 
	 * @param aDate
	 *            Date de la mesure
	 * @param aZ
	 *            Valeur de la cote
	 * @param aValues
	 *            Valeurs des mesures
	 */
	protected void importMeasures(Date aDate, Double aZ, Double aValues[]) {
		// 1. Enregistrement mesure de la cote
		if (aZ != null) {
			if (this.z.hasMeasure(aDate)) {
				// 1. Enregistrement trace d'ex�cution
				logger.log(Level.WARNING, "Z mesure (" + Survey.format(aDate)
						+ "," + Survey.format(aZ) + ") ignor�e");
			} else {
				// 1. Enregistrement valeur de la mesure
				this.z.addMeasure(aDate, aZ);
			}
		}

		// 2. Enregistrement mesure des instruments
		for (int i = 0; i < aValues.length; i++) {
			if (aValues[i] != null) {
				// 1. Lecture liste des mesures de l'instrument
				SensorMeasures measures = this.sensors.get(i);

				if (!measures.hasMeasure(aDate)) {
					// 1. Calcul valeur de la mesure � partir de la valeur brute relev�e
					Double mesValue = measures.getComputedMeasure(aValues[i]);
					
					// 2. Enregistrement valeur de la mesure
					measures.addMeasure(aDate, mesValue);
					
					// 3. Enregistrement de la valeur brute relev�e
					measures.setRawValue(aDate, aValues[i]);
				} else {
					// 1. Enregistrement trace d'ex�cution
					logger.log(Level.WARNING, measures.getSensor().getId()
							+ " mesure (" + Survey.format(aDate) + ","
							+ Survey.format(aValues[i]) + ") ignor�e");
				}
			}
		}
	}

	/**
	 * Importation des mesures
	 * 
	 * @param aDate
	 *            Date de la mesure
	 * @param aRain
	 *            Valeur de la pluie
	 */
	protected void importRainMeasure(Date aDate, Double aRain) {
		if (aRain != null) {
			if (this.pluie.hasMeasure(aDate)) {
				// 1. Enregistrement trace d'ex�cution
				logger.log(Level.WARNING, "Pluie mesure (" + Survey.format(aDate)
						+ "," + Survey.format(aRain) + ") ignor�e");
			} else {
				// 1. Enregistrement valeur de la mesure
				this.pluie.addMeasure(aDate, aRain);
			}
		} else {
			// 1. Enregistrement trace d'ex�cution
			logger.log(Level.WARNING, "Pluie mesure (" + Survey.format(aDate)
					+ ") absente");			
		}
	}
	
	/**
	 * Installation des styles de textes de la console
	 * 
	 * @param aDocument
	 *            Document propri�taire des styles
	 */
	private void installConsoleStyles(StyledDocument aDocument) {
		// 1. Lecture r�f�rence au style par d�faut
		Style defStyle = StyleContext.getDefaultStyleContext().getStyle(
				StyleContext.DEFAULT_STYLE);

		// 2. Cr�ation styles d'afichage des info/erreurs/warnings
		Style infoStyle = aDocument.addStyle(Level.INFO.getName(), defStyle);
		Style errorStyle = aDocument
				.addStyle(Level.SEVERE.getName(), infoStyle);
		Style warningStyle = aDocument.addStyle(Level.WARNING.getName(),
				infoStyle);

		// 3. Configuration des styles
		StyleConstants.setFontFamily(infoStyle, "SansSerif");
		StyleConstants.setBackground(errorStyle, Color.red);

		StyleConstants.setBackground(warningStyle, Color.orange);
		StyleConstants.setItalic(warningStyle, false);
	}

	/**
	 * Controle de validite des champs de saisie des noms de fichiers
	 */
	protected void checkFields() {
		// 1. Initialisation etat des boutons
		boolean isEnabled = false;

		if (this.damFilePath.getText().length() == 0) {
			// 1. Actualisation etat des boutons
			isEnabled = false;
		} else {
			// 1 . Construction r�f�rence aux fichiers
			File damFile = new File(this.damFilePath.getText());

			// 2. Actualisation etat des boutons
			isEnabled = damFile.isFile() && damFile.canRead();
		}

		if (isEnabled && this.rainFilePath.getText().length() != 0) {
			// 1 . Construction r�f�rence aux fichiers
			File rainFile = new File(this.rainFilePath.getText());

			// 2. Actualisation etat des boutons
			isEnabled = rainFile.isFile() && rainFile.canRead();
		}

		// 2.Actualisation etat des composants
		this.run.setEnabled(isEnabled);
	}

	@Override
	public void actionPerformed(ActionEvent aEvent) {
		if (aEvent.getSource() == this.close) {
			if (this.apply.isEnabled()) {
				// 1. Demande de confirmation de sortie sans sauvegarde
				int choice = JOptionPane.showConfirmDialog(this,
						"D�sirez-vous enregistrer les modifications ?", Survey
								.getApplicationName(),
						JOptionPane.YES_NO_OPTION);

				if (choice == JOptionPane.YES_OPTION) {
					// 1. Enregistrement des modifications
					Survey.getInstance().applyChanges();
				}
			}

			// 1 . Fermeture du dialogue
			this.setVisible(false);

			// 2. Liberation des ressources
			this.dispose();

			// 3. Lib�ration des caches
			Survey.getInstance().cancelChanges();
		} else if (aEvent.getSource() == this.run) {
			// 1. Importation des fichiers de donn�es
			this.doImport();
			
			// 2. Actualisation etat du bouton
			this.run.setEnabled(false);
		} else if (aEvent.getSource() == this.apply) {
			// 1. Enregistrement des modifications
			Survey.getInstance().applyChanges();

			// 3. Actualisation etat du bouton
			this.apply.setEnabled(false);
		} else if (aEvent.getSource() == this.damFileBrowse) {
			// 1. Ouverture d'un s�lecteur de fichier
			JFileChooser chooser = new JFileChooser();

			// 2. Initialisation fichier s�lectionn�
			chooser.setSelectedFile(new File(this.damFilePath.getText()));

			// 3. Ouverture du s�lecteur de fichier
			if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
				// 1. Actualisation nom du fichier
				this.damFilePath.setText(chooser.getSelectedFile()
						.getAbsolutePath());
			}
		} else if (aEvent.getSource() == this.rainFileBrowse) {
			// 1. Ouverture d'un s�lecteur de fichier
			JFileChooser chooser = new JFileChooser();

			// 2. Initialisation fichier s�lectionn�
			chooser.setSelectedFile(new File(this.rainFilePath.getText()));

			// 3. Ouverture du s�lecteur de fichier
			if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
				// 1. Actualisation nom du fichier
				this.rainFilePath.setText(chooser.getSelectedFile()
						.getAbsolutePath());
			}
		}
	}

	@Override
	public void changedUpdate(DocumentEvent aEvent) {
		this.checkFields();
	}

	@Override
	public void insertUpdate(DocumentEvent aEvent) {
		this.checkFields();
	}

	@Override
	public void removeUpdate(DocumentEvent aEvent) {
		this.checkFields();
	}
}
