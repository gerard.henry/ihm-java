package controls;

import db.Barrage;
import db.Sensor;
import db.SensorMeasures;
import db.SensorResultats;

public interface BaseViewFactory {
	/**
	 * Construction vue d'edition des informations générales d'un barrage
	 * 
	 * @param aBarrage
	 *            caractéristiques d'un barrage
	 */
	public BaseView getBarrageInfoView(Barrage aBarrage);

	/**
	 * Construction vue d'edition des résultats d'analyse d'un instrument/variable
	 * 
	 * @param aResultat
	 *            Résultats d'une analyse
	 */
	public BaseView getSensorResultatsView(SensorResultats aResultat);

	/**
	 * Construction vue d'edition des mesures d'un instrument/variable
	 * 
	 * @param aMeasures
	 *            Mesures d'un instrument/variable
	 */
	public BaseView getSensorMeasuresView(SensorMeasures aMeasures);
	
	/**
	 * Construction vue d'edition des informations générales d'un
	 * instrument/variable
	 * 
	 * @param aSensor
	 *            caractéristiques d'un instrument/variable
	 */
	public BaseView getSensorInfoView(Sensor aSensor);

}
