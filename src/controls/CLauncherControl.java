package controls;

import gui.CLauncherGui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import tools.CIniFileReader;


/**
 * Contr�les de la fen�tre de lancement de l'application.
 * 
 * @author  :  soci�t� CS SI
 * @version : 1.0 du 14/12/09
 */
public class CLauncherControl extends CLauncherGui
{
	private static final long serialVersionUID = 1L;
	
	// Messages d'erreurs relatifs au passage d'arguments sur la ligne de
	// commandes.
	private static final String ERRMSG1 = "Error : no argument found in the command line.";
	private static final String ERRMSG2 = "Call  : java CSurveyLauncher [language configuration file]";

	
/**
 * Point d'entr�e de l'application.
 * 
 * @author  :  soci�t� CS SI
 * @version : 1.0 du 09/11/09
 * 
 * @param pArgs : liste des arguments pass�s sur la ligne de commande.
 */
	public static void main(String[] pArgs)
	{
		// 1. Lecture chemin du fichier des messages
		String msgFile = System.getProperty("survey.messages");
	
		if (msgFile != null)
		{
			CIniFileReader.setIniFileName(msgFile);
		}
		else
		{
			System.err.println(ERRMSG1);
			System.err.println(ERRMSG2);
			System.exit(-1);
		}
		
		// Lancement de l'application.
		SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				CLauncherControl lLauncher = new CLauncherControl();

				lLauncher.setLocationRelativeTo(null);
				lLauncher.setVisible(true);
			}
		});
	}
	
/**
 * Constructeur sans argument.
 * 
 * @author  :  soci�t� CS SI
 * @version : 1.0 du 14/11/09
 */
	protected CLauncherControl()
	{
		super();
	    
	    connectListeners();
    }
		
/**
 * Permet de connecter les composants � leurs �couteurs.
 * 
 * @author  :  soci�t� CS SI
 * @version : 1.0 du 14/12/09
 */
	private void connectListeners()
	{
	    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

	    //---------------------------------------------------------------------
	    // Connexion de l'�couteur permettant de fermer la fen�tre de
		// lancement.
	    //---------------------------------------------------------------------
		mOkButton.addActionListener(new ActionListener()
		    {
				public void actionPerformed(ActionEvent lEvent)
				{				
				    CLauncherControl.this.setVisible(false);
				    				    
					SwingUtilities.invokeLater(new Runnable()
					{
						public void run()
						{
							CSurveyControl lApplication = new CSurveyControl();
							
							lApplication.setLocationRelativeTo(null);
							lApplication.setVisible(true);
						}
					});
		        }
		    }
		);
	}
}