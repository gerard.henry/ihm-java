package controls.panels;

import gui.panels.SensorsGui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;

import javax.swing.JOptionPane;

import tools.CIniFileReader;
import controls.CSurveyControl;
import cs.sgl.SGL;
import cs.ui.Dialogs;
import db.Barrage;
import db.Sensor;
import db.Survey;

/**
 * Contr�les du panneau de gestion des senseurs et variables.
 * 
 * @author : soci�t� CS SI
 * @version : 1.0 du 17/12/09
 */
public class SensorsControl extends
		SensorsGui implements KeyListener {
	private static final long serialVersionUID = 1L;

	// Constantes permettant de sp�cifier le mode d'utilisation du panneau.
	public static final int CREATE_MODE = 0;
	public static final int MODIFY_MODE = 1;
	public static final int INFORMATION_MODE = 2;

	// Mode d'utilisation du panneau :
	private int mMode;

	// Flag permettant de savoir si l'instrument/variable a �t� modifi� ou pas.
	private boolean mSensorOrVariableUpdated;

	/** R�f�rence au barrage actif. */
	protected Barrage barrage;
	
	/** Identificateur de l'instrument/variable edit�. */
	protected String sensorId;
	
	protected CSurveyControl mParent;
	
	/**
	 * Constructeur.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 17/12/09
	 * 
	 * @param aBarrage Le barrage propri�taire des instruments/variables
	 * @param aSensorId Identificateur de l'instrument/variable � editer
	 * @param pMode
	 *            le mode de fonctionnement du panneau.
	 */
	public SensorsControl(CSurveyControl aParent,Barrage aBarrage,String aSensorId,
			int pMode) {
		super();

		// 1. Initialisation des attributs
		this.barrage = aBarrage;
		this.sensorId = aSensorId;
		mMode = pMode;
		mSensorOrVariableUpdated = false;
		mParent = aParent;
		
		connectListeners();

		// Configuration du panneau selon le mode de fonctionnement sp�cifi�.
		switch (pMode) {
		case INFORMATION_MODE:
			loadSensorOrVariableFeatures();

			mNatureTextField.setEnabled(false);
			mTypeTextField.setEnabled(false);
			mGroupTextField.setEnabled(false);
			mIdTextField.setEnabled(false);
			mDescriptionTextArea.setEnabled(false);
			mStateTextField.setEnabled(false);
			mUnitTextField.setEnabled(false);
			mInstallationDateTextField.setEnabled(false);
			mModificationDateTextField.setEnabled(false);
			mRegressionDateTextField.setEnabled(false);
			mReferenceValueTextField.setEnabled(false);
			mMinimumValueTextField.setEnabled(false);
			mMaximumValueTextField.setEnabled(false);
			mMaximumVariationTextField.setEnabled(false);
			mMeasuresCountTextField.setEnabled(false);
			mV1TextField.setEnabled(false);
			mV2TextField.setEnabled(false);
			mV3TextField.setEnabled(false);
			mV4TextField.setEnabled(false);
			mV5TextField.setEnabled(false);
			mV6TextField.setEnabled(false);
			mV7TextField.setEnabled(false);
			mV8TextField.setEnabled(false);
			mV9TextField.setEnabled(false);
			mFormulaTextField.setEnabled(false);

			mModifyButton.setEnabled(true);
			mAddButton.setEnabled(true);
			mSaveButton.setEnabled(false);
			mDeleteButton.setEnabled(false);
			mCancelButton.setEnabled(false);
			mSearchButton.setEnabled(true);
			mPrintButton.setEnabled(true);
			mCloseButton.setEnabled(true);
			break;

		case CREATE_MODE:
			mNatureTextField.setEnabled(true);
			mTypeTextField.setEnabled(true);
			mGroupTextField.setEnabled(true);
			mIdTextField.setEnabled(true);
			mDescriptionTextArea.setEnabled(true);
			mStateTextField.setEnabled(true);
			mUnitTextField.setEnabled(true);
			mInstallationDateTextField.setEnabled(true);
			mModificationDateTextField.setEnabled(false);
			mRegressionDateTextField.setEnabled(false);
			mReferenceValueTextField.setEnabled(true);
			mMinimumValueTextField.setEnabled(true);
			mMaximumValueTextField.setEnabled(true);
			mMaximumVariationTextField.setEnabled(true);
			mMeasuresCountTextField.setEnabled(false);
			mV1TextField.setEnabled(true);
			mV2TextField.setEnabled(true);
			mV3TextField.setEnabled(true);
			mV4TextField.setEnabled(true);
			mV5TextField.setEnabled(true);
			mV6TextField.setEnabled(true);
			mV7TextField.setEnabled(true);
			mV8TextField.setEnabled(true);
			mV9TextField.setEnabled(true);
			mFormulaTextField.setEnabled(true);

			mInstallationDateTextField.setValue(new Date());
			mStateTextField.setText("NN");
			mMeasuresCountTextField.setValue(0);
			
			mModifyButton.setEnabled(false);
			mAddButton.setEnabled(false);
			mSaveButton.setEnabled(false);
			mDeleteButton.setEnabled(false);
			mCancelButton.setEnabled(false);
			mSearchButton.setEnabled(false);
			mPrintButton.setEnabled(false);
			mCloseButton.setEnabled(true);
			break;

		case MODIFY_MODE:
			loadSensorOrVariableFeatures();

			mNatureTextField.setEnabled(false);
			mTypeTextField.setEnabled(false);
			mGroupTextField.setEnabled(true);
			mIdTextField.setEnabled(false);
			mDescriptionTextArea.setEnabled(true);
			mStateTextField.setEnabled(true);
			mUnitTextField.setEnabled(true);
			mInstallationDateTextField.setEnabled(true);
			mModificationDateTextField.setEnabled(false);
			mRegressionDateTextField.setEnabled(false);
			mReferenceValueTextField.setEnabled(true);
			mMinimumValueTextField.setEnabled(true);
			mMaximumValueTextField.setEnabled(true);
			mMaximumVariationTextField.setEnabled(true);
			mMeasuresCountTextField.setEnabled(false);
			mV1TextField.setEnabled(true);
			mV2TextField.setEnabled(true);
			mV3TextField.setEnabled(true);
			mV4TextField.setEnabled(true);
			mV5TextField.setEnabled(true);
			mV6TextField.setEnabled(true);
			mV7TextField.setEnabled(true);
			mV8TextField.setEnabled(true);
			mV9TextField.setEnabled(true);
			mFormulaTextField.setEnabled(true);

			mModifyButton.setEnabled(false);
			mAddButton.setEnabled(true);
			mSaveButton.setEnabled(true);
			mDeleteButton.setEnabled(true);
			mCancelButton.setEnabled(true);
			mSearchButton.setEnabled(false);
			mPrintButton.setEnabled(true);
			mCloseButton.setEnabled(true);
			break;
		}
	}

	/**
	 * Permet de connecter les composants � leurs �couteurs.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 17/12/09
	 */
	private void connectListeners() {
		// ---------------------------------------------------------------------
		// Connexion des �couteurs permettant de modifier le flag permettant de
		// savoir si le barrage a �t� sauv� ou pas.
		// ---------------------------------------------------------------------
		mNatureTextField.addKeyListener(this);
		mTypeTextField.addKeyListener(this);
		mGroupTextField.addKeyListener(this);
		mIdTextField.addKeyListener(this);
		mDescriptionTextArea.addKeyListener(this);
		mStateTextField.addKeyListener(this);
		mUnitTextField.addKeyListener(this);
		mInstallationDateTextField.addKeyListener(this);
		mModificationDateTextField.addKeyListener(this);
		mRegressionDateTextField.addKeyListener(this);
		mReferenceValueTextField.addKeyListener(this);
		mMinimumValueTextField.addKeyListener(this);
		mMaximumValueTextField.addKeyListener(this);
		mMaximumVariationTextField.addKeyListener(this);
		mMeasuresCountTextField.addKeyListener(this);
		mV1TextField.addKeyListener(this);
		mV2TextField.addKeyListener(this);
		mV3TextField.addKeyListener(this);
		mV4TextField.addKeyListener(this);
		mV5TextField.addKeyListener(this);
		mV6TextField.addKeyListener(this);
		mV7TextField.addKeyListener(this);
		mV8TextField.addKeyListener(this);
		mV9TextField.addKeyListener(this);
		mFormulaTextField.addKeyListener(this);

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de modifier l'instrument ou la
		// variable.
		// ---------------------------------------------------------------------
		mModifyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				mMode = MODIFY_MODE;

				mNatureTextField.setEnabled(false);
				mTypeTextField.setEnabled(true);
				mGroupTextField.setEnabled(true);
				mIdTextField.setEnabled(false);
				mDescriptionTextArea.setEnabled(true);
				mStateTextField.setEnabled(true);
				mUnitTextField.setEnabled(true);
				mInstallationDateTextField.setEnabled(true);
				mModificationDateTextField.setEnabled(false);
				mRegressionDateTextField.setEnabled(false);
				mReferenceValueTextField.setEnabled(true);
				mMinimumValueTextField.setEnabled(true);
				mMaximumValueTextField.setEnabled(true);
				mMaximumVariationTextField.setEnabled(true);
				mMeasuresCountTextField.setEnabled(false);
				mV1TextField.setEnabled(true);
				mV2TextField.setEnabled(true);
				mV3TextField.setEnabled(true);
				mV4TextField.setEnabled(true);
				mV5TextField.setEnabled(true);
				mV6TextField.setEnabled(true);
				mV7TextField.setEnabled(true);
				mV8TextField.setEnabled(true);
				mV9TextField.setEnabled(true);
				mFormulaTextField.setEnabled(true);

				mModifyButton.setEnabled(false);
				mAddButton.setEnabled(true);
				mSaveButton.setEnabled(true);
				mDeleteButton.setEnabled(true);
				mCancelButton.setEnabled(true);
				mSearchButton.setEnabled(false);
				mPrintButton.setEnabled(true);
				mCloseButton.setEnabled(true);
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de rajouter un instrument ou une
		// variable.
		// ---------------------------------------------------------------------
		mAddButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				mMode = CREATE_MODE;

				// -------------------------------------------------
				// Nettoyage de l'IHM.
				// -------------------------------------------------
				mNatureTextField.setText("");
				mTypeTextField.setText("");
				mGroupTextField.setText("");
				mIdTextField.setText("");
				mDescriptionTextArea.setText("");
				mStateTextField.setText("");
				mUnitTextField.setText("");
				mInstallationDateTextField.setValue(new Date());
				mModificationDateTextField.setValue(null);
				mRegressionDateTextField.setValue(null);
				mReferenceValueTextField.setValue(0.0);
				mMinimumValueTextField.setValue(0.0);
				mMaximumValueTextField.setValue(0.0);
				mMaximumVariationTextField.setValue(0.0);
				mMeasuresCountTextField.setValue(0);
				mV1TextField.setValue(0.);
				mV2TextField.setValue(0.);
				mV3TextField.setValue(0.);
				mV4TextField.setValue(0.);
				mV5TextField.setValue(0.);
				mV6TextField.setValue(0.);
				mV7TextField.setValue(0.);
				mV8TextField.setValue(0.);
				mV9TextField.setValue(0.);
				mFormulaTextField.setText("");

				// -------------------------------------------------
				// Activation/d�sactivation des widgets.
				// -------------------------------------------------
				mNatureTextField.setEnabled(true);
				mTypeTextField.setEnabled(true);
				mGroupTextField.setEnabled(true);
				mIdTextField.setEnabled(true);
				mDescriptionTextArea.setEnabled(true);
				mStateTextField.setEnabled(true);
				mUnitTextField.setEnabled(true);
				mInstallationDateTextField.setEnabled(true);
				mModificationDateTextField.setEnabled(false);
				mRegressionDateTextField.setEnabled(false);
				mReferenceValueTextField.setEnabled(true);
				mMinimumValueTextField.setEnabled(true);
				mMaximumValueTextField.setEnabled(true);
				mMaximumVariationTextField.setEnabled(true);
				mMeasuresCountTextField.setEnabled(false);
				mV1TextField.setEnabled(true);
				mV2TextField.setEnabled(true);
				mV3TextField.setEnabled(true);
				mV4TextField.setEnabled(true);
				mV5TextField.setEnabled(true);
				mV6TextField.setEnabled(true);
				mV7TextField.setEnabled(true);
				mV8TextField.setEnabled(true);
				mV9TextField.setEnabled(true);
				mFormulaTextField.setEnabled(true);

				mModifyButton.setEnabled(false);
				mAddButton.setEnabled(false);
				mSaveButton.setEnabled(false);
				mDeleteButton.setEnabled(false);
				mCancelButton.setEnabled(false);
				mSearchButton.setEnabled(false);
				mPrintButton.setEnabled(true);
				mCloseButton.setEnabled(true);
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de sauvegarder un instrument ou
		// une variable.
		// ---------------------------------------------------------------------
		mSaveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				switch (mMode) {
				case CREATE_MODE:
					mSensorOrVariableUpdated = false;

					if (checkFieldValues() == true) {
						saveSensorOrVariable();

						mNatureTextField.setEnabled(false);
						mTypeTextField.setEnabled(false);
						mGroupTextField.setEnabled(false);
						mIdTextField.setEnabled(false);
						mDescriptionTextArea.setEnabled(false);
						mStateTextField.setEnabled(false);
						mUnitTextField.setEnabled(false);
						mInstallationDateTextField.setEnabled(false);
						mModificationDateTextField.setEnabled(false);
						mRegressionDateTextField.setEnabled(false);
						mReferenceValueTextField.setEnabled(false);
						mMinimumValueTextField.setEnabled(false);
						mMaximumValueTextField.setEnabled(false);
						mMaximumVariationTextField.setEnabled(false);
						mMeasuresCountTextField.setEnabled(false);
						mV1TextField.setEnabled(false);
						mV2TextField.setEnabled(false);
						mV3TextField.setEnabled(false);
						mV4TextField.setEnabled(false);
						mV5TextField.setEnabled(false);
						mV6TextField.setEnabled(false);
						mV7TextField.setEnabled(false);
						mV8TextField.setEnabled(false);
						mV9TextField.setEnabled(false);
						mFormulaTextField.setEnabled(false);

						mModifyButton.setEnabled(true);
						mSaveButton.setEnabled(false);
						mDeleteButton.setEnabled(true);
						mCancelButton.setEnabled(false);
						mPrintButton.setEnabled(true);
					} 
					break;

				case MODIFY_MODE:
					mSensorOrVariableUpdated = false;

					if (checkFieldValues() == true) {
						updateSensorOrVariable();

						mNatureTextField.setEnabled(false);
						mTypeTextField.setEnabled(false);
						mGroupTextField.setEnabled(false);
						mIdTextField.setEnabled(false);
						mDescriptionTextArea.setEnabled(false);
						mStateTextField.setEnabled(false);
						mUnitTextField.setEnabled(false);
						mInstallationDateTextField.setEnabled(false);
						mModificationDateTextField.setEnabled(false);
						mRegressionDateTextField.setEnabled(false);
						mReferenceValueTextField.setEnabled(false);
						mMinimumValueTextField.setEnabled(false);
						mMaximumValueTextField.setEnabled(false);
						mMaximumVariationTextField.setEnabled(false);
						mMeasuresCountTextField.setEnabled(false);
						mV1TextField.setEnabled(false);
						mV2TextField.setEnabled(false);
						mV3TextField.setEnabled(false);
						mV4TextField.setEnabled(false);
						mV5TextField.setEnabled(false);
						mV6TextField.setEnabled(false);
						mV7TextField.setEnabled(false);
						mV8TextField.setEnabled(false);
						mV9TextField.setEnabled(false);
						mFormulaTextField.setEnabled(false);

						mModifyButton.setEnabled(true);
						mSaveButton.setEnabled(false);
						mDeleteButton.setEnabled(true);
						mCancelButton.setEnabled(false);
						mPrintButton.setEnabled(true);
					}
					break;
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de supprimer un instrument ou une
		// variable.
		// ---------------------------------------------------------------------
		mDeleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				if (Dialogs.showConfirmDialog(CIniFileReader
						.getString("CONFMSG13"), CIniFileReader
						.getString("CONFMSG14")) == JOptionPane.YES_OPTION) {
					// Effacement de l'instrument/variable dans le fichier
					// de donn�es.
					deleteSensorOrVariable();
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de supprimer les modifications
		// effectu�es sur un instrument ou une variable.
		// ---------------------------------------------------------------------
		mCancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				loadSensorOrVariableFeatures();

				mModifyButton.setEnabled(false);
				mAddButton.setEnabled(true);
				mSaveButton.setEnabled(true);
				mDeleteButton.setEnabled(true);
				mCancelButton.setEnabled(true);
				mSearchButton.setEnabled(false);
				mPrintButton.setEnabled(true);
				mCloseButton.setEnabled(true);
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de chercher un instrument ou une
		// variable.
		// ---------------------------------------------------------------------
		mSearchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'imprimer un instrument ou une
		// variable.
		// ---------------------------------------------------------------------
		mPrintButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				try {
					String lFileName = CSurveyControl.PRINT_DIRECTORY
							+ barrage.getId() + "_"
							+ mIdTextField.getText() + ".txt";
					PrintWriter lPrintWriter = new PrintWriter(lFileName);

					lPrintWriter.print(CIniFileReader.getString("P1L1") + " "
							+ barrage.getId() + "\n\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L1")
							+ " " + mIdTextField.getText() + "\t\t");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L3")
							+ " " + mNatureTextField.getText() + "\t\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L4")
							+ " " + mTypeTextField.getText() + "\t\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L5")
							+ " " + mGroupTextField.getText() + "\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L7")
							+ " " + mDescriptionTextArea.getText() + "\n\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L8")
							+ " " + mStateTextField.getText() + "\t\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L9")
							+ " " + mUnitTextField.getText() + "\t\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L10")
							+ " " + mInstallationDateTextField.getText()
							+ "\n\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L11")
							+ " " + mModificationDateTextField.getText()
							+ "\t\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L12")
							+ " " + mRegressionDateTextField.getText() + "\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L13")
							+ " " + mReferenceValueTextField.getText() + "\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L14")
							+ " " + mMinimumValueTextField.getText() + "\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L15")
							+ " " + mMaximumValueTextField.getText() + "\t");
					lPrintWriter
							.print("  " + CIniFileReader.getString("P2L16")
									+ " "
									+ mMaximumVariationTextField.getText()
									+ "\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L17")
							+ " " + mMeasuresCountTextField.getText() + "\n\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L29")
							+ "\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L19")
							+ " " + mV1TextField.getText() + "\t\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L20")
							+ " " + mV2TextField.getText() + "\t\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L21")
							+ " " + mV3TextField.getText() + "\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L22")
							+ " " + mV4TextField.getText() + "\t\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L23")
							+ " " + mV5TextField.getText() + "\t\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L24")
							+ " " + mV6TextField.getText() + "\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L25")
							+ " " + mV7TextField.getText() + "\t\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L26")
							+ " " + mV8TextField.getText() + "\t\t");
					lPrintWriter.print("  " + CIniFileReader.getString("P2L27")
							+ " " + mV9TextField.getText() + "\n");

					lPrintWriter.print("  " + CIniFileReader.getString("P2L28")
							+ " " + mFormulaTextField.getText() + "\n");

					lPrintWriter.close();

					Dialogs.showInformationDialog(CIniFileReader
							.getString("INFMSG2")
							+ lFileName);
				} catch (FileNotFoundException lException) {
					lException.printStackTrace();
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de fermer la fen�tre.
		// ---------------------------------------------------------------------
		mCloseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				if (mSensorOrVariableUpdated == true) {
					if (Dialogs.showConfirmDialog(CIniFileReader
							.getString("CONFMSG11"), CIniFileReader
							.getString("CONFMSG12")) == JOptionPane.YES_OPTION) {
						mSensorOrVariableUpdated = false;
					}
				} else {
					mSensorOrVariableUpdated = false;
				}
				mParent.closeView();
			}
		});
	}

	/**
	 * M�thode permettant de v�rifier si les champs saisis sont corrects ou non.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 18/01/10
	 * 
	 * @return true si les champs sont corrects et false sinon.
	 */
	private boolean checkFieldValues() {
		boolean lResult = true;

		// ---------------------------------------------------------------------
		// La nature doit �tre �gale � "I" ou "V".
		// ---------------------------------------------------------------------
		if ((mNatureTextField.getText().equals("I") == false)
				&& (mNatureTextField.getText().equals("V") == false)
				&& (mNatureTextField.getText().equals("") == false)) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG22"),
					CIniFileReader.getString("ERRMSG19"));
			lResult = false;
		}

		// ---------------------------------------------------------------------
		// L'�tat doit �tre �gal � "OO", "ON" ou "NN".
		// ---------------------------------------------------------------------
		if ((mStateTextField.getText().equals("OO") == false)
				&& (mStateTextField.getText().equals("ON") == false)
				&& (mStateTextField.getText().equals("NN") == false)
				&& (mStateTextField.getText().equals("") == false)) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG27"),
					CIniFileReader.getString("ERRMSG19"));
			lResult = false;
		}

		// -----------------------------------------------------------------
		// La valeur minimale doit �tre inf�rieure ou �gale � la
		// valeur maximale.
		// -----------------------------------------------------------------
		if (mMinimumValueTextField.getValue() > mMaximumValueTextField.getValue()) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG36"),
					CIniFileReader.getString("ERRMSG19"));
			lResult = false;
		}

		// -----------------------------------------------------------------
		// V�rification syntaxe de la formule
		// -----------------------------------------------------------------
		String formula = mFormulaTextField.getText();
		
		if (formula != null && formula.trim().length() != 0) {

			try {
				// 1. Compilation de la formule
				SGL.compile(formula);
			} catch (Throwable e) {
				Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG40"),
						CIniFileReader.getString("ERRMSG19"));
				lResult = false;
			}
		}
		return lResult;
	}

	/**
	 * M�thode permettant de supprimer l'instrument/variable courant(e).
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 18/01/10
	 */
	@SuppressWarnings("unchecked")
	private void deleteSensorOrVariable() {
		// 1. Lecture d�finition de l'instrument/variable
		Sensor sensor = barrage.getSensor(this.sensorId);
		
		//2 . Suppression de l'instrument/variable
		Survey.getInstance().removeSensor(sensor);
	}

	/**
	 * M�thode permettant de sauvegarder le barrage actif.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 18/01/10
	 */
	private void saveSensorOrVariable() {
		// 1. Cr�ation d'un instrument/Variable
		Sensor sensor = new Sensor();

		// 2. Initialisation barrage propri�taire
		sensor.setBarrage(this.barrage);
		
		// 3. Initialisation de l'instrument
		sensor.setNature(mNatureTextField.getText());
		sensor.setType(mTypeTextField.getText());
		sensor.setGroup(mGroupTextField.getText());
		sensor.setId(mIdTextField.getValue());
		sensor.setDescription(mDescriptionTextArea.getText());
		sensor.setState(mStateTextField.getText());
		sensor.setUnit(mUnitTextField.getText());
		sensor.setInstallationDate(mInstallationDateTextField.getValue());
		sensor.setUpdateDate(mModificationDateTextField.getValue());
		sensor.setRegressionDate(mRegressionDateTextField.getValue());
		sensor.setRefValue(mReferenceValueTextField.getValue());
		sensor.setMinimumValue(mMinimumValueTextField.getValue());
		sensor.setMaximumValue(mMaximumValueTextField.getValue());
		sensor.setMaximumVariation(mMaximumVariationTextField.getValue());
		sensor.setMeasuresCount(mMeasuresCountTextField.getValue());
		sensor.setV1(mV1TextField.getValue());
		sensor.setV2(mV2TextField.getValue());
		sensor.setV3(mV3TextField.getValue());
		sensor.setV4(mV4TextField.getValue());
		sensor.setV5(mV5TextField.getValue());
		sensor.setV6(mV6TextField.getValue());
		sensor.setV7(mV7TextField.getValue());
		sensor.setV8(mV8TextField.getValue());
		sensor.setV9(mV9TextField.getValue());
		sensor.setFormula(mFormulaTextField.getText());

		// 3. Enregistrement d�finition de l'instrument
		Survey.getInstance().addSensor(sensor, true);
		
		// 4. Actualisation identificateur de l'instrument edit�
		this.sensorId = sensor.getId();
	}

	/**
	 * M�thode permettant de sauvegarder l'instrument/variable courant(e).
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 18/01/10
	 */
	private void updateSensorOrVariable() {
		// 1. Cr�ation definition d'un instrument/Variable
		Sensor sensor = new Sensor();

		// 2. Initialisation barrage propri�taire
		sensor.setBarrage(this.barrage);
		
		// 3. Initialisation de l'instrument
		sensor.setNature(mNatureTextField.getText());
		sensor.setType(mTypeTextField.getText());
		sensor.setGroup(mGroupTextField.getText());
		sensor.setId(mIdTextField.getText());
		sensor.setDescription(mDescriptionTextArea.getText());
		sensor.setState(mStateTextField.getText());
		sensor.setUnit(mUnitTextField.getText());
		sensor.setInstallationDate(mInstallationDateTextField.getValue());
		sensor.setUpdateDate(mModificationDateTextField.getValue());
		sensor.setRegressionDate(mRegressionDateTextField.getValue());
		sensor.setRefValue(mReferenceValueTextField.getValue());
		sensor.setMinimumValue(mMinimumValueTextField.getValue());
		sensor.setMaximumValue(mMaximumValueTextField.getValue());
		sensor.setMaximumVariation(mMaximumVariationTextField.getValue());
		sensor.setMeasuresCount(mMeasuresCountTextField.getValue());
		sensor.setV1(mV1TextField.getValue());
		sensor.setV2(mV2TextField.getValue());
		sensor.setV3(mV3TextField.getValue());
		sensor.setV4(mV4TextField.getValue());
		sensor.setV5(mV5TextField.getValue());
		sensor.setV6(mV6TextField.getValue());
		sensor.setV7(mV7TextField.getValue());
		sensor.setV8(mV8TextField.getValue());
		sensor.setV9(mV9TextField.getValue());
		sensor.setFormula(mFormulaTextField.getText());

		// 3. Enregistrement d�finition de l'instrument
		Survey.getInstance().updateSensor(sensor, true);
	}

	// M�thode de l'interface keyListener.
	public void keyPressed(KeyEvent pEvent) {
	}

	public void keyTyped(KeyEvent pEvent) {
	}

	/**
	 * M�thode permettant de modifier le flag permettant de savoir si
	 * l'instrument ou la variable a �t� modifi� ou pas.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 15/01/10
	 */
	public void keyReleased(KeyEvent pEvent) {
		mSensorOrVariableUpdated = true;

		if (pEvent.getSource() == mIdTextField) {
			if ((mIdTextField.getText().length() > 0)
					&& (mIdTextField.getText().length() <= 8)) {
				mSaveButton.setEnabled(true);
				mPrintButton.setEnabled(true);
			} else {
				mSaveButton.setEnabled(false);
				mPrintButton.setEnabled(false);
			}
		}
	}

	/**
	 * Permet de charger les informations relatives � l'instrument ou � la
	 * variable � partir du fichier XML associ� au barrage actif.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 15/01/10
	 */
	private void loadSensorOrVariableFeatures() {
		// 1. Lecture d�finition de l'instrument/variable
		Sensor sensor = barrage.getSensor(this.sensorId);

		// 2. Initialisation des composants
		mNatureTextField.setText(sensor.getNature());
		mTypeTextField.setText(sensor.getType());
		mGroupTextField.setText(sensor.getGroup());
		mIdTextField.setText(sensor.getId());
		mDescriptionTextArea.setText(sensor.getDescription());
		mStateTextField.setText(sensor.getState());
		mUnitTextField.setText(sensor.getUnit());
		mInstallationDateTextField.setValue(sensor.getInstallationDate());
		mModificationDateTextField.setValue(sensor.getInstallationDate());
		mRegressionDateTextField.setValue(sensor.getRegressionDate());
		mReferenceValueTextField.setValue(sensor.getRefValue());
		mMinimumValueTextField.setValue(sensor.getMinimumValue());
		mMaximumValueTextField.setValue(sensor.getMaximumValue());
		mMaximumVariationTextField.setValue(sensor.getMaximumVariation());
		mMeasuresCountTextField.setValue(sensor.getMeasuresCount());
		mV1TextField.setValue(sensor.getV1());
		mV2TextField.setValue(sensor.getV2());
		mV3TextField.setValue(sensor.getV3());
		mV4TextField.setValue(sensor.getV4());
		mV5TextField.setValue(sensor.getV5());
		mV6TextField.setValue(sensor.getV6());
		mV7TextField.setValue(sensor.getV7());
		mV8TextField.setValue(sensor.getV8());
		mV9TextField.setValue(sensor.getV9());
		mFormulaTextField.setText(sensor.getFormula());
	}
}
