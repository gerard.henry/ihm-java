package controls.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import calcul.SensorCorrection;

@SuppressWarnings("serial")
public class AnalyseCorrectionResultats extends JPanel implements ChangeListener {
	/** Composants internes. */
	protected JTabbedPane tabbedPane;
	
	/** Liste des corrections de mesures. */
	protected SensorCorrection sensors[];
	
	/** Index de la page s�lectionn�e. */
	protected int currentPage;
	
	/**
	 * Constructeur
	 */
	public AnalyseCorrectionResultats() {
		// 1. Initialisation des attributs
		this.sensors = null;
		this.currentPage = -1;
		
		// 2. Construction de l'interface
		this.buildUI();
	}
	
	/**
	 * Actualisation de la liste des instruments
	 * @param aSensors Liste des instruments
	 */
	public void setSensors(SensorCorrection aSensors[]) {

		if ( this.sensors != null ) {
			// 1. D�sinstallation des listeners
			this.tabbedPane.removeChangeListener(this);
			
			// 2. Suppression de toutes les pages
			this.tabbedPane.removeAll();
			
			// 3. Actualisation index de la page active
			this.currentPage = -1;
		} 
		
		// 2. Actualisation liste des instruments
		this.sensors = aSensors;
		
		if ( this.sensors != null ) {
			//1 . Cr�ation pages d'affichages des r�sultats
			for ( int i = 0 ; i < this.sensors.length ; i++) {
				this.tabbedPane.addTab(this.sensors[i].getSensor().getId(),new JPanel());
			}
			
			// 2. Installation des listeners
			this.tabbedPane.addChangeListener(this);
			
			// 3. S�lection de la premi�re page
			this.showPage(0);
		}
	}
	
	/**
	 * Construction de l'interface
	 */
	protected void buildUI() {
		// 1. Initialisation layout du panneau principal
		this.setLayout(new BorderLayout());
		
		// 2. Cr�ation des composants
		this.tabbedPane = new JTabbedPane();
		
		// 3. Configuration des composants
		this.tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		this.tabbedPane.setPreferredSize(new Dimension(500,300));
		
		// 3. Installation des listeners
		this.add(this.tabbedPane,BorderLayout.CENTER);
	}

	@Override
	public void stateChanged(ChangeEvent aEvent) {
		this.showPage(this.tabbedPane.getSelectedIndex());
	}
	
	/**
	 * Visualisation de la page avec cet index
	 * 
	 * @param aIndex
	 *            Index de la page � visualiser
	 */
	protected void showPage(int aPageIndex) {
		if (this.currentPage != -1) {
			// 1. Actualisation de la page pr�c�dente
			this.tabbedPane.setComponentAt(this.currentPage, new JPanel());
		}

		// 3. Actualisation index de la page courante
		this.currentPage = aPageIndex;
		
		// 4. Cr�ation panneau d'affichage de la page courante
		this.tabbedPane.setComponentAt(this.currentPage, new SensorCorrectionResultats(this.sensors[this.currentPage]));
	}
}
