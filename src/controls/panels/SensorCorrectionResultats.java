package controls.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import calcul.SensorCorrection;
import db.Survey;

@SuppressWarnings({ "serial", "unused" })
public class SensorCorrectionResultats extends JPanel implements ChangeListener {
	public static final DecimalFormat f4dec = new DecimalFormat(
	"#####0.0000");
	
	public class CorrectionTableMode extends AbstractTableModel {
		@Override
		public String getColumnName(int aColumn) {
			switch (aColumn) {
			case 0:
				return "Date";
			case 1:
				return "Valeur mesur�e";
			case 2:
				return "Valeur calcul�e";
			case 3:
				return "Ecart";
			case 4:
				return "Saison";
			case 5:
				return "Cote";
			case 6:
				return "Pluie";
			case 7:
				return "Valeur corrig�e";
			default:
				return null;
			}
		}

		@Override
		public int getColumnCount() {
			return 8;
		}

		@Override
		public int getRowCount() {
			return sensor.getMeasureDates().size();
		}

		@Override
		public Object getValueAt(int aRow, int aColumn) {
			switch (aColumn) {
			case 0:
				return Survey.format(sensor.getMeasureDates().get(aRow));
			case 1:
				return Survey.format(sensor.getMeasureValues().get(aRow));
			case 2:
				return Survey.format(sensor.getMeasureComputed().get(aRow));
			case 3:
				return Survey.format(sensor.getMeasureEps().get(aRow),f4dec);
			case 4:
				return Survey.format(sensor.getSeasonEffects().get(aRow),f4dec);
			case 5:
				return Survey.format(sensor.getHydroEffects().get(aRow),f4dec);
			case 6:
				return Survey.format(sensor.getRainEffects().get(aRow),f4dec);
			case 7:
				return Survey.format(sensor.getMeasureCorrected().get(aRow));
			default:
				return null;
			}
		}

	}

	/** Composants internes. */
	protected JTabbedPane tabbedPane;
	protected JLabel sensorLabel;

	/** Mesures corrig�es de l'instrument. */
	protected SensorCorrection sensor;

	/** Index de la page courante. */
	protected int currentPage;

	/**
	 * Constructeur
	 * 
	 * @param aSensor
	 *            Instrument pour lequel on affiche les r�sultats
	 */
	public SensorCorrectionResultats(SensorCorrection aSensor) {
		// 1. Initialisation des attributs
		this.currentPage = -1;
		this.sensor = aSensor;

		// 2. Construction de l'interface
		this.buildUI();

		// 3. Visualisation de la premi�re page
		this.showPage(0);
	}

	/**
	 * Construction de l'interface
	 */
	protected void buildUI() {
		// 1. Initialisation layout du panneau principal
		this.setLayout(new BorderLayout());

		// 2. Cr�ation des composants
		this.sensorLabel = new JLabel(
				"Correction des mesures selon les r�sultats d'analyse du "
						+ Survey.format(this.sensor.getSensor()
								.getRegressionDate()));
		this.tabbedPane = new JTabbedPane();

		// 3 . Configuration des composants
		this.tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		this.tabbedPane.addTab("Correction des mesures", new JPanel());
		this.tabbedPane.addTab("Graphe des mesures", new JPanel());
		this.tabbedPane.addTab("Analyse � conditions constantes", new JPanel());
		this.tabbedPane.addTab("Analyse des effets de la pluviom�trie",
				new JPanel());
		this.tabbedPane.addTab("Analyse saisonni�re", new JPanel());
		this.tabbedPane.addTab(
				"Analyse des effets de la variation du plan d'eau",
				new JPanel());
		this.tabbedPane.addTab("Valeur mesur�e/Valeur estim�e", new JPanel());

		// 4. Installation des composants
		this.add(this.sensorLabel, BorderLayout.NORTH);
		this.add(this.tabbedPane, BorderLayout.CENTER);

		// 5. Installation des listeners
		this.tabbedPane.addChangeListener(this);
	}

	@Override
	public void stateChanged(ChangeEvent aEvent) {
		this.showPage(this.tabbedPane.getSelectedIndex());
	}

	/**
	 * Visualisation de la page avec cet index
	 * 
	 * @param aIndex
	 *            Index de la page � visualiser
	 */
	protected void showPage(int aPageIndex) {
		if (this.currentPage != -1) {
			// 1. Actualisation de la page pr�c�dente
			this.tabbedPane.setComponentAt(this.currentPage, new JPanel());

		}

		// 1. Actualisation index de la page courante
		this.currentPage = aPageIndex;

		// 2. Cr�ation panneau d'affichage de la page
		JPanel pagePane = new JPanel(new BorderLayout());

		switch (this.currentPage) {
		case 0: {
			// 1. Cr�ation des composants
			JTable table = new JTable(new CorrectionTableMode());

			// 2. Installation des composants
			pagePane.add(new JScrollPane(table), BorderLayout.CENTER);

			break;
		}
		case 1:
			// 1. Installation des composants
			pagePane.add(this.createMeasureGraph(), BorderLayout.CENTER);
			
			break;
		case 2:
			// 1. Installation des composants
			pagePane.add(this.createConstantConditionGraph(), BorderLayout.CENTER);
			
			break;
		case 3:
			// 1. Installation des composants
			pagePane.add(this.createRainEffectGraph(), BorderLayout.CENTER);
			
			break;
		case 4:
			// 1. Installation des composants
			pagePane.add(this.createSeasonEffectGraph(), BorderLayout.CENTER);
			
			break;
		case 5:
			// 1. Installation des composants
			pagePane.add(this.createHydroEffectGraph(), BorderLayout.CENTER);
			
			break;
		case 6:
			// 1. Installation des composants
			pagePane.add(this.createCompareGraph(), BorderLayout.CENTER);
			
			break;
		}
		
		// 3. Actualisation de la page
		this.tabbedPane.setComponentAt(this.currentPage, pagePane);
		
	}
	
	/**
	 * Cr�ation du graphe des mesures
	 * @return Graphe des mesures
	 */
	public ChartPanel createMeasureGraph() {
		// 1. Cr�ation des s�ries de valeurs � afficher
		XYSeries measuresValue = new XYSeries("Mesure");
		XYSeries measuresComputed = new XYSeries("Mesure calcul�e");
		
		// 2. Initialisation des s�ries
		List<Double> x = this.sensor.getReductedTime();
		List<Double> y1 = this.sensor.getMeasureValues();
		List<Double> y2 = this.sensor.getMeasureComputed();
		
		for ( int i = 0 ; i< x.size(); i++) {
			double ix = x.get(i);
			double iy1 = y1.get(i);
			double iy2 = y2.get(i);
			measuresValue.add(ix,iy1);
			measuresComputed.add(ix,iy2);
		}
		
		// 3. Cr�ation collection des s�ries du graphe
		XYSeriesCollection series = new XYSeriesCollection();
		
		// 4. Construction collection des s�ries du graphe
		series.addSeries(measuresValue);
		series.addSeries(measuresComputed);
		
		// 4. Construction titre du graphe
		String title = this.sensor.getBarrage().getId()+" "+this.sensor.getSensor().getId()+" "+this.sensor.getSensor().getTitle();
		
		// 6. Cr�ation du graphe � visualiser
		JFreeChart chart = ChartFactory.createXYLineChart(
	            title,
	            "Graphe des mesures",
	            "",
	            series,
	            PlotOrientation.VERTICAL,
	            true,
	            true,
	            false
	        );

		// 7. Configuration du graphe
		XYPlot plot = (XYPlot) chart.getPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, true);
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesLinesVisible(1, true);
        renderer.setSeriesShapesVisible(1, true);
        plot.setRenderer(renderer);
        ((NumberAxis)plot.getRangeAxis()).setAutoRangeIncludesZero(false);
        
		// 7. Cr�ation panneau d'affichage du graphe
		ChartPanel chartPane = new ChartPanel(chart);

		// 8. Configuration du panneau
		chartPane.setMouseZoomable(true, false);

		return chartPane;
	}
	
	/**
	 * Cr�ation du graphe � conditions constantes
	 * @return Graphe � conditions constantes
	 */
	public ChartPanel createConstantConditionGraph() {
		// 1. Cr�ation des s�ries de valeurs � afficher
		XYSeries timeEffects = new XYSeries("VRef + Effet du temps");
		XYSeries measuresCorrected = new XYSeries("Mesure corrig�e");
		
		// 2. Initialisation des s�ries
		List<Double> x = this.sensor.getReductedTime();
		List<Double> y1 = this.sensor.getMeasureCorrected();
		List<Double> y2 = this.sensor.getTimeEffects();
		double vref = this.sensor.getSensor().getRefValue();
		
		for ( int i = 0 ; i< x.size(); i++) {
			double ix = x.get(i);
			double iy1 = y1.get(i);
			double iy2 = y2.get(i)+vref;
			measuresCorrected.add(ix,iy1);
			timeEffects.add(ix,iy2);
		}
		
		// 3. Cr�ation collection des s�ries du graphe
		XYSeriesCollection series = new XYSeriesCollection();
		
		// 4. Construction collection des s�ries du graphe
		series.addSeries(measuresCorrected);
		series.addSeries(timeEffects);
		
		// 4. Construction titre du graphe
		String title = this.sensor.getBarrage().getId()+" "+this.sensor.getSensor().getId()+" "+this.sensor.getSensor().getTitle();
		
		// 6. Cr�ation du graphe � visualiser
		JFreeChart chart = ChartFactory.createXYLineChart(
	            title,
	            "Analyse � conditions constantes",
	            "",
	            series,
	            PlotOrientation.VERTICAL,
	            true,
	            true,
	            false
	        );

		// 7. Configuration du graphe
		XYPlot plot = (XYPlot) chart.getPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, true);
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesLinesVisible(1, true);
        renderer.setSeriesShapesVisible(1, true);
        plot.setRenderer(renderer);
        ((NumberAxis)plot.getRangeAxis()).setAutoRangeIncludesZero(false);
        
		// 7. Cr�ation panneau d'affichage du graphe
		ChartPanel chartPane = new ChartPanel(chart);

		// 8. Configuration du panneau
		chartPane.setMouseZoomable(true, false);

		return chartPane;
	}
	
	/**
	 * Cr�ation du graphe d'analyse des effets de la pluiviom�trie
	 * @return Graphe d'analyse des effets de la pluiviom�trie
	 */
	public ChartPanel createRainEffectGraph() {
		// 1. Cr�ation des s�ries de valeurs � afficher
		XYSeries pt = new XYSeries("P(t)");
		XYSeries pteps = new XYSeries("P(t)+Epsilon");
		
		// 2. Initialisation des s�ries
		List<Double> x = this.sensor.getReductedTime();
		List<Double> y1 = this.sensor.getRainEffects();
		List<Double> y2 = this.sensor.getMeasureEps();
		
		
		for ( int i = 0 ; i< x.size(); i++) {
			double ix = x.get(i);
			double iy1 = y1.get(i);
			double iy2 = iy1 + y2.get(i);
			pt.add(ix,iy1);
			pteps.add(ix,iy2);
		}
		
		// 3. Cr�ation collection des s�ries du graphe
		XYSeriesCollection series = new XYSeriesCollection();
		
		// 4. Construction collection des s�ries du graphe
		series.addSeries(pt);
		series.addSeries(pteps);
		
		// 4. Construction titre du graphe
		String title = this.sensor.getBarrage().getId()+" "+this.sensor.getSensor().getId()+" "+this.sensor.getSensor().getTitle();
		
		// 6. Cr�ation du graphe � visualiser
		JFreeChart chart = ChartFactory.createXYLineChart(
	            title,
	            "Analyse des effets de la pluviom�trie",
	            "",
	            series,
	            PlotOrientation.VERTICAL,
	            true,
	            true,
	            false
	        );

		// 7. Configuration du graphe
		XYPlot plot = (XYPlot) chart.getPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, true);
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesLinesVisible(1, true);
        renderer.setSeriesShapesVisible(1, true);
        plot.setRenderer(renderer);
        ((NumberAxis)plot.getRangeAxis()).setAutoRangeIncludesZero(false);
        
		// 7. Cr�ation panneau d'affichage du graphe
		ChartPanel chartPane = new ChartPanel(chart);

		// 8. Configuration du panneau
		chartPane.setMouseZoomable(true, false);

		return chartPane;
	}
	
	/**
	 * Cr�ation du graphe d'analyse des effets saisonniers
	 * @return Graphe d'analyse des effets saisonniers
	 */
	public ChartPanel createSeasonEffectGraph() {
		// 1. Cr�ation des s�ries de valeurs � afficher
		XYSeries st = new XYSeries("S(theta)");
		XYSeries steps = new XYSeries("S(theta)+Epsilon");
		
		// 2. Initialisation des s�ries
		List<Double> x = this.sensor.getSeasonAngle();
		List<Double> y1 = this.sensor.getSeasonEffects();
		List<Double> y2 = this.sensor.getMeasureEps();
		
		
		for ( int i = 0 ; i< x.size(); i++) {
			double ix = x.get(i)*58.131343;
			double iy1 = y1.get(i);
			double iy2 = iy1 + y2.get(i);
			st.add(ix,iy1);
			steps.add(ix,iy2);
		}
		
		// 3. Cr�ation collection des s�ries du graphe
		XYSeriesCollection series = new XYSeriesCollection();
		
		// 4. Construction collection des s�ries du graphe
		series.addSeries(st);
		series.addSeries(steps);
		
		// 4. Construction titre du graphe
		String title = this.sensor.getBarrage().getId()+" "+this.sensor.getSensor().getId()+" "+this.sensor.getSensor().getTitle();
		
		// 6. Cr�ation du graphe � visualiser
		JFreeChart chart = ChartFactory.createXYLineChart(
	            title,
	            "Analyse saisonni�re",
	            "",
	            series,
	            PlotOrientation.VERTICAL,
	            true,
	            true,
	            false
	        );

		// 7. Configuration du graphe
		XYPlot plot = (XYPlot) chart.getPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, true);
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesLinesVisible(1, true);
        renderer.setSeriesShapesVisible(1, true);
        plot.setRenderer(renderer);
        ((NumberAxis)plot.getRangeAxis()).setAutoRangeIncludesZero(false);
        
		// 7. Cr�ation panneau d'affichage du graphe
		ChartPanel chartPane = new ChartPanel(chart);

		// 8. Configuration du panneau
		chartPane.setMouseZoomable(true, false);

		return chartPane;
	}
	
	/**
	 * Cr�ation du graphe d'analyse des effets de la variation du plan d'eau
	 * @return Graphe d'analyse des  effets de la variation du plan d'eau
	 */
	public ChartPanel createHydroEffectGraph() {
		// 1. Cr�ation des s�ries de valeurs � afficher
		XYSeries hz = new XYSeries("H(z)");
		XYSeries hzeps = new XYSeries("H(z)+Epsilon");
		
		// 2. Initialisation des s�ries
		List<Double> x = this.sensor.getCote();
		List<Double> y1 = this.sensor.getHydroEffects();
		List<Double> y2 = this.sensor.getMeasureEps();
		
		
		for ( int i = 0 ; i< x.size(); i++) {
			double ix = x.get(i);
			double iy1 = y1.get(i);
			double iy2 = iy1 + y2.get(i);
			hz.add(ix,iy1);
			hzeps.add(ix,iy2);
		}
		
		// 3. Cr�ation collection des s�ries du graphe
		XYSeriesCollection series = new XYSeriesCollection();
		
		// 4. Construction collection des s�ries du graphe
		series.addSeries(hz);
		series.addSeries(hzeps);
		
		// 4. Construction titre du graphe
		String title = this.sensor.getBarrage().getId()+" "+this.sensor.getSensor().getId()+" "+this.sensor.getSensor().getTitle();
		
		// 6. Cr�ation du graphe � visualiser
		JFreeChart chart = ChartFactory.createXYLineChart(
	            title,
	            "Analyse des effets de la variation du plan d'eau",
	            "",
	            series,
	            PlotOrientation.VERTICAL,
	            true,
	            true,
	            false
	        );

		// 7. Configuration du graphe
		XYPlot plot = (XYPlot) chart.getPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, true);
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesLinesVisible(1, true);
        renderer.setSeriesShapesVisible(1, true);
        plot.setRenderer(renderer);
        ((NumberAxis)plot.getRangeAxis()).setAutoRangeIncludesZero(false);
        
		// 7. Cr�ation panneau d'affichage du graphe
		ChartPanel chartPane = new ChartPanel(chart);

		// 8. Configuration du panneau
		chartPane.setMouseZoomable(true, false);

		return chartPane;
	}
	
	/**
	 * Cr�ation du graphe de comparaison de la valeur mesur�e et de la valeur estim�e
	 * @return Graphe de comparaison de la valeur mesur�e et de la valeur estim�e
	 */
	public ChartPanel createCompareGraph() {
		// 1. Cr�ation des s�ries de valeurs � afficher
		XYSeries cmp = new XYSeries("Mesure calcul�e(Mesure)");
		
		// 2. Initialisation des s�ries
		List<Double> x = this.sensor.getMeasureValues();
		List<Double> y1 = this.sensor.getMeasureComputed();
		
		
		for ( int i = 0 ; i< x.size(); i++) {
			double ix = x.get(i);
			double iy1 = y1.get(i);
			cmp.add(ix,iy1);
		}
		
		// 3. Cr�ation collection des s�ries du graphe
		XYSeriesCollection series = new XYSeriesCollection();
		
		// 4. Construction collection des s�ries du graphe
		series.addSeries(cmp);
		
		// 4. Construction titre du graphe
		String title = this.sensor.getBarrage().getId()+" "+this.sensor.getSensor().getId()+" "+this.sensor.getSensor().getTitle();
		
		// 6. Cr�ation du graphe � visualiser
		JFreeChart chart = ChartFactory.createXYLineChart(
	            title,
	            "Valeur mesur�e/Valeur estim�e",
	            "",
	            series,
	            PlotOrientation.VERTICAL,
	            true,
	            true,
	            false
	        );

		// 7. Configuration du graphe
		XYPlot plot = (XYPlot) chart.getPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, true);
        renderer.setSeriesShapesVisible(0, true);
        plot.setRenderer(renderer);
        ((NumberAxis)plot.getRangeAxis()).setAutoRangeIncludesZero(false);
        
		// 7. Cr�ation panneau d'affichage du graphe
		ChartPanel chartPane = new ChartPanel(chart);

		// 8. Configuration du panneau
		chartPane.setMouseZoomable(true, false);

		return chartPane;
	}
}
