package controls.panels;

import gui.panels.BarragesGui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;

import javax.swing.JOptionPane;

import tools.CIniFileReader;
import controls.CSurveyControl;
import cs.ui.Dialogs;
import db.Barrage;
import db.Survey;

/**
 * Contr�les du panneau de gestion d'un barrage.
 * 
 * @author : soci�t� CS SI
 * @version : 1.0 du 14/12/09
 */
public class BarragesControl extends BarragesGui
		implements KeyListener {
	private static final long serialVersionUID = 1L;

	// Mode d'utilisation du panneau :
	// CREATE_MODE, MODIFY_MODE ou NFORMATION_MODE.
	private int mMode;

	// Flag permettant de savoir si le barrage a �t� modifi� ou pas.
	private boolean mDamUpdated;

	// Dimensions maximales des champs cha�nes de caract�res.
	private static final int DAM_ID_MAX_LENGTH = 32;
	private static final int DAM_TITLE_MAX_LENGTH = 80;
	private static final int DAM_DESCRIPTION_MAX_LENGTH = 240;

	// Constantes permettant de sp�cifier le mode d'utilisation du panneau.
	public static final int CREATE_MODE = 0;
	public static final int MODIFY_MODE = 1;
	public static final int INFORMATION_MODE = 2;

	// R�pertoires de donn�es.
	public static final String DB_DIRECTORY = "db/";
	public static final String DON_DIRECTORY = "db/don/";
	public static final String INS_DIRECTORY = "db/ins/";
	public static final String RES_DIRECTORY = "db/res/";
	public static final String DON_BACKUP_DIRECTORY = "db/backup/don/";
	public static final String INS_BACKUP_DIRECTORY = "db/backup/ins/";
	public static final String RES_BACKUP_DIRECTORY = "db/backup/res/";

	/** Barrage en cours d'edition. */
	protected Barrage barrage;
	
	protected CSurveyControl mParent;
	
	/**
	 * Constructeur.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 14/12/09
	 * 
	 * @param aBarrage Un barrage � editer
	 * @param pMode
	 *            le mode de fonctionnement du panneau.
	 */
	public BarragesControl(CSurveyControl aParent,Barrage aBarrage,int pMode) {
		super();

		// 1. Initialisation des attributs
		this.barrage = aBarrage;
		mMode = pMode;
		mDamUpdated = false;
		mParent = aParent;
		
		connectListeners();

		// Configuration du panneau selon le mode de fonctionnement sp�cifi�.
		switch (pMode) {
		case INFORMATION_MODE:
			loadDamFeatures();

			mIdTextField.setEnabled(false);
			mTitleTextArea.setEnabled(false);
			mDescriptionTextArea.setEnabled(false);
			mLaunchingDateTextField.setEnabled(false);
			mSensorsCountTextField.setEnabled(false);
			mStockedVariablesCountTextField.setEnabled(false);
			mUpdateDateTextField.setEnabled(false);
			mSaveDateTextField.setEnabled(false);
			mMeasuresCountTextField.setEnabled(false);
			mWaterMinLevelTextField.setEnabled(false);
			mWaterNominalLevelTextField.setEnabled(false);
			mWaterMaxLevelTextField.setEnabled(false);

			mModifyButton.setEnabled(true);
			mSaveButton.setEnabled(false);
			mDeleteButton.setEnabled(false);
			mCancelButton.setEnabled(false);
			mPrintButton.setEnabled(true);
			mCloseButton.setEnabled(true);
			break;

		case CREATE_MODE:
			mIdTextField.setEnabled(true);
			mTitleTextArea.setEnabled(true);
			mDescriptionTextArea.setEnabled(true);
			mLaunchingDateTextField.setEnabled(true);
			mSensorsCountTextField.setEnabled(false);
			mStockedVariablesCountTextField.setEnabled(false);
			mUpdateDateTextField.setEnabled(false);
			mSaveDateTextField.setEnabled(false);
			mMeasuresCountTextField.setEnabled(false);
			mWaterMinLevelTextField.setEnabled(true);
			mWaterNominalLevelTextField.setEnabled(true);
			mWaterMaxLevelTextField.setEnabled(true);

			mLaunchingDateTextField.setValue(new Date());
			mWaterMinLevelTextField.setValue(0.0);
			mWaterNominalLevelTextField.setValue(0.0);
			mWaterMaxLevelTextField.setValue(0.0);
			
			mModifyButton.setEnabled(false);
			mSaveButton.setEnabled(false);
			mDeleteButton.setEnabled(false);
			mCancelButton.setEnabled(false);
			mPrintButton.setEnabled(true);
			mCloseButton.setEnabled(true);
			break;

		case MODIFY_MODE:
			loadDamFeatures();

			mIdTextField.setEnabled(false);
			mTitleTextArea.setEnabled(true);
			mDescriptionTextArea.setEnabled(true);
			mLaunchingDateTextField.setEnabled(true);
			mSensorsCountTextField.setEnabled(false);
			mStockedVariablesCountTextField.setEnabled(false);
			mUpdateDateTextField.setEnabled(false);
			mSaveDateTextField.setEnabled(false);
			mMeasuresCountTextField.setEnabled(false);
			mWaterMinLevelTextField.setEnabled(true);
			mWaterNominalLevelTextField.setEnabled(true);
			mWaterMaxLevelTextField.setEnabled(true);

			mModifyButton.setEnabled(false);
			mSaveButton.setEnabled(true);
			mDeleteButton.setEnabled(true);
			mCancelButton.setEnabled(true);
			mPrintButton.setEnabled(true);
			mCloseButton.setEnabled(true);
			break;
		}
	}

	/**
	 * Permet de connecter les composants � leurs �couteurs.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 14/12/09
	 */
	private void connectListeners() {
		// ---------------------------------------------------------------------
		// Connexion des �couteurs permettant de modifier le flag permettant de
		// savoir si le barrage a �t� sauv� ou pas.
		// ---------------------------------------------------------------------
		mIdTextField.addKeyListener(this);
		mTitleTextArea.addKeyListener(this);
		mDescriptionTextArea.addKeyListener(this);
		mLaunchingDateTextField.addKeyListener(this);
		mSensorsCountTextField.addKeyListener(this);
		mStockedVariablesCountTextField.addKeyListener(this);
		mUpdateDateTextField.addKeyListener(this);
		mSaveDateTextField.addKeyListener(this);
		mMeasuresCountTextField.addKeyListener(this);
		mWaterMinLevelTextField.addKeyListener(this);
		mWaterNominalLevelTextField.addKeyListener(this);
		mWaterMaxLevelTextField.addKeyListener(this);

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de modifier le barrage.
		// ---------------------------------------------------------------------
		mModifyButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				mMode = MODIFY_MODE;

				mIdTextField.setEnabled(false);
				mTitleTextArea.setEnabled(true);
				mDescriptionTextArea.setEnabled(true);
				mLaunchingDateTextField.setEnabled(true);
				mSensorsCountTextField.setEnabled(false);
				mStockedVariablesCountTextField.setEnabled(false);
				mUpdateDateTextField.setEnabled(false);
				mSaveDateTextField.setEnabled(false);
				mMeasuresCountTextField.setEnabled(false);
				mWaterMinLevelTextField.setEnabled(true);
				mWaterNominalLevelTextField.setEnabled(true);
				mWaterMaxLevelTextField.setEnabled(true);

				mModifyButton.setEnabled(false);
				mSaveButton.setEnabled(true);
				mDeleteButton.setEnabled(true);
				mCancelButton.setEnabled(true);
				mPrintButton.setEnabled(true);
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de sauver le barrage.
		// ---------------------------------------------------------------------
		mSaveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				switch (mMode) {
				case CREATE_MODE:
					mDamUpdated = false;

					if (checkFieldValues() == true) {
						saveDam();

						mIdTextField.setEnabled(false);
						mTitleTextArea.setEnabled(false);
						mDescriptionTextArea.setEnabled(false);
						mLaunchingDateTextField.setEnabled(false);
						mSensorsCountTextField.setEnabled(false);
						mStockedVariablesCountTextField.setEnabled(false);
						mUpdateDateTextField.setEnabled(false);
						mSaveDateTextField.setEnabled(false);
						mMeasuresCountTextField.setEnabled(false);
						mWaterMinLevelTextField.setEnabled(false);
						mWaterNominalLevelTextField.setEnabled(false);
						mWaterMaxLevelTextField.setEnabled(false);

						mModifyButton.setEnabled(true);
						mSaveButton.setEnabled(false);
						mDeleteButton.setEnabled(true);
						mCancelButton.setEnabled(false);
						mPrintButton.setEnabled(true);
					}
					break;

				case MODIFY_MODE:
					mDamUpdated = false;

					if (checkFieldValues() == true) {
						updateDam();

						mIdTextField.setEnabled(false);
						mTitleTextArea.setEnabled(false);
						mDescriptionTextArea.setEnabled(false);
						mLaunchingDateTextField.setEnabled(false);
						mSensorsCountTextField.setEnabled(false);
						mStockedVariablesCountTextField.setEnabled(false);
						mUpdateDateTextField.setEnabled(false);
						mSaveDateTextField.setEnabled(false);
						mMeasuresCountTextField.setEnabled(false);
						mWaterMinLevelTextField.setEnabled(false);
						mWaterNominalLevelTextField.setEnabled(false);
						mWaterMaxLevelTextField.setEnabled(false);

						mModifyButton.setEnabled(true);
						mSaveButton.setEnabled(false);
						mDeleteButton.setEnabled(true);
						mCancelButton.setEnabled(false);
						mPrintButton.setEnabled(true);
					}
					break;
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de supprimer le barrage.
		// ---------------------------------------------------------------------
		mDeleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				if (Dialogs.showConfirmDialog(CIniFileReader
						.getString("CONFMSG9"), CIniFileReader
						.getString("CONFMSG10")) == JOptionPane.YES_OPTION) {
					// 1. Suppression du barrage edit�
					Survey.getInstance().removeBarrage(barrage.getId());
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'annuler les modifications.
		// ---------------------------------------------------------------------
		mCancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				loadDamFeatures();

				mModifyButton.setEnabled(true);
				mSaveButton.setEnabled(false);
				mDeleteButton.setEnabled(true);
				mCancelButton.setEnabled(false);
				mPrintButton.setEnabled(true);
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'imprimer le barrage.
		// ---------------------------------------------------------------------
		mPrintButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				try {
					String lFileName = CSurveyControl.PRINT_DIRECTORY
							+ mIdTextField.getText() + ".txt";
					PrintWriter lPrintWriter = new PrintWriter(lFileName);

					lPrintWriter.print(CIniFileReader.getString("P1L1") + " "
							+ mIdTextField.getText() + "\n\n");
					lPrintWriter.print("  " + CIniFileReader.getString("P1L2")
							+ " " + mTitleTextArea.getText() + "\n\n");
					lPrintWriter.print("  " + CIniFileReader.getString("P1L3")
							+ " " + mDescriptionTextArea.getText() + "\n\n");
					lPrintWriter.print("  " + CIniFileReader.getString("P1L4")
							+ " " + mLaunchingDateTextField.getText() + "\n");
					lPrintWriter.print("  " + CIniFileReader.getString("P1L5")
							+ " " + mSensorsCountTextField.getText() + "\n");
					lPrintWriter.print("  " + CIniFileReader.getString("P1L6")
							+ " " + mStockedVariablesCountTextField.getText()
							+ "\n");
					lPrintWriter.print("  " + CIniFileReader.getString("P1L7")
							+ " " + mUpdateDateTextField.getText() + "\n");
					lPrintWriter.print("  " + CIniFileReader.getString("P1L8")
							+ " " + mSaveDateTextField.getText() + "\n");
					lPrintWriter.print("  " + CIniFileReader.getString("P1L9")
							+ " " + mMeasuresCountTextField.getText() + "\n\n");
					lPrintWriter.print("  " + CIniFileReader.getString("P1L10")
							+ " " + mWaterMinLevelTextField.getText() + "\n");
					lPrintWriter.print("  " + CIniFileReader.getString("P1L11")
							+ " " + mWaterNominalLevelTextField.getText()
							+ "\n");
					lPrintWriter.print("  " + CIniFileReader.getString("P1L12")
							+ " " + mWaterMaxLevelTextField.getText() + "\n");

					lPrintWriter.close();

					Dialogs.showInformationDialog(CIniFileReader
							.getString("INFMSG2")
							+ lFileName);
				} catch (FileNotFoundException lException) {
					lException.printStackTrace();
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de fermer la fen�tre.
		// ---------------------------------------------------------------------
		mCloseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				if (mDamUpdated == true) {
					if (Dialogs.showConfirmDialog(CIniFileReader
							.getString("CONFMSG5"), CIniFileReader
							.getString("CONFMSG6")) == JOptionPane.YES_OPTION) {
						mDamUpdated = false;
					}
				} else {
					mDamUpdated = false;
				}
				
				mParent.closeView();

			}
		});
	}

	// M�thode de l'interface keyListener.
	public void keyPressed(KeyEvent pEvent) {
	}

	public void keyTyped(KeyEvent pEvent) {
	}

	/**
	 * M�thode permettant de modifier le flag permettant de savoir si le barrage
	 * a �t� modifi� ou pas.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 14/12/09
	 */
	public void keyReleased(KeyEvent pEvent) {
		mDamUpdated = true;
		if (pEvent.getSource() == mIdTextField) {
			if ((mIdTextField.getText().length() > 0)
					&& (mIdTextField.getText().length() <= DAM_ID_MAX_LENGTH)) {
				mSaveButton.setEnabled(true);
				mPrintButton.setEnabled(true);
			} else {
				mSaveButton.setEnabled(false);
				mPrintButton.setEnabled(false);
			}
		}
	}

	/**
	 * Permet de charger les informations relatives au barrage actif � partir du
	 * fichier XML contenant les informations sur les barrages.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 16/12/09
	 */
	private void loadDamFeatures() {
		// 1. Initialisation des composants
		mIdTextField.setText(barrage.getId());
		mTitleTextArea.setText(barrage.getTitle());
		mDescriptionTextArea.setText(barrage.getDescription());
		mLaunchingDateTextField.setValue(barrage.getLaunchingDate());
		mSensorsCountTextField.setText(barrage.getSensorsCount() + "");
		mStockedVariablesCountTextField.setText(barrage
				.getStockedVariablesCount()
				+ "");
		mSaveDateTextField.setValue(barrage.getSaveDate());
		mUpdateDateTextField.setValue(barrage.getUpdateDate());
		mMeasuresCountTextField.setText(barrage.getMeasuresCount() + "");
		mWaterMinLevelTextField.setValue(barrage.getWaterMinLevel());
		mWaterNominalLevelTextField.setValue(barrage.getWaterNominalLevel());
		mWaterMaxLevelTextField.setValue(barrage.getWaterMaxLevel());
	}

	/**
	 * M�thode permettant de sauvegarder le barrage actif.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 14/12/09
	 */
	private void saveDam() {
		// 1. Cr�ation d�finition du barrage
		this.barrage = new Barrage();

		// 2. Initialisation d�finition du barrage
		barrage.setId(mIdTextField.getText());
		barrage.setTitle(mTitleTextArea.getText());
		barrage.setDescription(mDescriptionTextArea.getText());
		barrage.setLaunchingDate((Date) mLaunchingDateTextField.getValue());
		barrage.setWaterMinLevel(mWaterMinLevelTextField.getValue());
		barrage.setWaterNominalLevel(mWaterNominalLevelTextField
				.getValue());
		barrage.setWaterMaxLevel(mWaterMaxLevelTextField.getValue());

		// 3. Enregistrement du barrage
		Survey.getInstance().addBarrage(barrage, true);
	}

	/**
	 * M�thode permettant de modifier le barrage actif.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 14/12/09
	 */
	private void updateDam() {
		// 1. Actualisation d�finition du barrage
		barrage.setTitle(mTitleTextArea.getText());
		barrage.setDescription(mDescriptionTextArea.getText());
		barrage.setLaunchingDate((Date) mLaunchingDateTextField.getValue());
		barrage.setWaterMinLevel(mWaterMinLevelTextField.getValue());
		barrage.setWaterNominalLevel(mWaterNominalLevelTextField
				.getValue());
		barrage.setWaterMaxLevel(mWaterMaxLevelTextField.getValue());

		// 3. Enregistrement des modifications
		Survey.getInstance().updateBarrage(barrage, true);
	}

	/**
	 * M�thode permettant de v�rifier si les champs saisis sont corrects ou non.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 22/12/09
	 * 
	 * @return true si les champs sont corrects et false sinon.
	 */
	private boolean checkFieldValues() {
		boolean lResult = true;

		// ---------------------------------------------------------------------
		// L'identificateur doit avoir une taille inf�rieure ou �gale �
		// DAM_ID_MAX_LENGTH caract�res.
		// ---------------------------------------------------------------------
		if (mIdTextField.getText().length() > DAM_ID_MAX_LENGTH) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG4"),
					CIniFileReader.getString("ERRMSG3"));
			lResult = false;
		}

		// ---------------------------------------------------------------------
		// Le titre doit avoir une taille inf�rieure ou �gale �
		// DAM_TITLE_MAX_LENGTH caract�res.
		// ---------------------------------------------------------------------
		if (mTitleTextArea.getText().length() > DAM_TITLE_MAX_LENGTH) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG5"),
					CIniFileReader.getString("ERRMSG3"));
			lResult = false;
		}

		// ---------------------------------------------------------------------
		// La description doit avoir une taille inf�rieure ou �gale �
		// DAM_DESCRIPTION_MAX_LENGTH caract�res.
		// ---------------------------------------------------------------------
		if (mDescriptionTextArea.getText().length() > DAM_DESCRIPTION_MAX_LENGTH) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG6"),
					CIniFileReader.getString("ERRMSG3"));
			lResult = false;
		}

		Double lMinLevel = mWaterMinLevelTextField.getValue();
		Double lNominalLevel = mWaterNominalLevelTextField.getValue();
		Double lMaxLevel = mWaterMaxLevelTextField.getValue();

		// -----------------------------------------------------------------
		// La cote du niveau minimal des eaux doit �tre positive ou nulle.
		// -----------------------------------------------------------------
		if (lMinLevel < 0) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG13"),
					CIniFileReader.getString("ERRMSG3"));
			lResult = false;
		}

		// -----------------------------------------------------------------
		// La cote du niveau normal des eaux doit �tre positive ou nulle.
		// -----------------------------------------------------------------
		if (lNominalLevel < 0) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG14"),
					CIniFileReader.getString("ERRMSG3"));
			lResult = false;
		}

		// -----------------------------------------------------------------
		// La cote des plus hautes eaux doit �tre positive ou nulle.
		// -----------------------------------------------------------------
		if (lMaxLevel < 0) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG15"),
					CIniFileReader.getString("ERRMSG3"));
			lResult = false;
		}

		// -----------------------------------------------------------------
		// La cote du niveau minimal des eaux doit �tre inf�rieure ou �gale
		// � celle nu niveau normal.
		// -----------------------------------------------------------------
		if (lMinLevel > lNominalLevel) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG16"),
					CIniFileReader.getString("ERRMSG3"));
			lResult = false;
		}

		// -----------------------------------------------------------------
		// La cote du niveau minimal des eaux doit �tre inf�rieure ou �gale
		// � celle des plus hautes eaux.
		// -----------------------------------------------------------------
		if (lMinLevel > lMaxLevel) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG17"),
					CIniFileReader.getString("ERRMSG3"));
			lResult = false;
		}

		// -----------------------------------------------------------------
		// La cote du niveau normal des eaux doit �tre inf�rieure ou �gale �
		// celle des plus hautes eaux.
		// -----------------------------------------------------------------
		if (lNominalLevel > lMaxLevel) {
			Dialogs.showErrorDialog(CIniFileReader.getString("ERRMSG18"),
					CIniFileReader.getString("ERRMSG3"));
			lResult = false;
		}

		return lResult;
	}
}