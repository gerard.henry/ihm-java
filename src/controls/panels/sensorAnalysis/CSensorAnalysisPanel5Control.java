package controls.panels.sensorAnalysis;

import java.awt.BorderLayout;

import tools.CAnalysis;
import tools.DrawPane;

@SuppressWarnings("serial")
public class CSensorAnalysisPanel5Control extends CSensorAnalysControlPage {
	/** Composants internes. */
	protected DrawPane drawPane;
	
	protected CSensorAnalysisPanel5Control(CSensorAnalysControl manager) {
		super(manager);
	
		// 1. Construction de l'interface
		this.buildUI();
	}

	@Override
	public void applyChanges() {
	}

	@Override
	protected void buildUI() {
		// 1. Initialisation layout de la page
		this.setLayout(new BorderLayout());
		
		// 2. Cr�ation des composants
		this.drawPane = new DrawPane();
		
		// 3. Installation des composants
		this.add(drawPane,BorderLayout.CENTER);
		
	}

	@Override
	public void enterPage(String aFrom) {
		// 1. Chargement du fichier graphique
		this.drawPane.loadFile(CAnalysis.DRAW_CONCAT_FILE);
	}
	
	@Override
	public boolean canDoNext() {
		return false;
	}
	
	@Override
	public boolean canDoPrevious() {
		return true;
	}
	
	@Override
	public String getPageName() {
		return "P5";
	}

	@Override
	public String getPageTitle() {
		return "Graphiques";
	}
}
