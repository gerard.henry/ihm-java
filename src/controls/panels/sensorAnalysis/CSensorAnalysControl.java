package controls.panels.sensorAnalysis;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import tools.CAnalysis;
import tools.CIniFileReader;

import db.Barrage;
import db.Survey;

@SuppressWarnings("serial")
public class CSensorAnalysControl extends JPanel implements ActionListener, PropertyChangeListener {
	/** Composants internes. */
	protected JButton next;
	protected JButton previous;
	protected JButton close;
	protected JButton finish;
	protected JLabel title;
	protected JPanel pages;
	
	/** Panneaux de l'assistant. */
	protected CSensorAnalysisPanel1Control page1;
	protected CSensorAnalysisPanel2Control page2;
	protected CSensorAnalysisPanel3Control page3;
	protected CSensorAnalysisPanel4Control page4;
	protected CSensorAnalysisPanel5Control page5;
	
	/** Donn�es de param�trage de l'analyse. */
	protected CAnalysis parameters;
	
	/** R�f�rence � la page courante. */
	protected CSensorAnalysControlPage current;
	
	/**
	 * COnstructeur
	 * @param aBarrage Barrage propri�taire des instruments/variables � analyser
	 */
	public CSensorAnalysControl(Barrage aBarrage) {
		// 1. Initialisation des attributs
		this.parameters = new CAnalysis(aBarrage);
		
		// 2. COnstruction du panneau
		this.buildUI();
	}
	
	/**
	 * Lecture des donn�es de param�trage de l'analyse
	 * @return Donn�es de param�trage de l'analyse
	 */
	public CAnalysis getParameters() {
		return this.parameters;
	}
	
	/**
	 * Construction du panneau
	 * 
	 */
	protected void buildUI() {
		// 1. Initialisation layout du panneau
		this.setLayout(new BorderLayout());
		
		// 2. Cr�ation des composants
		this.next = new JButton();
		this.previous = new JButton();
		this.close = new JButton();
		this.finish = new JButton();
		this.title = new JLabel();
		this.pages = new JPanel();
		this.page1 = new CSensorAnalysisPanel1Control(this);
		this.page2 = new CSensorAnalysisPanel2Control(this);
		this.page3 = new CSensorAnalysisPanel3Control(this);
		this.page4 = new CSensorAnalysisPanel4Control(this);
		this.page5 = new CSensorAnalysisPanel5Control(this);	
		
		JPanel buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		// 3. Configuration des composants
		previous.setText(CIniFileReader.getString("P5B1"));
		previous.setToolTipText(CIniFileReader.getString("P5B1TTT"));
		next.setText(CIniFileReader.getString("P5B2"));
		next.setToolTipText(CIniFileReader.getString("P5B2TTT"));
		close.setText(CIniFileReader.getString("P5B3"));
		close.setToolTipText(CIniFileReader.getString("P5B3TTT"));
		finish.setText("Enregistrer");
		finish.setToolTipText("Enregistrer les r�sultats des calculs");
		title.setText("Calculs de r�gression");
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setFont(new java.awt.Font("Tahoma",1,12));
		this.pages.setLayout(new CardLayout());
		
		// 4. Installation des composants
		buttons.add(previous);
		buttons.add(next);
		buttons.add(finish);
		buttons.add(close);
		this.pages.add(page1,page1.getPageName());
		this.pages.add(page2,page2.getPageName());
		this.pages.add(page3,page3.getPageName());
		this.pages.add(page4,page4.getPageName());
		this.pages.add(page5,page5.getPageName());
		this.add(buttons,BorderLayout.SOUTH);
		this.add(this.pages,BorderLayout.CENTER);
		this.add(this.title,BorderLayout.NORTH);
		
		// 5. Installation des listeners
		this.next.addActionListener(this);
		this.previous.addActionListener(this);
		this.close.addActionListener(this);
		this.finish.addActionListener(this);
		this.page1.addPropertyChangeListener("Page.Valid",this);
		this.page2.addPropertyChangeListener("Page.Valid",this);
		this.page3.addPropertyChangeListener("Page.Valid",this);
		this.page4.addPropertyChangeListener("Page.Valid",this);		
		this.page5.addPropertyChangeListener("Page.Valid",this);
		
		// 6. Visualisation de la premi�re page
		this.showPage(page1);
	}

	@Override
	public void actionPerformed(ActionEvent aEvent) {
		if ( aEvent.getSource() == this.next) {
			this.doProcessNext();
		} else if ( aEvent.getSource() == this.previous) {
			this.doProcessPrevious();
		} else if ( aEvent.getSource() == this.close) {
			// 1. R�initialisation des caches
			Survey.getInstance().cancelChanges();
			
			// 2. Fermeture de la boite de dialogue
			SwingUtilities.getWindowAncestor(this).setVisible(false);
		}else if ( aEvent.getSource() == this.finish) {
			// 1. Enregistrement des modifications de la base
			Survey.getInstance().applyChanges();
			
			// 2. Fermeture de la boite de dialogue
			SwingUtilities.getWindowAncestor(this).setVisible(false);
		}
	}
	
	/**
	 * Navigation vers la page suivante
	 */
	private void doProcessNext() {
		if ( this.current == page1) {
			this.showPage(page2);
		} else if ( this.current == page2) {
			this.showPage(page3);
		} else if ( this.current == page3) {
			this.showPage(page4);
		} else if ( this.current == page4) {
			this.showPage(page5);
		} 
	}
	
	/**
	 * Navigation vers la page prec�dante
	 */
	private void doProcessPrevious() {
		if ( this.current == page2) {
			this.showPage(page1);
		} else if ( this.current == page3) {
			this.showPage(page2);
		} else if ( this.current == page4) {
			this.showPage(page1);
		} else if ( this.current == page5) {
			this.showPage(page4);
		} 
	}
	
	/**
	 * Visualisation de la page
	 * @param aPage La page � visualiser
	 */
	private void showPage(CSensorAnalysControlPage aPage) {
		if ( this.current != null ) {
			// 1. Enregistrement des donn�es de la page courante
			this.current.applyChanges();
			
			// 2. Notification de sortie de la page
			this.current.leavePage(aPage.getPageName());
		}
		
		// 2. Lecture LayoutManager du panneau d'affichage des pages
		CardLayout layout = (CardLayout)this.pages.getLayout();
		
		// 3. Notification d'entr�e dans la page
		aPage.enterPage(current != null ? current.getPageName() : null);
	
		// 4. Actualisation r�f�rence � la page courante
		current = aPage;
		
		// 5. Visualisation de la page
		layout.show(this.pages, current.getPageName());
		
		// 6. Initialisation etat des controles
		this.next.setEnabled(this.current.canDoNext());
		this.previous.setEnabled(this.current.canDoPrevious());
		this.close.setEnabled(current.canDoCancel());
		this.finish.setEnabled(current.canDoFinish());
		
		// 6. Initialisation titre du panneau
		title.setText("Calculs de regr�ssion : "+this.current.getPageTitle());
	}

	@Override
	public void propertyChange(PropertyChangeEvent aEvent) {
		// 1. Actualisation etat des controles
		this.next.setEnabled(this.current.canDoNext());
		this.previous.setEnabled(this.current.canDoPrevious());
		this.close.setEnabled(current.canDoCancel());
		this.finish.setEnabled(current.canDoFinish());
	}
}
