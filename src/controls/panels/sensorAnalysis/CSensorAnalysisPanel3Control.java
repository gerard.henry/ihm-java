package controls.panels.sensorAnalysis;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;

import cs.ui.DoubleField;

import tools.CIniFileReader;

/**
 * Contr�le de l'�cran d'analyse n�3.
 * 
 * @author : soci�t� CS SI
 * @version : 1.0 du 27/01/10
 */
@SuppressWarnings("serial")
public class CSensorAnalysisPanel3Control extends CSensorAnalysControlPage
		implements ActionListener {
	/** Composants internes. */
	protected JCheckBox mSelectVariablesCheckBox1;
	protected DoubleField mFisherSnedecorOutputThresholdTextField1;
	protected DoubleField mFisherSnedecorInputThresholdTextField1;
	protected JCheckBox mPreselectTermsCheckBox;
	protected DoubleField mFisherSnedecorOutputThresholdTextField2;
	protected DoubleField mFisherSnedecorInputThresholdTextField2;

	/** Format d'�dition des donn�es. */
	protected NumberFormat format = new DecimalFormat("###0.000");

	/** Valeur par d�faut des champs. */
	protected Double defaultValue = new Double(4.);

	/**
	 * Constructeur.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 27/01/10
	 * 
	 * @param pParent
	 *            le widget p�re.
	 */
	public CSensorAnalysisPanel3Control(CSensorAnalysControl pParent) {
		super(pParent);

		// 1. Initialisation validite de la page
		this.isValid = true;
		
		// 2 . Construction de l'interface
		this.buildUI();
	}

	@Override
	public void applyChanges() {
		this.manager.getParameters().sSelectVariables = mSelectVariablesCheckBox1
				.isSelected();
		this.manager.getParameters().sFisherSnedecorInputThreshold1 = (Double) mFisherSnedecorInputThresholdTextField1
				.getValue();
		this.manager.getParameters().sFisherSnedecorOutputThreshold1 = (Double) mFisherSnedecorOutputThresholdTextField1
				.getValue();
		this.manager.getParameters().sPreselectTerms = mPreselectTermsCheckBox
				.isSelected();
		this.manager.getParameters().sFisherSnedecorInputThreshold2 = (Double) mFisherSnedecorInputThresholdTextField2
				.getValue();
		this.manager.getParameters().sFisherSnedecorOutputThreshold2 = (Double) mFisherSnedecorOutputThresholdTextField2
				.getValue();

	}

	@Override
	protected void buildUI() {
		// 1. Initialisation layout du panneau
		this.setLayout(new GridBagLayout());

		// 1. Cr�ation des composants
		JLabel mFisherSnedecorInputThresholdLabel1 = new JLabel();
		JLabel mFisherSnedecorOutputThresholdLabel1 = new JLabel();
		JLabel mFisherSnedecorInputThresholdLabel2 = new JLabel();
		JLabel mFisherSnedecorOutputThresholdLabel2 = new JLabel();

		mSelectVariablesCheckBox1 = new JCheckBox();
		mFisherSnedecorInputThresholdTextField1 = new DoubleField(3);
		mFisherSnedecorOutputThresholdTextField1 = new DoubleField(3);
		mPreselectTermsCheckBox = new JCheckBox();
		mFisherSnedecorOutputThresholdTextField2 = new DoubleField(3);
		mFisherSnedecorInputThresholdTextField2 = new DoubleField(3);

		// 3. Configuration des composants
		mSelectVariablesCheckBox1.setText(CIniFileReader.getString("P5L8"));
		mSelectVariablesCheckBox1.setToolTipText(CIniFileReader
				.getString("P5L8TTT"));
		mFisherSnedecorInputThresholdLabel1.setText(CIniFileReader
				.getString("P5L9"));
		mFisherSnedecorOutputThresholdLabel1.setText(CIniFileReader
				.getString("P5L10"));
		mFisherSnedecorInputThresholdTextField1.setValue(defaultValue);
		mFisherSnedecorInputThresholdTextField1.setToolTipText(CIniFileReader
				.getString("P5L9TTT"));
		mFisherSnedecorOutputThresholdTextField1.setValue(defaultValue);
		mFisherSnedecorOutputThresholdTextField1.setToolTipText(CIniFileReader
				.getString("P5L10TTT"));
		mPreselectTermsCheckBox.setText(CIniFileReader.getString("P5L13"));
		mPreselectTermsCheckBox.setToolTipText(CIniFileReader
				.getString("P5L13TTT"));
		mFisherSnedecorInputThresholdTextField2.setValue(defaultValue);
		mFisherSnedecorInputThresholdTextField2.setToolTipText(CIniFileReader
				.getString("P5L11TTT"));
		mFisherSnedecorOutputThresholdTextField2.setValue(defaultValue);
		mFisherSnedecorOutputThresholdTextField2.setToolTipText(CIniFileReader
				.getString("P5L12TTT"));
		mFisherSnedecorInputThresholdLabel2.setText(CIniFileReader
				.getString("P5L11"));
		mFisherSnedecorOutputThresholdLabel2.setText(CIniFileReader
				.getString("P5L12"));
		this.mFisherSnedecorInputThresholdTextField1.setEnabled(false);
		this.mFisherSnedecorOutputThresholdTextField1.setEnabled(false);
		this.mFisherSnedecorInputThresholdTextField2.setEnabled(false);
		this.mFisherSnedecorOutputThresholdTextField2.setEnabled(false);

		// 4. Cr�ation des contraintes de positionnement
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 3, 10, 3);
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.gridwidth = 2;
		this.add(mSelectVariablesCheckBox1, gbc);

		gbc.gridy = 1;
		gbc.gridwidth = 1;
		gbc.anchor = GridBagConstraints.EAST;
		this.add(mFisherSnedecorInputThresholdLabel1, gbc);

		gbc.gridx = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		this.add(mFisherSnedecorInputThresholdTextField1, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		gbc.anchor = GridBagConstraints.EAST;
		this.add(mFisherSnedecorOutputThresholdLabel1, gbc);

		gbc.gridx = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		this.add(mFisherSnedecorOutputThresholdTextField1, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 2;
		this.add(mPreselectTermsCheckBox, gbc);

		gbc.gridy = 4;
		gbc.gridwidth = 1;
		gbc.anchor = GridBagConstraints.EAST;
		this.add(mFisherSnedecorInputThresholdLabel2, gbc);

		gbc.gridx = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		this.add(mFisherSnedecorInputThresholdTextField2, gbc);

		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridwidth = 1;
		gbc.anchor = GridBagConstraints.EAST;
		this.add(mFisherSnedecorOutputThresholdLabel2, gbc);

		gbc.gridx = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		this.add(mFisherSnedecorOutputThresholdTextField2, gbc);

		// 5. Installation des listeners
		this.mPreselectTermsCheckBox.addActionListener(this);
		this.mSelectVariablesCheckBox1.addActionListener(this);
	}

	@Override
	public String getPageName() {
		return "P3";
	}
	
	@Override
	public String getPageTitle() {
		return "Param�tres de l'analyse (2/2)";
	}
	
	@Override
	public boolean canDoCancel() {
		return true;
	}
	
	@Override
	public void actionPerformed(ActionEvent aEvent) {
		if (aEvent.getSource() == mSelectVariablesCheckBox1) {
			this.mFisherSnedecorInputThresholdTextField1
					.setEnabled(mSelectVariablesCheckBox1.isSelected());
			this.mFisherSnedecorOutputThresholdTextField1
					.setEnabled(mSelectVariablesCheckBox1.isSelected());
		} else if (aEvent.getSource() == mPreselectTermsCheckBox) {
			this.mFisherSnedecorInputThresholdTextField2
					.setEnabled(mPreselectTermsCheckBox.isSelected());
			this.mFisherSnedecorOutputThresholdTextField2
					.setEnabled(mPreselectTermsCheckBox.isSelected());
		}
	}
}