package controls.panels.sensorAnalysis;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import calcul.NASOUS;

import tools.CIniFileReader;
import db.Survey;

/**
 * Contr�le de l'�cran d'analyse n�2.
 * 
 * @author :  soci�t� CS SI
 * @version : 1.0 du 27/01/10
 */
@SuppressWarnings("serial")
public class CSensorAnalysisPanel2Control extends CSensorAnalysControlPage implements DocumentListener, ActionListener {
	/** Composants internes. */
	protected JCheckBox mGraphEditCheckBox;
	protected JFormattedTextField mEndDateTextField;
	protected JFormattedTextField mBeginDateTextField;
	protected JCheckBox mRangeEditCheckBox;
	protected JCheckBox mTableEditCheckBox;
	protected JCheckBox mDrawCurveGCC;
	protected JComboBox mAdjustmentNatureComboBox;
	protected JTextField filePath;
	protected JButton fileBrowse;
	

	/**
	 * Constructeur.
	 * 
	 * @author :  soci�t� CS SI
	 * @version : 1.0 du 27/01/10
	 * 
	 * @param pParent
	 *            le widget p�re.
	 */
	public CSensorAnalysisPanel2Control(CSensorAnalysControl pParent) {
		super(pParent);

		// 1. Initialisation validite de la page
		this.isValid = true;
		
		// 1. Construction de l'interface
		this.buildUI();
	}

	@Override
	public void applyChanges() {
		this.manager.getParameters().sAdjustmentModel = ((NASOUS)mAdjustmentNatureComboBox
				.getSelectedItem());
		this.manager.getParameters().sBeginDate = (Date) mBeginDateTextField
				.getValue();
		this.manager.getParameters().sEndDate = (Date) mEndDateTextField
				.getValue();
		this.manager.getParameters().sTableEdit = mTableEditCheckBox
				.isSelected();
		this.manager.getParameters().sDrawCurveGCC = mDrawCurveGCC
		.isSelected();
		this.manager.getParameters().sGraphEdit = mGraphEditCheckBox
				.isSelected();
		this.manager.getParameters().sRangeEdit = mRangeEditCheckBox
				.isSelected();
		this.manager.getParameters().outFile = filePath.getText();
	}

	@Override
	protected void buildUI() {
		// 1. Initialisation layout du panneau
		this.setLayout(new GridBagLayout());

		// 2. Cr�ation des composants
		JLabel mAdjustmentNatureLabel = new JLabel();
		JLabel mBeginDateLabel = new JLabel();
		JLabel mEndDateLabel = new JLabel();
		JLabel mResultsOutTypeLabel = new JLabel();

		mAdjustmentNatureComboBox = new JComboBox();
		mBeginDateTextField = new JFormattedTextField(Survey.dateFormat);
		mEndDateTextField = new JFormattedTextField(Survey.dateFormat);
		mTableEditCheckBox = new JCheckBox();
		mGraphEditCheckBox = new JCheckBox();
		mRangeEditCheckBox = new JCheckBox();
		mDrawCurveGCC = new JCheckBox();

		filePath = new JTextField(48);
		fileBrowse = new JButton("Parcourir...");
		
		// 3. Configuration des composants
		this.setAlignmentY(0.f);
		mAdjustmentNatureLabel.setText(CIniFileReader.getString("P5L2"));
		mBeginDateLabel.setText(CIniFileReader.getString("P5L3"));
		mBeginDateLabel.setAlignmentX(1.f);
		mEndDateLabel.setText(CIniFileReader.getString("P5L4"));
		mEndDateLabel.setAlignmentX(1.f);
		mResultsOutTypeLabel.setText(CIniFileReader.getString("P5L5"));
		SimpleDateFormat fmt = new SimpleDateFormat("-dd-MM-yyyy-HH-mm-ss");
		File fout = new File("db/analysis/"+this.manager.getParameters().getBarrage().getId()+fmt.format(new Date())+".zip");
		filePath.setText(fout.getAbsolutePath());
		
		if ( this.manager.parameters.getBarrage().getSensor("PLUIE") == null) {
			mAdjustmentNatureComboBox.setModel(new DefaultComboBoxModel(new NASOUS[] {
					new NASOUS(NASOUS.Model.HST),
					new NASOUS(NASOUS.Model.HS)}));			
		} else {
			mAdjustmentNatureComboBox.setModel(new DefaultComboBoxModel(new NASOUS[] {
					new NASOUS(NASOUS.Model.HSTP),
					new NASOUS(NASOUS.Model.HST),
					new NASOUS(NASOUS.Model.HTP),
					new NASOUS(NASOUS.Model.HSP),
					new NASOUS(NASOUS.Model.HS),
					new NASOUS(NASOUS.Model.HP) }));			
		}
		
		mAdjustmentNatureComboBox.setToolTipText(CIniFileReader
				.getString("P5L2TTT"));
		mTableEditCheckBox.setText(CIniFileReader.getString("P5L6"));
		mTableEditCheckBox.setToolTipText(CIniFileReader.getString("P5L6TTT"));
		
		mDrawCurveGCC.setText(CIniFileReader.getString("P5L76"));
		mDrawCurveGCC.setToolTipText(CIniFileReader.getString("P5L76TTT"));
		
		mGraphEditCheckBox.setText(CIniFileReader.getString("P5L7"));
		mGraphEditCheckBox.setToolTipText(CIniFileReader.getString("P5L7TTT"));
		mRangeEditCheckBox.setText(CIniFileReader.getString("P5L75"));
		mRangeEditCheckBox.setToolTipText(CIniFileReader.getString("P5L75TTT"));
		mBeginDateTextField.setToolTipText(CIniFileReader.getString("P5L3TTT"));
		mEndDateTextField.setToolTipText(CIniFileReader.getString("P5L4TTT"));
		mBeginDateTextField.setValue(this.manager.getParameters().getBarrage().getLaunchingDate());
		mEndDateTextField.setValue(new Date());
		
		// 4. Cr�ation des contraintes de positionnement
		GridBagConstraints gbc = new GridBagConstraints();

		// 5. Installation des composants
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 3, 10, 3);
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.weightx = 0;
		gbc.weighty = 0;
		this.add(mAdjustmentNatureLabel, gbc);

		gbc.gridx = 1;
		gbc.gridwidth = 3;
		this.add(mAdjustmentNatureComboBox, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth= 1;
		gbc.anchor = GridBagConstraints.EAST;
		this.add(mBeginDateLabel, gbc);

		gbc.gridx = 1;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		this.add(mBeginDateTextField, gbc);

		gbc.gridx = 2;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.EAST;
		this.add(mEndDateLabel, gbc);

		gbc.gridx = 3;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		this.add(mEndDateTextField, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.EAST;
		this.add(mResultsOutTypeLabel, gbc);

		gbc.gridx = 1;
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.gridwidth = 3;
		this.add(filePath, gbc);
		gbc.gridx = 4;
		gbc.gridwidth = 1;
		this.add(fileBrowse, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 3;
		this.add(mTableEditCheckBox, gbc);

		gbc.gridy = 4;
		this.add(mGraphEditCheckBox, gbc);

		gbc.gridy = 5;
		this.add(mRangeEditCheckBox, gbc);
		
		gbc.gridy = 6;
		this.add(mDrawCurveGCC, gbc);
		
		// 6. Installation des listeners
		this.filePath.getDocument().addDocumentListener(this);
		this.fileBrowse.addActionListener(this);
	}

	@Override
	public String getPageName() {
		return "P2";
	}
	
	@Override
	public String getPageTitle() {
		return "Param�tres de l'analyse (1/2)";
	}
	
	@Override
	public boolean canDoCancel() {
		return true;
	}

	@Override
	public void changedUpdate(DocumentEvent arg0) {
		checkFields();
	}

	@Override
	public void insertUpdate(DocumentEvent arg0) {
		checkFields();
		
	}

	@Override
	public void removeUpdate(DocumentEvent arg0) {
		checkFields();		
	}

	
	/**
	 * Controle des valeurs edit�es
	 */
	protected void checkFields() {
		// 1. Lecture donn�es edit�es
		String path = (String) this.filePath.getText();
				

		if ( path.trim().length() == 0 ) {
			this.setPageValid(false);
		} else {
			// 1. Cr�ation r�f�rence au fichier
			File file = new File(path.trim());
			
			if ( file.exists()) {
				if ( file.isDirectory() || !file.canWrite()) {
					this.setPageValid(false);
				}
			} else {
				// 1. Lecture r�pertoire parent
				File dir = file.getParentFile();
				
				this.setPageValid(dir != null && dir.canWrite());
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// 1. Cr�ation d'un s�lecteur de fichier
		JFileChooser chooser = new JFileChooser();
		
		// 2. Configuration du s�lecteur de fichier
		chooser.setMultiSelectionEnabled(false);
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		chooser.setSelectedFile(new File(this.filePath.getText()));
		
		// 3. Ouverture du dialogue
		if ( chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			// 1. Actualisation chemin du fichier s�lectionn�
			this.filePath.setText(chooser.getSelectedFile().getAbsolutePath());
		}		
	}

}