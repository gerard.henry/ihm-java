package controls.panels.sensorAnalysis;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public abstract class CSensorAnalysControlPage extends JPanel{
	/** Validit� des donn�es edit�es dans la page. */
	protected boolean isValid;
	
	/** R�f�rence au gestionnaire d'analyse des instruments/variable. */
	protected CSensorAnalysControl manager;
	
	/**
	 * Constructeur
	 * @param aManager Gestionnaire d'analyse des instruments/variable
	 */
	protected CSensorAnalysControlPage(CSensorAnalysControl aManager) {
		// 1. Initialisation des attributs
		this.manager = aManager;
	}
	
	/**
	 * Les donn�es edit�es dans la page sont-elles valides
	 * @return true si les donn�es sont valides
	 */
	public boolean isPageValid() {
		return this.isValid;
	}
	
	/**
	 * Modification validit� de la page
	 * @param aPageValid Validit� de la pabe
	 */
	public void setPageValid(boolean aPageValid) {
		this.firePropertyChange("Page.Valid", this.isValid,
				this.isValid = aPageValid);	
	}
	/**
	 * Autorisation d'affichage de la page pr�c�dente
	 * @return true si la page pr�c�dente peut-�tre affich�e
	 */
	public boolean canDoPrevious() {
		return true;
	}
	
	/**
	 * Autorisation d'affichage de la page suivante
	 * @return true si la page suivante peut-�tre affich�e
	 */
	public boolean canDoNext() {
		return this.isValid;
	}
	
	/**
	 * Autorisation de validation de l'assistant
	 * @return true si la validation de l'assistant est permise
	 */
	public boolean canDoFinish() {
		return false;
	}

	/**
	 * Autorisation d'abandon de l'assistant
	 * @return true si l'abandon de l'assistant est permise
	 */
	public boolean canDoCancel() {
		return false;
	}
	
	/**
	 * Notification de visualisation de la page
	 * @param aFrom Nom de la page pr�c�dente
	 */
	public void enterPage(String aFrom) {
		
	}
	
	/**
	 * Notification de sortie de la page
	 * @param aTo Nom de la prochaine page
	 */
	public void leavePage(String aTo) {
		
	}
	
	/**
	 * Lecture titre de la page
	 * @return titre de la page
	 */
	public String getPageTitle() {
		return this.getPageName();
	}
	
	/**
	 * Lecture nom de la page
	 * @return Nom de la page
	 */
	public abstract String getPageName();
	
	/**
	 * Enregistrement des donn�es de la page
	 */
	public abstract void applyChanges();
	
	/**
	 * Construction de l'interface
	 */
	protected abstract void buildUI();
}
