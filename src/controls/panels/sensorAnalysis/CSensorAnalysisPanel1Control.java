package controls.panels.sensorAnalysis;

import java.awt.BorderLayout;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import tools.CIniFileReader;
import db.Sensor;
import db.SensorMeasures;
import db.Survey;

/**
 * Contr�le de l'�cran d'analyse n�1.
 * 
 * @author : soci�t� CS SI
 * @version : 1.0 du 27/01/10
 */
@SuppressWarnings("serial")
public class CSensorAnalysisPanel1Control extends CSensorAnalysControlPage
		implements ListSelectionListener {

	protected class SensorModel extends AbstractTableModel {

		/**
		 * Constructeur
		 */
		public SensorModel() {
		}

		@Override
		public String getColumnName(int aColumn) {
			switch (aColumn) {
			case 0:
				return "Identificateur";
			case 1:
				return "Installation";
			case 2:
				return "Regression";
			case 3:
				return "Premi�re mesure";
			case 4:
				return "Derni�re mesure";
			case 5:
				return "Nb mesures";
			}

			return null;
		}

		@Override
		public int getColumnCount() {
			return 6;
		}

		@Override
		public int getRowCount() {
			return sensors.size();
		}

		@Override
		public Object getValueAt(int aRow, int aColumn) {
			// 1. Lecture d�finition de l'instrument/variable
			Sensor sensor = sensors.get(aRow);

			// 2. Lecture des mesures de l'instrument/variable
			SensorMeasures measures = sensor.getMeasures();

			switch (aColumn) {
			case 0:
				return sensor.getId();
			case 1:
				return Survey.format(sensor.getInstallationDate());
			case 2:
				return Survey.format(sensor.getRegressionDate());
			case 3:
				return (measures == null ? "" : Survey.format(measures
						.getDateFirstMeasure()));
			case 4:
				return (measures == null ? "" : Survey.format(measures
						.getDateLastMeasure()));
			case 5:
				return (measures == null ? "" : measures.getMeasureCount());
			}

			return null;
		}
	}

	/** Composants internes. */
	protected JTable mSensorsList;

	/** Liste des instruments/variables. */
	protected List<Sensor> sensors;

	/**
	 * Constructeur.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 27/01/10
	 * 
	 * @param pParent
	 *            le widget p�re.
	 */
	public CSensorAnalysisPanel1Control(CSensorAnalysControl pParent) {
		super(pParent);

		// 1. Initialisation validite de la page
		this.isValid = false;

		// 2. Lecture liste des instruments/variables disponibles
		sensors = manager.getParameters().getBarrage().getSensors();

		// 3. Suppression des instruments sans mesures
		for ( Iterator<Sensor> i = sensors.iterator() ; i.hasNext(); ) {
			// 1. Lecture r�f�rence au ieme instrument/variable
			Sensor isensor = i.next();
			
			if (isensor.getMeasuresCount() == 0 ) {
				i.remove();
			}
		}
		
		// 3. Construction de l'interface
		this.buildUI();
	}

	@Override
	protected void buildUI() {
		// 1. Initialisation layout de la page
		this.setLayout(new BorderLayout());

		// 2. Cr�ation des composants internes
		this.mSensorsList = new JTable(new SensorModel());

		// 3. Configuration des composants
		this.setBorder(BorderFactory.createTitledBorder(CIniFileReader
				.getString("P5L1")));
		this.mSensorsList.setRowSorter(new TableRowSorter(this.mSensorsList.getModel()));
		
		// 4. Installation des composants
		this.add(new JScrollPane(this.mSensorsList), BorderLayout.CENTER);

		// 5. Installation des listeners
		this.mSensorsList.getSelectionModel().addListSelectionListener(this);
	}

	@Override
	public boolean canDoPrevious() {
		return false;
	}

	@Override
	public boolean canDoCancel() {
		return true;
	}
	
	@Override
	public void applyChanges() {
		// 1. Lecture liste des instruments/variables s�lectionn�s
		int indexes[] = this.mSensorsList.getSelectedRows();

		// 2. Cr�ation liste des instruments/variables s�lectionn�s
		Sensor selection[] = new Sensor[indexes.length];

		// 3. Initialisation liste des instruments/variables s�lectionn�s
		for (int i = 0; i < indexes.length; i++) {
			selection[i] = sensors.get(mSensorsList.convertRowIndexToModel(indexes[i]));
		}

		// 4. Actualisation des parametres de l'analyse
		this.manager.getParameters().sItemsList = selection;
	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		if (this.mSensorsList.getSelectedRowCount() != 0) {
			this.firePropertyChange("Page.Valid", this.isValid,
					this.isValid = true);
		} else {
			this.firePropertyChange("Page.Valid", this.isValid,
					this.isValid = false);
		}
	}

	@Override
	public String getPageName() {
		return "P1";
	}
	
	@Override
	public String getPageTitle() {
		return "S�lection des instruments";
	}
}