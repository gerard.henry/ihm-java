package controls.panels.sensorAnalysis;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import tools.CAnalysis;

import db.SensorResultats;
import db.Survey;

@SuppressWarnings("serial")
public class CSensorAnalysisPanel4Control extends CSensorAnalysControlPage
		implements PropertyChangeListener {
	/** Composants internes. */
	protected JProgressBar progress;
	protected JTextPane console;
	protected JTable resultats;

	public class AnalysisLogHandler extends Handler {

		@Override
		public void close() throws SecurityException {
		}

		@Override
		public void flush() {
		}

		@Override
		public void publish(final LogRecord aRecord) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					// 1. Lecture r�f�rence au document
					StyledDocument document = console.getStyledDocument();

					// 2. Lecture position dans le document
					int start = document.getLength();
					try {
						// 1. Enregistrement du message de trace
						document.insertString(start, aRecord.getMessage()
								+ "\n", document.getStyle(aRecord.getLevel()
								.getName()));

						// 2. Lecture position dans le document
						int end = document.getLength();
					} catch (BadLocationException e) {
					}
				}
			});
		}
	}

	protected class SensorsResultatModel extends AbstractTableModel {
		/** R�sultats de l'analyse des instruments/variable. */
		protected SensorResultats resultats[];

		/**
		 * Constructeur
		 * 
		 * @param aCount
		 *            Nombre de r�sultats de mesure
		 */
		public SensorsResultatModel(int aCount) {
			// 1. Initialisation des attributs
			this.resultats = new SensorResultats[aCount];
		}

		/**
		 * Actualisation r�sultats de l'analyse
		 * 
		 * @param aList
		 *            Liste courante des r�sultats
		 */
		public void setResultats(List<SensorResultats> aList) {
			// 1. Actualisation valeur des r�sultats
			for (int i = 0; i < aList.size() && i < this.resultats.length; i++) {
				this.resultats[i] = aList.get(i);
			}

			// 2. Notification des modifications
			this.fireTableDataChanged();
		}

		@Override
		public String getColumnName(int aColumn) {
			switch (aColumn) {
			case 0:
				return "Instrument";
			case 1:
				return "Nasous";
			case 2:
				return "Dur�e";
			case 3:
				return "D�but";
			case 4:
				return "Fin";
			case 5:
				return "Nb valeurs";
			case 6:
				return "R2";
			case 7:
				return "MOYCOTE";
			case 8:
				return "SIGCOTE";
			case 9:
				return "TERMCONST";
			case 10:
				return "T";
			case 11:
				return "EXPT";
			case 12:
				return "EXPMT";
			case 13:
				return "SIN";
			case 14:
				return "COS";
			case 15:
				return "SICO";
			case 16:
				return "SIN2";
			case 17:
				return "Z";
			case 18:
				return "Z2";
			case 19:
				return "Z3";
			case 20:
				return "Z4";
			case 21:
				return "P1";
			case 22:
				return "P2";
			case 23:
				return "P3";
			case 24:
				return "P4";
			case 25:
				return "P56";
			case 26:
				return "P78";
			default:
				return null;
			}
		}

		@Override
		public int getColumnCount() {
			return 27;
		}

		@Override
		public int getRowCount() {
			return resultats.length;
		}

		@Override
		public Object getValueAt(int aRow, int aColumn) {
			// 1. Lecture r�sultat de l'analyse
			SensorResultats resultat = this.resultats[aRow];

			if (resultat == null) {
				return null;
			} else {
				switch (aColumn) {
				case 0:
					return resultat.getId();
				case 1:
					return resultat.getLNasous();
				case 2:
					return resultat.getLDuration();
				case 3:
					return Survey.format(resultat.getLAnalyseBeginDate());
				case 4:
					return Survey.format(resultat.getLAnalyseEndDate());
				case 5:
					return resultat.getLValuesCount();
				case 6:
					return Survey.format(resultat.getLR2());
				case 7:
					return Survey.format(resultat.getLMeanCote());
				case 8:
					return Survey.format(resultat.getLSigCote());
				case 9:
					return Survey.format(resultat.getLConstantTerm());
				case 10:
					return Survey.format(resultat.getLT());
				case 11:
					return Survey.format(resultat.getLExpT());
				case 12:
					return Survey.format(resultat.getLExpMT());
				case 13:
					return Survey.format(resultat.getLSin());
				case 14:
					return Survey.format(resultat.getLCos());
				case 15:
					return Survey.format(resultat.getLSico());
				case 16:
					return Survey.format(resultat.getLSin2());
				case 17:
					return Survey.format(resultat.getLZ());
				case 18:
					return Survey.format(resultat.getLZ2());
				case 19:
					return Survey.format(resultat.getLZ3());
				case 20:
					return Survey.format(resultat.getLZ4());
				case 21:
					return Survey.format(resultat.getLP1());
				case 22:
					return Survey.format(resultat.getLP2());
				case 23:
					return Survey.format(resultat.getLP3());
				case 24:
					return Survey.format(resultat.getLP4());
				case 25:
					return Survey.format(resultat.getLP56());
				case 26:
					return Survey.format(resultat.getLP78());
				default:
					return null;
				}
			}
		}
	}

	/** Intercepteur des traces d'ex�cution. */
	protected AnalysisLogHandler logHandler;

	/** Drapeau indiquent que les calculs sont en cours. */
	protected boolean isRunning;
	
	/**
	 * Constructeur.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 27/01/10
	 * 
	 * @param pParent
	 *            le widget p�re.
	 */
	public CSensorAnalysisPanel4Control(CSensorAnalysControl pParent) {
		super(pParent);

		// 1. Initialisation des attributs
		this.isValid = false;
		this.logHandler = new AnalysisLogHandler();
		this.isRunning = false;

		// 2 . Construction de l'interface
		this.buildUI();
	}

	@Override
	public void applyChanges() {
	}

	@Override
	protected void buildUI() {
		// 1. Initialisation layout de la page. */
		this.setLayout(new BorderLayout());

		// 2. Cr�ation des composants
		this.progress = new JProgressBar();
		this.console = new JTextPane();
		this.resultats = new JTable(new SensorsResultatModel(1));

		JScrollPane consoleSP = new JScrollPane(this.console);
		JScrollPane resultatsSP = new JScrollPane(this.resultats);
		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

		// 3. Configuration des composants
		console.setEditable(false);
		consoleSP.setPreferredSize(new Dimension(400, 200));
		resultatsSP.setPreferredSize(new Dimension(400, 200));
		this.installConsoleStyles(this.console.getStyledDocument());
		this.resultats.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		this.progress.setStringPainted(true);

		// 4. Installation des composants
		splitPane.setLeftComponent(resultatsSP);
		splitPane.setRightComponent(consoleSP);
		this.add(this.progress, BorderLayout.NORTH);
		this.add(splitPane, BorderLayout.CENTER);

	}

	@Override
	public String getPageName() {
		return "P4";
	}

	@Override
	public String getPageTitle() {
		return "Ex�cution des calculs";
	}

	@Override
	public boolean canDoPrevious() {
		return false;
	}

	@Override
	public boolean canDoNext() {
		// 1. Cr�ation r�f�rence au fichier graphique
		File graFile = new File(CAnalysis.DRAW_CONCAT_FILE);
		
		return graFile.exists();
	}

	@Override
	public boolean canDoFinish() {
		return this.isValid;
	}
	
	@Override
	public boolean canDoCancel() {
		return !this.isRunning;
	}
	
	@Override
	public void enterPage(String aFrom) {
		if (aFrom.equals("P3")) {
			// 1. Initialisation mod�le des r�sultats
			this.resultats.setModel(new SensorsResultatModel(this.manager
					.getParameters().sItemsList.length));

			// 2. R�initialisation contenu de la console
			this.console.setText("");

			// 3. Initialisation handler des traces d'ex�cution
			this.manager.getParameters().getLogger()
					.addHandler(this.logHandler);

			// 4. Installation des listeners
			this.manager.getParameters().addPropertyChangeListener(this);

			// 4. Lancement de l'analyse
			this.manager.getParameters().execute();
			
			// 5. Actualisation drapeau d'�tat
			this.isRunning = true;
		}
	}

	@Override
	public void leavePage(String aTo) {
		if (aTo.equals("P4")) {
			// 1 . D�sinstallation handler des traces d'ex�cution
			this.manager.getParameters().getLogger().removeHandler(
					this.logHandler);

			// 2. D�sinstallation des listeners
			this.manager.getParameters().removePropertyChangeListener(this);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent aEvent) {
		if ("progress".equals(aEvent.getPropertyName())) {
			progress.setValue((Integer) aEvent.getNewValue());
		} else if ("resultats".equals(aEvent.getPropertyName())) {
			// 1. Lecture liste courante des resultats de l'analyse
			List<SensorResultats> list = (List<SensorResultats>) aEvent
					.getNewValue();

			// 2. Actualisation table de visualisation des r�sultats
			((SensorsResultatModel) this.resultats.getModel())
					.setResultats(list);
		} else if ("sensor".equals(aEvent.getPropertyName())) {
			// 1. Lecture nom de l'instrument en cours d'analyse
			String sensorId = (String) aEvent.getNewValue();

			// 2. Actualisation legende de la barre de progression
			this.progress.setString(sensorId);
		}else if ("done".equals(aEvent.getPropertyName())) {
			// 1. Lecture liste courante des resultats de l'analyse
			List<SensorResultats> list = (List<SensorResultats>) aEvent
					.getNewValue();
			
			// 2. Actualisation drapeau d'�tat
			this.isRunning = false;
			
			for ( int i = 0 ; i < list.size(); i++) {
				if ( list.get(i) != null) {
					// 1. Au moins un r�sultat doit etre valide 
					this.firePropertyChange("Page.Valid",this.isValid,this.isValid = true);
					
					// 2. Arret du parcours
					return;
				}
			}
			
			// 3. Pour forcer la mise � jour
			this.firePropertyChange("Page.Valid", true, false);
		}
	}

	/**
	 * Installation des styles de textes de la console
	 * 
	 * @param aDocument
	 *            Document propri�taire des styles
	 */
	private void installConsoleStyles(StyledDocument aDocument) {
		// 1. Lecture r�f�rence au style par d�faut
		Style defStyle = StyleContext.getDefaultStyleContext().getStyle(
				StyleContext.DEFAULT_STYLE);

		// 2. Cr�ation styles d'afichage des info/erreurs/warnings
		Style infoStyle = aDocument.addStyle(Level.INFO.getName(), defStyle);
		Style errorStyle = aDocument
				.addStyle(Level.SEVERE.getName(), infoStyle);
		Style warningStyle = aDocument.addStyle(Level.WARNING.getName(),
				infoStyle);

		// 3. Configuration des styles
		StyleConstants.setFontSize(errorStyle, 14);
		StyleConstants.setFontFamily(infoStyle, "SansSerif");

		StyleConstants.setBackground(errorStyle, Color.red);

		StyleConstants.setBackground(warningStyle, Color.orange);
		StyleConstants.setItalic(warningStyle, false);
	}
}
