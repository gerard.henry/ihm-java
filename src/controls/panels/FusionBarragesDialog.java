package controls.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import db.Barrage;
import db.Sensor;
import db.SensorMeasures;
import db.Survey;

@SuppressWarnings("serial")
public class FusionBarragesDialog extends JDialog implements ActionListener {
	/** Composants internes. */
	protected JComboBox frBarrage;
	protected JComboBox toBarrage;
	protected JButton apply;
	protected JButton close;
	protected JButton save;
	protected JTextPane console;

	public class ConsoleHandler extends Handler {

		@Override
		public void close() throws SecurityException {
		}

		@Override
		public void flush() {
		}

		@Override
		public void publish(final LogRecord aRecord) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					// 1. Lecture r�f�rence au document
					StyledDocument document = console.getStyledDocument();

					// 2. Lecture position dans le document
					int start = document.getLength();
					try {
						// 1. Enregistrement du message de trace
						document.insertString(start, aRecord.getMessage()
								+ "\n", document.getStyle(aRecord.getLevel()
								.getName()));
					} catch (BadLocationException e) {
					}
				}
			});
		}
	}

	/** Logger des traces d'ex�cution. */
	protected Logger logger = Logger.getLogger(FusionBarragesDialog.class
			.getName());

	/** Drapeau de pr�sence de donn�es � enregistrer. */
	protected boolean isDirty;

	/**
	 * Constructeur
	 */
	public FusionBarragesDialog() {
		// 1. Initialisation des attributs
		this.isDirty = false;

		// 2. Construction de l'interface
		this.buildUI();

		// 3. Installation handler des traces d'ex�cution
		this.logger.addHandler(new ConsoleHandler());
	}

	/**
	 * Construction de l'interface
	 * 
	 */
	protected void buildUI() {
		// 1. Initialisation layout du panneau
		this.getContentPane().setLayout(new BorderLayout());

		// 2. Lecture liste des noms de barrage
		List<String> names = Survey.getInstance().getBarragesNames();

		// 3. Cr�ation des composants
		this.frBarrage = new JComboBox(names.toArray());
		this.toBarrage = new JComboBox(names.toArray());
		this.console = new JTextPane();
		this.close = new JButton("Fermer");
		this.save = new JButton("Enregistrer");
		this.apply = new JButton("Fusionner");

		JPanel buttonPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JPanel choicePane = new JPanel(new GridLayout(2, 2));
		JScrollPane consoleSP = new JScrollPane(this.console);

		// 4. Configuration des composants
		console.setEditable(false);
		consoleSP.setPreferredSize(new Dimension(400, 200));
		this.installConsoleStyles(this.console.getStyledDocument());

		// 5. Installation des composants
		choicePane.add(new JLabel("Barrage � mettre � jour"));
		choicePane.add(this.frBarrage);
		choicePane.add(new JLabel("Barrage de mise � jour"));
		choicePane.add(this.toBarrage);
		buttonPane.add(this.apply);
		buttonPane.add(this.save);
		buttonPane.add(this.close);
		this.getContentPane().add(choicePane, BorderLayout.NORTH);
		this.getContentPane().add(consoleSP, BorderLayout.CENTER);
		this.getContentPane().add(buttonPane, BorderLayout.SOUTH);

		// 6. Installation des listeners
		this.frBarrage.addActionListener(this);
		this.toBarrage.addActionListener(this);
		this.close.addActionListener(this);
		this.apply.addActionListener(this);
		this.save.addActionListener(this);

		// 7. Initialisation etat des composants
		this.close.setEnabled(true);
		this.save.setEnabled(false);
		this.apply.setEnabled(false);

		// 8. Initialisation propri�tes du dialogue
		this.setModal(true);
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.setTitle(Survey.getApplicationName() + "- Mise � jour barrage");
	}

	/**
	 * Ex�cution de la fusion des deux barrages
	 * 
	 * @param aTo
	 *            Barrage r�sultat de la fusion
	 * @param aFrom
	 *            Barrage source
	 */
	protected void doFusion(Barrage aTo, Barrage aFrom) {
		// 1. R�initialisation de la console
		this.console.setText("");

		// 2. Impression message de trace
		logger.log(Level.INFO, "Mise � jour du barrage " + aTo.getId()
				+ " � partir du barrage " + aFrom.getId());

		// 3. Lecture liste des instruments � importer
		List<Sensor> sensors = aFrom.getSensors();

		for (int i = 0; i < sensors.size(); i++) {
			// 1. Lecture d�finition du ieme instrument/variable
			Sensor isensor = sensors.get(i);

			// 2. Lecture liste des mesures de l'instrument
			SensorMeasures imeasures = isensor.getMeasures();

			// 3. Lecture d�finition de l'instrument dans le barrage � mettre �
			// joue
			Sensor toSensor = aTo.getSensor(isensor.getId());

			if (toSensor == null) {
				// 1. Cr�ation d'une copie de l'instrument/variable
				toSensor = isensor.copy();

				// 2. Rattagement au barrage � mettre � jour
				toSensor.setBarrage(aTo);

				// 3. Impression trace
				logger.log(Level.INFO, "Cr�ation "
						+ (toSensor.getNature().equals("I") ? "Instrument "
								: "Variable ") + toSensor.getId());

				if (imeasures != null) {
					// 1. Cr�ation d'une copie de la liste de mesures
					SensorMeasures toMeasures = imeasures.copy();

					// 2. Rattachement � l'instrument
					toMeasures.setSensor(toSensor);

					// 3. Ajout de l'instrument/variable au barrage destination
					Survey.getInstance().addSensor(toSensor, false);

					// 4. Enregistrement liste de mesures
					Survey.getInstance()
							.updateSensorMeasures(toMeasures, false);
				} else {
					// 1. Ajout de l'instrument/variable au barrage destination
					Survey.getInstance().addSensor(toSensor, false);
				}
			} else {
				if (imeasures != null) {
					// 1. Lecture liste des mesures du barrage � mettre � jour
					SensorMeasures toMeasures = toSensor.getMeasures();

					if (toMeasures == null) {
						// 1. Cr�ation de la liste de mesures
						toMeasures = new SensorMeasures(toSensor);
					}

					// 1. Lecture liste des dates de mesures
					List<Date> idates = imeasures.getDatesMeasure();

					for (int j = 0; j < idates.size(); j++) {
						// 1. Lecture date de la jeme mesure
						Date jdate = idates.get(j);

						// 2. Lecture valeur de la mesure
						Double jvalue = imeasures.getMeasure(jdate);

						if (toMeasures.hasMeasure(jdate)) {
							// 1. Lecture valeur actuelle de la mesure
							Double toValue = toMeasures.getMeasure(jdate);

							if (!toValue.equals(jvalue)) {
								// 1. Impression trace
								logger.log(Level.WARNING, (toSensor.getNature()
										.equals("I") ? "Instrument "
										: "Variable ")
										+ toSensor.getId()
										+ " : Mesure("
										+ Survey.format(jdate)
										+ ","
										+ Survey.format(jvalue) + ") ignor�e");
							}
						} else {
							// 1. Enregistrement de la nouvelle mesure
							toMeasures.addMeasure(jdate, jvalue);
							
							// 2. Lecture valeur de la mesure brute
							Double rawValue = imeasures.getRawValue(jdate);
							
							if ( rawValue != null ) {
								// 1. Enregistrement valeur brute relev�e
								toMeasures.setRawValue(jdate, rawValue);
							}
						}
					}

					// 3. Enregistrement de la liste de mesures
					Survey.getInstance()
							.updateSensorMeasures(toMeasures, false);
				}
			}
		}
		this.isDirty = true;
	}

	@Override
	public void actionPerformed(ActionEvent aEvent) {
		if (aEvent.getSource() == this.frBarrage
				|| aEvent.getSource() == toBarrage) {
			if (frBarrage.getSelectedIndex() != toBarrage.getSelectedIndex()) {
				this.apply.setEnabled(true);
			} else {
				this.apply.setEnabled(false);
			}
		} else if (aEvent.getSource() == this.close) {
			if (this.isDirty) {
				// 1. Demande de confirmation de la sortie sans sauvegarde
				int choice = JOptionPane
						.showConfirmDialog(
								this,
								"Le barrage "
										+ this.frBarrage.getSelectedItem()
										+ " a chang�. D�sirez-vous enregistrer les modifications ?",
								Survey.getApplicationName(),
								JOptionPane.YES_NO_OPTION);

				if (choice == JOptionPane.YES_OPTION) {
					// 1. Enregistrement des modifications
					Survey.getInstance().applyChanges();
				}
			}
			// 2. Nettoyage des caches
			Survey.getInstance().cancelChanges();

			// 3. fermeture du dialogue
			this.setVisible(false);

			// 4. Lib�ration des ressources
			this.dispose();

		} else if (aEvent.getSource() == this.apply) {
			// 1. Lecture nom des barrages
			String fromName = (String) this.frBarrage.getSelectedItem();
			String toName = (String) this.toBarrage.getSelectedItem();

			// 2. Ex�cution de la fusion
			this.doFusion(Survey.getInstance().getBarrage(fromName), Survey
					.getInstance().getBarrage(toName));

			// 3. Autorisation de fusion
			this.save.setEnabled(true);
			this.frBarrage.setEnabled(false);
			this.toBarrage.setEnabled(false);
			this.apply.setEnabled(false);
		} else if (aEvent.getSource() == this.save) {
			// 1. Sauvegarde des modifications
			Survey.getInstance().applyChanges();

			// 2. Actualisation etat des controles
			this.save.setEnabled(false);
			this.apply.setEnabled(false);
			this.frBarrage.setEnabled(true);
			this.toBarrage.setEnabled(true);

			// 3. Actualisation drapeau de pr�sence de donn�es � enregistrer
			this.isDirty = false;
		}
	}

	/**
	 * Installation des styles de textes de la console
	 * 
	 * @param aDocument
	 *            Document propri�taire des styles
	 */
	private void installConsoleStyles(StyledDocument aDocument) {
		// 1. Lecture r�f�rence au style par d�faut
		Style defStyle = StyleContext.getDefaultStyleContext().getStyle(
				StyleContext.DEFAULT_STYLE);

		// 2. Cr�ation styles d'afichage des info/erreurs/warnings
		Style infoStyle = aDocument.addStyle(Level.INFO.getName(), defStyle);
		Style errorStyle = aDocument
				.addStyle(Level.SEVERE.getName(), infoStyle);
		Style warningStyle = aDocument.addStyle(Level.WARNING.getName(),
				infoStyle);

		// 3. Configuration des styles
		StyleConstants.setFontSize(errorStyle, 14);
		StyleConstants.setFontFamily(infoStyle, "SansSerif");

		StyleConstants.setBackground(errorStyle, Color.red);

		StyleConstants.setBackground(warningStyle, Color.orange);
		StyleConstants.setItalic(warningStyle, false);
	}
}
