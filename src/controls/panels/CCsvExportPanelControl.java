package controls.panels;

import gui.panels.CCsvExportPanelGui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import tools.CIniFileReader;
import controls.CSurveyControl;
import cs.ui.Dialogs;
import db.Barrage;
import db.Sensor;
import db.SensorMeasures;

/**
 * Contr�le de l'�cran d'exportation des donn�es au format Excel.
 * 
 * @author : soci�t� CS SI
 * @version : 1.0 du 03/02/10
 */
@SuppressWarnings("serial")
public class CCsvExportPanelControl extends CCsvExportPanelGui {
	/** R�f�rence au barrage actif. */
	protected Barrage barrage;

	/** Format d'impression des dates et nombres. */
	protected static final SimpleDateFormat format0 = new SimpleDateFormat(
			"dd/MM/yyyy");
	protected static final SimpleDateFormat format1 = new SimpleDateFormat(
			"yyyy mm dd");
	protected static final SimpleDateFormat format2 = new SimpleDateFormat(
			"dd MM yy");
	protected static final DecimalFormat formatn = new DecimalFormat(
			"####0.000");

	/**
	 * Constructeur.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 03/02/10
	 * 
	 * @param pParent
	 *            le widget p�re.
	 */
	public CCsvExportPanelControl(CSurveyControl pParent) {
		super(pParent);

		// 1. Lecture r�f�rence au barrage actif
		this.barrage = pParent.getBarrage();

		// 2. Lecture liste des instruments/variables du barrage courant
		List<Sensor> sensors = barrage.getSensors();

		// 3. Cr�ation mod�le de la liste de s�lection
		SensorModel mSensorsListModel = new SensorModel(sensors);

		// 4. Affectation du mod�le de la liste de s�lection au composant
		mSensorsTable.setModel(mSensorsListModel);

		// 5. Initialisation premi�re et derni�re date valide
		this.mBeginDateTextField.setValue(barrage.getDateFirstMeasure());
		this.mEndDateTextField.setValue(barrage.getDateLastMeasure());

		// 6. Initialisation des controles
		this.mExportButton.setEnabled(false);

		connectListeners();
	}

	/**
	 * Permet de connecter les composants � leurs �couteurs.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 03/02/10
	 */
	private void connectListeners() {
		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant d'acc�der � l'�cran suivant.
		// ---------------------------------------------------------------------
		mExportButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				try {
					exportCsvFile();
				} catch (FileNotFoundException lException1) {
					lException1.printStackTrace();
				} catch (ParseException lException2) {
					lException2.printStackTrace();
				}
			}
		});

		// ---------------------------------------------------------------------
		// Connexion de l'�couteur permettant de fermer la fen�tre.
		// ---------------------------------------------------------------------
		mCloseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent lEvent) {
				mParent.closeView();
			}
		});

		mSensorsTable.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent arg0) {
						// 1. Actualisation etat des controles
						mExportButton.setEnabled(mSensorsTable
								.getSelectedRowCount() != 0);
					}
				});
	}

	/**
	 * Permet d'exporter les donn�es sous la forme d'un fichier CSV.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 03/02/10
	 * 
	 * @throws FileNotFoundException
	 */
	private void exportCsvFile() throws FileNotFoundException, ParseException {
		// 1. Cr�ation d'un s�lecteur de fichier
		JFileChooser chooser = new JFileChooser() {
			@Override
			public String getName(File aFile) {
				return aFile.getName();
			}
		};

		// 2. Configuration du s�lecteur de fichier
		chooser.setCurrentDirectory(new File("db/export"));
		chooser.setMultiSelectionEnabled(false);
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

		// 3. Ouverture du dialogue
		if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			// 1. Lecture chemin du fichier s�lectionn�
			String lFileName = chooser.getSelectedFile().getAbsolutePath();

			// 2. Cr�ation Writer au format CSV
			PrintWriter lPrintWriter = new PrintWriter(lFileName);

			// 3. Lecture liste des instruments/variables du barrage courant
			List<Sensor> sensors = barrage.getSensors();

			// 4. Lecture index des instruments s�lectionn�s. */
			int indexes[] = mSensorsTable.getSelectedRows();

			// 5. Cr�ation liste des mesures des instruments/variables
			// s�lectionn�s
			SensorMeasures selection[] = new SensorMeasures[indexes.length];

			// 6. Initialisation de la s�lection courante
			for (int i = 0; i < indexes.length; i++) {
				// 1. Lecture liste des mesures de l'instrument/variable
				SensorMeasures iMeasures = sensors.get(indexes[i])
						.getMeasures();

				if (iMeasures == null) {
					// 1. Cr�ation d'une liste de mesures vide
					selection[i] = new SensorMeasures(sensors.get(indexes[i]));
				} else {
					selection[i] = iMeasures;
				}
			}

			if (mColumnsHeaders.isSelected() == true) {
				for (int i = 0; i < selection.length; i++) {
					// 1. Impression identificateur de l'instrument
					lPrintWriter.print(";" + selection[i].getSensor().getId());
				}

				lPrintWriter.println();

				for (int i = 0; i < selection.length; i++) {
					// 1. Impression unite de l'instrument
					lPrintWriter
							.print(";" + selection[i].getSensor().getUnit());
				}
				lPrintWriter.println();

			}

			// 7. Lecture d�finition de l'intervalle d'exportation
			Date frDate = (Date) mBeginDateTextField.getValue();
			Date toDate = (Date) mEndDateTextField.getValue();

			// 8. Lecture r�f�rence au calendrier courant
			Calendar cuDate = Calendar.getInstance();

			// 9. Initialisation date courante
			cuDate.setTime(frDate);

			// 10. Cr�ation buffer des valeurs de mesures
			Double values[] = new Double[selection.length];

			// 11. Lecture mode de s�lection des mesures
			int modeSelection = mDateTriggerCombobox.getSelectedIndex();

			// 12. Lecture mode d'impression date des mesures
			int modeFormat = mDatesFormatCombobox.getSelectedIndex();

			// 13. Initialisation de l'age de la mesure/ � la date de d�but
			int dayCount = 0;

			do {
				// 1. Initialisation variables contextuelles
				int nbMeasures = 0;
				boolean hasZMeasure = false;
				boolean hasPluieMeasure = false;
				boolean printMeasures = true;

				// 2. Lecture valeurs des mesures � la date courante
				for (int i = 0; i < selection.length; i++) {
					if (selection[i].hasMeasure(cuDate.getTime())) {
						// 1. Lecture valeur courante de la mesure
						values[i] = selection[i].getMeasure(cuDate.getTime());

						// 2. Actualisation variables contextuelles
						nbMeasures = nbMeasures + 1;

						if (selection[i].getSensor().getId().equals("Z")) {
							hasZMeasure = true;
						} else if (selection[i].getSensor().getId().equals(
								"PLUIE")) {
							hasPluieMeasure = true;
						}

					} else {
						// 1. Aucune mesure n'est disponible
						values[i] = null;
					}
				}

				// 3. Filtrage en fonction du mode de s�lection des mesures
				switch (modeSelection) {
				case 0:/** Au moins une mesure doit exister. */
					if (nbMeasures == 0) {
						// 1. Poursuite du traitement
						printMeasures = false;
					}
					break;
				case 1:/** Toutes les mesures doivent exister. */
					if (nbMeasures != selection.length) {
						// 1. Poursuite du traitement
						printMeasures = false;
					}
					break;
				case 2:/** Au moins une mesure diff�rente de Z et de PLUIE. */
					if ((nbMeasures - (hasZMeasure ? 1 : 0) - (hasPluieMeasure ? 1
							: 0)) < 1) {
						// 1. Poursuite du traitement
						printMeasures = false;
					}
					break;
				case 3:/** Toutes les mesures doivent exister sauf Z et PLUIE. */
					if ((nbMeasures - (hasZMeasure ? 1 : 0) - (hasPluieMeasure ? 1
							: 0)) != (selection.length - (hasZMeasure ? 1 : 0) - (hasPluieMeasure ? 1
							: 0))) {
						// 1. Poursuite du traitement
						printMeasures = false;
					}
					break;
				case 4:/** Impression quel que soit les mesures. */
					break;
				}

				if (printMeasures) {
					// 4. Impression date de la mesure
					switch (modeFormat) {

					case 0: // Date au format dd/MM/yyyy.
						lPrintWriter.print(format0.format(cuDate.getTime())
								+ ";");
						break;

					case 1:// Date au format yyyy mm dd.
						lPrintWriter.print(format1.format(cuDate.getTime())
								+ ";");
						break;

					case 2:// Date au format dd MM yy.
						lPrintWriter.print(format2.format(cuDate.getTime())
								+ ";");
						break;

					case 3:// Date affich�e sous forme de nombre de jours
						lPrintWriter.print(dayCount + ";");
						break;

					case 4: // Date affich�e sous forme de nombre d'ann�es.
						// 1. Calcul date en format decimal
						double decDate = cuDate.get(Calendar.YEAR)
								+ (cuDate.get(Calendar.DAY_OF_YEAR) - 1.)
								/ 365.25;

						lPrintWriter.print(formatn.format(decDate) + ";");
						break;
					}

					// 5. Impression valeurs des mesur
					for (int i = 0; i < values.length; i++) {
						// 1. Impression
						if (values[i] == null) {
							// 1. Impression chaine de substitution
							lPrintWriter.print(mVoidValueSymbolTextField
									.getText());
						} else {
							// 2. Impression valeur de la mesure
							lPrintWriter.print(formatn.format(values[i]));
						}

						// 2. Impression s�parateur de champ
						if (i < (values.length - 1)) {
							// 1. Impression s�parateur de champ
							lPrintWriter.print(";");
						} else {
							// 1. Impression s�parateur de ligne
							lPrintWriter.println();
						}
					}
				}

				cuDate.add(Calendar.DATE, 1);
				dayCount++;

			} while (cuDate.getTime().compareTo(toDate) <= 0);

			// 13. Fermeture writer
			lPrintWriter.close();
		}
	}
}