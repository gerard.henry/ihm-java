package controls.panels;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import db.Barrage;
import db.Survey;

@SuppressWarnings("serial")
public class BarrageExportDialog extends JDialog implements PropertyChangeListener, ActionListener, DocumentListener {
	/** Composants internes. */
	protected JFormattedTextField frDate;
	protected JFormattedTextField toDate;	
	protected JTextField filePath;
	protected JButton fileBrowse;
	protected JButton ok;
	protected JButton cancel;
	
	/** R�f�rence au barrage � exporter. */
	protected Barrage barrage;
	
	/**
	 * Constructeur
	 * @param aBarrage Barrage � exporter
	 */
	public BarrageExportDialog(Barrage aBarrage) {
		// 1. Initialisation des attributs
		this.barrage = aBarrage;
		
		// 2. Construction de l'interface
		this.buildUI();
		
		// 3. Initialisation titre du dialogue
		this.setTitle(Survey.getTitle("Sauvegarde barrage", barrage.getId()));
		
		// 3. R�solution dimensions du dialogue 
		this.pack();
	}
	
	/**
	 * Construction de l'interface
	 */
	protected void buildUI() {
		// 1. Initialisation layout du panneau principal
		this.getContentPane().setLayout(new BorderLayout());
		
		// 2. Cr�ation des composants
		this.frDate = new JFormattedTextField(Survey.dateFormat);
		this.toDate = new JFormattedTextField(Survey.dateFormat);
		this.filePath = new JTextField(64);
		this.fileBrowse = new JButton();
		this.ok = new JButton("Exporter");
		this.cancel = new JButton("Fermer");
		
		// 3. Cr�ation des labels panneaux interm�diaires
		JLabel frLabel = new JLabel("Date premi�re mesure");
		JLabel toLabel = new JLabel("Date derni�re mesure");
		JLabel pathLabel = new JLabel("Fichier d'exportation");
		JPanel editPane = new JPanel(new GridBagLayout());
		JPanel cmdPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		
		// 4. Cr�ation fichier d'export par defaut
		File exportFile = new File("db/export",barrage.getId()+".xml");
		
		// 4. Configuration des composants
		this.frDate.setValue(this.barrage.getDateFirstMeasure());
		this.toDate.setValue(this.barrage.getDateLastMeasure());
		this.filePath.setText(exportFile.getAbsolutePath());
		this.fileBrowse.setText("Parcourir...");
		this.ok.setEnabled(false);
		
		// 5. Cr�ation des contraintes de positionnement
		GridBagConstraints gbc = new GridBagConstraints();
		
		// 6. Installation des composants
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10,5,10,5);
		editPane.add(frLabel, gbc);
		
		gbc.gridx = 1;
		editPane.add(this.frDate,gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		editPane.add(toLabel,gbc);
		
		gbc.gridx = 1;
		editPane.add(toDate,gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		editPane.add(pathLabel,gbc);
		
		gbc.gridx = 1;
		gbc.gridwidth = 2;
		gbc.weightx= 1.;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		editPane.add(filePath,gbc);
		
		gbc.gridx = 3;
		gbc.gridwidth = 1;
		gbc.weightx = 0.;
		editPane.add(fileBrowse,gbc);
		
		cmdPane.add(this.ok);
		cmdPane.add(this.cancel);
		
		this.getContentPane().add(editPane,BorderLayout.CENTER);
		this.getContentPane().add(cmdPane,BorderLayout.SOUTH);
		
		// 7. Installation des listeners
		this.frDate.addPropertyChangeListener(this);
		this.toDate.addPropertyChangeListener(this);
		this.filePath.getDocument().addDocumentListener(this);
		this.fileBrowse.addActionListener(this);
		this.cancel.addActionListener(this);
		this.ok.addActionListener(this);
		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		
		// 8. Controle des donn�es edit�es
		this.checkFields();
	}

	@Override
	public void propertyChange(PropertyChangeEvent aEvent) {

		if ( aEvent.getPropertyName().equals("value")) {
			this.checkFields();
		}
	}

	@Override
	public void actionPerformed(ActionEvent aEvent) {
		if ( aEvent.getSource() == this.ok ) {
			// 1. Validation de l'exportation
			Survey.getInstance().exportBarrage(this.barrage,this.filePath.getText(),(Date)this.frDate.getValue(),(Date)this.toDate.getValue());

			// 2. Fermeture du dialogue
			this.setVisible(false);
			
			// 3. Liberation des ressources
			this.dispose();
		} else if ( aEvent.getSource() == this.cancel ) {
			// 1. Fermeture du dialogue
			this.setVisible(false);
			
			// 2. Liberation des ressources
			this.dispose();
		} else if ( aEvent.getSource() == this.fileBrowse ) {
			// 1. Cr�ation d'un s�lecteur de fichier
			JFileChooser chooser = new JFileChooser();
			
			// 2. Configuration du s�lecteur de fichier
			chooser.setMultiSelectionEnabled(false);
			chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			chooser.setSelectedFile(new File(this.filePath.getText()));
			
			// 3. Ouverture du dialogue
			if ( chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
				// 1. Actualisation chemin du fichier s�lectionn�
				this.filePath.setText(chooser.getSelectedFile().getAbsolutePath());
			}
			
		}
	}
	
	/**
	 * Controle des valeurs edit�es
	 */
	protected void checkFields() {
		// 1. Lecture donn�es edit�es
		Date from = (Date) this.frDate.getValue();
		Date to = (Date) this.toDate.getValue();
		String path = (String) this.filePath.getText();
				
		// 2. Initialisation validit� des donn�es
		boolean isValid = true;
		
		// 3. V�rification validit� des donn�es
		if ( from.compareTo(to) > 0 ) {
			isValid = false;
		}
		
		if ( path.trim().length() == 0 ) {
			isValid = false;
		} else {
			// 1. Cr�ation r�f�rence au fichier
			File file = new File(path.trim());
			
			if ( file.exists()) {
				if ( file.isDirectory() || !file.canWrite()) {
					isValid = false;
				}
			} else {
				// 1. Lecture r�pertoire parent
				File dir = file.getParentFile();
				
				isValid = dir != null && dir.canWrite();
			}
		}
		
		// 4. Actualisation etat des controles
		this.ok.setEnabled(isValid);
	}

	@Override
	public void changedUpdate(DocumentEvent arg0) {
		this.checkFields();
	}

	@Override
	public void insertUpdate(DocumentEvent arg0) {
		this.checkFields();
	}

	@Override
	public void removeUpdate(DocumentEvent arg0) {
		this.checkFields();
	}
}
