package controls.panels;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import calcul.SensorCorrection;
import cs.ui.DateField;
import db.Barrage;
import db.Sensor;
import db.SensorMeasures;
import db.SensorResultats;
import db.Survey;

@SuppressWarnings("serial")
public class CorrectionAnalyseDialog extends JDialog implements ActionListener,
		ListSelectionListener {
	/** Composants internes. */
	protected JRadioButton allMeasures;
	protected JRadioButton intMeasures;
	protected JRadioButton useFile;
	protected DateField firstDate;
	protected DateField lastDate;
	protected JList sensors;
	protected JTextField filePath;
	protected JButton fileBrowse;
	protected JButton apply;
	protected JButton close;
	protected JSplitPane splitPane;
	protected AnalyseCorrectionResultats resultatsPane;

	/** R�f�rence au barrage propri�taire des instruments. */
	protected Barrage barrage;

	/** Liste des instruments s�lectionn�s. */
	protected SensorCorrection sensorCorrections[];

	/**
	 * Constructeur
	 * 
	 * @param aBarrage
	 *            barrage propri�taire des instruments
	 */
	public CorrectionAnalyseDialog(Barrage aBarrage) {
		// 1. Initialisation des attributs
		this.barrage = aBarrage;

		// 2. Construction de l'interface
		this.buildUI();
	}

	/**
	 * Construction de l'interface
	 */
	protected void buildUI() {
		GridBagConstraints gbc = new GridBagConstraints();

		// 1. Lecture r�f�rence au panneau principal
		JPanel contentPane = (JPanel) this.getContentPane();

		// 2. Initialisation layout du panneau principal
		contentPane.setLayout(new BorderLayout());

		// 3. Cr�ation des composants
		this.sensors = new JList(this.getSensorNames().toArray());
		this.splitPane = new JSplitPane();
		this.resultatsPane = new AnalyseCorrectionResultats();
		this.allMeasures = new JRadioButton("Toutes les mesures");
		this.intMeasures = new JRadioButton("Mesures faites entre le");
		this.useFile = new JRadioButton("Imprimer dans le fichier");
		this.firstDate = new DateField(Survey.dateFormat);
		this.lastDate = new DateField(Survey.dateFormat);
		this.filePath = new JTextField(32);
		this.fileBrowse = new JButton("...");
		this.apply = new JButton("Corriger");
		this.close = new JButton("Fermer");

		// 4. Cr�ation des composants interm�diaires
		JPanel leftPane = new JPanel(new BorderLayout());
		JPanel editPane = new JPanel(new GridBagLayout());
		JPanel buttonPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JScrollPane sensorSP = new JScrollPane(this.sensors);
		ButtonGroup group = new ButtonGroup();

		// 5. Configuration des composants
		sensorSP.setBorder(BorderFactory
				.createTitledBorder("Instrument(s)/Variable(s)"));
		group.add(this.allMeasures);
		group.add(this.intMeasures);
		this.allMeasures.setSelected(true);
		this.useFile.setSelected(true);
		this.filePath.setEnabled(true);
		this.fileBrowse.setEnabled(true);
		this.filePath.setText(System.getProperty("user.home") + "/CORRIGE.LST");
		this.firstDate.setEnabled(false);
		this.lastDate.setEnabled(false);
		this.apply.setEnabled(false);
		this.firstDate.setValue(this.barrage.getDateFirstMeasure());
		this.lastDate.setValue(this.barrage.getDateLastMeasure());

		// 3. Installation des composants
		buttonPane.add(this.apply);
		buttonPane.add(this.close);

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(10, 5, 10, 5);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		editPane.add(this.allMeasures, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		editPane.add(this.intMeasures, gbc);
		gbc.gridx = 1;
		gbc.weightx = 1.;
		editPane.add(this.firstDate, gbc);
		gbc.gridx = 2;
		gbc.weightx = 0.;
		editPane.add(new JLabel(" et le "), gbc);
		gbc.gridx = 3;
		gbc.weightx = 1.;
		editPane.add(this.lastDate, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.weightx = 0.;
		editPane.add(this.useFile, gbc);
		gbc.gridx = 1;
		gbc.weightx = 1.;
		gbc.gridwidth = 3;
		editPane.add(this.filePath, gbc);
		gbc.gridx = 4;
		gbc.weightx = 0.;
		editPane.add(this.fileBrowse, gbc);

		splitPane.setLeftComponent(leftPane);
		splitPane.setRightComponent(resultatsPane);
		leftPane.add(sensorSP, BorderLayout.NORTH);
		leftPane.add(editPane, BorderLayout.CENTER);
		contentPane.add(this.splitPane, BorderLayout.CENTER);
		contentPane.add(buttonPane, BorderLayout.SOUTH);

		// 4. Installation des listeners
		this.allMeasures.addActionListener(this);
		this.intMeasures.addActionListener(this);
		this.useFile.addActionListener(this);
		this.sensors.getSelectionModel().addListSelectionListener(this);
		this.apply.addActionListener(this);
		this.close.addActionListener(this);
		this.fileBrowse.addActionListener(this);

		// 5. Initialisation des propri�tes du dialogue
		this.setTitle(Survey.getApplicationName() + " - Correction analyse");
		this.setModal(true);
	}

	/**
	 * Lecture liste des noms d'instrument/variable disponibles
	 * 
	 * @return Liste noms d'instrument/variable disponibles
	 */
	protected List<String> getSensorNames() {
		// 1. Lecture liste des noms d'instruments/variables
		List<String> names = this.barrage.getSensorsNames();

		// 2. Suppression des instruments/variables non analys�s
		for (Iterator<String> i = names.iterator(); i.hasNext();) {
			// 1. Lecture nom du ieme instrument/variable
			String iname = i.next();

			// 2. Lecture des r�sultats de l'analyse
			SensorResultats resultats = this.barrage.getSensorResultats(iname);

			if (resultats == null || resultats.getLNasous() == null) {
				i.remove();
			}
		}

		return names;
	}

	@Override
	public void actionPerformed(ActionEvent aEvent) {
		if (aEvent.getSource() == this.intMeasures
				|| aEvent.getSource() == this.allMeasures) {
			this.firstDate.setEnabled(this.intMeasures.isSelected());
			this.lastDate.setEnabled(this.intMeasures.isSelected());
		} else if (aEvent.getSource() == this.useFile) {
			if (this.useFile.isSelected()) {
				this.filePath.setEnabled(true);
				this.fileBrowse.setEnabled(true);
			} else {
				this.filePath.setEnabled(false);
				this.filePath.setText(System.getProperty("user.home")
						+ "/CORRIGE.LST");
				this.fileBrowse.setEnabled(false);
			}
		} else if (aEvent.getSource() == this.apply) {
			// 1. Lecture liste des instruments/variables s�lectionn�s
			Object names[] = this.sensors.getSelectedValues();

			// 2. Cr�ation liste des instruments/variables � corriger
			Sensor slist[] = new Sensor[names.length];

			// 3. Initialisation liste des instruments/variables � corriger
			for (int i = 0; i < slist.length; i++) {
				slist[i] = this.barrage.getSensor(names[i].toString());
			}

			// 4. Construction liste des instruments � corriger
			this.doCorrections(slist, this.firstDate.getValue(), this.lastDate
					.getValue());

			// 5. Impression des mesures corrig�es
			if ( this.useFile.isSelected()) {
				this.doPrintCorrections(new File(this.filePath.getText()));
			}
			
			// 6. Actualisation panneau d'affichage des r�sultats
			this.resultatsPane.setSensors(this.sensorCorrections);

		} else if (aEvent.getSource() == this.close) {
			// 1. Fermeture du dialogue
			this.setVisible(false);

			// 2. Liberation des ressources
			this.dispose();
		} else if (aEvent.getSource() == this.fileBrowse) {
			// 1. Ouverture d'un s�lecteur de fichier
			JFileChooser chooser = new JFileChooser();

			// 2. Initialisation fichier s�lectionn�
			chooser.setSelectedFile(new File(this.filePath.getText()));

			// 3. Ouverture du s�lecteur de fichier
			if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
				// 1. Actualisation nom du fichier
				this.filePath.setText(chooser.getSelectedFile()
						.getAbsolutePath());
			}

		}

	}

	@Override
	public void valueChanged(ListSelectionEvent aEvent) {
		if (!aEvent.getValueIsAdjusting()) {
			if (this.sensors.getSelectedIndex() != -1) {
				this.apply.setEnabled(true);
			} else {
				this.apply.setEnabled(false);
			}
		}
	}

	/**
	 * Calcul des corrections pour la liste d'instruments/variables
	 * 
	 * @param aList
	 *            Liste des instruments/variables s�lectionn�s
	 * @param aFrom
	 *            Date de d�but d'utilisation des mesures
	 * @param aTo
	 *            Date de fin d'utilisation des mesures
	 * @param aFile
	 *            Fichier d'impression
	 * @throws FileNotFoundException
	 */
	protected void doCorrections(Sensor[] slist, Date aFrom, Date aTo) {
		SensorMeasures z = null;
		SensorMeasures pluie = null;

		// 1. Recherche d�finition des variables cote et pluie
		Sensor zVariable = this.barrage.getSensor("Z");
		Sensor pluieVariable = this.barrage.getSensor("PLUIE");

		if (zVariable != null) {
			// 1. Lecture liste des mesures pour la cote du barrage
			z = zVariable.getMeasures();
		}

		if (pluieVariable != null) {
			// 1. Lecture liste des mesures de la pluie
			pluie = pluieVariable.getMeasures();
		}

		// 2. Cr�ation liste des mesures corrig�es
		List<SensorCorrection> clist = new ArrayList<SensorCorrection>();

		for (int i = 0; i < slist.length; i++) {
			// 1. Lecture r�f�rence au ieme instrument/variable
			Sensor isensor = slist[i];

			// 2. Lecture liste des mesures de l'instrument/variable
			SensorMeasures imeasures = this.barrage.getSensorMeasures(isensor
					.getId());

			// 3. Si l'instrument ne poss�de pas de mesures on passe au suivant
			if (imeasures == null || imeasures.getMeasureCount() == 0)
				continue;

			// 5. Actualisation liste des mesures corrig�es
			clist.add(new SensorCorrection(imeasures, z, pluie, aFrom, aTo));
		}

		// 3. Actualisation liste des instruments corrig�s
		this.sensorCorrections = clist.toArray(new SensorCorrection[clist
				.size()]);
	}

	/**
	 * Impression des corrections
	 * 
	 * @param aFile
	 *            Fichier d'impression
	 * @throws FileNotFoundException
	 */
	protected void doPrintCorrections(File aFile) {
		PrintStream out = null;
		try {
			// 1. Cr�ation flux d'�criture des r�sultats
			out = new PrintStream(aFile);

			// 2. Generation nom et titre du barrage
			out
					.println("Barrage " + barrage.getId() + " "
							+ barrage.getTitle());

			for (int i = 0; i < sensorCorrections.length; i++) {
				// 1. Lecture r�f�rence au ieme instrument/variable
				Sensor isensor = sensorCorrections[i].getSensor();

				// 2. Generation donn�es d'identification de l'instrument
				out.println((isensor.getNature().equals("I") ? "Instrument "
						: "Variable ")
						+ isensor.getId()
						+ " Valeur ref:"
						+ isensor.getRefValue());
				out
						.println("Correction des mesures selon les r�sultats d'analyse du "
								+ Survey.format(isensor.getRegressionDate()));

				// 3. G�n�ration entete de la table des mesures corrig�es
				out
						.println("*****************************************************************************");
				out
						.println("*  date     * valeur * valeur *   �   *    termes r�versibles    *  valeur  *");
				out
						.println("*           * mesur�e*calcul�e*       * saison  * cote  * pluie  * corrig�e *");
				out
						.println("*****************************************************************************");

				// 3. Lecture liste des donn�es � imprimer
				List<Date> dates = sensorCorrections[i].getMeasureDates();
				List<Double> measures = sensorCorrections[i].getMeasureValues();
				List<Double> computed = sensorCorrections[i]
						.getMeasureComputed();
				List<Double> epsilon = sensorCorrections[i].getMeasureEps();
				List<Double> season = sensorCorrections[i].getSeasonEffects();
				List<Double> hydro = sensorCorrections[i].getHydroEffects();
				List<Double> rain = sensorCorrections[i].getRainEffects();
				List<Double> corrected = sensorCorrections[i]
						.getMeasureCorrected();

				for (int j = 0; j < dates.size(); j++) {
					// 1. Impression des r�sultats
					out
							.printf(
									"*%s %8.3f %8.3f %8.4f %8.4f %8.4f %8.4f %8.3f *\n",
									Survey.format(dates.get(j)), measures
											.get(j), computed.get(j), epsilon
											.get(j), season.get(j), hydro
											.get(j), rain.get(j), corrected
											.get(j));

				}

				// 4 G�n�ration fin de table
				out
						.println("*****************************************************************************");

			}
		} catch(FileNotFoundException fnfex) {
			
		} finally {
			// 1. Fermeture du flux de g�n�ration
			if ( out != null ) out.close();
		}
	}
}