package db;

import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Element;

public class XMLSensorResultats extends XMLFile<SensorResultats> {
	/** Sérialiseur/Sésérialiseur d'un objet SensorResultats. */
	protected Marshaller marshaller;
	protected Unmarshaller unmarshaller;

	/** Barrage propriétaire de ces mesures. */
	protected Barrage barrage;

	/**
	 * Constructeur
	 * 
	 * @param aBase Gestionnaire de la base
	 * @param aBarrage
	 *            Barrage propriétaire de ces mesures
	 */
	public XMLSensorResultats(SurveyBase aBase,Barrage aBarrage) {
		super(aBase);

		// 1. Initialisation des attributs
		this.barrage = aBarrage;
		
		try {
			// 1. Création contexte de binding
			JAXBContext context = JAXBContext.newInstance(SensorResultats.class);

			// 2. Création Sérialiseur/Sésérialiseur d'un objet Barrage
			unmarshaller = context.createUnmarshaller();
			marshaller = context.createMarshaller();
		} catch (JAXBException e) {
			// 1. Enregistrement trace d'exécution
			Survey.logger.log(Level.SEVERE, "", e);
		}
	}

	/**
	 * Lecture barrage propriétaire de ces mesures
	 * 
	 * @return Barrage propriétaire de ces mesures
	 */
	public Barrage getBarrage() {
		return this.barrage;
	}

	@Override
	protected String getObjectId(SensorResultats aResultats) {
		return aResultats.getSensor().getId();
	}

	@Override
	public void marshall(SensorResultats aObject, Element aElement) {
		try {
			this.marshaller.marshal(aObject, aElement);
		} catch (JAXBException e) {
			// 1. Enregistrement trace d'exécution
			Survey.logger.log(Level.SEVERE, "marshall", e);
		}
	}

	@Override
	public SensorResultats unmarshall(Element aElement) {
		try {
			return (SensorResultats) this.unmarshaller.unmarshal(aElement);
		} catch (JAXBException e) {
			// 1. Enregistrement trace d'exécution
			Survey.logger.log(Level.SEVERE, "unmarshall", e);

			return null;
		}
	}
	
	@Override
	protected void afterUnmarshalling(SensorResultats aResultats) {
		// 1. Initialisation référence au barrage
		aResultats.setSensor(this.barrage.getSensor(aResultats.id));
	}

	@Override
	public String getFileName() {
		return "res/"+ this.barrage.getId() + ".xml";
	}
}
