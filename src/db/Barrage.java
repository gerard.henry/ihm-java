package db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "dam")
public class Barrage implements SurveyObject {
	/** Attributs d'un barrage. */
	protected String id;
	protected String title;
	protected String description;
	protected Date launchingDate;
	protected int sensorsCount;
	protected int stockedVariablesCount;
	protected Date updateDate;
	protected Date saveDate;
	protected int measuresCount;
	protected double waterMinLevel;
	protected double waterNominalLevel;
	protected double waterMaxLevel;

	/** Gestionnaire de la liste des instruments du barrages. */
	@XmlTransient
	protected XMLSensors sensors;

	/** Gestionnaire des mesures des instruments/variables du barrage. */
	@XmlTransient
	protected XMLSensorMeasures measures;
	
	/** Gestionnaire des analyses des instruments/variables du barrage. */
	@XmlTransient
	protected XMLSensorResultats resultats;
		
	/**
	 * Constructeur
	 */
	public Barrage() {
	}

	/**
	 * Lecture liste des instruments et variables du barrage
	 * 
	 * @return Liste des noms d'instruments et de variables du barrage
	 */
	public List<Sensor> getSensors() {
		// 1. Lecture de la liste des instruments/variables
		return this.getXMLSensors().getAll();
	}

	/**
	 * Lecture liste ordonn�e des identificateurs des instruments/variables
	 * 
	 * @return Liste ordonn�es des identificateurs des instruments/variables
	 */
	public List<String> getSensorsNames() {
		// 1. Cr�ation liste des identificateurs des instruments/variables
		List<String> names = new ArrayList<String>();
		
		// 2. Lecture liste des des instruments/variables 
		List<Sensor> sensors = this.getSensors();
		
		// 3. Construction liste des identificateurs de barrages
		for ( int i = 0 ; i < sensors.size(); i++) {
			names.add(sensors.get(i).getId());
		}
		
		// 4. Tri de la liste des identificateurs de barrages
		Collections.sort(names);
		
		return names;
	}
	/**
	 * Lecture d�finition d'un instrument/variable
	 * 
	 * @param aId
	 *            Identificateur de l'instrument/variable
	 * @return D�finition de l'instrument/variable
	 */
	public Sensor getSensor(String aId) {
		return this.getXMLSensors().get(aId);
	}

	/**
	 * Lecture liste des mesures d'un instrument
	 * @param aSensorId Identificateur d'un instrument/variable
	 */
	public SensorMeasures getSensorMeasures(String aSensorId) {
		return this.getXMLSensorMeasures().get(aSensorId);
	}

	/**
	 * Lecture r�sultat de l'analyse d'un instrument
	 * @param aSensorId Identificateur d'un instrument/variable
	 */
	public SensorResultats getSensorResultats(String aSensorId) {
		return this.getXMLSensorResultats().get(aSensorId);
	}
	
	/**
	 * Actualisation du nombre de mesures
	 */
	public void updateMeasureCount() {
		// 1. Initialisation nombre de mesures du barrage
		int dmc = 0;
		
		// 2. Lecture liste des mesures des instruments du barrage
		List<SensorMeasures> mlist = this.getXMLSensorMeasures().getAll();
		
		for ( int i = 0 ; i < mlist.size() ; i++) {
			// 1. Lecture de la ieme liste de mesures
			SensorMeasures imeasures = mlist.get(i);
			
			if ( imeasures != null ) {
				// 1. Lecture nombre de mesures effectives
				int imc = imeasures.getMeasureCount();
			
				// 2. Actualisation nombre de mesures de l'instrument
				imeasures.getSensor().setMeasuresCount(imc);
				
				// 3. Actualisation nombre de mesures du barrage
				dmc += imc;
			}
		}
		
		// 3. Actualisation nombre de mesures du barrage
		this.setMeasuresCount(dmc);
	}
	
	/**
	 * Lecture date de la premi�re mesure
	 * 
	 * @return Date de la premi�re mesure
	 */
	public Date getDateFirstMeasure() {
		// 1. Initialisation date de la premi�re mesure
		Date first = null;

		// 2. Lecture liste des instruments du barrage
		List<Sensor> sensors = this.getSensors();
		
		for (int i = 0; i < sensors.size(); i++) {
			// 1. Lecture liste des mesures du ieme instrument/ variable
			SensorMeasures iMeasures = this.getSensorMeasures(sensors.get(i).getId());

			// 2. Si l'instrument ne pos��de pas de mesures on l'ignore
			if ( iMeasures == null ) continue;
			
			// 3. Lecture premi�re date editable du ieme instrument/variable
			Date iDate = iMeasures.getDateFirstMeasure();

			if (first == null) {
				// 1. Initialisation premi�re date editable
				first = iDate;
			} else {
				if (iDate.before(first)) {
					// 1. Initialisation premi�re date editable
					first = iDate;
				}
			}
		}

		return first;
	}

	/**
	 * Lecture date de la derni�re mesure
	 * 
	 * @return Date de la derni�re mesure
	 */
	public Date getDateLastMeasure() {
		// 1. Initialisation date de la derni�re mesure
		Date last = null;

		// 2. Lecture liste des instruments du barrage
		List<Sensor> sensors = this.getSensors();
		
		for (int i = 0; i < sensors.size(); i++) {
			// 1. Lecture liste des mesures du ieme instrument/ variable
			SensorMeasures iMeasures = this.getSensorMeasures(sensors.get(i).getId());

			// 2. Si l'instrument ne pos��de pas de mesures on l'ignore
			if ( iMeasures == null ) continue;
			
			// 3. Lecture premi�re date editable du ieme instrument/variable
			Date iDate = iMeasures.getDateLastMeasure();

			if (last == null) {
				// 1. Initialisation premi�re date editable
				last = iDate;
			} else {
				if (iDate.after(last)) {
					// 1. Initialisation premi�re date editable
					last = iDate;
				}
			}
		}
		return last;
	}

	/**
	 * Lecture gestionnaire des instruments/variables du barrage
	 * 
	 * @return Gestionnaire des instruments/variables du barrage
	 */
	public XMLSensors getXMLSensors() {
		if (this.sensors == null) {
			// 1. Cr�ation gestionnaire de la liste des instruments/variables
			this.sensors = new XMLSensors(Survey.getInstance().getBase(),this);
		}

		return this.sensors;
	}

	/**
	 * Lecture gestionnaire des mesures des instruments/variables du barrage
	 * 
	 * @return Gestionnaire des mesures des instruments/variables du barrage
	 */
	public XMLSensorMeasures getXMLSensorMeasures() {
		if (this.measures == null) {
			// 1. Cr�ation gestionnaire de la liste des instruments/variables
			this.measures = new XMLSensorMeasures(Survey.getInstance().getBase(),this);
		}

		return this.measures;
	}

	/**
	 * Lecture gestionnaire des analyses des instruments/variables du barrage
	 * 
	 * @return Gestionnaire des mesures des instruments/variables du barrage
	 */
	public XMLSensorResultats getXMLSensorResultats() {
		if (this.resultats == null) {
			// 1. Cr�ation gestionnaire de la liste des instruments/variables
			this.resultats = new XMLSensorResultats(Survey.getInstance().getBase(),this);
		}

		return this.resultats;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return (title != null ? title : "");
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return (description != null ? description : "");
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlJavaTypeAdapter(XMLDateAdapter.class) 
	public Date getLaunchingDate() {
		return launchingDate;
	}

	public void setLaunchingDate(Date launchingDate) {
		this.launchingDate = launchingDate;
	}

	public int getSensorsCount() {
		return sensorsCount;
	}

	public void setSensorsCount(int sensorsCount) {
		this.sensorsCount = sensorsCount;
	}

	public int getStockedVariablesCount() {
		return stockedVariablesCount;
	}

	public void setStockedVariablesCount(int stockedVariablesCount) {
		this.stockedVariablesCount = stockedVariablesCount;
	}

	@XmlJavaTypeAdapter(XMLDateAdapter.class) 
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@XmlJavaTypeAdapter(XMLDateAdapter.class) 
	public Date getSaveDate() {
		return saveDate;
	}

	public void setSaveDate(Date saveDate) {
		this.saveDate = saveDate;
	}

	public int getMeasuresCount() {
		return measuresCount;
	}

	public void setMeasuresCount(int measuresCount) {
		this.measuresCount = measuresCount;
	}

	public double getWaterMinLevel() {
		return waterMinLevel;
	}

	public void setWaterMinLevel(double waterMinLevel) {
		this.waterMinLevel = waterMinLevel;
	}

	public double getWaterNominalLevel() {
		return waterNominalLevel;
	}

	public void setWaterNominalLevel(double waterNominalLevel) {
		this.waterNominalLevel = waterNominalLevel;
	}

	public double getWaterMaxLevel() {
		return waterMaxLevel;
	}

	public void setWaterMaxLevel(double waterMaxLevel) {
		this.waterMaxLevel = waterMaxLevel;
	}
}
