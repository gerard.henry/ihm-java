package db;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;

import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;

public class SensorMeasures implements SurveyObject {
	/** R�f�rence � l'instrument/variable. */
	protected Sensor sensor;

	/** Liste des mesures. */
	protected SortedMap<Date, Double> measures;

	/** Valeurs brutes relev�es. */
	protected Map<Date,Double> rawValues;
	
	/** Contexte d'�valuation des valeurs des mesures. */
	protected MeasureEvalContext evalContext;

	/** Premi�re/derni�re date permises pour une mesure. */
	protected Date firstAllowedDate;
	protected Date lastAllowedDate;

	/**
	 * Constructeur
	 * 
	 * @param aSensor
	 *            Instrument/Variable propri�taire de la mesure
	 */
	public SensorMeasures(Sensor aSensor) {
		// 1. Initialisation des attributs
		this.sensor = aSensor;
		this.measures = new TreeMap<Date, Double>();
		this.rawValues = new HashMap<Date,Double>();
		this.evalContext = (aSensor != null ? new MeasureEvalContext(
				this.sensor) : null);
	}

	/**
	 * Cr�ation d'une copie de la liste de mesures
	 * 
	 * @return Copie de la liste de mesures
	 */
	public SensorMeasures copy() {
		// 1. Cr�ation d'une liste de mesures
		SensorMeasures copy = new SensorMeasures(null);

		// 2. Initialisation de la copie
		this.sensor = null;
		this.evalContext = null;

		for (Iterator<Date> i = this.measures.keySet().iterator(); i.hasNext();) {
			// 1. Lecture date et valeur de la mesure
			Date idate = i.next();
			Double ivalue = this.measures.get(idate);

			// 2. Enregistrement date et valeur de la mesure
			copy.measures.put(idate, ivalue);
		}

		for (Iterator<Date> i = this.rawValues.keySet().iterator(); i.hasNext();) {
			// 1. Lecture date et valeur relev�e
			Date idate = i.next();
			Double ivalue = this.rawValues.get(idate);

			// 2. Enregistrement date et valeur relev�e
			copy.rawValues.put(idate, ivalue);
		}
		return copy;
	}

	/**
	 * Lecture de l'instrument/variable propri�taire des mesures
	 * 
	 * @return Instrument/Variable propri�taire des mesures
	 */
	public Sensor getSensor() {
		return sensor;
	}

	/**
	 * Lecture identificateur de l'instrument
	 * @return Identificateur de l'instrument
	 */
	public String getId() {
		return this.sensor.getId();
	}
	
	/**
	 * Initialisation de l'instrument/variable propri�taire des mesures
	 * 
	 * @param aSensor
	 *            Instrument/Variable propri�taire des mesures
	 */
	public void setSensor(Sensor aSensor) {
		sensor = aSensor;
		this.evalContext = new MeasureEvalContext(this.sensor);
	}

	/**
	 * Construction index des dates de mesures pour un ensemble de mesures
	 * 
	 * @param aMeasures
	 *            Liste des mesures d'un ensemble d'instrument/variable
	 * @return liste ordonn�e des dates de mesures
	 */
	public static List<Date> getDatesMeasure(SensorMeasures aMeasures[]) {
		// 1. Cr�ation ensemble des dates de mesures
		HashSet<Date> dates = new HashSet<Date>();

		// 2. Initialisation ensemble des dates de mesures
		for (int i = 0; i < aMeasures.length; i++) {
			if (aMeasures[i] != null) {
				dates.addAll(aMeasures[i].getDatesMeasure());
			}
		}

		// 3. Cr�ation liste ordonn�e des dates de mesure
		List<Date> odates = new ArrayList<Date>();

		// 4. Initialisation liste ordonn�e des dates de mesure
		odates.addAll(dates);

		// 5. Tri des dates
		Collections.sort(odates);

		return odates;
	}

	/**
	 * Lecture du nombre de mesures
	 * 
	 * @return Nombre de mesures
	 */
	public int getMeasureCount() {
		return this.measures.size();
	}

	/**
	 * La mesure est-elle editable
	 * 
	 * @param aDate
	 *            Date de la mesure
	 */
	public boolean isMeasureEditable(Date aDate) {
		if (this.sensor.isMesuresEditables()) {
			if (aDate.compareTo(this.getFirstAllowedDate()) >= 0
					&& aDate.compareTo(this.getLastAllowedDate()) <= 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Enregistrement d'une valeur brute
	 * @param aDate Date du relev�
	 * @param aValue Valeur relev�e
	 */
	public void setRawValue(Date aDate,Double aValue) {
		if ( aValue == null ) {
			this.rawValues.remove(aDate);
		} else {
			this.rawValues.put(aDate, aValue);
		}
	}
	
	/**
	 * Lecture valeur brute r�l�v�
	 * @param aDate Date du relev�
	 * @return Valeur brute relev�e
	 */
	public Double getRawValue(Date aDate) {
		return this.rawValues.get(aDate);
	}
	
	/**
	 * Ajout d'une mesure. Si une mesure existe d�ja pour cette date elle est
	 * modifi�e
	 * 
	 * @param aDate
	 *            Date de la mesure
	 * @param aValue
	 *            Valeur de la mesure
	 */
	public void addMeasure(Date aDate, double aValue) {
		// 1. Enregistrement de la mesure
		this.measures.put(aDate, aValue);
	}

	/*
	 * Suppression d'une mesure
	 * 
	 * @param aDate Date de la mesure
	 */
	public void removeMeasure(Date aDate) {
		this.measures.remove(aDate);
		this.rawValues.remove(aDate);
	}

	/**
	 * Lecture pr�sence d'une mesure
	 * 
	 * @param aDate
	 *            Date de la mesure
	 */
	public boolean hasMeasure(Date aDate) {
		return this.measures.containsKey(aDate);
	}

	/**
	 * Lecture valeur d'une mesure
	 * 
	 * @param aDate
	 *            Date de la mesure
	 */
	public Double getMeasure(Date aDate) {
		return this.measures.get(aDate);
	}

	/**
	 * V�rification de validit� d'une mesure
	 * 
	 * @param aDate
	 *            Date de la mesure
	 * @param aValue
	 *            Valeur de la mesure
	 */
	public boolean isValidMeasure(Date aDate, double aValue) {				
		if (aValue >= this.sensor.getMinimumValue()
				&& aValue <= this.sensor.getMaximumValue()) {
			// 1. Lecture date de la premi�re mesure qui pr�c�de cette date
			Date previousDate = this.getDateOfPreviousMeasure(aDate);

			if (previousDate == null) {
				return true;
			} else {
				// 1. Lecture valeur de la mesure pr�c�dente
				double previousValue = this.getMeasure(previousDate);

				if (Math.abs(aValue - previousValue) > this.sensor
						.getMaximumVariation()) {
					return false;
				} else {
					return true;
				}
			}

		} else {
			return false;
		}
	}

	
	public double getComputedMeasure(double aValue) {
		return this.evalContext.getValue(aValue);
	}

	/**
	 * Lecture liste des dates de mesure
	 * 
	 * @return Liste des dates de mesure
	 */
	public List<Date> getDatesMeasure() {
		// 1. Cr�ation liste des dates de mesures
		List<Date> dates = new ArrayList<Date>();

		// 2. Initialisation liste des dates de mesures
		dates.addAll(this.measures.keySet());

		// 3. Tri de la liste des mesures
		Collections.sort(dates);

		return dates;
	}

	/**
	 * Lecture valeur minimale permise pour une mesure
	 * 
	 * @return Valeur minimale permise pour une mesure
	 */
	public double getMinimumValueAllowed() {
		return this.sensor.getMinimumValue();
	}

	/**
	 * Lecture valeur maximale permise pour une mesure
	 * 
	 * @return Valeur maximale permise pour une mesure
	 */
	public double getMaximumValueAllowed() {
		return this.sensor.getMaximumValue();
	}

	/**
	 * Lecture variation maximale entre deux mesures
	 * 
	 * @return Variation maximale entre deux mesures
	 */
	public double getMaximumVariationAllowed() {
		return this.sensor.getMaximumVariation();
	}

	/**
	 * Lecture premi�re date permise pour une mesure
	 * 
	 * @return Premi�re date permise pour une mesure
	 */
	public Date getFirstAllowedDate() {
		if (this.firstAllowedDate == null) {
			// 1. Lecture date d'installation de l'instrument
			firstAllowedDate = this.sensor.getInstallationDate();

			if (firstAllowedDate == null) {
				// 1.Si aucune date d'installation n'est pr�cis�e on
				// utilise la date de la premi�re mesure
				firstAllowedDate = this.getDateFirstMeasure();

				if (firstAllowedDate == null) {
					// 1. Si aucune mesure n'est pr�sente on utilise la date de
					// mise en eau du barrage
					firstAllowedDate = this.sensor.getBarrage()
							.getLaunchingDate();

					if (firstAllowedDate == null)
						// 1. En d�sespoir de cause on utilise la date de mise �
						// jour
						firstAllowedDate = this.sensor.getBarrage()
								.getUpdateDate();
				}
			}
		}
		return firstAllowedDate;
	}

	/**
	 * Lecture derni�re date permise pour une mesure
	 * 
	 * @return Derni�re date permise pour une mesure
	 */
	public Date getLastAllowedDate() {
		if (this.lastAllowedDate == null) {
			// 1. Lecture date actuelle
			Calendar now = Calendar.getInstance();

			// 2. R�initialisation heure,min,sec
			now.set(Calendar.HOUR_OF_DAY, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.MILLISECOND, 0);

			// 3. Initialisation derni�re date permise
			lastAllowedDate = now.getTime();
		}
		return this.lastAllowedDate;
	}

	/**
	 * Lecture date de la premi�re mesure
	 * 
	 * @return Date de la premi�re mesure
	 */
	public Date getDateFirstMeasure() {
		try {
			return this.measures.firstKey();
		} catch (NoSuchElementException ign) {
			return null;
		}
	}

	/**
	 * Lecture date de la derni�re mesure
	 * 
	 * @return Date de la derni�re mesure
	 */
	public Date getDateLastMeasure() {
		try {
			return this.measures.lastKey();
		} catch (NoSuchElementException ign) {
			return null;
		}
	}

	/**
	 * Lecture date de la premi�re mesure avant cette date
	 * 
	 * @param aDate
	 *            Une date de mesure
	 * @return Lecture date de la mesure pr�cedente
	 */
	public Date getDateOfPreviousMeasure(Date aDate) {
		// 1. Lecture liste des dates de mesure effectu�es avant cette date
		SortedMap<Date, Double> beforeList = measures.headMap(aDate);

		if (beforeList != null) {
			try {
				return beforeList.lastKey();
			} catch (NoSuchElementException ign) {
			}
		}

		return null;
	}

	/**
	 * Lecture date de la premi�re mesure apr�s cette date
	 * 
	 * @param aDate
	 *            Une date de mesure
	 * @return Lecture date de la mesure pr�cedente
	 */
	public Date getDateOfNextMeasure(Date aDate) {
		// 1. Lecture liste des dates de mesure effectu�es apr�s cette date
		SortedMap<Date, Double> afterList = measures.tailMap(aDate);

		if (afterList != null) {
			try {
				if (afterList.size() > 1) {
					// 1. Lecture liste des dates de mesures
					Iterator<Date> i = afterList.keySet().iterator();

					if (this.measures.containsKey(aDate)) {
						// 1. la premi�re est la date pass�e en param�tre
						i.next();
					}

					// 3. la deuxi�me est la date de mesure suivante
					return i.next();

				}
			} catch (NoSuchElementException ign) {
			}
		}

		return null;
	}

	/**
	 * Lecture liste des mesures comprises entre ces deux dates
	 * 
	 * @param aFrom
	 *            Date de d�but de l'intervalle
	 * @param aTo
	 *            Date de fin de l'intervalle
	 */
	public SensorMeasures getSubMeasures(Date aFrom, Date aTo) {
		if (aFrom == null) {
			// 1. Initialisation date de d�but de l'intervalle
			aFrom = this.getDateFirstMeasure();
		}

		if (aTo == null) {
			// 1. Initialisation date de fin de l'intervalle
			aTo = this.getDateLastMeasure();
		}

		// 1. Cr�ation d'une liste de mesures
		SensorMeasures subList = new SensorMeasures(this.sensor);

		for (Iterator<Date> i = this.measures.keySet().iterator(); i.hasNext();) {
			// 1. Lecture date de la mesure
			Date datMes = i.next();

			if (datMes.compareTo(aFrom) >= 0 && datMes.compareTo(aTo) <= 0) {
				// 1. Lecture valeur de la mesure
				Double valMes = this.measures.get(datMes);

				// 2. Lecture valeur brute relev�e
				Double rawValue = this.rawValues.get(datMes);
				
				// 3. Enregistrement valeur de la mesure
				subList.addMeasure(datMes, valMes);
				
				// 4. Enregistrement valeur brute relev�e
				if ( rawValue != null ) {
					subList.setRawValue(datMes, rawValue);
				}
			}
		}

		return subList;
	}

	/**
	 * Lecture liste des mesures comprises entre ces deux dates
	 * 
	 * @param aRef
	 *            Date de r�f�rence
	 * @param aDayCount
	 *            Nombre de jours ajout�s/retir�s � la date de reference
	 */
	public SensorMeasures getSubMeasures(Date aRef, int dayCount) {
		// 1. Cr�ation gestionnaire de dates
		Calendar calendar = Calendar.getInstance();

		// 2. Initialisation gestionnaire de dates
		calendar.setTime(aRef);

		if (dayCount > 0) {
			// 1. Prise en compte de l'offset
			calendar.add(Calendar.DATE, dayCount);

			return getSubMeasures(aRef, calendar.getTime());
		} else {
			// 1. Prise en compte de l'offset
			calendar.add(Calendar.DATE, dayCount);

			return getSubMeasures(calendar.getTime(), aRef);
		}
	}

	/**
	 * Lecture s�rie des mesures
	 * @return S�rie des mesures
	 */
	public TimeSeries getTimeSeries() {
		// 2. Cr�ation s�rie de valeur
		TimeSeries series = new TimeSeries(sensor.getId());

		for (Iterator<Date> i = this.measures.keySet().iterator(); i.hasNext();) {
			// 1. Lecture date et valeur de la mesure
			Date dMeasure = i.next();
			
			// 2. Lecture valeur brute de la mesure
			double vMeasure = this.measures.get(dMeasure);

			try {
				// 1. Enregistrement de la mesure
				series.add(new Day(dMeasure), vMeasure);

			} catch (SeriesException e) {
				// 1. Impression trace d'ex�cution
				Survey.logger.log(Level.SEVERE, sensor.getId(), e);
			}
		}

		return series;
	}
}
