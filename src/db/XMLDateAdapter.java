package db;

import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class XMLDateAdapter extends XmlAdapter<String, Date> {

	public Date unmarshal(String date) throws Exception {
		return Survey.dateFormat.parse(date);
	}

	public String marshal(Date date) throws Exception {
		return Survey.dateFormat.format(date);
	}
}