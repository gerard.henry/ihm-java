package db;

import java.util.Date;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLSensorMeasures extends XMLFile<SensorMeasures> {
	/** Barrage propri�taire de ces mesures. */
	protected Barrage barrage;

	/**
	 * Constructeur
	 * 
	 * @param aBase Gestionnaire de la base
	 * @param aBarrage
	 *            Barrage propri�taire de ces mesures
	 */
	public XMLSensorMeasures(SurveyBase aBase,Barrage aBarrage) {
		super(aBase);

		// 1. Initialisation des attributs
		this.barrage = aBarrage;
	}

	/**
	 * Lecture barrage propri�taire de ces mesures
	 * 
	 * @return Barrage propri�taire de ces mesures
	 */
	public Barrage getBarrage() {
		return this.barrage;
	}

	@Override
	protected String getObjectId(SensorMeasures aMeasures) {
		return aMeasures.getSensor().getId();
	}

	@Override
	public void marshall(SensorMeasures aObject, Element aElement) {
		// 1. Lecture r�f�rence au document cible
		Document document = aElement.getOwnerDocument();

		// 2. Lecture r�f�rence � l'instrument variable
		Sensor sensor = aObject.getSensor();

		// 3. Lecture liste des dates de mesures
		List<Date> dates = aObject.getDatesMeasure();

		// 4. Cr�ation element racine de la liste de mesures
		Element root = document.createElement("measures");

		// 5. Enregistrement identificateur de l'instrument
		root.setAttribute("sensor", sensor.getId());

		// 6. Enregistrement de l'�lement
		aElement.appendChild(root);

		for (int i = 0; i < dates.size(); i++) {
			// 1. Cr�ation element pour la ieme mesure
			Element ielement = document.createElement("measure");

			// 2. Enregistrement de l'�l�ment
			root.appendChild(ielement);

			// 3. Lecture date et valeur de la mesure
			Date datMes = dates.get(i);
			Double valMes = aObject.getMeasure(datMes);
			Double rawValue = aObject.getRawValue(datMes);
			
			// 4. Enregistrement date et valeur de la mesure
			ielement.setAttribute("date", Survey.dateFormat.format(datMes));
			ielement.setAttribute("value", valMes.toString());
			
			// 5. Enregistrement valeur brute relev�e
			if ( rawValue != null ) {
				ielement.setAttribute("raw", rawValue.toString());
			}
		}

	}

	@Override
	public SensorMeasures unmarshall(Element aElement) {
		// 1. Lecture identificateur de l'instrument
		String sensorId = aElement.getAttribute("sensor");

		// 2. Recherche de l'instrument
		Sensor sensor = this.barrage.getSensor(sensorId);

		// 3. Cr�ation liste des mesures de l'instrument
		SensorMeasures measures = new SensorMeasures(sensor);

		// 4. Lecture liste des noeud fils
		NodeList children = aElement.getChildNodes();

		for (int i = 0; i < children.getLength(); i++) {
			// 1. Lecture r�f�rence au ieme noeud
			Node inode = children.item(i);

			// 2. Seul les noeuds de type Element sont pris en compte
			if (inode.getNodeType() != Node.ELEMENT_NODE)
				continue;

			// 3. Conversion dans le type requis
			Element ielement = (Element) inode;

			try {
				// 1. Lecture date et valeur de la mesure
				Date datMes = Survey.dateFormat.parse(ielement
						.getAttribute("date"));
				Double valMes = Double.parseDouble(ielement
						.getAttribute("value"));

				// 2. Enregistrement date et valeur de la mesure
				measures.addMeasure(datMes, valMes);
				
				if ( ielement.hasAttribute("raw")) {
					// 1. Lecture valeur brute relev�e
					Double rawValue = Double.parseDouble(ielement.getAttribute("raw"));
	
					// 2. Enregistrement de la valeur brute relev�e
					measures.setRawValue(datMes,rawValue);
				}
			} catch (Exception ign) {

			}
		}
		return measures;
	}

	@Override
	public String getFileName() {
		return "don/"+ this.barrage.getId() + ".xml";
	}
}
