package db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SurveyBase implements SurveyObject {
	/** Codification des modes d'utilisation de la base. */
	public enum Mode {
		IMPORTATION, NORMAL
	}

	/** Mode d'utilisation de la base. */
	protected Mode mode;

	/** R�pertoire de lecture des fichiers XML. */
	protected String inputPath;

	/** R�pertoire d''�criture des fichiers XML. */
	protected String outputPath;

	/** Liste des barrages de la base. */
	protected XMLBarrages barrages;
	
	/**
	 * Constructeur
	 * 
	 * @param aInputPath
	 *            R�pertoire de lecture des fichiers XML
	 * @param aOutputPath
	 *            R�pertoire d''�criture des fichiers XML
	 */
	public SurveyBase(String aInputPath, String aOutputPath) {
		// 1. Initialisation des attributs
		this.inputPath = aInputPath;
		this.outputPath = aOutputPath;
		this.mode = Mode.NORMAL;
		this.barrages = new XMLBarrages(this);
	}

	/**
	 * Lecture gestionnaire des barrages de la base
	 * @return Gestionnaire des barrages de la base
	 */
	public XMLBarrages getXMLBarrages() {
		return this.barrages;
	}
	
	/**
	 * La base est-elle modifi�
	 * @return True si la base est modifi�
	 */
	public boolean isDirty() {
		// 1. Lecture liste des fichiers de la base
		List<XMLFile<?>> files = this.getXMLFiles();
		
		for ( Iterator<XMLFile<?>> i = files.iterator() ; i.hasNext() ; ) {
			// 1. Lecture ieme source de donn�es
			XMLFile<?> ifile = i.next();
			
			if( ifile.isDirty() ) return true;
		}
		
		return false;
	}

	/**
	 * R�initialisation des caches
	 */
	public void clearCaches() {
		// 1. Lecture liste des fichiers de la base
		List<XMLFile<?>> files = this.getXMLFiles();
		
		for ( Iterator<XMLFile<?>> i = files.iterator() ; i.hasNext() ; ) {
			// 1. Lecture ieme source de donn�es
			XMLFile<?> ifile = i.next();
			
			// 1. Enregistrement des modifications
			ifile.clearCache();
		}
	}
	
	/**
	 * Enregistrement de toutes les modifications
	 */
	public void applyChanges() {		
		// 1. Lecture liste des fichiers de la base
		List<XMLFile<?>> files = this.getXMLFiles();
		
		for ( Iterator<XMLFile<?>> i = files.iterator() ; i.hasNext() ; ) {
			// 1. Lecture ieme source de donn�es
			XMLFile<?> ifile = i.next();
			
			if( ifile.isDirty() ) {
				// 1. Enregistrement des modifications
				ifile.save();
			}
		}
	}
	
	/**
	 * Lecture mode d'utilisation de la base
	 * 
	 * @return Mode d'utilisation de la base
	 */
	public Mode getMode() {
		return this.mode;
	}

	/**
	 * Modification mode d'utilisation de la base
	 * 
	 * @param aMode
	 *            Mode d'utilisation de la base
	 */
	public void setMode(Mode aMode) {
		this.mode = aMode;
	}
	
	/**
	 * Lecture r�pertoire de lecture des fichiers XML
	 * @return R�pertoire de lecture des fichiers XML
	 */
	public String getInputPath() {
		return inputPath;
	}

	/**
	 * Modification r�pertoire de lecture des fichiers XML
	 * @param aPath R�pertoire de lecture des fichiers XML
	 */
	public void setInputPath(String aPath) {
		this.inputPath = aPath;
	}

	/**
	 * Lecture r�pertoire d'�criture des fichiers XML
	 * @return R�pertoire d'�criture des fichiers XML
	 */
	public String getOutputPath() {
		return outputPath;
	}

	/**
	 * Modification r�pertoire d'�criture des fichiers XML
	 * @param aPath R�pertoire d'�criture des fichiers XML
	 */
	public void setOutputPath(String aPath) {
		this.outputPath = aPath;
	}
	
	/**
	 * Lecture liste des fichiers de la base. 
	 * @return Liste des fichiers de la base
	 */
	public List<XMLFile<?>> getXMLFiles() {
		// 1. Cr�ation liste des fichiers de la base
		List<XMLFile<?>> fileList = new ArrayList <XMLFile<?>>();
		
		// 2. Lecture liste des barrages de la base
		List<Barrage> allBarrages = this.barrages.getAll();
		
		for ( int i = 0 ; i < allBarrages.size(); i++) {
			// 1. Lecture d�finition du ieme barrage
			Barrage ibarrage = allBarrages.get(i);
			
			// 2. Actualisation liste des fichiers de la base
			fileList.add(ibarrage.getXMLSensorMeasures());
			fileList.add(ibarrage.getXMLSensorResultats());
			fileList.add(ibarrage.getXMLSensors());
		}
		
		// 3. Enregistrement dictionnaire des barrages
		fileList.add(this.barrages);
		
		return fileList;
	}

	@Override
	public String getId() {
		return "Survey";
	}
}
