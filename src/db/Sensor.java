package db;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "sensor")
public class Sensor implements SurveyObject  {
	/** Référence au barrage. */
	@XmlTransient protected Barrage barrage;

	/** Attributs de l'instrument/variable. */
	protected String nature;
	protected String type;
	protected String group;
	protected String id;
	protected String internalCode;
	protected String title;
	protected String description;
	protected String state;
	protected String unit;
	protected Date installationDate;
	protected Date updateDate;
	protected Date regressionDate;
	protected double refValue;
	protected double minimumValue;
	protected double maximumValue;
	protected double maximumVariation;
	protected int measuresCount;
	protected double v1;
	protected double v2;
	protected double v3;
	protected double v4;
	protected double v5;
	protected double v6;
	protected double v7;
	protected double v8;
	protected double v9;
	protected String formula;

	/**
	 * Constructeur
	 */
	public Sensor() {
		// 1. Initialisation des attributs
		this.barrage = null;
	}

	/**
	 * Création d'un copie de l'instrument/variable

	 */
	public Sensor copy() {
		// 1. Création d'un nouvel instrument/variable
		Sensor sensor = new Sensor();
		
		// 2. Initialisation de la copie
		sensor.nature = this.nature;
		sensor.type = this.type;
		sensor.group = this.group;
		sensor.id = this.id;
		sensor.internalCode = this.internalCode;
		sensor.title = this.title;
		sensor.description = this.description;
		sensor.state = this.state;
		sensor.unit = this.unit;
		sensor.installationDate = this.installationDate;
		sensor.updateDate = this.updateDate;
		sensor.regressionDate = this.regressionDate;
		sensor.refValue = this.refValue;
		sensor.minimumValue = this.minimumValue;
		sensor.maximumValue = this.maximumValue;
		sensor.maximumVariation = this.maximumVariation;
		sensor.measuresCount = this.measuresCount;
		sensor.v1 = this.v1;
		sensor.v2 = this.v2;
		sensor.v3 = this.v3;
		sensor.v4 = this.v4;
		sensor.v5 = this.v5;
		sensor.v6 = this.v6;
		sensor.v7 = this.v7;
		sensor.v8 = this.v8;
		sensor.v9 = this.v9;
		sensor.formula = this.formula;
		sensor.barrage = null;
		
		return sensor;
	}

	/**
	 * Lecture barrage propriétaire de ces intruments/variables
	 * @return Barrage propriétaire
	 */
	@XmlTransient 
	public Barrage getBarrage() {
		return barrage;
	}

	/**
	 * initialisation barrage propriétaire de ces intruments/variables
	 * @param aBarrage Barrage propriétaire
	 */
	public void setBarrage(Barrage aBarrage) {
		 barrage = aBarrage;
	}

	/**
	 * Lecture liste des mesures de l'instrument/variable
	 * 
	 * @return Liste des noms d'instruments et de variables du barrage
	 */
	public SensorMeasures getMeasures() {
		return this.barrage.getSensorMeasures(this.id);
	}

	/**
	 *  Les mesures de cet instrument/variable sont elle modifiables
	 * @param true si les mesures de cet instrument/Variable sont modifiables
	 */
	public boolean  isMesuresEditables() {
		return this.state.startsWith("O");
	}
	

	public String getNature() {
		return (nature != null ? nature : "");
	}

	public void setNature(String nature) {
		this.nature = nature;
	}

	public String getType() {
		return (type != null ? type : "");
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGroup() {
		return (group != null ? group : "");
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInternalCode() {
		return internalCode;
	}

	public void setInternalCode(String internalCode) {
		this.internalCode = internalCode;
	}

	public String getTitle() {
		return (title != null ? title : "");
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return (description != null ? description : "");
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getState() {
		return (state != null ? state : "");
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUnit() {
		return (unit != null ? unit : "");
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@XmlJavaTypeAdapter(XMLDateAdapter.class) 
	public Date getInstallationDate() {
		return installationDate;
	}

	public void setInstallationDate(Date installationDate) {
		this.installationDate = installationDate;
	}

	@XmlJavaTypeAdapter(XMLDateAdapter.class) 
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@XmlJavaTypeAdapter(XMLDateAdapter.class) 
	public Date getRegressionDate() {
		return regressionDate;
	}

	public void setRegressionDate(Date regressionDate) {
		this.regressionDate = regressionDate;
	}

	public double getRefValue() {
		return refValue;
	}

	public void setRefValue(double refValue) {
		this.refValue = refValue;
	}

	public double getMinimumValue() {
		return minimumValue;
	}

	public void setMinimumValue(double minimumValue) {
		this.minimumValue = minimumValue;
	}

	public double getMaximumValue() {
		return maximumValue;
	}

	public void setMaximumValue(double maximumValue) {
		this.maximumValue = maximumValue;
	}

	public double getMaximumVariation() {
		return maximumVariation;
	}

	public void setMaximumVariation(double maximumVariation) {
		this.maximumVariation = maximumVariation;
	}

	public int getMeasuresCount() {
		return measuresCount;
	}

	public void setMeasuresCount(int measuresCount) {
		this.measuresCount = measuresCount;
	}

	public double getV1() {
		return v1;
	}

	public void setV1(double v1) {
		this.v1 = v1;
	}

	public double getV2() {
		return v2;
	}

	public void setV2(double v2) {
		this.v2 = v2;
	}

	public double getV3() {
		return v3;
	}

	public void setV3(double v3) {
		this.v3 = v3;
	}

	public double getV4() {
		return v4;
	}

	public void setV4(double v4) {
		this.v4 = v4;
	}

	public double getV5() {
		return v5;
	}

	public void setV5(double v5) {
		this.v5 = v5;
	}

	public double getV6() {
		return v6;
	}

	public void setV6(double v6) {
		this.v6 = v6;
	}

	public double getV7() {
		return v7;
	}

	public void setV7(double v7) {
		this.v7 = v7;
	}

	public double getV8() {
		return v8;
	}

	public void setV8(double v8) {
		this.v8 = v8;
	}

	public double getV9() {
		return v9;
	}

	public void setV9(double v9) {
		this.v9 = v9;
	}

	public String getFormula() {
		return (formula != null ? formula : "");
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}
}
