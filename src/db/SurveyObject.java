package db;

public interface SurveyObject {
	/**
	 * Lecture identificateur de l'objet
	 * @return Identificateur de l'objet
	 */
	public String getId();
}
