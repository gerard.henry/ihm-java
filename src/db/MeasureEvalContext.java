package db;

import java.util.logging.Level;

import cs.sgl.SGL;
import cs.sgl.compiler.EvalContext;
import cs.sgl.compiler.EvaluateException;
import cs.sgl.compiler.Expression;
import cs.sgl.parser.ParseException;
import cs.util.ARTException;
import cs.util.Converters;

public class MeasureEvalContext implements EvalContext {
	/** R�f�rence � l'instrument/variable. */
	protected Sensor sensor;

	/** Valeur brute de la mesure. */
	protected double value;
	
	/** Expression permettant de calculer la mesure � partir de sa valeur brute. */
	protected Expression expression;

	/**
	 * Constructeur
	 * @param aSensor Un instrument/variable
	 */
	public MeasureEvalContext(Sensor aSensor) {
		// 1. Initialisation des attributs
		this.sensor = aSensor;
		
		// 2. Lecture formule de calcul de la mesure
		String formula = this.sensor.getFormula();

		if (formula != null && formula.trim().length() != 0) {

			try {
				// 1. Compilation de la formule
				this.expression = SGL.compile(formula);
			} catch (ParseException e) {
				// 1. Impression trace d'ex�cution
				Survey.logger.log(Level.SEVERE, "Instrument : "
						+ this.sensor.getId() + ", Formula : " + formula, e);
			}
		}
	}

	/**
	 * Calcul de la valeur de la mesure
	 * @param aValue Valeur brute de la mesure
	 */
	public double getValue(double aValue) {
		// 1. Initialisation valeur brute de la mesure
		this.value = aValue;
		
		if (this.expression == null) {
			return aValue;
		} else {
			try {
				// 1. Calcul de la mesure
				return Converters.toDouble(this.expression
						.computeValue(this), Double.NaN);
			} catch (EvaluateException e) {
				// 1. Impression trace d'ex�cution
				Survey.logger.log(Level.SEVERE, "Instrument : "
						+ this.sensor.getId() + ", Formula : "
						+ this.sensor.getFormula(), e);
				
				return Double.NaN;
			}
		}		
	}
	
	@Override
	public Object executeFunction(String pName, Object[] pArgs)
	        throws EvaluateException
	{
		//---------------------------------------------------------------------
		// Fonction racine carr�e.
		//---------------------------------------------------------------------
		if (pName.equalsIgnoreCase("sqr") == true)
		{
			if (pArgs.length == 1)
			{
				return Math.sqrt(Converters.toDouble(pArgs[0], Double.NaN));
			}
			else
			{
				throw new EvaluateException("Function sqr : Bad syntax");
			}
		}
		//---------------------------------------------------------------------
		// Fonction puissance.
		//---------------------------------------------------------------------
		else if (pName.equalsIgnoreCase("pow") == true)
		{
			if (pArgs.length == 2)
			{
				Object lObject = null;
				
				
				try
				{
					lObject = Math.pow(Converters.toDouble(pArgs[0]),
							           Converters.toDouble(pArgs[1]));
				}
				catch (ARTException lException)
				{
					lException.printStackTrace();
				}
				return lObject;
			}
			else
			{
				throw new EvaluateException("Function pow : Bad syntax");
			}
		}
		//---------------------------------------------------------------------
		// Fonction cosinus.
		//---------------------------------------------------------------------
		else if (pName.equalsIgnoreCase("cos") == true)
		{
			if (pArgs.length == 1)
			{
				return Math.cos(Converters.toDouble(pArgs[0], Double.NaN));
			}
			else
			{
				throw new EvaluateException("Function cos : Bad syntax");
			}
		}
		//---------------------------------------------------------------------
		// Fonction sinus.
		//---------------------------------------------------------------------
		else if (pName.equalsIgnoreCase("sin") == true)
		{
			if (pArgs.length == 1)
			{
				return Math.sin(Converters.toDouble(pArgs[0], Double.NaN));
			}
			else
			{
				throw new EvaluateException("Function sin : Bad syntax");
			}
		}
		//---------------------------------------------------------------------
		// Fonction tangente.
		//---------------------------------------------------------------------
		else if (pName.equalsIgnoreCase("tan") == true)
		{
			if (pArgs.length == 1)
			{
				return Math.tan(Converters.toDouble(pArgs[0], Double.NaN));
			}
			else
			{
				throw new EvaluateException("Function tan : Bad syntax");
			}
		}
		//---------------------------------------------------------------------
		// Fonction exponentielle.
		//---------------------------------------------------------------------
		else if (pName.equalsIgnoreCase("exp") == true)
		{
			if (pArgs.length == 1)
			{
				return Math.exp(Converters.toDouble(pArgs[0], Double.NaN));
			}
			else
			{
				throw new EvaluateException("Function exp : Bad syntax");
			}
		}
		//---------------------------------------------------------------------
		// Fonction logarithme n�p�rien.
		//---------------------------------------------------------------------
		else if (pName.equalsIgnoreCase("log") == true)
		{
			if (pArgs.length == 1)
			{
				return Math.log(Converters.toDouble(pArgs[0], Double.NaN));
			}
			else
			{
				throw new EvaluateException("Function log : Bad syntax");
			}
		}
		//---------------------------------------------------------------------
		// Fonction logarithme � base 10.
		//---------------------------------------------------------------------
		else if (pName.equalsIgnoreCase("log10") == true)
		{
			if (pArgs.length == 1)
			{
				return Math.log10(Converters.toDouble(pArgs[0], Double.NaN));
			}
			else
			{
				throw new EvaluateException("Function log10 : Bad syntax");
			}
		}
		else
		{
			throw new EvaluateException("executeFunction : function \""
					+ pName + "\" not found.");
		}
	}
	

	@Override
	public Object getVariable(String pName) throws EvaluateException
	{
		if (pName.equalsIgnoreCase("V0") == true)
		{
			return this.value;
		}
		else if (pName.equalsIgnoreCase("V1") == true)
		{
			return this.sensor.getV1();
		}
		else if (pName.equalsIgnoreCase("V2") == true)
		{
			return this.sensor.getV2();
		}
		else if (pName.equalsIgnoreCase("V3") == true)
		{
			return this.sensor.getV3();
		}
		else if (pName.equalsIgnoreCase("V4") == true)
		{
			return this.sensor.getV4();
		}
		else if (pName.equalsIgnoreCase("V5") == true)
		{
			return this.sensor.getV5();
		}
		else if (pName.equalsIgnoreCase("V6") == true)
		{
			return this.sensor.getV6();
		}
		else if (pName.equalsIgnoreCase("V7") == true)
		{
			return this.sensor.getV7();
		}
		else if (pName.equalsIgnoreCase("V8") == true)
		{
			return this.sensor.getV9();
		}
		else if (pName.equalsIgnoreCase("V9") == true)
		{
			return this.sensor.getV9();
		}
		else
		{
			throw new EvaluateException("getVariable : variable \""
					+ pName + "\" not found.");
		}
	}
}
