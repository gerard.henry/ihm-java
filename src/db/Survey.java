package db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import tools.CIniFileReader;

public class Survey {

	/** Enregistreur des erreurs d'ex�cution. */
	public static final Logger logger = Logger
			.getLogger(Survey.class.getName());

	/** Format par d�faut des dates et des nombres. */
	public static final DateFormat dateFormat = new SimpleDateFormat(
			"dd/MM/yyyy");
	public static final DecimalFormat numberFormat = new DecimalFormat(
			"#####0.000");

	/** Instance unique de cette classe. */
	protected static final Survey instance = new Survey();

	/** Gestionnaire de la base. */
	protected SurveyBase base;

	/** Liste des objets notifi�s lors des changement de la base. */
	protected EventListenerList listeners;

	/** Jeux de caract�res permettant d'importer les donn�es Survey. */
	protected static final Charset surveyCharset = Charset.availableCharsets()
			.get("IBM850");

	/** Liste des �venements de notification. */
	protected SurveyChangeEvent eventList;

	/**
	 * Constructeur
	 */
	protected Survey() {
		// 1. Initialisation des attributs
		this.base = new SurveyBase("db", "db");
		this.listeners = new EventListenerList();
		this.eventList = new SurveyChangeEvent();
	}

	/**
	 * Enregistrement d'un objet notifi� lors d'un changement de la base
	 * 
	 * @param aListener
	 *            Un objet a ajouter � la liste de notification
	 */
	public void addSurveyListener(SurveyListener aListener) {
		this.listeners.add(SurveyListener.class, aListener);
	}

	/**
	 * Retrait d'un objet de la liste de notification des changements de la base
	 * 
	 * @param aListener
	 *            Un objet a retirer de la liste de notification
	 */
	public void removeSurveyListener(SurveyListener aListener) {
		this.listeners.remove(SurveyListener.class, aListener);
	}

	/*
	 * Construction titre des fen�tres applicatives
	 * 
	 * @param aScreen Nom de la fonction
	 * 
	 * @param aBarrage Nom du barrage actif
	 */
	public static String getTitle(String aScreen, String aBarrage) {
		String lTitle = CIniFileReader.getString("P0APPLNAME") + " "
				+ CIniFileReader.getString("P0APPLVERSION");

		if (!aScreen.isEmpty()) {
			lTitle += " - " + aScreen;
		}
		if (!aBarrage.isEmpty()) {
			lTitle += " - " + CIniFileReader.getString("INFMSG1") + " \""
					+ aBarrage + "\"";
		}

		return lTitle;
	}

	/**
	 * Lecture nom et version de l'application
	 * 
	 * @Return Nom et version de l'application
	 */
	public static String getApplicationName() {
		return CIniFileReader.getString("P0APPLNAME") + " "
				+ CIniFileReader.getString("P0APPLVERSION");
	}

	/**
	 * Lecture Instance unique de cette classe
	 * 
	 * @return Instance unique de cette classe.
	 */
	public static Survey getInstance() {
		return Survey.instance;
	}

	/**
	 * Enregistrement des modification apport�es � la base
	 */
	public void applyChanges() {
		// 1. Enregistrement des donn�es modifi�es
		this.base.applyChanges();

		// 2. Notification des modifications
		this.fireBaseChanged(new SurveyChangeEvent(this.eventList));

		// 3. R�initialisation liste des changements
		this.eventList.clear();
	}

	/**
	 * Annulation des modification apport�es � la base
	 */
	public void cancelChanges() {
		// 1. R�initialisation de la base
		this.base.clearCaches();

		// 2 . R�initialisation de la liste des �venement en attente
		this.eventList.clear();
	}

	/**
	 * Lecture gestionnaire de la base
	 * 
	 * @return Gestionnaire de la base
	 */
	public SurveyBase getBase() {
		return this.base;
	}

	/**
	 * lecture gestionnaire des barrages
	 * 
	 * @return Gestionnaire des barrages
	 */
	public XMLBarrages getXMLBarrages() {
		return this.base.getXMLBarrages();
	}

	/**
	 * Lecture liste des barrages
	 * 
	 * @return Liste des barrages
	 */
	public List<Barrage> getBarrages() {
		return this.base.getXMLBarrages().getAll();
	}

	/**
	 * Lecture liste ordonn�e des identificateurs de barrages
	 * 
	 * @return Liste ordonn�es des identificateurs de barrages
	 */
	public List<String> getBarragesNames() {
		// 1. Cr�ation liste des noms de barrages
		List<String> names = new ArrayList<String>();

		// 2. Lecture liste des barrages
		List<Barrage> barrages = this.getBarrages();

		// 3. Construction liste des identificateurs de barrages
		for (int i = 0; i < barrages.size(); i++) {
			names.add(barrages.get(i).getId());
		}

		// 4. Tri de la liste des identificateurs de barrages
		Collections.sort(names);

		return names;
	}

	/**
	 * Lecture d�finition d'un barrage
	 * 
	 * @param aId
	 *            Identificateur du barrage
	 * @return D�finition du barrage
	 */
	public Barrage getBarrage(String aId) {
		return this.base.getXMLBarrages().get(aId);
	}

	/**
	 * Ajout d�finition d'un barrage
	 * 
	 * @param aBarrage
	 *            D�finition du nouveau barrage
	 * @param aSave
	 *            true si les modifications doivent �tre enregistr�es
	 */
	public void addBarrage(Barrage aBarrage, boolean aSave) {
		// 1. Enregistrement d�finition du barrage
		this.base.getXMLBarrages().add(aBarrage);

		// 2. Actualisation liste des changements
		this.eventList.insertAddChange(aBarrage);

		// 3. Enregistrement des modifications
		if (aSave) {
			this.applyChanges();
		}
	}

	/**
	 * Actualisation d�finition d'un barrage
	 * 
	 * @param aBarrage
	 *            Barrage � actualiser
	 * @param aSave
	 *            true si les modifications doivent �tre enregistr�es
	 */
	public void updateBarrage(Barrage aBarrage, boolean aSave) {
		// 1. Actualisation d�finition du barrage
		this.base.getXMLBarrages().update(aBarrage);

		// 2. Actualisation liste des modifications
		this.eventList.insertUpdateChange(aBarrage);

		// 3. Enregistrement des modifications
		if (aSave) {
			this.applyChanges();
		}
	}

	/**
	 * Suppression d�finition d'un barrage
	 * 
	 * @param aId
	 *            Identificateur du barrage � supprimer
	 */
	public void removeBarrage(String Id) {
		Barrage barrage = null;

		// 1. Suppression d�finition du barrage
		if ((barrage = this.base.getXMLBarrages().remove(Id)) != null) {
			// 1. Lecture gestionnaire des instruments du barrage
			XMLSensors sensorsMgr = barrage.getXMLSensors();

			// 2. Suppression de la liste des instruments
			sensorsMgr.delete();

			// 3. Lecture gestionnaire des mesures du barrage
			XMLSensorMeasures measuresMgr = barrage.getXMLSensorMeasures();

			// 4. Suppression des mesures du barrage
			measuresMgr.delete();

			// 5. Lecture gestionnaire des analyses du barrage
			XMLSensorResultats resultatsMgr = barrage.getXMLSensorResultats();

			// 6. Suppression des analyse du barrage
			resultatsMgr.delete();

			// 7. Actualisation liste des changements
			this.eventList.insertRemoveChange(barrage);

			// 7. Enregistrement des modifications
			this.applyChanges();
		}
	}

	/**
	 * Exportation de la d�finition d'un barrage
	 * 
	 * @param aBarrage
	 *            Barrage � exporter
	 * @param aFile
	 *            Nom du fichier d'exportation
	 * @param aFrom
	 *            Date de d�but d'exportation des mesures (null pour la premi�re
	 *            mesure)
	 * @param aTo
	 *            Date de fin d'exportation des mesures (null pour la derni�re)
	 * 
	 */
	public void exportBarrage(Barrage aBarrage, String aFile, Date aFrom,
			Date aTo) {
		// 1. Construction d'un document vide
		Document document = createDocument();

		// 2. Lecture Liste des instruments/variables du barrage
		List<Sensor> sensors = aBarrage.getSensors();

		// 5. Initialisation nombre de mesures du barrage
		int nbMeasures = 0;

		for (int i = 0; i < sensors.size(); i++) {
			// 1. Lecture d�finition de l'instrument/variable
			Sensor iSensor = sensors.get(i);

			if (iSensor.getMeasuresCount() != 0) {
				// 1. Lecture mesures de l'instrument/variable
				SensorMeasures iMeasures = iSensor.getMeasures()
						.getSubMeasures(aFrom, aTo);

				// 2. Actualisation nombre de mesures de l'instrument
				iSensor.setMeasuresCount(iMeasures.getMeasureCount());

				// 3. Actualisation nombre de mesures du barrage
				nbMeasures += iSensor.getMeasuresCount();

				// 4. Exportation caract�ristiques de l'instrument/variable
				aBarrage.getXMLSensors().marshall(iSensor,
						document.getDocumentElement());

				// 5. Exportation des mesures de l'instrument/variable
				aBarrage.getXMLSensorMeasures().marshall(iMeasures,
						document.getDocumentElement());
			} else {
				// 1. Exportation caract�ristiques de l'instrument/variable
				aBarrage.getXMLSensors().marshall(iSensor,
						document.getDocumentElement());

			}
		}

		// 8. Actualisation d�finition du barrage
		aBarrage.setSaveDate(new Date());
		aBarrage.setMeasuresCount(nbMeasures);

		// 9. Exportation d�finition du barrage
		this.base.getXMLBarrages().marshall(aBarrage,
				document.getDocumentElement());

		// 10. Enregistrement du document
		saveDocument(aFile, document);

		// 11. Annulation des modifications locales
		this.base.clearCaches();
	}

	/**
	 * Importation de la d�finition d'un barrage
	 * 
	 * @param aFile
	 *            Nom du fichier d'exportation
	 * 
	 */
	public Barrage importBarrage(String aFile) {
		Barrage barrage = null;

		try {
			// 1. Passage en mode importation
			Survey.getInstance().getBase().setMode(SurveyBase.Mode.IMPORTATION);

			// 2. R�initialisation des caches
			Survey.getInstance().cancelChanges();

			// 3. Lecture d�finition du barrage
			Document document = loadDocument(aFile);

			if (document != null) {
				// 1. Lecture liste des elements racines des donn�es
				NodeList nodes = document.getDocumentElement().getChildNodes();

				for (int i = 0; i < nodes.getLength(); i++) {
					// 1. Lecture ieme noeud de la hi�rarchie
					Node inode = nodes.item(i);

					// 2. Seul les noeuds de type Element sont pris en compte
					if (inode.getNodeType() != Node.ELEMENT_NODE)
						continue;

					if (inode.getNodeName().equals("dam")) {
						// 1. Lecture caract�ristiques du barrage
						barrage = this.base.getXMLBarrages().unmarshall(
								(Element) inode);
					}
				}

				if (barrage != null) {
					// 1. Lecture gestionnaire des instruments et des mesures
					XMLSensors sensorsMgr = barrage.getXMLSensors();
					XMLSensorMeasures measuresMgr = barrage
							.getXMLSensorMeasures();

					for (int i = 0; i < nodes.getLength(); i++) {
						// 1. Lecture ieme noeud de la hi�rarchie
						Node inode = nodes.item(i);

						// 2. Seul les noeuds de type Element sont pris en
						// compte
						if (inode.getNodeType() != Node.ELEMENT_NODE)
							continue;

						if (inode.getNodeName().equals("sensor")) {
							// 1. Lecture d�finition de l'instrument
							Sensor sensor = sensorsMgr
									.unmarshall((Element) inode);

							// 2. Initialisation reference au barrage
							sensor.setBarrage(barrage);

							// 3. Enregistrement d�finition de l'instrument
							sensorsMgr.add(sensor);
						}
					}

					for (int i = 0; i < nodes.getLength(); i++) {
						// 1. Lecture ieme noeud de la hi�rarchie
						Node inode = nodes.item(i);

						// 2. Seul les noeuds de type Element sont pris en
						// compte
						if (inode.getNodeType() != Node.ELEMENT_NODE)
							continue;

						if (inode.getNodeName().equals("measures")) {
							// 1. Lecture d�finition des mesures
							SensorMeasures measures = measuresMgr
									.unmarshall((Element) inode);

							// 2. Enregistrement d�finition des mesures
							measuresMgr.add(measures);
						}
					}
				}
			}
		} finally {
			// 1. Restitution mode d'utilisation normal
			Survey.getInstance().getBase().setMode(SurveyBase.Mode.NORMAL);
		}
		return barrage;
	}

	/**
	 * Ajout d�finition d'un instrument
	 * 
	 * @param aSensor
	 *            D�finition de l'instrument/variable
	 * @param aSave
	 *            true si les modifications doivent �tre enregistr�es
	 */
	public void addSensor(Sensor aSensor, boolean aSave) {
		// 1. Lecture d�finition du barrage
		Barrage barrage = aSensor.getBarrage();

		// 2. Lecture gestionnaire des instruments
		XMLSensors sensorsMgr = barrage.getXMLSensors();

		// 3. Enregistrement d�finition de l'instrument
		sensorsMgr.add(aSensor);

		// 4. Actualisation nombre de variables/instrument
		if (aSensor.getNature().equals("V")) {
			barrage
					.setStockedVariablesCount(barrage
							.getStockedVariablesCount() + 1);
		} else {
			barrage.setSensorsCount(barrage.getSensorsCount() + 1);
		}

		// 5. Actualisation du barrage
		this.updateBarrage(barrage, aSave);

		// 6. Actualisation liste des changements
		this.eventList.insertUpdateChange(aSensor);

		// 7. Enregistrement des modifications
		if (aSave) {
			this.applyChanges();
		}
	}

	/**
	 * Actualisation d�finition d'un instrument
	 * 
	 * @param aSensor
	 *            D�finition de l'instrument/variable
	 * @param aSave
	 *            true si les modifications doivent �tre enregistr�es
	 */
	public void updateSensor(Sensor aSensor, boolean aSave) {
		// 1. Lecture d�finition du barrage
		Barrage barrage = aSensor.getBarrage();

		// 2. Lecture gestionnaire des instruments
		XMLSensors sensorsMgr = barrage.getXMLSensors();

		// 3. Enregistrement d�finition de l'instrument
		sensorsMgr.update(aSensor);

		// 4. Actualisation des modifications
		this.eventList.insertUpdateChange(aSensor);

		// 5. Enregistrement des modifications
		if (aSave) {
			this.applyChanges();
		}
	}

	/**
	 * suppression d�finition d'un instrument
	 * 
	 * @param aId
	 *            Identificateur de l'instrument/variable
	 */
	public void removeSensor(Sensor aSensor) {
		// 1. Lecture d�finition du barrage
		Barrage barrage = aSensor.getBarrage();

		// 2. Lecture gestionnaire des instruments
		XMLSensors sensorsMgr = barrage.getXMLSensors();

		// 3. suppression d�finition de l'instrument
		if (sensorsMgr.get(aSensor.getId()) != null) {
			// 1. Lecture gestionnaire des mesures
			XMLSensorMeasures measuresMgr = barrage.getXMLSensorMeasures();

			if (measuresMgr != null) {
				// 1. Suppression des mesures de l'instrument
				measuresMgr.remove(aSensor.getId());
			}

			// 2. Lecture gestionnaire des r�sultats
			XMLSensorResultats resultatsMgr = barrage.getXMLSensorResultats();

			if (resultatsMgr != null) {
				// 1. Suppression des r�sultats d'analyse
				resultatsMgr.remove(aSensor.getId());
			}

			// 3. Suppression de l'instrument
			sensorsMgr.remove(aSensor.getId());

			// 5. Actualisation nombre de variable/instrument du barrage
			if (aSensor.getNature().equals("V")) {
				barrage.setStockedVariablesCount(barrage
						.getStockedVariablesCount() - 1);
			} else {
				barrage.setSensorsCount(barrage.getSensorsCount() - 1);
			}

			// 6. Actualisation nombre de mesures du barrage
			barrage.setMeasuresCount(barrage.getMeasuresCount()
					- aSensor.getMeasuresCount());

			// 7. Enregistrement des modifications
			this.updateBarrage(barrage, false);

			// 8. Actualisation liste des changements
			this.eventList.insertRemoveChange(aSensor);

			// 9. Enregistrement des modifications
			this.applyChanges();
		}

	}

	/**
	 * Actualisation d'une liste de mesures
	 * 
	 * @param aMeasures
	 *            Liste de mesures
	 * @param aSave
	 *            true si les modifications doivent �tre enregistr�s
	 */
	public void updateSensorMeasures(SensorMeasures aMeasures, boolean aSave) {
		// 1. Lecture r�f�rence � l'instrument
		Sensor sensor = aMeasures.getSensor();

		// 2. Lecture r�f�rence au barrage
		Barrage barrage = sensor.getBarrage();

		// 3. Lecture gestionnaire des mesures
		XMLSensorMeasures measuresMgr = barrage.getXMLSensorMeasures();

		// 4. Lecture nombre de mesures
		int measureCount = aMeasures.getMeasureCount();

		if (measuresMgr.get(sensor.getId()) == null) {
			// 1. Ajout de la liste de mesures
			measuresMgr.add(aMeasures);

			// 2. Actualisation du nombres de mesures de l'instrument
			sensor.setMeasuresCount(measureCount);

			// 3. Actualisation du nombre de mesure du barrage
			barrage.setMeasuresCount(barrage.getMeasuresCount() + measureCount);

			// 4. Actualisation d�finition de l'instrument
			this.updateSensor(sensor, aSave);

			// 5. Actualisation d�finition du barrage
			this.updateBarrage(barrage, aSave);

			// 6. Actualisation liste des changements
			this.eventList.insertAddChange(aMeasures);
		} else {
			// 1. Actualisation de la liste de mesures
			measuresMgr.update(aMeasures);

			// 2. Actualisation du nombres de mesures de l'instrument
			sensor.setMeasuresCount(measureCount);

			// 2. Initialisation nombre de mesures du barrage
			int barrageMeasureCount = 0;

			// 3. Lecture liste des instruments du barrage
			List<Sensor> sensors = barrage.getSensors();

			// 4. Calcul nombre de mesures pour le barrage
			for (int i = 0; i < sensors.size(); i++) {
				barrageMeasureCount += sensor.getMeasuresCount();
			}

			// 5. Actualisation du nombre de mesure du barrage
			barrage.setMeasuresCount(barrageMeasureCount);

			// 6. Actualisation d�finition de l'instrument
			this.updateSensor(sensor, aSave);

			// 7. Actualisation d�finition du barrage
			this.updateBarrage(barrage, aSave);

			// 8. Actualisation liste des changements
			this.eventList.insertUpdateChange(aMeasures);
		}

		// 5. Enregistrement des modifications
		if (aSave) {
			this.applyChanges();
		}
	}

	/**
	 * Actualisation du r�sultat de l'analyse d'un instrument
	 * 
	 * @param aResultats
	 *            R�sultat de l'analyse
	 * @param aSave
	 *            true si les modifications doivent �tre enregistr�s
	 */
	public void updateSensorResultats(SensorResultats aResultats, boolean aSave) {
		// 1. Lecture r�f�rence � l'instrument
		Sensor sensor = aResultats.getSensor();

		// 2. Lecture r�f�rence au barrage
		Barrage barrage = sensor.getBarrage();

		// 3. Lecture gestionnaire des resultats
		XMLSensorResultats resultatsMgr = barrage.getXMLSensorResultats();

		if (resultatsMgr.get(sensor.getId()) == null) {
			// 1. Ajout du r�sultat de l'analyse
			resultatsMgr.add(aResultats);

			// 2. Actualisation date de regression de l'instrument
			sensor.setRegressionDate(new Date());

			// 4. Actualisation d�finition de l'instrument
			this.updateSensor(sensor, aSave);

			// 5. Actualisation liste des modifications
			this.eventList.insertAddChange(aResultats);

		} else {
			// 1. Actualisation de la liste de mesures
			resultatsMgr.update(aResultats);

			// 2. Actualisation date de regression de l'instrument
			sensor.setRegressionDate(new Date());

			// 3. Actualisation d�finition de l'instrument
			this.updateSensor(sensor, aSave);

			// 4. Actualisation liste des modifications
			this.eventList.insertUpdateChange(aResultats);
		}

		// 4. Enregistrement des modifications
		if (aSave) {
			this.applyChanges();
		}
	}

	/**
	 * Importation des fichiers de donn�es au format DBase
	 * 
	 * @param aDir
	 *            R�pertoire contenant les fichiers au format dBase
	 */
	public boolean importDB(String aDir) {
		// 1. Initialisation mode d'utilisation de la base
		this.base.setMode(SurveyBase.Mode.IMPORTATION);

		// 2. R�initialisation des caches
		this.base.clearCaches();

		try {
			if (this.importBarrages(aDir, this.base.getXMLBarrages())) {
				// 1. Lecture liste des barrages
				List<Barrage> barrages = this.base.getXMLBarrages().getAll();

				for (int i = 0; i < barrages.size(); i++) {
					// 1. Lecture r�f�rence au barrage courant
					Barrage ibarrage = barrages.get(i);

					// 2. Lecture gestionnaire des instruments du barrage
					XMLSensors sensorsMgr = ibarrage.getXMLSensors();

					// 3. Importation d�finition des instruments
					if (!this.importSensors(aDir, sensorsMgr)) {
						// 1. Restitution mode normal d'utilisation de la base
						this.base.setMode(SurveyBase.Mode.NORMAL);

						// 2. Abandon du traitement
						return false;
					} else {
						// 1. Cr�ation table de correspondance codeInterne/
						// instrument
						HashMap<String, Sensor> cintos = new HashMap<String, Sensor>();

						// 2. Lecture liste des instruments
						List<Sensor> sensorList = sensorsMgr.getAll();

						for (int j = 0; j < sensorList.size(); j++) {
							// 1. Lecture d�finition du ieme instrument/variable
							Sensor isensor = sensorList.get(j);

							// 2. Actualisation table de correspondance
							cintos.put(isensor.getInternalCode(), isensor);
						}

						// 1. Lecture gestionnaire des mesures des instruments
						XMLSensorMeasures measuresMgr = barrages.get(i)
								.getXMLSensorMeasures();

						// 2. Importation des mesures des instruments
						if (!this.importMeasures(aDir, measuresMgr, cintos)) {
							// 1. Restitution mode normal d'utilisation de la
							// base
							this.base.setMode(SurveyBase.Mode.NORMAL);

							// 2. Abandon du traitement
							return false;
						} else {
							// 1. Lecture gestionnaire des r�sultats d'analyse
							XMLSensorResultats resultatsMgr = barrages.get(i)
									.getXMLSensorResultats();

							// 2. Importation des analyses des instruments
							if (!this.importResultats(aDir, resultatsMgr)) {
								// 1. Restitution mode normal d'utilisation de
								// la base
								this.base.setMode(SurveyBase.Mode.NORMAL);

								// 2. Abandon du traitement
								return false;
							}
						}
					}
				}
			} else {
				// 1. Restitution mode normal d'utilisation de la base
				this.base.setMode(SurveyBase.Mode.NORMAL);

				// 2. Abanson du traitement
				return false;
			}
		} finally {
			// 1. Restitution mode normal d'utilisation de la base
			this.base.setMode(SurveyBase.Mode.NORMAL);
		}

		// 2. Actualisation liste des changements
		this.eventList.insertUpdateChange(this.base);

		// 3. Enregistrement des modifications
		this.applyChanges();

		// 4. R�initialisation des caches
		this.base.clearCaches();

		return true;
	}

	/**
	 * Importation liste des barrages initialement au format dBase
	 * 
	 * @param aDir
	 *            R�pertoire des fichiers au format dBase
	 * @param aManager
	 *            Gestionnaire des barrages
	 */
	protected boolean importBarrages(String aDir, XMLBarrages aManager) {
		InputStream in = null;
		Object[] lRowObjects = null;

		// 1. Construction nom du fichier au format dBase
		String fileName = aDir + "/LISTEBAR.DBF";

		try {
			// 1. Cr�ation flux de lecture de la liste des barrages
			in = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			// 1. G�n�ration trace d'ex�cution
			logger.log(Level.SEVERE, "Fichier <" + fileName + "> absent");

			// 2. Abandon de l'importation
			return false;
		}

		try {
			// 1. Cr�ation d'un lecteur au format dBase
			DBFile reader = new DBFile(in);

			// 2. Initialisation jeu de caract�res
			reader.setCharactersetName("IBM850");

			while (reader.nextRecord()) {
				// 1. Cr�ation d'un nouveau barrage
				Barrage barrage = new Barrage();

				// 2. Initialisation propri�tes du barrage
				barrage.setId(reader.getField(0).toString("").trim());
				barrage.setTitle(reader.getField(1).toString("").trim());
				barrage.setDescription(reader.getField(2).toString("").trim());
				barrage.setLaunchingDate(reader.getField(3).toDate(null));
				barrage.setSensorsCount(reader.getField(4).toDouble(0.0)
						.intValue());
				barrage.setStockedVariablesCount(reader.getField(5).toDouble(
						0.0).intValue());
				barrage.setSaveDate(reader.getField(6).toDate(null));
				barrage.setUpdateDate(reader.getField(7).toDate(null));
				barrage.setMeasuresCount(reader.getField(8).toDouble(0.0)
						.intValue());
				barrage.setWaterMinLevel(reader.getField(9).toDouble(0.0));
				barrage.setWaterNominalLevel(reader.getField(10).toDouble(0.0));
				barrage.setWaterMaxLevel(reader.getField(11).toDouble(0.0));

				// 1. Enregistrement d�finition du barrage
				aManager.add(barrage);
			}
		} catch (IOException e) {
			// 1. G�n�ration trace d'ex�cution
			logger.log(Level.SEVERE, "Fichier >" + fileName
					+ "> erreur de lecture", e);

			// 2. Abandon de l'importation
			return false;
		} finally {
			if (in != null) {
				try {
					// 1. fermeture du flux de lecture du fichier
					in.close();
				} catch (IOException e) {
				}
			}
		}

		return true;
	}

	/**
	 * Importation des instruments d'un barrage
	 * 
	 * @param aDir
	 *            R�pertoire contenant les fichiers au format dBase
	 * @param aManager
	 *            Gestionnaire des instruments/variables
	 */
	protected boolean importSensors(String aDir, XMLSensors aManager) {
		InputStream in = null;

		// 1. Construction nom du fichier au format dBase
		String fileName = aDir + "/" + aManager.getBarrage().getId() + ".INS";

		try {
			// 1. Cr�ation flux de lecture de la liste des barrages
			in = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			// 1. G�n�ration trace d'ex�cution
			logger.log(Level.SEVERE, "Fichier <" + fileName + "> absent");

			// 2. Abandon de l'importation
			return false;
		}

		try {
			// 1. Cr�ation d'un lecteur au format dBase
			DBFile reader = new DBFile(in);

			// 2. Initialisation jeu de caract�res
			reader.setCharactersetName("IBM850");

			while (reader.nextRecord()) {
				// 1. Cr�ation d'un nouveau instrument/Variable
				Sensor sensor = new Sensor();

				// 2. Initialisation r�f�rence au barrage
				sensor.setBarrage(aManager.getBarrage());

				// 3. Initialisation propri�tes de l'instrument/variable
				sensor.setNature(reader.getField(0).toString("").trim());
				sensor.setType(reader.getField(1).toString("").trim());
				sensor.setGroup(reader.getField(2).toString("").trim());
				sensor.setId(reader.getField(3).toString("").trim());
				sensor.setInternalCode(decodeSurveyCode(reader.getField(4)
						.toString("")));
				sensor.setTitle(reader.getField(5).toString("").trim());
				sensor.setDescription(reader.getField(6).toString("").trim());
				sensor.setState(reader.getField(7).toString("").trim());
				sensor.setUnit(reader.getField(8).toString("").trim());
				sensor.setInstallationDate(reader.getField(9).toDate(null));
				sensor.setUpdateDate(reader.getField(10).toDate(null));
				sensor.setRegressionDate(reader.getField(11).toDate(null));
				sensor.setRefValue(reader.getField(12).toDouble(0.0));
				sensor.setMinimumValue(reader.getField(13).toDouble(0.0));
				sensor.setMaximumValue(reader.getField(14).toDouble(0.0));
				sensor.setMaximumVariation(reader.getField(15).toDouble(0.0));
				sensor.setMeasuresCount(reader.getField(16).toDouble(0.0)
						.intValue());
				sensor.setV1(reader.getField(17).toDouble(0.0));
				sensor.setV2(reader.getField(18).toDouble(0.0));
				sensor.setV3(reader.getField(19).toDouble(0.0));
				sensor.setV4(reader.getField(20).toDouble(0.0));
				sensor.setV5(reader.getField(21).toDouble(0.0));
				sensor.setV6(reader.getField(22).toDouble(0.0));
				sensor.setV7(reader.getField(23).toDouble(0.0));
				sensor.setV8(reader.getField(24).toDouble(0.0));
				sensor.setV9(reader.getField(25).toDouble(0.0));
				sensor.setFormula(reader.getField(26).toString("").trim());

				// 1. Enregistrement d�finition de l'instrument
				aManager.add(sensor);
			}
		} catch (IOException e) {
			// 1. G�n�ration trace d'ex�cution
			logger.log(Level.SEVERE, "Fichier <" + fileName
					+ "> erreur de lecture", e);

			// 2. Abandon de l'importation
			return false;
		} finally {
			if (in != null) {
				try {
					// 1. fermeture du flux de lecture du fichier
					in.close();
				} catch (IOException e) {
				}
			}
		}

		return true;
	}

	/**
	 * Importation liste des mesures initialement au format dBase
	 * 
	 * @param aDir
	 *            R�pertoire contenant les fichiers au format dBase
	 * @param aManager
	 *            Gestionnaire des barrages
	 * @param aMap
	 *            Mapping entre le code interne d'un instrument et son id
	 */
	protected boolean importMeasures(String aDir, XMLSensorMeasures aManager,
			HashMap<String, Sensor> aMap) {
		InputStream in = null;

		// 1. Construction nom du fichier au format dBase
		String fileName = aDir + "/" + aManager.getBarrage().getId() + ".DON";

		try {
			// 1. Cr�ation flux de lecture de la liste des barrages
			in = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			// 1. G�n�ration trace d'ex�cution
			logger.log(Level.SEVERE, "Fichier <" + fileName + "> absent");

			// 2. Abandon de l'importation
			return false;
		}

		try {
			// 1. Cr�ation d'un lecteur au format dBase
			DBFile reader = new DBFile(in);

			// 2. Initialisation jeu de caract�res
			reader.setCharactersetName("IBM850");

			while (reader.nextRecord()) {

				// 1. Lecture code interne de la mesure
				String cin = decodeSurveyCode(reader.getField(1).toString(""));

				// 2. Lecture instrument/variable
				Sensor sensor = aMap.get(cin);

				if (sensor == null) {
					// 1. Anomalie dans le fichier d'entr�e
					continue;
				}


				// 3. Lecture date,valeur de la mesure et valeur brute relev�e
				Date datMes = reader.getField(0).toDate(null);
				Double valMes = reader.getField(2).toDouble(0.0);
				Double rawValue = reader.getField(3).toDouble(null);
				
				// 4. Recherche liste des mesures de l'instrument
				SensorMeasures measures = aManager.get(sensor.getId());

				if (measures == null) {
					// 1. Cr�ation de la liste de mesures
					measures = new SensorMeasures(sensor);

					// 2. Enregistrement liste des mesures
					aManager.add(measures);
				}

				// 4. Enregistrement de la mesure
				measures.addMeasure(datMes, valMes / 1000.);
				
				// 5. Enregistrement valeur brute relev�e
				if ( rawValue != null) {
					measures.setRawValue(datMes, rawValue/1000.);
				}
			}

		} catch (IOException e) {
			// 1. G�n�ration trace d'ex�cution
			logger.log(Level.SEVERE, "Fichier <" + fileName
					+ ">  erreur de lecture", e);

			// 2. Abandon de l'importation
			return true;
		} finally {
			if (in != null) {
				try {
					// 1. fermeture du flux de lecture du fichier
					in.close();
				} catch (IOException e) {
				}
			}
		}

		return true;
	}

	/**
	 * Importation liste des r�sultats d'analyse initialement au format dBase
	 * 
	 * @param aDir
	 *            R�pertoire contenant les fichiers au format dBase
	 * @param aManager
	 *            Gestionnaire des r�sultats d'analyse
	 * 
	 */
	protected boolean importResultats(String aDir, XMLSensorResultats aManager) {
		InputStream in = null;

		// 1. Construction nom du fichier au format dBase
		String fileName = aDir + "/" + aManager.getBarrage().getId() + ".RES";

		try {
			// 1. Cr�ation flux de lecture de la liste des barrages
			in = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			// 1. G�n�ration trace d'ex�cution
			logger.log(Level.SEVERE, "Fichier <" + fileName + "> absent");

			// 2. Abandon de l'importation
			return false;
		}

		try {
			// 1. Cr�ation d'un lecteur au format dBase
			DBFile reader = new DBFile(in);

			while (reader.nextRecord()) {

				// 1. Lecture identificateur de l'instrument
				String sensorId = reader.getField(0).toString("").trim();

				// 2. Lecture d�finition de l'instrument
				Sensor sensor = aManager.getBarrage().getSensor(sensorId);

				if (sensor == null) {
					// 1. Instrument inconnu, ignor�
					continue;
				}

				// 3. Cr�ation d'un r�sultat d'analyse
				SensorResultats resultats = new SensorResultats();

				// 4. Initialisation r�f�rence � l'instrument
				resultats.setSensor(sensor);

				// 5. Lecture des r�sultats de l'analyse
				resultats.setId(reader.getField(0).toString("").trim());
				resultats.setLNasous(reader.getField(1).toDouble(0.0)
						.intValue());
				resultats.setLDuration(reader.getField(2).toDouble(0.0)
						.intValue());
				resultats.setLAnalyseBeginDate(reader.getField(3).toDate(null));
				resultats.setLAnalyseEndDate(reader.getField(4).toDate(null));
				resultats.setLValuesCount(reader.getField(5).toDouble(0.0)
						.intValue());
				resultats.setLR2(reader.getField(6).toDouble(0.0));
				resultats.setLMeanCote(reader.getField(7).toDouble(0.0));
				resultats.setLSigCote(reader.getField(8).toDouble(0.0));
				resultats.setLConstantTerm(reader.getField(9).toDouble(0.0));
				resultats.setLT(reader.getField(10).toDouble(0.0));
				resultats.setLExpT(reader.getField(11).toDouble(0.0));
				resultats.setLExpMT(reader.getField(12).toDouble(0.0));
				resultats.setLSin(reader.getField(13).toDouble(0.0));
				resultats.setLCos(reader.getField(14).toDouble(0.0));
				resultats.setLSico(reader.getField(15).toDouble(0.0));
				resultats.setLSin2(reader.getField(16).toDouble(0.0));
				resultats.setLZ(reader.getField(17).toDouble(0.0));
				resultats.setLZ2(reader.getField(18).toDouble(0.0));
				resultats.setLZ3(reader.getField(19).toDouble(0.0));
				resultats.setLZ4(reader.getField(20).toDouble(0.0));
				resultats.setLP1(reader.getField(21).toDouble(0.0));
				resultats.setLP2(reader.getField(22).toDouble(0.0));
				resultats.setLP3(reader.getField(23).toDouble(0.0));
				resultats.setLP4(reader.getField(24).toDouble(0.0));
				resultats.setLP56(reader.getField(25).toDouble(0.0));
				resultats.setLP78(reader.getField(26).toDouble(0.0));

				// 5. Enregistrement du r�sultat de l'analyse
				aManager.add(resultats);
			}

		} catch (IOException e) {
			// 1. G�n�ration trace d'ex�cution
			logger.log(Level.SEVERE, "Fichier <" + fileName
					+ "> erreur de lecture", e);

			// 2. Abandon de l'importation
			return true;
		} finally {
			if (in != null) {
				try {
					// 1. fermeture du flux de lecture du fichier
					in.close();
				} catch (IOException e) {
				}
			}
		}

		return true;
	}

	/**
	 * D�codage d'un entier sur deux octets repr�sentant un code
	 * d'identification interne
	 * 
	 * @param aCoded
	 *            Valeur de l'entier sur deux octets
	 * @return Valeur d�cod�e de l'entier
	 */
	public static String decodeSurveyCode(String aCoded) {
		return 256 * ((int) aCoded.charAt(0)) + ((int) aCoded.charAt(1)) + "";
	}

	/**
	 * D�codage d'une date (Format ancienne application) sur deux octets
	 * repr�sentant le nombre de jours apr�s le 1/1/1900.
	 * 
	 * @param aCoded
	 *            Valeur de la date sur deux octets
	 * @return Valeur d�cod�e de la date
	 */
	public static Date decodeSurveyDate(String aCoded) {
		// 1. Calcul nombre de jours depuis le 1/1/1900
		int dayCount = 256 * ((int) aCoded.charAt(0))
				+ ((int) aCoded.charAt(1));

		// 2. Lecture gestionnaire de date
		Calendar calendar = Calendar.getInstance();

		// 3. Initialisation de la date
		calendar.set(1900, 0, 1, 0, 0, 0);
		calendar.add(Calendar.DATE, dayCount);

		// 4. Lecture valeur de la date
		return calendar.getTime();
	}

	/**
	 * D�codage d'une chaine de caract�re au format Survey
	 * 
	 * @param aValue
	 *            Une valeur repr�sentant une chaine de caract�re
	 * @return Valeur de la cha�ne de caract�res d�cod�es
	 */
	public static String decodeSurveyString(Object aValue) {
		if (aValue == null) {
			return "";
		} else {
			ByteBuffer bb = ByteBuffer.wrap(((String) aValue).getBytes());

			CharBuffer cb = surveyCharset.decode(bb);

			// 1. Conversion de la chaine
			String decoded = cb.toString(); // new

			return decoded.trim();
		}
	}

	/**
	 * 
	 * Mise en forme d'une date
	 * 
	 * @param aDate
	 *            date � mettre en forme
	 */
	public static String format(Date aDate) {
		if (aDate == null) {
			return "";
		} else {
			return dateFormat.format(aDate);
		}
	}

	/**
	 * Mise en forme d'une nombre
	 * 
	 * @param aDate
	 *            date � mettre en forme
	 */
	public static String format(Double aNumber) {
		if (aNumber == null) {
			return "";
		} else {
			return numberFormat.format(aNumber);
		}
	}

	/**
	 * Mise en forme d'une nombre
	 * 
	 * @param aDate
	 *            date � mettre en forme
	 */
	public static String format(Double aNumber, DecimalFormat aFormat) {
		if (aNumber == null) {
			return "";
		} else {
			return aFormat.format(aNumber);
		}
	}

	/**
	 * Cr�ation d'un document
	 * 
	 * @return Document cr��
	 */
	public static Document createDocument() {
		Document document = null;

		try {
			// 1. Lecture r�f�rence � la fabrique de DocumentBuilder
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();

			// 2. Cr�ation d'une instance de DocumentBuilder
			DocumentBuilder builder = factory.newDocumentBuilder();

			// 3. Construction d'un document vide
			document = builder.newDocument();

			// 4. Cr�ation �lement racine du document
			Element root = document.createElement("Survey");

			// 5. Affectation element racine du document
			document.appendChild(root);

		} catch (ParserConfigurationException e) {
			// 1. Enregistrement trace d'ex�cution
			Survey.logger.log(Level.SEVERE, "", e);
		}

		return document;
	}

	/**
	 * Lecture d'un fichier au format XML
	 * 
	 * @param aFilePath
	 *            Chemin du fichier
	 * @return Document image du fichier
	 */
	public static Document loadDocument(String aFilePath) {
		Document document = null;

		// 1. Cr�ation fichier repr�sentant la source de donn�es
		File file = new File(aFilePath);

		if (file.exists() && file.canRead()) {
			try {
				// 1. Lecture r�f�rence � la fabrique de DocumentBuilder
				DocumentBuilderFactory factory = DocumentBuilderFactory
						.newInstance();

				// 2. Cr�ation d'une instance de DocumentBuilder
				DocumentBuilder builder = factory.newDocumentBuilder();

				// 3. Construction repr�sentation DOM du fichier XML
				document = builder.parse(file);

			} catch (Exception e) {
				// 1. Enregistrement trace d'ex�cution
				Survey.logger.log(Level.SEVERE, "", e);
			}
		}

		return document;
	}

	/**
	 * Enregistrement document au format XML
	 * 
	 * @param aFilePath
	 *            Chemin du fichier
	 * @return Document � sauvegarder
	 */
	public static void saveDocument(String aFilePath, Document aDocument) {
		FileOutputStream out = null;

		// 1. Cr�arion r�f�rence au fichier XML
		File file = new File(aFilePath);

		try {
			if (!file.exists()) {
				// 1. Lecture r�pertoire parent
				File dir = file.getParentFile();

				// 2. Cr�ation des r�pertoires parent
				dir.mkdirs();
			}

			// 2. Cr�ation du fichier XML
			file.createNewFile();

			// 3. Cr�ation flux d'�criture du fichier XML
			out = new FileOutputStream(file);

			// 4. Cr�ation source et r�sultat de la transformation
			DOMSource source = new DOMSource(aDocument);
			StreamResult result = new StreamResult(out);

			// 5. Cr�ation d'une fabrique de Transformer
			TransformerFactory factory = TransformerFactory.newInstance();

			// 6. Cr�ation d'une instance de Transformer
			Transformer transformer = factory.newTransformer();

			// 7. Initialisation propri�tes du Transformer
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			// 8. Transformation du mod�le DOM en fichier XML
			transformer.transform(source, result);
		} catch (Exception e) {
			// 1. Enregistrement trace d'ex�cution
			Survey.logger.log(Level.SEVERE, "", e);
		} finally {
			try {
				if (out != null) {
					// 1. Fermeture du flux de g�n�ration du fichier XML
					out.close();
				}
			} catch (IOException ign) {
			}
		}
	}

	/**
	 * Notification des modifications de la base
	 */
	private void fireBaseChanged(final SurveyChangeEvent aEvents) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				// 1. Lecture liste des objets � notifier
				Object[] notificationList = listeners.getListenerList();

				for (int i = notificationList.length - 2; i >= 0; i -= 2) {
					if (notificationList[i] == SurveyListener.class) {
						((SurveyListener) notificationList[i + 1])
								.baseChanged(aEvents);
					}
				}
			}
		});

	}
}
