package db;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name="sensor")
public class SensorResultats implements SurveyObject {
	/** R�f�rence � l'instrument/variable propri�taire des r�sultats. */
	protected Sensor sensor;

	/** Attributs du r�sultat de l'analyse. */
	protected String id;
	protected Integer lNasous;
	protected Integer lDuration;
	protected Date lAnalyseBeginDate;
	protected Date lAnalyseEndDate;
	protected Integer lValuesCount;
	protected Double lR2;
	protected Double lMeanCote;
	protected Double lSigCote;
	protected Double lConstantTerm;
	protected Double lT;
	protected Double lExpT;
	protected Double lExpMT;
	protected Double lSin;
	protected Double lCos;
	protected Double lSico;
	protected Double lSin2;
	protected Double lZ;
	protected Double lZ2;
	protected Double lZ3;
	protected Double lZ4;
	protected Double lP1;
	protected Double lP2;
	protected Double lP3;
	protected Double lP4;
	protected Double lP56;
	protected Double lP78;

	/**
	 * Constructeur
	 */
	public SensorResultats() {
	}

	/**
	 * Lecture r�f�rence � l'instrument/variable propri�taire des resultats de
	 * l'analyse
	 * 
	 * @return Sensor propri�taire des r�sultats
	 */
	@XmlTransient
	public Sensor getSensor() {
		return this.sensor;
	}

	/**
	 * initialisation barrage propri�taire de ces intruments/variables
	 * 
	 * @param aBarrage
	 *            Barrage propri�taire
	 */
	public void setSensor(Sensor aSensor) {
		sensor = aSensor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getLNasous() {
		return lNasous;
	}

	public void setLNasous(Integer nasous) {
		lNasous = nasous;
	}

	public Integer getLDuration() {
		return lDuration;
	}

	public void setLDuration(Integer duration) {
		lDuration = duration;
	}

	@XmlJavaTypeAdapter(XMLDateAdapter.class)
	public Date getLAnalyseBeginDate() {
		return lAnalyseBeginDate;
	}

	public void setLAnalyseBeginDate(Date analyseBeginDate) {
		lAnalyseBeginDate = analyseBeginDate;
	}

	@XmlJavaTypeAdapter(XMLDateAdapter.class)
	public Date getLAnalyseEndDate() {
		return lAnalyseEndDate;
	}

	public void setLAnalyseEndDate(Date analyseEndDate) {
		lAnalyseEndDate = analyseEndDate;
	}

	public Integer getLValuesCount() {
		return lValuesCount;
	}

	public void setLValuesCount(Integer valuesCount) {
		lValuesCount = valuesCount;
	}

	public Double getLR2() {
		return lR2;
	}

	public void setLR2(Double lr2) {
		lR2 = lr2;
	}

	public Double getLMeanCote() {
		return lMeanCote;
	}

	public void setLMeanCote(Double meanCote) {
		lMeanCote = meanCote;
	}

	public Double getLSigCote() {
		return lSigCote;
	}

	public void setLSigCote(Double sigCote) {
		lSigCote = sigCote;
	}

	public Double getLConstantTerm() {
		return lConstantTerm;
	}

	public void setLConstantTerm(Double constantTerm) {
		lConstantTerm = constantTerm;
	}

	public Double getLT() {
		return lT;
	}

	public void setLT(Double lt) {
		lT = lt;
	}

	public Double getLExpT() {
		return lExpT;
	}

	public void setLExpT(Double expT) {
		lExpT = expT;
	}

	public Double getLExpMT() {
		return lExpMT;
	}

	public void setLExpMT(Double expMT) {
		lExpMT = expMT;
	}

	public Double getLSin() {
		return lSin;
	}

	public void setLSin(Double sin) {
		lSin = sin;
	}

	public Double getLCos() {
		return lCos;
	}

	public void setLCos(Double cos) {
		lCos = cos;
	}

	public Double getLSico() {
		return lSico;
	}

	public void setLSico(Double sico) {
		lSico = sico;
	}

	public Double getLSin2() {
		return lSin2;
	}

	public void setLSin2(Double sin2) {
		lSin2 = sin2;
	}

	public Double getLZ() {
		return lZ;
	}

	public void setLZ(Double lz) {
		lZ = lz;
	}

	public Double getLZ2() {
		return lZ2;
	}

	public void setLZ2(Double lz2) {
		lZ2 = lz2;
	}

	public Double getLZ3() {
		return lZ3;
	}

	public void setLZ3(Double lz3) {
		lZ3 = lz3;
	}

	public Double getLZ4() {
		return lZ4;
	}

	public void setLZ4(Double lz4) {
		lZ4 = lz4;
	}

	public Double getLP1() {
		return lP1;
	}

	public void setLP1(Double lp1) {
		lP1 = lp1;
	}

	public Double getLP2() {
		return lP2;
	}

	public void setLP2(Double lp2) {
		lP2 = lp2;
	}

	public Double getLP3() {
		return lP3;
	}

	public void setLP3(Double lp3) {
		lP3 = lp3;
	}

	public Double getLP4() {
		return lP4;
	}

	public void setLP4(Double lp4) {
		lP4 = lp4;
	}

	public Double getLP56() {
		return lP56;
	}

	public void setLP56(Double lp56) {
		lP56 = lp56;
	}

	public Double getLP78() {
		return lP78;
	}

	public void setLP78(Double lp78) {
		lP78 = lp78;
	}
}
