package db;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;
import java.util.logging.Level;

public class DBFile {
	/** Codification des types de champs. */
	public static final byte FIELD_TYPE_C = (byte) 'C';
	public static final byte FIELD_TYPE_L = (byte) 'L';
	public static final byte FIELD_TYPE_N = (byte) 'N';
	public static final byte FIELD_TYPE_F = (byte) 'F';
	public static final byte FIELD_TYPE_D = (byte) 'D';

	protected abstract class Field {
		/* Donn�es de d�finition d'un champ */
		protected String fieldName;
		protected int fieldLength;
		protected byte decimalCount;

		/** Valeur du champ pour le dernier enregistrement lu. */
		protected byte values[];

		/*
		 * Constructeur
		 */
		private Field() {
		}

		public byte[] getValues() {
			return values;
		}

		public String toString(String aDefault) {
			try {
				return new String(this.values, DBFile.this.characterSetName);
			} catch (UnsupportedEncodingException e) {
				Survey.logger.log(Level.SEVERE, "Field : " + this.fieldName, e);
				return null;
			}
		}

		public Date toDate(Date aDefault) {
			Survey.logger.log(Level.SEVERE, "Field : " + this.fieldName
					+ " isn't a date field");

			return null;
		}

		public Float toFloat(Float aDefault) {
			Survey.logger.log(Level.SEVERE, "Field : " + this.fieldName
					+ " isn't a float field");

			return null;
		}

		public Double toDouble(Double aDefault) {
			Survey.logger.log(Level.SEVERE, "Field : " + this.fieldName
					+ " isn't a double field");

			return null;
		}

		public Boolean toBoolean(Boolean adefault) {
			Survey.logger.log(Level.SEVERE, "Field : " + this.fieldName
					+ " isn't a logical field");

			return null;
		}

		protected abstract void readValues() throws IOException;

		public abstract byte getDataType();

		public String getName() {
			return this.fieldName;
		}

		public int getFieldLength() {
			return fieldLength;
		}

		public int getDecimalCount() {
			return decimalCount;
		}
	}

	public class StringField extends Field {
		@Override
		protected void readValues() throws IOException {
			values = new byte[this.getFieldLength()];
			dataInputStream.read(values);
		}

		@Override
		public Date toDate(Date aDefault) {
			if (values.length == 2) {
				int lv, rv;

				if (values[0] < 0) {
					lv = 256 + values[0];
				} else {
					lv = values[0];
				}
				if (values[1] < 0) {
					rv = 256 + values[1];
				} else {
					rv = values[1];
				}
				int dayCount = 256 * lv + rv;

				Calendar calendar = Calendar.getInstance();

				calendar.set(1900, 0, 1, 0, 0, 0);
				calendar.add(Calendar.DATE, dayCount);

				return calendar.getTime();
			} else {
				Survey.logger.log(Level.SEVERE, "Field : " + this.fieldName
						+ " isn't a date field");

				return null;
			}
		}

		@Override
		public byte getDataType() {
			return FIELD_TYPE_C;
		}

	}

	public class FloatField extends Field {
		@Override
		public Float toFloat(Float aDefault) {
			if (!DBFile.this.isEmpty(values)) {
				try {
					if (values.length > 0 && !DBFile.this.contains(values, (byte) '?')) {

						return new Float(new String(values));
					} else {
						return null;
					}
				} catch (NumberFormatException e) {
					Survey.logger.log(Level.SEVERE, "Field : " + this.fieldName
							+ " isn't a float field", e);

					return null;
				}
			} else {
				return aDefault;
			}
		}

		@Override
		protected void readValues() throws IOException {
			values = new byte[this.getFieldLength()];
			dataInputStream.read(values);
			values = DBFile.this.trimLeftSpaces(values);
		}

		@Override
		public byte getDataType() {
			return FIELD_TYPE_F;
		}

	}

	public class DoubleField extends Field {
		@Override
		public Double toDouble(Double aDefault) {

			if (!DBFile.this.isEmpty(this.values)) {
				try {
					if (values.length > 0 && !DBFile.this.contains(values, (byte) '?')) {
						return new Double(new String(values));
					} else {
						return null;
					}
				} catch (NumberFormatException e) {
					System.out.println(this.toString("")+"/");
					Survey.logger.log(Level.SEVERE, "Field : " + this.fieldName
							+ " isn't a double field", e);

					return null;
				}
			} else {
				return aDefault;
			}
		}

		@Override
		protected void readValues() throws IOException {
			values = new byte[this.getFieldLength()];
			dataInputStream.read(values);
			values = DBFile.this.trimLeftSpaces(values);
		}

		@Override
		public byte getDataType() {
			return FIELD_TYPE_N;
		}

	}

	public class DateField extends Field {
		@Override
		public Date toDate(Date aDefault) {
			if (!DBFile.this.isEmpty(values)) {
				try {
					GregorianCalendar calendar = new GregorianCalendar(Integer
							.parseInt(new String(values, 0, 4)), Integer
							.parseInt(new String(values, 4, 2)) - 1, Integer
							.parseInt(new String(values, 6, 2)));

					return calendar.getTime();
				} catch (NumberFormatException e) {
					Survey.logger.log(Level.SEVERE, "Field : " + this.fieldName
							+ " isn't a date field", e);

					return null;
				}
			} else {
				return aDefault;
			}
		}

		@Override
		protected void readValues() throws IOException {
			values = new byte[8];
			dataInputStream.read(values);
		}

		@Override
		public byte getDataType() {
			return FIELD_TYPE_D;
		}

	}

	public class BooleanField extends Field {
		@Override
		public Boolean toBoolean(Boolean aDefault) {
			if (values[0] == 'Y' || values[0] == 't' || values[0] == 'T'
					|| values[0] == 't') {
				return Boolean.TRUE;
			} else {

				return Boolean.FALSE;
			}
		}

		@Override
		protected void readValues() throws IOException {
			values = new byte[1];
			dataInputStream.read(values);
		}

		@Override
		public byte getDataType() {
			return FIELD_TYPE_L;
		}

	}

	/** Jeu de caract�res utilis� pour d�coder les chaines de caract�res. */
	protected String characterSetName = "8859_1";

	/** Donn�es de d�finition du fichier au format dbase. */
	private short headerLength;
	private int numberOfRecords;
	private short recordLength;

	private Field fields[];

	/** Flux de lecture du fichier au format dbase. */
	protected DataInputStream dataInputStream;

	/**
	 * Constructeur
	 * 
	 * @param aIn
	 *            Flux de lecture du fichier au format dbase
	 * @throws IOException
	 */
	public DBFile(InputStream aIn) throws IOException {
		this.dataInputStream = new DataInputStream(aIn);
		this.readHeader();

		/* it might be required to leap to the start of records at times */
		int t_dataStartIndex = this.headerLength
				- (32 + (32 * this.fields.length)) - 1;
		if (t_dataStartIndex > 0) {

			dataInputStream.skip(t_dataStartIndex);
		}
	}

	/**
	 * Lecture nombre d'enregistrement pr�sents dans la fichier
	 * 
	 * @return Nombre d'enregistrements
	 */
	public int getRecordCount() {

		return this.numberOfRecords;
	}

	/**
	 * Lecture d�finition d'un champ
	 * 
	 * @param aIndex
	 *            Index du champ
	 * @return D�finition du champ
	 */
	public Field getField(int index) {
		return this.fields[index];
	}

	/**
	 * Lecture nombre de champs pr�sents dans le fichier.
	 * 
	 * @return Nombre de champs
	 */
	public int getFieldCount() {
		if (this.fields != null) {
			return this.fields.length;
		} else {
			return 0;
		}
	}

	/**
	 * Lecture jeu de caract�res utilis� pour d�coder les chaines de caract�res
	 * 
	 * @return Nom du jeu de caract�res
	 */
	public String getCharactersetName() {

		return this.characterSetName;
	}

	/**
	 * Modification jeu de caract�res utilis� pour d�coder les chaines de
	 * caract�res
	 * 
	 * @param Nom
	 *            du jeu de caract�res
	 */
	public void setCharactersetName(String characterSetName) {

		this.characterSetName = characterSetName;
	}

	public boolean nextRecord() throws IOException {
		byte buffer[] = null;

		try {
			// 1. On ignore les enregistrements pr�sents dans le fichier mais
			// supprim�
			boolean isDeleted = false;
			do {
				if (isDeleted) {
					dataInputStream.skip(this.recordLength - 1);
				}

				int t_byte = dataInputStream.readByte();
				if (t_byte == 0x1A) {
					return false;
				}

				isDeleted = (t_byte == '*');
			} while (isDeleted);

			for (int i = 0; i < this.fields.length; i++) {
				// 1. Lecture valeur du champ
				fields[i].readValues();
			}

		} catch (EOFException e) {
			return false;
		}

		return true;
	}

	/**
	 * Lecture donn�es de d�finition du fichier au format dbase
	 */
	protected void readHeader() throws IOException {
		dataInputStream.readByte();
		dataInputStream.readByte();
		dataInputStream.readByte();
		dataInputStream.readByte();
		numberOfRecords = this.readLittleEndianInt();
		headerLength = this.readLittleEndianShort();
		recordLength = this.readLittleEndianShort();
		this.readLittleEndianShort();
		dataInputStream.readByte();
		dataInputStream.readByte();
		this.readLittleEndianInt();
		dataInputStream.readInt();
		dataInputStream.readInt();
		dataInputStream.readByte();
		dataInputStream.readByte();
		this.readLittleEndianShort();

		Vector<Field> v_fields = new Vector<Field>();

		Field field = this.readField();
		while (field != null) {

			v_fields.addElement(field);
			field = this.readField();
		}

		this.fields = v_fields.toArray(new Field[v_fields.size()]);
	}

	/**
	 * Lecture donn�es de d�finition d'un champ
	 * 
	 * @return D�finition du champ ou null si fin de liste
	 */
	protected Field readField() throws IOException {
		Field field = null;

		// 1. Lecture premier caract�re du nom du champ ou code
		// de fin de la liste des champs
		byte t_byte = dataInputStream.readByte();

		if (t_byte != (byte) 0x0d) {
			byte name[] = new byte[11];
			byte dummy[] = new byte[7];

			// 1. Lecture nom du champ
			name[0] = t_byte;
			dataInputStream.readFully(name, 1, 10);

			switch (dataInputStream.readByte()) {
			case FIELD_TYPE_C:
				field = new StringField();
				break;
			case FIELD_TYPE_D:
				field = new DateField();
				break;
			case FIELD_TYPE_F:
				field = new FloatField();
				break;
			case FIELD_TYPE_L:
				field = new BooleanField();
				break;
			case FIELD_TYPE_N:
				field = new DoubleField();
				break;
			default: {
				throw new IOException("Unsupported field type");
			}

			}
			// 2. Assignation nom et type du champ
			for (int i = 0; i < name.length; i++) {
				if (name[i] == (byte) 0) {
					field.fieldName = new String(name, 0, i);
					break;
				}
			}

			// 2. Lecture donn�es de d�finition du champ
			this.readLittleEndianInt();
			field.fieldLength = dataInputStream.readUnsignedByte();
			field.decimalCount = dataInputStream.readByte();
			this.readLittleEndianShort();
			dataInputStream.readByte();
			this.readLittleEndianShort();
			dataInputStream.readByte();
			dataInputStream.readFully(dummy);
			dataInputStream.readByte();
		}
		return field;
	}

	private int readLittleEndianInt() throws IOException {

		int bigEndian = 0;
		for (int shiftBy = 0; shiftBy < 32; shiftBy += 8) {

			bigEndian |= (dataInputStream.readUnsignedByte() & 0xff) << shiftBy;
		}

		return bigEndian;
	}

	private short readLittleEndianShort() throws IOException {

		int low = dataInputStream.readUnsignedByte() & 0xff;
		int high = dataInputStream.readUnsignedByte();

		return (short) (high << 8 | low);
	}

	private byte[] trimLeftSpaces(byte[] arr) {
		StringBuffer t_sb = new StringBuffer(arr.length);

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != ' ') {
				t_sb.append((char) arr[i]);
			}
		}

		return t_sb.toString().getBytes();
	}

	private boolean contains(byte[] arr, byte value) {

		boolean found = false;
		for (int i = 0; i < arr.length; i++) {

			if (arr[i] == value) {

				found = true;
				break;
			}
		}

		return found;
	}

	private boolean isEmpty(byte[] arr) {

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != ' ' && arr[i] != (byte)0) {
				return false;
			}
		}

		return true;
	}
}
