package db;

import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Element;

public class XMLBarrages extends XMLFile<Barrage> {
	/** Sérialiseur/Sésérialiseur d'un objet Barrage. */
	protected Marshaller marshaller;
	protected Unmarshaller unmarshaller;

	/**
	 * Constructeur
	 * @param aBase Gestionnaire de la base
	 */
	public XMLBarrages(SurveyBase aBase) {
		super(aBase);

		try {
			// 1. Création contexte de binding
			JAXBContext context = JAXBContext.newInstance(Barrage.class);

			// 2. Création Sérialiseur/Sésérialiseur d'un objet Barrage
			unmarshaller = context.createUnmarshaller();
			marshaller = context.createMarshaller();
		} catch (JAXBException e) {
			// 1. Enregistrement trace d'exécution
			Survey.logger.log(Level.SEVERE, "", e);
		}
	}

	@Override
	public String getObjectId(Barrage aObject) {
		return aObject.getId();
	}

	@Override
	public void marshall(Barrage aObject, Element aElement) {
		try {
			this.marshaller.marshal(aObject, aElement);
		} catch (JAXBException e) {
			// 1. Enregistrement trace d'exécution
			Survey.logger.log(Level.SEVERE, "marshall", e);
		}
	}

	@Override
	public Barrage unmarshall(Element aElement) {
		try {
			return (Barrage) this.unmarshaller.unmarshal(aElement);
		} catch (JAXBException e) {
			// 1. Enregistrement trace d'exécution
			Survey.logger.log(Level.SEVERE, "unmarshall", e);

			return null;
		}
	}

	@Override
	public String getFileName() {
		return "damsList.xml";
	}
}
