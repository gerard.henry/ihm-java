package db;

import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Element;

public class XMLSensors extends XMLFile<Sensor> {
	/** Sérialiseur/Sésérialiseur d'un objet Sensor. */
	protected Marshaller marshaller;
	protected Unmarshaller unmarshaller;

	/** Le barrage propriétaire de ces intruments/variables. */
	protected Barrage barrage;
	
	/**
	 * Constructeur
	 * @param aBase Gestionnaire de la base
	 * @param aBarrage Le barrage propriétaire de ces intruments/variables
	 */
	public XMLSensors(SurveyBase aBase,Barrage aBarrage) {
		super(aBase);

		// 1. Initialisation des attributs
		this.barrage = aBarrage;
		
		try {
			// 1. Création contexte de binding
			JAXBContext context = JAXBContext.newInstance(Sensor.class);

			// 2. Création Sérialiseur/Sésérialiseur d'un objet Barrage
			unmarshaller = context.createUnmarshaller();
			marshaller = context.createMarshaller();
		} catch (JAXBException e) {
			// 1. Enregistrement trace d'exécution
			Survey.logger.log(Level.SEVERE, "", e);
		}
	}

	/**
	 * Lecture barrage propriétaire de ces instruments/variables
	 * @return Barrage propriétaire de ces instruments/variables
	 */
	public Barrage getBarrage() {
		return this.barrage;
	}
	
	@Override
	protected String getObjectId(Sensor aObject) {
		return aObject.id;
	}

	@Override
	public void marshall(Sensor aObject, Element aElement) {
		try {
			this.marshaller.marshal(aObject, aElement);
		} catch (JAXBException e) {
			// 1. Enregistrement trace d'exécution
			Survey.logger.log(Level.SEVERE, "marshall", e);
		}
	}

	@Override
	public Sensor unmarshall(Element aElement) {
		try {
			return (Sensor) this.unmarshaller.unmarshal(aElement);
		} catch (JAXBException e) {
			// 1. Enregistrement trace d'exécution
			Survey.logger.log(Level.SEVERE, "unmarshall", e);

			return null;
		}
	}

	@Override
	protected void afterUnmarshalling(Sensor aSensor) {
		// 1. Initialisation référence au barrage
		aSensor.setBarrage(this.barrage);
	}

	@Override
	public String getFileName() {
		return "ins/"+this.barrage.getId()+".xml";
	}
}
