package db;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class XMLFile<T> {
	/** Gestionnaire de la base. */
	protected SurveyBase base;

	/** Liste des objets. */
	protected Map<String, T> objects;

	/** Drapeau de modification de la liste des objets. */
	protected boolean isDirty;

	/**
	 * Constructeur
	 * 
	 * @param aBase
	 *            Gestionnaire de la base
	 */
	public XMLFile(SurveyBase aBase) {
		// 1. Initialisation des attributs
		this.isDirty = false;
		this.objects = null;
		this.base = aBase;
	}

	/**
	 * Lecture drapeau de modification de la table des barrages
	 * 
	 * @return true si au moins un barrage a �t� modifi�/ajoute/supprim�e
	 */
	public boolean isDirty() {
		return this.isDirty;
	}

	/**
	 * Suppression du fichier de donn�es
	 */
	public void delete() {
		// 1. Cr�ation fichier repr�sentant la source de donn�es
		File file = new File(this.base.getInputPath() + "/" + this.getFileName());

		// 2. Suppression du fichier
		file.delete();
	}

	/**
	 * Lecture liste des objets
	 * 
	 * @return Liste des objets
	 */
	public List<T> getAll() {
		if (this.objects == null) {
			// 1. Lecture liste des objets
			this.load();
		}

		return new ArrayList<T>(this.objects.values());
	}

	/**
	 * Lecture d�finition d'un objet
	 * 
	 * @param aId
	 *            Identificateur de l'objet
	 * @return L'objet avec cet identificateur
	 */
	public T get(String aId) {
		if (this.objects == null) {
			// 1. Lecture liste des objets
			this.load();
		}

		return this.objects.get(aId);
	}

	/**
	 * Enregistrement d�finition d'un objet
	 * 
	 * @param aObject
	 *            Un objet � enregistrer
	 * @return L'objet enregistr�
	 */
	public T add(T aObject) {
		if (this.objects == null) {
			// 1. Lecture liste des objets
			this.load();
		}

		if (this.objects.containsKey(this.getObjectId(aObject))) {
			// 1. L'objet est d�j� pr�sent dans la liste
			return null;
		} else {
			// 1. Enregistrement de l'objet
			this.objects.put(this.getObjectId(aObject), aObject);

			// 2. Enregistrement drapeau de modification
			this.isDirty = true;

			return aObject;
		}
	}

	/**
	 * Modification d�finition d'un objet
	 * 
	 * @param aObject
	 *            Un objet � actualiser
	 * @return L'objet actualis�
	 */
	public T update(T aObject) {
		if (this.objects == null) {
			// 1. Lecture liste des objets
			this.load();
		}

		if (!this.objects.containsKey(this.getObjectId(aObject))) {
			// 1. L'objet n'est pas dans la liste
			return null;
		} else {
			// 1. Enregistrement de l'objet
			this.objects.put(this.getObjectId(aObject), aObject);

			// 2. Enregistrement drapeau de modification
			this.isDirty = true;

			return aObject;
		}
	}

	/**
	 * Suppression d'un objet
	 * 
	 * @param aId
	 *            Identificateur de l'objet a supprimer
	 * @return Objet supprim�
	 */
	public T remove(String aId) {
		if (this.objects == null) {
			// 1. Lecture liste des objets
			this.load();
		}

		// 1. Suppression d�finition de l'objet
		T removed = this.objects.remove(aId);

		if (removed != null) {
			// 1. Actualisation drapeau de modification
			this.isDirty = true;
		}

		return removed;
	}

	/**
	 * Lecture gestionnaire de la base
	 * 
	 * @return Gestionnaire de la base
	 */
	public SurveyBase getBase() {
		return this.base;
	}

	/**
	 * R�initialisation du cache
	 */
	public void clearCache() {
		this.objects = null;
	}


	/**
	 * Lecture nom du fichier de persistance
	 * 
	 * @return Nom du fichier de persistance
	 */
	public abstract String getFileName();

	/**
	 * D�s�rialisation d'un objet
	 * 
	 * @param aElement
	 *            Un Element d�finissant l'objet
	 * @return Objet construit � partir de l'�l�ment
	 */
	public abstract T unmarshall(Element aElement);

	/**
	 * S�rialisation d'un objet
	 */
	public abstract void marshall(T aObject, Element aElement);

	/**
	 * Notification de d�s�rialisation de l'objet
	 * 
	 * @param aObjet
	 *            Objet d�s�rialis�
	 */
	protected void afterUnmarshalling(T aObject) {
	}

	/**
	 * Notification de s�rialisation de l'objet
	 * 
	 * @param aObjet
	 *            Objet � s�rialiser
	 */
	protected void beforeMarshalling(T aObject) {
	}

	/**
	 * Lecture identificateur d'un objet
	 * 
	 * @param aObject
	 *            Un objet de ce type
	 * @return Identificateur unique de l'objet
	 */
	protected abstract String getObjectId(T aObject);

	/**
	 * Lecture liste des objets
	 */
	protected void load() {
		// 1. Cr�ation d'une liste d'objets vide
		this.objects = new HashMap<String, T>();

		if (this.base.getMode() == SurveyBase.Mode.NORMAL) {
			// 2. Lecture DOM
			Document document = this.loadDocument();

			if (document != null) {
				// 1. Lecture element racine de la hierarchie
				Element root = document.getDocumentElement();

				// 2. Lecture liste des noeuds fils
				NodeList children = root.getChildNodes();

				for (int i = 0; i < children.getLength(); i++) {
					// 1. Lecture d�finition du ieme noeud
					Node inode = children.item(i);

					// 2. Seul les noeuds du type element sont pris en compte
					if (inode.getNodeType() != Node.ELEMENT_NODE) {
						continue;
					}

					// 3. D�s�rialisation de l'objet
					T iobject = this.unmarshall((Element) inode);

					// 4. Notification de fin de d�s�rialisation
					this.afterUnmarshalling(iobject);

					// 5. Enregistrement d�finition de l'objet
					if (iobject != null)
						this.objects.put(this.getObjectId(iobject), iobject);
				}
			}
		}
	}

	/**
	 * Enregistrement de la liste d'objets
	 */
	protected void save() {
		// 1. Y a t-il quelque chose � sauvegarder
		if (this.objects == null)
			return;
		
		// 2. Construction d'un document vide
		Document document = Survey.createDocument();

		if (document != null) {
			for (Iterator<T> i = this.objects.values().iterator(); i.hasNext();) {
				// 1. Lecture d�finition du ieme objet
				T iobject = i.next();

				// 2. Notification de s�rialisation de l'objet
				this.beforeMarshalling(iobject);

				// 3. S�rialisation du ieme objet
				this.marshall(iobject, document.getDocumentElement());
			}

			// 6. Enregistrement du document
			this.saveDocument(document);

			// 7. Mise a jour drapeau de modification
			this.isDirty = false;
		}
	}

	/**
	 * Lecture d'un fichier au format XML
	 * 
	 * @return Document repr�sentant le fichier
	 */
	private Document loadDocument() {
		return Survey.loadDocument(this.base.getInputPath() + "/"
				+ this.getFileName());
	}

	/**
	 * Enregistrement document fichier au format XML
	 * 
	 * @param aFileName
	 *            Nom du fichier format XML
	 * @return Document repr�sentant le fichier
	 */
	private void saveDocument(Document aDocument) {
		Survey.saveDocument(this.base.getOutputPath() + "/" + this.getFileName(),
				aDocument);
	}
}
