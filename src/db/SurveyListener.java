package db;

import java.util.EventListener;

public interface SurveyListener extends EventListener {
	/**
	 * Notification d'une modification de la base
	 * @param aEvent Description des modifications
	 */
	public void baseChanged(SurveyChangeEvent aEvent);
}
