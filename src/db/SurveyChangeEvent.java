package db;

import java.util.ArrayList;
import java.util.List;

public class SurveyChangeEvent {
	public class Change {
		/** Type de modification. */
		protected int type;
		
		/** Objet modifi�. */
		protected SurveyObject target;
		
		/**
		 * Constructeur
		 * @param aType Type de modification
		 * @param aTarget Objet modifi�
		 */
		public Change(int aType,SurveyObject aTarget) {
			// 1. Initialisation des attributs
			this.type = aType;
			this.target = aTarget;
		}

		/**
		 * Lecture type de la modification
		 * @return Type de la modification
		 */
		public int getType() {
			return type;
		}

		/**
		 * Lecture r�f�rence � l'objet modifi�
		 * @return R�f�rence � l'objet modifie
		 */
		public SurveyObject getTarget() {
			return target;
		}
		
	}
	/** Codification des types de modification. */
	public static final int ADDED = 1;
	public static final int UPDATED = 2;
	public static final int REMOVED = 3;
	
	/** Liste des modifications de la base. */
	protected List<Change> changes;


	/**
	 * Constructeur
	 */
	public SurveyChangeEvent() {
		// 1. Initialisation des attributs
		this.changes = new ArrayList<Change>();
	}
	
	/**
	 * Constructeur de copie
	 * @param aSrc Objet � copier
	 */
	public SurveyChangeEvent(SurveyChangeEvent aEvent) {
		// 1. Initialisation des attributs
		this.changes = new ArrayList<Change>();
		
		for ( int i = 0 ; i < aEvent.changes.size(); i++) {
			// 1. Lecture changement � dupliquer
			Change ichange = aEvent.changes.get(i);
			
			// 2. Actualisation copie de la liste des changements
			this.changes.add(new Change(ichange.getType(),ichange.getTarget()));
		}
	}
	
	/**
	 * R�initialisation de la liste de modification
	 */
	public void clear() {
		// 1; R�initialisation de la liste des modifications
		this.changes.clear();
	}
	
	/**
	 * Lecture nombre de modifications en attente de notification
	 * @return Nombre de modifications en attente de notification
	 */
	public int getPendingChangesCount() {
		return this.changes.size();
	}
	
	/**
	 * Lecture changement � l'index sp�cifi�
	 * @param aIndex Index d'un changement
	 * @return Changement � l'index sp�cifi�
	 */
	public Change getPendingChangesAt(int aIndex) {
		return this.changes.get(aIndex);
	}
	
	/**
	 * Notification de l'ajout d'un objet
	 * @param aTarget Objet ajout�
	 */
	public void insertAddChange(SurveyObject aTarget) {
		//1 . Enregistrement de la modification
		this.changes.add(new Change(ADDED,aTarget));
	}
	
	/**
	 * Notification de la modification d'un objet
	 * @param aTarget Objet modifi�
	 */
	public void insertUpdateChange(SurveyObject aTarget) {
		//1 . Enregistrement de la modification
		this.changes.add(new Change(UPDATED,aTarget));
	}
	
	/**
	 * Notification de la suppression d'un objet
	 * @param aTarget Objet supprime
	 */
	public void insertRemoveChange(SurveyObject aTarget) {
		//1 . Enregistrement de la modification
		this.changes.add(new Change(REMOVED,aTarget));
	}
}
