package cs.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.NumberFormat;
import java.text.ParseException;

@SuppressWarnings("serial")
public class DoubleField extends AbstractField<Double,Double> implements FocusListener,
		ActionListener {
	/** Format d'affichage et d'�dition de la valeur. */
	protected NumberFormat format;

	/**
	 * Constructeur
	 * 
	 * @param aFractionDigits
	 *            Nombre de d�cimales
	 */
	public DoubleField(int aFractionDigits) {
		// 1. Initialisation des attributs
		this.format = NumberFormat.getInstance();

		// 2. Initialisation format d'affichage
		this.format.setParseIntegerOnly(false);
		this.format.setMinimumFractionDigits(aFractionDigits);
		this.format.setMaximumFractionDigits(aFractionDigits);

		// 3. Installation des listeners
		this.addFocusListener(this);
		this.addActionListener(this);

		// 4. Mise en forme valeur du champ
		this.setValue(0.0);
	}

	/**
	 * Constructeur
	 * 
	 * @param aFormat
	 *            Format d'affichage/�dition de la valeur
	 */
	public DoubleField(NumberFormat aFormat) {
		this(aFormat, 0.0);
	}

	/**
	 * Constructeur
	 * 
	 * @param aFormat
	 *            Format d'affichage/�dition de la valeur
	 * @param aValue
	 *            Valeur initiale du champ
	 */
	public DoubleField(NumberFormat aFormat, Double aValue) {
		// 1. Initialisation des attributs
		this.format = aFormat;

		// 2. Installation des listeners
		this.addFocusListener(this);

		// 3. Mise en format valeur du champ
		this.setValue(aValue);
	}

	/**
	 * Lecture valeur du champ
	 * 
	 * @return Valeur du champ
	 */
	public Double getValue() {
		return this.value;
	}

	/**
	 * Modification valeur du champ
	 * 
	 * @param aValue
	 *            Valeur du champ
	 */
	public void setValue(Double aValue) {
		// 1. Actualisation valeur du champ
		if (aValue != null) {
			if (this.minValue != null) {
				if (aValue < this.minValue) {
					// 1. Valeur incorrecte : Restitution valeur pr�c�dence
					this.setText(format.format(this.value));

					// 2. Abandon du traitement
					return;
				}
			}

			if (this.maxValue != null) {
				if (aValue > this.maxValue) {
					// 1. Valeur incorrecte : Restitution valeur pr�c�dence
					this.setText(format.format(this.value));

					// 2. Abandon du traitement
					return;
				}
			}

		} else {
			if (!this.isNullAllowed()) {
				// 1. Valeur incorrecte : Restitution valeur pr�c�dence
				this.setText(format.format(this.value));

				// 2. Abandon du traitement
				return;
			}
		}

		// 1. Actualisation valeur du champ
		this.value = aValue;

		// 2. Affichage de la nouvelle valeur
		this.setText(format.format(aValue));
	}

	@Override
	public void focusGained(FocusEvent aEvent) {
	}

	@Override
	public void focusLost(FocusEvent aEvent) {
		this.checkValue();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.checkValue();
	}

	/**
	 * Controle de la valeur du champ
	 */
	private void checkValue() {
		Double edited = null;
		
		// 1. Lecture valeur du champ
		String text = this.getText();

		if (text.trim().length() != 0) {
			try {
				// 1. D�codage en utilisant le format d'edition
				edited = this.format.parse(text).doubleValue();
			} catch (ParseException pex) {
				try {
					// 1. D�codage en utilisant le format par d�faut
					edited = Double.parseDouble(text);
				} catch (NumberFormatException nfex) {
				}
			}
		}

		// 2. Mise en format de la valeur
		this.setValue(edited);
	}
}
