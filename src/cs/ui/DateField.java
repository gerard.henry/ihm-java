package cs.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

@SuppressWarnings("serial")
public class DateField extends AbstractField<Date,Date> implements FocusListener,
		ActionListener {
	/** Format d'affichage et d'édition de la valeur. */
	protected DateFormat format;

	/**
	 * Constructeur
	 * 
	 * @param aFormat
	 *            Format d'affichage/édition de la valeur
	 * @param aValue
	 *            Valeur initiale du champ
	 */
	public DateField(DateFormat aFormat) {
		// 1. Initialisation des attributs
		this.format = aFormat;

		// 2. Installation des listeners
		this.addFocusListener(this);
	}

	/**
	 * Lecture valeur du champ
	 * 
	 * @return Valeur du champ
	 */
	public Date getValue() {
		return this.value;
	}

	/**
	 * Modification valeur du champ
	 * 
	 * @param aValue
	 *            Valeur du champ
	 */
	public void setValue(Date aValue) {
		// 1. Actualisation valeur du champ
		if (aValue != null) {
			if (this.minValue != null) {
				if (aValue.compareTo(this.minValue) < 0) {
					// 1. Valeur incorrecte : Restitution valeur précédence
					this.setText(format.format(this.value));

					// 2. Abandon du traitement
					return;
				}
			}

			if (this.maxValue != null) {
				if (aValue.compareTo(this.maxValue) > 0) {
					// 1. Valeur incorrecte : Restitution valeur précédence
					this.setText(format.format(this.value));

					// 2. Abandon du traitement
					return;
				}
			}

		} else {
			if (!this.isNullAllowed()) {
				// 1. Valeur incorrecte : Restitution valeur précédence
				this.setText(format.format(this.value));

				// 2. Abandon du traitement
				return;
			}
		}

		// 1. Actualisation valeur du champ
		this.value = aValue;


		// 1. Affichage de la nouvelle valeur
		if ( this.value != null ) {
			this.setText(format.format(aValue));
		} else {
			this.setText("");
		}
	}

	@Override
	public void focusGained(FocusEvent aEvent) {
	}

	@Override
	public void focusLost(FocusEvent aEvent) {
		this.checkValue();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.checkValue();
	}

	/**
	 * Controle de la valeur du champ
	 */
	private void checkValue() {
		Date edited = null;

		// 1. Lecture valeur du champ
		String text = this.getText();

		if (text.trim().length() != 0) {
			try {
				// 1. Décodage en utilisant le format d'edition
				edited = this.format.parse(text);
			} catch (ParseException pex) {
			}
		}

		// 2. Mise en format de la valeur
		this.setValue(edited);
	}
}
