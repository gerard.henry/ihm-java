package cs.ui;

import javax.swing.JTextField;

@SuppressWarnings("serial")
public abstract class AbstractField<T,R> extends JTextField {
	/** Une valeur nulle est-elle permise. */
	protected boolean isNullAllowed;
	
	/** Intervalle de validite de la valeur du champ. */
	protected R minValue;
	protected R maxValue;
	
	/** Valeur du champ. */
	protected T value;

	/**
	 * Constructeur
	 */
	protected AbstractField() {
		// 1. Initialisation des attributs
		this.isNullAllowed = false;
	}
	
	/**
	 * Lecture valeur du champ
	 * @return Valeur du champ
	 */
	public abstract T getValue();
	
	/**
	 * Modification valeur du champ
	 * @param aValue Nouvelle valeur du champ
	 */
	public abstract void setValue(T aValue);
	
	/**
	 * Une valeur nulle est-elle permise
	 * @return true si une valeur nulle est valide
	 */
	public boolean isNullAllowed() {
		return isNullAllowed;
	}


	/**
	 * Une valeur nulle est-elle permise
	 * @param aIsNullAllowed true si une valeur nulle est valide
	 */
	public void setNullAllowed(boolean aIsNullAllowed) {
		this.isNullAllowed = aIsNullAllowed;
	}

	/**
	 * Initialisation intervalle de validite de la valeur du champ
	 * @param aMin Valeur minimale permise
	 * @param aMax Valeur maximale permise
	 */
	public void setValueRange(R aMin,R aMax) {
		// 1. Initialisation intervalle de validite
		this.minValue = aMin;
		this.maxValue = aMax;
	}
	
	/**
	 * Lecture valeur minimale permise pour la valeur du champ
	 * @return Valeur minimale permise pour la valeur du champ
	 */
	public R getMinValue() {
		return minValue;
	}

	/**
	 * Modification valeur minimale permise pour la valeur du champ
	 * @param aValue Valeur minimale permise pour la valeur du champ
	 */
	public void setMinValue(R aValue) {
		this.minValue = aValue;
	}

	/**
	 * Lecture valeur maximale permise pour la valeur du champ
	 * @return Valeur minimale permise pour la valeur du champ
	 */
	public R getMaxValue() {
		return maxValue;
	}

	/**
	 * Modification valeur maximale permise pour la valeur du champ
	 * @param aValue Valeur minimale permise pour la valeur du champ
	 */
	public void setMaxValue(R aValue) {
		this.maxValue = aValue;
	}
}
