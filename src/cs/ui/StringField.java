package cs.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

@SuppressWarnings("serial")
public class StringField extends AbstractField<String, Integer> implements
		FocusListener, ActionListener {
	/**
	 * Constructeur
	 * 
	 * @param aFractionDigits
	 *            Nombre de décimales
	 */
	public StringField() {
		// 3. Installation des listeners
		this.addFocusListener(this);
		this.addActionListener(this);
	}

	/**
	 * Lecture valeur du champ
	 * 
	 * @return Valeur du champ
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * Modification valeur du champ
	 * 
	 * @param aValue
	 *            Valeur du champ
	 */
	public void setValue(String aValue) {
		// 1. Actualisation valeur du champ
		if (aValue != null) {
			if (this.minValue != null) {
				if (aValue.length() < this.minValue) {
					// 1. Valeur incorrecte : Restitution valeur précédence
					this.setText(this.value);

					// 2. Abandon du traitement
					return;
				}
			}

			if (this.maxValue != null) {
				if (aValue.length() > this.maxValue) {
					// 1. Valeur incorrecte : Restitution valeur précédence
					this.setText(this.value);

					// 2. Abandon du traitement
					return;
				}
			}

		} else {
			if (!this.isNullAllowed()) {
				// 1. Valeur incorrecte : Restitution valeur précédence
				this.setText(this.value);

				// 2. Abandon du traitement
				return;
			}
		}

		// 1. Actualisation valeur du champ
		this.value = aValue;

		// 2. Affichage de la nouvelle valeur
		this.setText(String.valueOf(aValue));
	}

	@Override
	public void focusGained(FocusEvent aEvent) {
	}

	@Override
	public void focusLost(FocusEvent aEvent) {
		this.checkValue();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.checkValue();
	}

	/**
	 * Controle de la valeur du champ
	 */
	private void checkValue() {
		// 1. Lecture valeur du champ
		String text = this.getText();

		if (text.trim().length() != 0) {
			this.setValue(text.trim());
		} else {
			this.setValue(null);
		}

	}
}
