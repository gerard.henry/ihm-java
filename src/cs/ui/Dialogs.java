package cs.ui;

import javax.swing.JOptionPane;

public class Dialogs {
	/**
	 * M�thode permettant d'afficher une bo�te de dialogue demandant �
	 * l'utilisateur confirmation.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 10/12/09
	 * 
	 * @param pMessage
	 *            le message � afficher dans la bo�te de dialogue.
	 * @param pTitle
	 *            le titre de la bo�te de dialogue.
	 */
	public static int showConfirmDialog(String pMessage, String pTitle) {
		return JOptionPane.showConfirmDialog(null, pMessage, pTitle,
				JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * M�thode permettant d'afficher une bo�te de dialogue d'information.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 13/01/10
	 * 
	 * @param pMessage
	 *            le message � afficher dans la bo�te de dialogue.
	 * @param pTitle
	 *            le titre de la bo�te de dialogue.
	 */
	public static void showInformationDialog(String pMessage) {
		JOptionPane.showMessageDialog(null, pMessage);
	}

	/**
	 * M�thode permettant d'afficher une bo�te de dialogue notifiant
	 * l'utilisateur d'une erreur.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 22/12/09
	 * 
	 * @param pMessage
	 *            le message � afficher dans la bo�te de dialogue.
	 * @param pTitle
	 *            le titre de la bo�te de dialogue.
	 */
	public static void showErrorDialog(String pMessage, String pTitle) {
		JOptionPane.showMessageDialog(null, pMessage, pTitle,
				JOptionPane.ERROR_MESSAGE);
	}

}
