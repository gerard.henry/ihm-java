package cs.sgl;

import java.io.StringReader;

import cs.sgl.parser.SigalQLParser;
import cs.sgl.parser.ParseException;
import cs.sgl.compiler.Expression;
import cs.sgl.compiler.EvalContext;
import cs.sgl.compiler.EvaluateException;

public class SGL
	{
	/**
	*	Compilation d'un expression
	*
	*	@param	exp	Expression � compiler
	*	@return		Expression compil�e
	*/
	public static Expression compile(String text) throws ParseException
		{
		// 1. Cr�ation d'un parser d'expression
		SigalQLParser parser = new SigalQLParser(new StringReader(text));

		// 2. Compilation de l'expression
		return parser.Expression();
		}

	/**
	*	Evaluation d'une expression
	*
	*	@param	exp	Expression � �valuer
	*	@param	ctx	Contexte d'�valuation
	*	@return		Resultat de l'�valuation
	*/
	public static Object evaluate(Expression exp,EvalContext ctx)  throws EvaluateException
		{
		return exp.computeValue(ctx);
		}
	}