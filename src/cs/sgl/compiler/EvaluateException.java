package cs.sgl.compiler;

public class EvaluateException extends Exception
	{
	/**
	*	Constructeur
	*
	*	@param	msg	Message descriptif de l'erreur
	*/
	public EvaluateException(String msg)
		{
		super(msg);
		}
	}