package cs.sgl.compiler;

public abstract class Operation implements Expression
	{
	/**	Liste des expressions utilis�es par l'op�ration. */
	protected Expression args[];

	/**
	*	Constructeur
	*
	*	@param	args	Liste des expressions utilis�es par l'op�ration
	*/	
	public Operation(Expression args[])
		{
		// 1. Initialisation des attributs
		this.args = args;
		}

	/**
	*	Lecture liste des expressions utilis�e par l'op�ration
	*
	*	@return 	Liste des expressions utilis�es par l'op�ration
	*/
	public Expression[] getArgs()
		{
		return this.args;
		}

	/**
	*	Test d'�galite entre deux objets
	*
	*	@param	left	Objet de gauche
	*	@param	right	Objet de droite
	*	@return		true si les deux objets sont �gaux, false dans le cas
	*				contraire
	*/
	public static Boolean EQ(Object left,Object right)
		{
		if      ( left == null && right == null )
			return Boolean.TRUE;
		else if ( left == null && right != null )
			return Boolean.FALSE;
		else if ( left != null && right == null )
			return Boolean.FALSE;
		else
			return new Boolean(left.equals(right));	
		}
	}
