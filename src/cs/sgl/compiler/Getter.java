package cs.sgl.compiler;

import java.lang.reflect.Method;

public class Getter extends Operation
	{
	/**
	*	Constructeur
	*
	*	@param	args	Liste des expressions utilis�es par l'op�ration
	*/	
	public Getter(Expression args[])
		{
		super(args);
		}


	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'�valuation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context)  throws EvaluateException
		{
		// 1. Lecture valeur de la variable
		Object current = args[0].computeValue(context);

		for ( int i = 1 ; i < args.length ; i++)
			{
			StringBuffer methodName = null;

			// 1. Si la variable est nulle arret du traitement
			if ( current == null ) return null;

			// 2. Conversion dans le type requis
			Variable var = (Variable)args[i];

			// 3. Lecture classe de la valeur courante
			Class valueClass = current.getClass();

			// 4. Construction nom de la m�thode
			methodName = new StringBuffer("get"+var.getName());

			// 5. Lecture troisi�me caract�re du nom de variable
			char fc = methodName.charAt(3);

			// 6. Capitalisation du troisi�me caract�re
                	methodName.setCharAt(3, Character.toUpperCase(fc));

			try
				{
				// 1. Recherche d�finition de la m�thode
				Method method = valueClass.getMethod(methodName.toString(),null);

				// 2. Invocation de la method
				current = method.invoke(current,null);
				}
			catch(Exception ex)
				{
				// 1. Impression d'un message d'erreur
				System.out.println("Method "+methodName+" not implemented by "+valueClass.getName());

				// 2. Arret du traitement
				return null;
				}
			}

		return current;
		}
	}
