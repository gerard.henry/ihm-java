package cs.sgl.compiler;

import cs.util.Converters;

public class LEOperation extends Operation
	{
	/**
	*	Constructeur
	*
	*	@param	left	Expression gauche
	*	@param	right	Expression droite
	*/	
	public LEOperation (Expression left,Expression right)
		{
		super(new Expression[]{left,right});
		}

	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'évaluation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context)  throws EvaluateException
		{
		// 1. Calcul du terme de gauche/droite
		Object left  = this.args[0].computeValue(context);
		Object right = this.args[1].computeValue(context);

		// 2. Conversion en valeurs numériques
		double leftValue  = Converters.toDouble(left ,Double.NaN);
		double rightValue = Converters.toDouble(right,Double.NaN);

		// 3. Comparaison des valeurs numériques
		return (leftValue <= rightValue ? Boolean.TRUE : Boolean.FALSE);
		}
	}