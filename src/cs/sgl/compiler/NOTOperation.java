package cs.sgl.compiler;

import cs.util.Converters;

public class NOTOperation extends Operation
	{
	/**
	*	Constructeur
	*
	*	@param	exp	Expression � evaluer
	*/	
	public NOTOperation (Expression exp)
		{
		super(new Expression[]{exp});
		}

	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'�valuation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context)  throws EvaluateException
		{
		// 1. Calcul valeur de l'expression
		Object ieme = this.args[0].computeValue(context);

		// 2. Conversion en valeur booleenne
		Boolean iemeValue = new Boolean(!Converters.toBoolean(ieme,true));
		
		return iemeValue ;
		}
	}