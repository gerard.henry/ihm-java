package cs.sgl.compiler;

import cs.util.Converters;

public class ADDOperation extends Operation
	{
	/**
	*	Constructeur
	*
	*	@param	args	Liste des expressions utilis�es par l'op�ration
	*/	
	public ADDOperation (Expression args[])
		{
		super(args);
		}

	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'�valuation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context) throws EvaluateException
		{
		// 1. Initialisation valeur de la somme
		double sum = 0.0;

		for ( int i = 0 ;  i < this.args.length ; i++)
			{
			// 1. Calcul valeur du ieme terme
			Object ieme = this.args[i].computeValue(context);

			// 2. Actualisation valeur de la somme
			sum += Converters.toDouble(ieme,Double.NaN);
			}
		
		return new Double(sum);
		}
	}