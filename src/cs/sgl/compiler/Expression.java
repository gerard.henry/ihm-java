package cs.sgl.compiler;

public interface Expression
	{
	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'évaluation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context) throws EvaluateException;
	}