package cs.sgl.compiler;


public class Function extends Operation
	{
	/**	Nom de la fonction. */
	protected String name;

	/**
	*	Constructeur
	*
	*	@param	name	Nom de la fonction
	*	@param	args	Liste des expressions utilis�es par l'op�ration
	*/	
	public Function(String name,Expression args[])
		{
		super(args);

		// 1. Initialisation des attributs	
		this.name = name;
		}

	/**
	*	Lecture nom de la fonction
	*
	*	@return 	Nom de la fonction
	*/
	public String getName()
		{
		return this.name;
		}

	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'�valuation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context)  throws EvaluateException
		{
		// 1. Allocation table des arguments calcul�s
		Object cargs[] = new Object[this.args.length];

		// 2. Evaluation valeur des arguments
		for ( int i = 0 ;  i < this.args.length ; i++)
			cargs[i] = this.args[i].computeValue(context);

		// 3. Execution de la fonction
		return context.executeFunction(this.name,cargs);
		}
	}
