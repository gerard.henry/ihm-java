package cs.sgl.compiler;

import cs.util.Converters;

public class OROperation extends Operation
	{
	/**
	*	Constructeur
	*
	*	@param	args	Liste des expressions utilis�es par l'op�ration
	*/	
	public OROperation (Expression args[])
		{
		super(args);
		}

	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'�valuation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context)  throws EvaluateException
		{
		for ( int i = 0 ;  i < this.args.length ; i++)
			{
			// 1. Calcul valeur du ieme terme
			Object ieme = this.args[i].computeValue(context);

			// 2. Conversion en valeur booleenne
			boolean iemeValue = Converters.toBoolean(ieme,false);

			// 3. Arret sur la premiere condition vrai
			if ( iemeValue ) return Boolean.TRUE;
			}
		
		return Boolean.FALSE;
		}
	}