package cs.sgl.compiler;

public class NEOperation extends Operation
	{
	/**
	*	Constructeur
	*
	*	@param	left	Expression gauche
	*	@param	right	Expression droite
	*/	
	public NEOperation (Expression left,Expression right)
		{
		super(new Expression[]{left,right});
		}

	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'évaluation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context)  throws EvaluateException
		{
		// 1. Calcul du terme de gauche/droite
		Object left  = this.args[0].computeValue(context);
		Object right = this.args[1].computeValue(context);

		if      ( left == null && right == null )
			return Boolean.FALSE;
		else if ( left == null && right != null )
			return Boolean.TRUE;
		else if ( left != null && right == null )
			return Boolean.TRUE;
		else
			return new Boolean(!left.equals(right));	
		}
	}