package cs.sgl.compiler;

public class Variable implements Expression
	{
	/**	Nom de la variable. */
	protected String name;

	/**
	*	Constructeur
	*
	*	@param	name	Nom de la variable
	*/	
	public Variable (String name)
		{
		// 1. Initialisation des attributs
		this.name = name;
		}

	/**
	*	Lecture nom de la variable référencee
	*
	*	@return 	Nom de la variable référencee
	*/
	public String getName()
		{
		return this.name;
		}

	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'évaluation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context)  throws EvaluateException
		{
		return context.getVariable(this.name);
		}
	}
