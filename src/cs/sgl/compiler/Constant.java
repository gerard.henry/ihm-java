package cs.sgl.compiler;

public class Constant implements Expression
	{
	/**	Valeur de la constante. */
	protected Object value;

	/**
	*	Constructeur
	*
	*	@param	value	Valeur de la constante
	*/
	public Constant(Object value)
		{
		// 1. Initialisation des attributs
		this.value = value;
		}

	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'évaluation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context)  throws EvaluateException
		{
		return this.value;
		}
	}