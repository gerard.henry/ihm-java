package cs.sgl.compiler;

public interface EvalContext
	{
	/**
	*	Lecture valeur de la variable avec le nom sp�cifi�
	*
	*	@param	name	Nom d'une variable
	*	@return		Valeur de la variable
	*/
	public Object getVariable(String name) throws EvaluateException;

	/**
	*	Ex�cution d'une fonction
	*
	*	@param	name	Nom de la fonction
	*	@param	args	Liste des arguments
	*	@return		Valeur de la fonction
	*/
	public Object executeFunction(String name,Object args[]) throws EvaluateException;
	}