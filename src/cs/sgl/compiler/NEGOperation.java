package cs.sgl.compiler;

import cs.util.Converters;

public class NEGOperation extends Operation
	{
	/**
	*	Constructeur
	*
	*	@param	args	Liste des expressions utilis�es par l'op�ration
	*/	
	public NEGOperation (Expression args[])
		{
		super(args);
		}

	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'�valuation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context)  throws EvaluateException
		{
		// 1. Calcul du terme 
		Object left  = this.args[0].computeValue(context);

		// 2. Conversion en valeurs num�riques
		double leftValue  = Converters.toDouble(left ,Double.NaN);

		return new Double(-leftValue);
		}
	}