package cs.sgl.compiler;


public class INOperation extends Operation
	{
	/**	Terme de gauche. */
	protected Expression left;

	/**
	*	Constructeur
	*
	*	@param	left	Terme de gauche
	*	@param	args	Liste des expressions utilis�es par l'op�ration
	*/	
	public INOperation(Expression left,Expression args[])
		{
		super(args);

		// 1. Initialisation des attributs	
		this.left = left;
		}

	/**
	*	Calcul valeur de l'expression
	*
	*	@param	context	Contexte de l'�valuation
	*	@return			Valeur de l'expression
	*/
	public Object computeValue(EvalContext context)  throws EvaluateException
		{
		// 1. Allocation table des arguments calcul�s
		Object cargs[] = new Object[this.args.length];

		// 2. Evaluation valeur des arguments
		for ( int i = 0 ;  i < this.args.length ; i++)
			cargs[i] = this.args[i].computeValue(context);

		// 3. Evaluation du terme de gauche
		Object lvalue = this.left.computeValue(context);

		// 6. Comparaison des l'arguments avec le nom de type
		for ( int i = 0 ; i < cargs.length ; i++)
			if ( Operation.EQ(lvalue,cargs[i]).booleanValue() ) return Boolean.TRUE;

		return Boolean.FALSE;
		}
	}
