package cs.util;

import java.util.logging.Logger;

public abstract class ProcessStream extends Thread {
	/** Enregistreur des traces d'ex�cution. */
	protected static Logger logger = Logger.getLogger(ProcessStream.class.getName());
	
	/** Buffer de redirection. */
	protected byte buffer[];
	
	/**
	 * Constructeur
	 */
	protected ProcessStream() {
		// 1; Initialisation des attributs
		this.buffer = new byte[1024*8];
	}
}
