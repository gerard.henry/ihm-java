package cs.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;

public class Stdout extends ProcessStream {
	/** Flux de sortie standard du processus. */
	protected InputStream out;

	/** Flux de redirection de la sortie standard. */
	protected OutputStream redirect;

	/**
	 * Constructeur
	 * 
	 * @param aProcess
	 *            Un processus
	 */
	public Stdout(Process aProcess) {
		// 1. Initialisation des attributs
		this.out = aProcess.getInputStream();
	}

	/**
	 * Lecture flux de redirection de la sortie standard du processus
	 * 
	 * @return Return lux de redirection de la sortie standard
	 */
	public OutputStream getRedirect() {
		return redirect;
	}

	/**
	 * Initialisation flux de redirection de la sortie standard du processus
	 * 
	 * @return Return lux de redirection de la sortie standard
	 */
	public void setRedirect(OutputStream aRedirect) {
		this.redirect = aRedirect;
	}

	@Override
	public void run() {
		try {
			while (this != null ) {
				// 1. Lecture caract�res suivant sur la sortie standard
				int count = this.out.read(buffer, 0, buffer.length);

				if (count == -1) {
					// 1. Le flux de sortie de la sortie standard est ferm�.
					break;
				} else {
					if (this.redirect != null) {
						this.redirect.write(buffer, 0, count);
					} 
				}
			}
		} catch (IOException ioex) {
			// 1. Log des traces d'ex�cution
			logger.log(Level.SEVERE, "stdout", ioex);
		} finally {
			try {
				// 1. Fermeture du flux de sortie standard
				this.out.close();
			} catch (IOException ioex) {
				// 1. Log des traces d'ex�cution
				logger.log(Level.SEVERE, "stdout", ioex);
			}
		}
	}
}
