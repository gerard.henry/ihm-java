package cs.util;

import java.text.MessageFormat;
import java.util.Hashtable;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class MessageManager {
	/** Resources (Messages) pour ce MessageManager. */
	private ResourceBundle bundle;

	/** Liste des MessageManager enregistr�s index�s par nom de package. */
	private static Hashtable<String, MessageManager> managers;

	/**
	 * Lecture objet MessageManager associ� au package packageName.
	 * 
	 * @param packageName
	 *            Nom d'un package.
	 * @return MessageManager pour ce package.
	 */
	public synchronized static MessageManager getManager(String packageName) {
		// 1. Creation cache des objets MessageManager
		if (MessageManager.managers == null)
			MessageManager.managers = new Hashtable<String,MessageManager>();

		// 2. Initialisation valeur par defaut pour le nom de package
		if (packageName == null)
			packageName = "";

		// 3. Lecture objet MessageManager � partir du cache
		MessageManager mgr = MessageManager.managers.get(packageName);

		if (mgr == null) {
			// 1. Construction d'un nouveau MessageManager pour ce package
			mgr = new MessageManager(packageName);

			// 2. Enregistrement de l'objet MessageManager dans le cache
			MessageManager.managers.put(packageName, mgr);
		}

		return mgr;
	}

	/**
	 * Constructeur priv�.
	 * 
	 * @param packageName
	 *            : Nom du package contenant un fichier de messages.
	 */
	private MessageManager(String packageName) {
		String fileName = null;

		// 1. Initialisation nom du fichier de messages
		if (packageName.trim().length() != 0)
			fileName = packageName + ".Messages";
		else
			fileName = "Messages";

		try {
			// 1. Chargement du fichier de message
			this.bundle = ResourceBundle.getBundle(fileName);
		} catch (MissingResourceException mrex) {
		}
	}

	/**
	 * Lecture texte du message associ� � la cl� key.
	 * 
	 * @param key
	 *            Identificateur du message.
	 */
	public String getString(String key) throws IllegalArgumentException {
		String msg = null;

		if (this.bundle != null) {
			if (key == null) {
				// 1. Propagation de l'erreur
				throw new IllegalArgumentException("Key is null");
			} else {
				try {
					// 1. Lecture texte du message associ� � la cl�
					msg = bundle.getString(key);
				} catch (MissingResourceException mre) {
					// 1. Propagation de l'erreur
					throw new IllegalArgumentException(
							"Can't find message for key '" + key + "'");
				}
			}
		}

		return msg;
	}

	public String getMessage(String key, Object[] args) {
		String msg = null;

		try {
			// 1. Lecture pattern de mise en forme du message
			String pattern = this.getString(key);

			if (pattern == null) {
				// 1. Mise en forme par defaut du message
				return this.buildDefaultMessage(key, args);
			} else {
				try {
					// 1. Mise en forme du message
					return MessageFormat.format(pattern, args);
				} catch (IllegalArgumentException iae) {
					// 1. Mise en forme par defaut du message
					return this.buildDefaultMessage(key, args);
				}
			}
		} catch (IllegalArgumentException mre) {
			// 1. Mise en forme par defaut du message
			return this.buildDefaultMessage(key, args);
		}
	}

	/**
	 * Mise en forme par defaut d'un message.
	 * 
	 * @param key
	 *            Nom du message
	 * @param args
	 *            Liste des arguments du message.
	 * @return Message mis en forme.
	 */
	protected String buildDefaultMessage(String key, Object args[]) {
		StringBuffer buf = new StringBuffer(key);

		if (args != null) {
			for (int i = 0; i < args.length; i++)
				buf.append(" arg[" + i + "]=" + args[i]);
		}

		return buf.toString();
	}
}
