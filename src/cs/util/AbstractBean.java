package cs.util;

import java.beans.PropertyChangeListener;

import javax.swing.event.SwingPropertyChangeSupport;

public class AbstractBean  {
	/** Liste des objets notifi�s lors des changements de param�tres. */
	protected SwingPropertyChangeSupport listeners;

	/**
	 * Constructeur
	 */
	public AbstractBean() {
		// 1. Cr�ation de la liste de notification
		this.listeners = new SwingPropertyChangeSupport(this);
	}

	/**
	 * Ajout d'un objet a la liste de notification
	 * 
	 * @param aListener
	 *            Objet a ajouter � la liste de notification
	 */
	public void addPropertyChangeListener(PropertyChangeListener aListener) {
		this.listeners.addPropertyChangeListener(aListener);
	}

	/**
	 * Retrait d'un objet de la liste de notification
	 * 
	 * @param aListener
	 *            Objet a retirer � la liste de notification
	 */
	public void removePropertyChangeListener(PropertyChangeListener aListener) {
		this.listeners.removePropertyChangeListener(aListener);
	}

	/**
	 * Ajout d'un objet a la liste de notification
	 * @param aName Nom d'une propri�te
	 * @param aListener
	 *            Objet a ajouter � la liste de notification
	 */
	public void addPropertyChangeListener(String aName,PropertyChangeListener aListener) {
		this.listeners.addPropertyChangeListener(aName,aListener);
	}

	/**
	 * Retrait d'un objet de la liste de notification
	 * 
	 * @param aName Nom d'une propri�te
	 * @param aListener
	 *            Objet a retirer � la liste de notification
	 */
	public void removePropertyChangeListener(String aName,PropertyChangeListener aListener) {
		this.listeners.removePropertyChangeListener(aName,aListener);
	}

	/**
	 * Notification d'une modificationde param�tres
	 * 
	 * @param aName
	 *            Nom de la propri�te
	 * @param oldValue
	 *            Ancienne valeur de la propri�te
	 * @param newValue
	 *            Nouvelle valeur de la propri�te
	 */
	public void firePropertyChange(String aName, Object oldValue,
			Object newValue) {
		if ( oldValue != null && newValue != null && oldValue.equals(newValue)) {
			// 1. Aucune notification est n�cesssaire
		}
		
		// 1. Notification du changement de valeur
		this.listeners.firePropertyChange(aName, oldValue, newValue);
	}
}
