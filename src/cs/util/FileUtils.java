package cs.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtils {
	/**
	 * Ecriture d'un fichier
	 * 
	 * @param aPath
	 *            Chemin du fichier
	 * @param aTo
	 *            Flux d'ecriture du fichier
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static void write(String aPath, OutputStream aTo)
			throws FileNotFoundException, IOException {
		FileInputStream in = null;
		try {
			in = new FileInputStream(aPath);
			FileUtils.write(in, aTo);
		} finally {
			if (in != null)
				in.close();
		}
	}

	/**
	 * Ecriture du flux aFrom dans le flux aTo
	 * 
	 * @param aFrom
	 *            Flux de lecture
	 * @param aTo
	 *            Flux d'ecriture
	 * @throws IOException
	 */
	public static void write(InputStream aFrom, OutputStream aTo)
			throws IOException {
		// 1. Cr�ation buffer de lecture
		byte bytes[] = new byte[1024 * 8];

		// 2. Initialisation nombre de bytes lus
		int nbytes = -1;

		// 3. Ecriture contenu du flux aFrom dans le flux aTo
		while ((nbytes = aFrom.read(bytes, 0, bytes.length)) != -1) {
			aTo.write(bytes, 0, nbytes);
		}

		// 3. Ecriture imm�diate des donn�es dans le flux aTo
		aTo.flush();
	}
}
