package cs.util;


@SuppressWarnings("serial")
public class ARTException extends Throwable
	{
	/**	Path du r�pertoire contenant les messages. */
	protected String basepath;

	/**	Identificateur du message d'erreur. */
	protected String msgid;

	/**	Liste des arguments permettant de construire le message. */
	protected Object args[];

	/** Exception a l'origine de cette exception. */
	private Throwable reason;

	/** 
	*	Constructeur. 
	*
	*	@param	creator	Objet qui a cr�e cette exception
	*	@param	msgid		Identificateur du message d'erreur
	*/
	public ARTException(Object creator,String msgid)
		{
		this(creator,msgid,new Object[]{},null);
		}

	/** 
	*	Constructeur. 
	*
	*	@param	creator	Objet qui a cr�e cette exception
	*	@param	msgid		Identificateur du message d'erreur
	*	@param	args		Liste des arguments permettant de construire le message
	*/
	public ARTException(Object creator,String msgid,Object args[])
		{
		this(creator,msgid,args,null);
		}

	/** 
	*	Constructeur. 
	*
	*	@param	creator	Objet qui a cr�e cette exception
	*	@param	msgid		Identificateur du message d'erreur
	*	@param	args		Liste des arguments permettant de construire le message
	*	@param	reason	Exception � l'origine de cette anomalie
	*/
	public ARTException(Object creator,String msgid,Object args[],Throwable reason)
		{
		// 1. Initialize object attributes
		this.basepath = "cs.util";
		this.msgid    = msgid  ;
		this.args     = args   ;
		this.reason   = reason ;

		if ( creator != null )
			{
			// 1. Lecture package contenant la classe a l'origine de l'exception
			Package pkg = creator.getClass().getPackage();

			if ( pkg != null )
				this.basepath = pkg.getName();
			}
		}

	/** 
	*	Constructeur. 
	*
	*	@param	basepath	Path du r�pertoire contenant les messages.
	*	@param	msgid		Identificateur du message d'erreur
	*/
	public ARTException(String basepath,String msgid)
		{
		this(basepath,msgid,new Object[]{},null);
		}

	/** 
	*	Constructeur. 
	*
	*	@param	basepath	Path du r�pertoire contenant les messages.
	*	@param	msgid		Identificateur du message d'erreur
	*	@param	args		Liste des arguments permettant de construire le message
	*/
	public ARTException(String basepath,String msgid,Object args[])
		{
		this(basepath,msgid,args,null);
		}

	/** 
	*	Constructeur. 
	*
	*	@param	basepath	Path du r�pertoire contenant les messages.
	*	@param	msgid		Identificateur du message d'erreur
	*	@param	args		Liste des arguments permettant de construire le message
	*	@param	reason	Exception � l'origine de cette anomalie
	*/
	public ARTException(String basepath,String msgid,Object args[],Throwable reason)
		{
		// 1. Initialize object attributes
		this.basepath = basepath;
		this.msgid    = msgid  ;
		this.args     = args   ;
		this.reason   = reason ;
		}

	/** 
	*	Constructeur. 
	*
	*	@param	msgid	Identificateur du message d'erreur
	*/
	ARTException(String msgid)
		{
		this(null,msgid,new Object[]{},null);
		}

	/** 
	*	Constructeur. 
	*
	*	@param	msgid		Identificateur du message d'erreur
	*	@param	args		Liste des arguments permettant de construire le message
	*	@param	reason	Exception � l'origine de cette anomalie
	*/
	ARTException(String msgid,Object args[])
		{
		this(null,msgid,args,null);
		}

	/** 
	*	Constructeur. 
	*
	*	@param	msgid		Identificateur du message d'erreur
	*	@param	reason	Exception � l'origine de cette anomalie
	*/
	ARTException(String msgid,Throwable reason)
		{
		this(null,msgid,new Object[]{},null);
		}

	/** 
	*	Constructeur. 
	*
	*	@param	msgid		Identificateur du message d'erreur
	*	@param	args		Liste des arguments permettant de construire le message
	*	@param	reason	Exception � l'origine de cette anomalie
	*/
	public ARTException(String msgid,Object args[],Throwable reason)
		{
		this(null,msgid,args,reason);
		}

	/**
	*	Throwable implementation : Lecture texte descriptif de l'exception	
	*/
	public String getMessage()
		{
		// 1. Lecture objet MessageManager fournisseur du texte du message
		MessageManager  manager = MessageManager.getManager(this.basepath);

		// 2. Mise en forme du message
		String msg =  manager.getMessage(this.msgid,this.args);

		// 3. Ajout de la cause de l'erreur
		if ( reason != null ) msg += " Caused by : "+reason;

		return msg;
		}

	/**
	*	Throwable implementation : Impression de la pile d'appel	
	*/
	public void printStackTrace() 
		{
		super.printStackTrace();

		if (reason != null) 
			{
			// 1. Impression message
			System.out.println("Caused by:");

			// 2. Impression de la pile d'appel a l'origine de cette exception
			reason.printStackTrace();
			}
		}

	/**
	*	Throwable implementation : Impression de la pile d'appel	
	*/
	public void printStackTrace(java.io.PrintStream ps) 
		{
		super.printStackTrace(ps);

		if (reason != null) 
			{
			// 1. Impression message
			ps.println("Caused by:");

			// 2. Impression de la pile d'appel a l'origine de cette exception
			reason.printStackTrace(ps);
			}
		}

	/**
	*	Throwable implementation : Impression de la pile d'appel	
	*/

	public void printStackTrace(java.io.PrintWriter pw) 
		{
		super.printStackTrace(pw);

		if (reason != null) 
			{
			// 1. Impression message
			pw.println("Caused by:");

			// 2. Impression de la pile d'appel a l'origine de cette exception
			reason.printStackTrace(pw);
			}
		}
	}