package cs.util;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.swing.KeyStroke;

public class Converters
	{
	/**
	*	Conversion en une chaine de caract�res
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@param	sepa	S�parateur des diff�rentes composantes
	*	@return		Valeur convertie.
	*/
	public static String toString(Object value,String def,String sepa)
		{
		if ( value != null )
			{
			if ( value.getClass().isArray() )
				return Converters.toString((Object [])value,def,sepa);
			else if ( value instanceof List )
				return Converters.toString(((List)value).toArray(),def,sepa);
			else
				return value.toString();
			}

		return def;
		}

	/**
	*	Conversion en une chaine de caract�res
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static String toString(Object value,String def)
		{
		if ( value != null )
			{
			if      ( value.getClass().isArray() )
				return Converters.toString((Object [])value,def," ");
			else if ( value instanceof List )
				return Converters.toString(((List)value).toArray(),def," ");
			else
				return value.toString();
			}

		return def;
		}

	/**
	*	Conversion en une chaine de caract�res
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@param	sepa	S�parateur des diff�rentes composantes
	*	@return		Valeur convertie.
	*/
	public static String toString(Object value[],String def,String sepa)
		{
		if ( value != null )
			{
			// 1. Cr�ation d'un buffer image
			StringBuffer img = new StringBuffer();

			for ( int i = 0 ; i < value.length ; i++)
				{
				if ( value[i] != null )
					{
					// 1. Actualisation du buffer image
					img.append(value[i].toString()+sepa);
					}			
				}

			if ( img.length() != 0 )
				return img.substring(0,img.length()-1);
			}
			
		return def;
		}

	/**
	*	Conversion en une valeur enti�re
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static Integer toInt(Object value) throws ARTException
		{
		if ( value != null )
			{
			if      ( value instanceof Integer)
				return (Integer)value; 
			else if ( value instanceof Number) 
				return new Integer(((Number)value).intValue());
			else if ( value instanceof String)
				{
				try
					{
					// 1. Conversion en valeur entiere		
					return new Integer(Integer.parseInt((String)value));
					}
				catch(NumberFormatException nfex)
					{
					// 1. Propagation d'une exception
					throw new ARTException("cs.util","Converters.NotInteger");
					}
				}
			else
				{
				// 1. Propagation d'une exception
				throw new ARTException("cs.util","Converters.NotInteger");
				}
			}
			
		return null;
		}

	/**
	*	Conversion en une valeur enti�re
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static int toInt(Object value,int def)
		{
		if ( value != null )
			{
			if      ( value instanceof Number) 
				return ((Number)value).intValue();
			else if ( value instanceof String)
				{
				try
					{
					// 1. Conversion en valeur entiere		
					return Integer.parseInt((String)value);
					}
				catch(NumberFormatException nfex)
					{
					}
				}
			}
			
		return def;
		}

	/**
	*	Conversion en une valeur enti�re
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static Integer toInt(Object value,Integer def)
		{
		if ( value != null )
			{
			if      ( value instanceof Integer)
				return (Integer)value; 
			else if ( value instanceof Number) 
				return new Integer(((Number)value).intValue());
			else if ( value instanceof String)
				{
				try
					{
					// 1. Conversion en valeur entiere		
					return new Integer(Integer.parseInt((String)value));
					}
				catch(NumberFormatException nfex)
					{
					}
				}
			}
			
		return def;
		}

	/**
	*	Conversion en une valeur num�rique simple pr�cision
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static float toFloat(Object value,float def)
		{
		if ( value != null )
			{
			if      ( value instanceof Number) 
				return ((Number)value).floatValue();
			else if ( value instanceof String)
				{
				try
					{
					// 1. Conversion en valeur entiere		
					return Float.parseFloat((String)value);
					}
				catch(NumberFormatException nfex)
					{
					nfex.printStackTrace();
					}
				}
			}
			
		return def;
		}


	/**
	*	Conversion en une valeur num�rique simple pr�cision
	*
	*	@param	value	Valeur � convertir
	*	@return		Valeur convertie.
	*/
	public static Float toFloat(Object value) throws ARTException
		{
		if ( value != null )
			{
			if      ( value instanceof Float)
				return (Float)value;
 			else if ( value instanceof Number) 
				return new Float(((Number)value).floatValue());
			else if ( value instanceof String)
				{
				try
					{
					// 1. Conversion en valeur entiere		
					return new Float(Float.parseFloat((String)value));
					}
				catch(NumberFormatException nfex)
					{
					// 1. Propagation d'une exception
					throw new ARTException("cs.util","Converters.NotFloat");
					}
				}
			else
				{
				// 1. Propagation d'une exception
				throw new ARTException("cs.util","Converters.NotFloat");
				}
			}
			
		return null;
		}

	/**
	*	Conversion en une valeur num�rique simple pr�cision
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static Float toFloat(Object value,Float def)
		{
		if ( value != null )
			{
			if      ( value instanceof Float)
				return (Float)value;
 			else if ( value instanceof Number) 
				return new Float(((Number)value).floatValue());
			else if ( value instanceof String)
				{
				try
					{
					// 1. Conversion en valeur entiere		
					return new Float(Float.parseFloat((String)value));
					}
				catch(NumberFormatException nfex)
					{
					nfex.printStackTrace();
					}
				}
			}
			
		return def;
		}

	/**
	*	Conversion en une valeur num�rique double pr�cision
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static double toDouble(Object value,double def)
		{
		if ( value != null )
			{
			if      ( value instanceof Number) 
				return ((Number)value).doubleValue();
			else if ( value instanceof String)
				{
				try
					{
					// 1. Conversion en valeur entiere		
					return Double.parseDouble((String)value);
					}
				catch(NumberFormatException nfex)
					{
					}
				}
			}
			
		return def;
		}

	/**
	*	Conversion en une valeur num�rique double pr�cision
	*
	*	@param	value	Valeur � convertir
	*	@return		Valeur convertie.
	*/
	public static Double toDouble(Object value) throws ARTException
		{
		if ( value != null )
			{
			if      ( value instanceof Double)
				return (Double)value; 
			else if ( value instanceof Number) 
				return new Double(((Number)value).doubleValue());
			else if ( value instanceof String)
				{
				try
					{
					// 1. Conversion en valeur entiere		
					return new Double(Double.parseDouble((String)value));
					}
				catch(NumberFormatException nfex)
					{
					// 1. Propagation d'une exception
					throw new ARTException("cs.util","Converters.NotDouble");
					}
				}
			else
				{
				// 1. Propagation d'une exception
				throw new ARTException("cs.util","Converters.NotDouble");	
				}
			}
			
		return null;
		}

	/**
	*	Conversion en une valeur num�rique double pr�cision
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static Double toDouble(Object value,Double def)
		{
		if ( value != null )
			{
			if      ( value instanceof Double)
				return (Double)value; 
			else if ( value instanceof Number) 
				return new Double(((Number)value).doubleValue());
			else if ( value instanceof String)
				{
				try
					{
					// 1. Conversion en valeur entiere		
					return new Double(Double.parseDouble((String)value));
					}
				catch(NumberFormatException nfex)
					{
					}
				}
			}
			
		return def;
		}

	/**
	*	Conversion en une valeur bool�enne
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static boolean toBoolean(Object value,boolean def)
		{
		if ( value != null )
			{
			if      ( value instanceof Boolean) 
				return ((Boolean)value).booleanValue();
			else if ( value instanceof Number)
				{
				// 1. Conversion dans le type requis
				Number number = (Number)value;

				if ( number.intValue() == 1 )
					return true;
				else
					return false;
				}
			else if ( value instanceof String)
				{
				// 1. Conversion dans le type requis
				String str = (String)value;

				if      ( str.equalsIgnoreCase("true") )
					return true;
				else if ( str.equalsIgnoreCase("false") )
					return false;
				}
			}
			
		return def;
		}

	/**
	*	Conversion en une valeur bool�enne
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static Boolean toBoolean(Object value,Boolean def)
		{
		if ( value != null )
			{
			if      ( value instanceof Boolean) 
				return (Boolean)value;
			else if ( value instanceof Number)
				{
				// 1. Conversion dans le type requis
				Number number = (Number)value;

				if ( number.intValue() == 1 )
					return Boolean.TRUE;
				else
					return Boolean.FALSE;
				}
			else if ( value instanceof String)
				{
				// 1. Conversion dans le type requis
				String str = (String)value;

				if      ( str.equalsIgnoreCase("true") )
					return Boolean.TRUE;
				else if ( str.equalsIgnoreCase("false") )
					return Boolean.FALSE;
				}
			}
			
		return def;
		}

	/**
	*	Conversion en une valeur bool�enne
	*
	*	@param	value	Valeur � convertir
	*	@return		Valeur convertie.
	*/
	public static Boolean toBoolean(Object value) throws ARTException
		{
		if ( value != null )
			{
			if      ( value instanceof Boolean) 
				return (Boolean)value;
			else if ( value instanceof Number)
				{
				// 1. Conversion dans le type requis
				Number number = (Number)value;

				if ( number.intValue() == 1 )
					return Boolean.TRUE;
				else
					return Boolean.FALSE;
				}
			else if ( value instanceof String)
				{
				// 1. Conversion dans le type requis
				String str = (String)value;

				if      ( str.equalsIgnoreCase("true") )
					return Boolean.TRUE;
				else if ( str.equalsIgnoreCase("false") )
					return Boolean.FALSE;
				else
					{
					// 1. Propagation d'une exception
					throw new ARTException("cs.util","Converters.NotBoolean");	
					}
				}
			else
				{
				// 1. Propagation d'une exception
				throw new ARTException("cs.util","Converters.NotBoolean");	
				}
			}
			
		return null;
		}

	/**
	*	Conversion en une couleur
	*
	*	@param	value	Valeur � convertir
	*	@return		Valeur convertie.
	*/
	public static Color toColor(Object value) throws ARTException
		{
		if ( value != null )
			{
			if      ( value instanceof Color) 
				return (Color)value;
			else if ( value instanceof String)
				{
				try
					{
					// 1. D�codage de la couleur		
					return Color.decode((String)value);
					}
				catch(NumberFormatException nfex)
					{
					// 1. Propagation d'une exception
					throw new ARTException("cs.util","Converters.NotColor");	
					}
				}
			else
				{
				// 1. Propagation d'une exception
				throw new ARTException("cs.util","Converters.NotColor");	
				}
			}
			
		return null;
		}

	/**
	*	Conversion en une couleur
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static Color toColor(Object value,Color def)
		{
		if ( value != null )
			{
			if      ( value instanceof Color) 
				return (Color)value;
			else if ( value instanceof String)
				{
				try
					{
					// 1. D�codage de la couleur		
					return Color.decode((String)value);
					}
				catch(NumberFormatException nfex)
					{
					}
				}
			}
			
		return def;
		}

	/**
	*	Conversion en une police de caract�res
	*
	*	@param	value	Valeur � convertir
	*	@return		Valeur convertie.
	*/
	public static Font toFont(Object value) throws ARTException
		{
		if ( value != null )
			{
			if      ( value instanceof Font) 
				return (Font)value;
			else if ( value instanceof String)
				{
				try
					{
					// 1. D�codage de la couleur		
					return Font.decode((String)value);
					}
				catch(NumberFormatException nfex)
					{
					// 1. Propagation d'une exception
					throw new ARTException("cs.util","Converters.NotFont");	
					}
				}
			else
				{
				// 1. Propagation d'une exception
				throw new ARTException("cs.util","Converters.NotFont");
				}
			}
			
		return null;
		}

	/**
	*	Conversion en une police de caract�res
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static Font toFont(Object value,Font def)
		{
		if ( value != null )
			{
			if      ( value instanceof Font) 
				return (Font)value;
			else if ( value instanceof String)
				{
				try
					{
					// 1. D�codage de la couleur		
					return Font.decode((String)value);
					}
				catch(NumberFormatException nfex)
					{
					}
				}
			}
			
		return def;
		}


	/**
	*	Conversion en un tableau d'objets
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static Object[] toArray(Object value,Object def[])
		{
		if ( value != null )
			{
			if ( value.getClass().isArray() )
				return (Object [])value;
			else if ( value instanceof List) 
				return ((List)value).toArray();
			else
				return new Object[]{value};
			}

		return def; 
		}

	/**
	*	Conversion d'une chaine de caracteres en une liste. La liste de
	*	s�parateurs possibles est fournie par seps
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@param	seps	Liste des s�parateurs 
	*/
	public static List toList(String value,List def,String seps)
		{
		if ( value != null )
			{
			// 1. Cr�ation de la liste r�sultat
			ArrayList list = new ArrayList();

			// 2. Cr�ation d'un parseur de la valeur
			StringTokenizer st = new StringTokenizer(value,seps);

			// 3. Actualisation de la liste r�sultat
			while ( st.hasMoreTokens() )
				list.add(st.nextToken());

			return list;
			}

		return def;
		}
	/**
	*	Conversion en un objet Map
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static Map toMap(Object value,Map def)
		{
		if ( value != null )
			{
			if      ( value instanceof Map) 
				return (Map)value;
			else if ( value instanceof String)
				{
				// 1. Conversion dans le type requis
				String src = (String)value;

				// 2 Cr�ation d'un tokenizer
				StringTokenizer st = new StringTokenizer(src,";");

				// 3. Initialisation liste des attributs
				LinkedHashMap map = null;

				while ( st.hasMoreTokens() )
					{
					String an = null;
					String av = null;

					// 1. Lecture de la paire nom valeur
					String pair  = st.nextToken();

					// 2. Lecture index du separateur entre le premier
					//	terme et le second
					int isv = pair.indexOf(":");
				
					if ( isv == -1 )
						{
						// 1. Lecture nom et valeur de l'argument
						an  = pair;
						av = "true";
						}
					else
						{
						// 1. Lecture nom et valeur de l'argument
						an = pair.substring(0,isv);
						av = pair.substring(isv+1);
						}

					// 1. Cr�ation si n�cessaire de la liste des attributs
					if ( map == null ) 
						{
						if ( def == null )
							map = new LinkedHashMap ();
						else
							map = new LinkedHashMap (def);
						}

					// 4. M�morisation de l'attribut
					map.put(an,av);
        				}

				return (map == null ? def : map);
				}
			}

		return def;
		}

	/**
	*	Conversion en un acc�l�rateur clavier
	*
	*	@param	value	Valeur � convertir
	*	@return		Valeur convertie.
	*/
	public static KeyStroke toKeyStroke (Object value) throws ARTException
		{
		KeyStroke keyStroke = null;

		if ( value != null )
			{
			if      ( value instanceof KeyStroke ) 
				keyStroke = (KeyStroke )value;
			else if ( value instanceof String)
				keyStroke = KeyStroke.getKeyStroke((String)value);	

			if ( keyStroke == null )
				{
				// 1. Propagation d'une exception
				throw new ARTException("cs.util","Converters.NotKeyStroke");
				}
	
			return keyStroke; 
			}
			
		return null;
		}

	/**
	*	Conversion en un acc�l�rateur clavier
	*
	*	@param	value	Valeur � convertir
	*	@param	def	Valeur retourn�e si la valeur est 
	*				null ou ne peut �tre convertie
	*	@return		Valeur convertie.
	*/
	public static KeyStroke toKeyStroke (Object value,KeyStroke def)
		{
		if ( value != null )
			{
			if      ( value instanceof KeyStroke ) 
				return (KeyStroke )value;
			else if ( value instanceof String)
				return KeyStroke.getKeyStroke((String)value);	
			}
			
		return def;
		}
	}
