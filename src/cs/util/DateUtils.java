package cs.util;

import java.util.Calendar;
import java.util.Date;

import db.Survey;

public class DateUtils {
	/**
	 * Calcul nombre de jours s�parant deux dates
	 * 
	 * @param aLeft
	 *            Une date
	 * @param aRight
	 *            Une autre date
	 * @return Nombre de jours s�parant les deux dates
	 */
	public static int getDayCount(Date aLeft, Date aRight) {
		Calendar left = Calendar.getInstance();
		Calendar right = Calendar.getInstance();

		// 1. Initialisation des dates
		left.setTime(aLeft);
		right.setTime(aRight);

		// 2. Lecture ann�e/mois/jour des deux dates
		int leftYear = left.get(Calendar.YEAR);
		int leftMonth = left.get(Calendar.MONTH)+1;
		int leftDay = left.get(Calendar.DAY_OF_MONTH);
		int rightYear = right.get(Calendar.YEAR);
		int rightMonth = right.get(Calendar.MONTH)+1;
		int rightDay = right.get(Calendar.DAY_OF_MONTH);

		// 3. Initialisation nombre de jours s�parant les dates
		int dayCount = 0;

		if (leftYear == rightYear) {
			if (leftMonth == rightMonth) {
				dayCount = rightDay - leftDay;
			} else {
				// 1. Initialisation nombre de jours dans le premier mois
				dayCount = getMonthDaysCount(leftMonth, leftYear) - leftDay;
				
				// 2. Calcul nombre de jours pour les mois interm�diaires
				for ( int i = leftMonth + 1 ; i < rightMonth ; i++) {
					// 1. Actualisation nombre de jours
					dayCount += getMonthDaysCount(i, leftYear);
				}
				
				// 3. Ajout nombre de jours dans le dernier mois
				dayCount += rightDay;
			}
		} else {
			// 1. Initialisation nombre de jours dans le premier mois
			dayCount = getMonthDaysCount(leftMonth, leftYear) - leftDay;

			// 2. Calcul nombre de jours avant la fin de l'ann�e
			for ( int i = leftMonth + 1 ; i <= 12 ; i++) {
				// 1. Actualisation nombre de jours
				dayCount += getMonthDaysCount(i, leftYear);
			}
			
			// 3. Calcul nombre de jours pour chaque ann�e interm�diaire
			for ( int i = leftYear + 1 ; i < rightYear ; i++) {
				if ( isLeapYear(i)) {
					dayCount += 366;
				} else {
					dayCount += 365;
				}
			}
			
			// 4. Calcul nombre de jours pour la derni�re ann�e
			for ( int i = 1 ; i < rightMonth ; i++) {
				// 1. Actualisation nombre de jours
				dayCount += getMonthDaysCount(i, rightYear);
			}	
			
			// 5. Ajout nombre de jours du dernier mois
			dayCount += rightDay;
		}

		return dayCount;
	}

	/**
	 * Lecture nombre de jours dans un mois
	 * 
	 * @param aMonth
	 *            Un mois (1..12)
	 * @param aYear
	 *            Une ann�e
	 * @return
	 */
	public static int getMonthDaysCount(int aMonth, int aYear) {

		switch (aMonth) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
		case 2:
			if (isLeapYear(aYear))
				return 29;
			else
				return 28;
		}
		return 0;
	}

	/**
	 * L'ann�e aYear est-elle bissextile
	 * 
	 * @param aYear
	 *            une ann�e return true si l'ann�e est bissextile
	 */
	public static boolean isLeapYear(int y) {
		return (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0));
	}
}
