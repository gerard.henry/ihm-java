package tools;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

import db.Survey;

@SuppressWarnings("serial")
public class DrawPane extends JPanel implements ActionListener {
	/** Dimension par d�faut du panneau. */
	public static final Dimension DEFAULT_SIZE = new Dimension(600, 400);

	protected class Page extends JPanel implements Printable {
		/** Liste de trac� de la page. */
		protected GeneralPath path;

		/**
		 * Constructeur
		 * 
		 * @param aDrawList
		 *            Liste des point � tracer
		 */
		public Page(GeneralPath aPath) {
			// 1. Initialisation des attributs
			this.path = aPath;
		}

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);

			// 1. Conversion dans le type requis
			Graphics2D g2d = (Graphics2D) g;

			// 2. Lecture dimension de la forme
			Rectangle2D bounds = path.getBounds2D();
			
			// 3. Calcul facteur d'�chelle
			double sx = 1. * this.getWidth() / bounds.getWidth();
			double sy = 1. * this.getHeight() / bounds.getHeight();

			// 4. Cr�ation forme transform�e
			AffineTransform str = new AffineTransform();
			double tw = bounds.getWidth()*0.95;
			double th = bounds.getHeight()*0.95;
			str.translate(bounds.getWidth()-tw/2, bounds.getHeight()-th/2);
			str.scale(sx*0.95, sy*0.95);
			Shape shape = path.createTransformedShape(str);

			// 5. Initialisation contexte graphique
			g2d.setColor(Color.black);
			g2d.setBackground(Color.white);
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);

			// 6. Sauvegarde transformation de trace courante
			AffineTransform otr = g2d.getTransform();

			// 7. Changement de rep�re
			g2d.translate(this.getWidth() / 2., this.getHeight() / 2.);
			g2d.scale(-1., 1.);
			g2d.rotate(Math.toRadians(180));
			g2d.translate(-this.getWidth() / 2., -this.getHeight() / 2.);
			
			// 8. Trace du composant
			g2d.draw(shape);

			// 9. restitution transformation de trace initial
			g2d.setTransform(otr);
		}

		@Override
		public int print(Graphics g, PageFormat format, int page)
				throws PrinterException {
			if (page > 0) {
				return Printable.NO_SUCH_PAGE;
			}

			// 1. Conversion dans le type requis
			Graphics2D g2d = (Graphics2D) g;

			// 2. Lecture dimension de la forme
			Rectangle2D bounds = path.getBounds2D();

			// 3. Calcul facteur d'�chelle
			double sx = 1. * format.getImageableWidth() / bounds.getWidth();
			double sy = 1. * format.getImageableHeight() / bounds.getHeight();

			// 4. Cr�ation forme transform�e
			AffineTransform str = new AffineTransform();
			double tw = bounds.getWidth()*0.95;
			double th = bounds.getHeight()*0.95;
			str.translate(bounds.getWidth()-tw/2, bounds.getHeight()-th/2);
			str.scale(sx*0.95, sy*0.95);
			Shape shape = path.createTransformedShape(str);

			// 5. Initialisation contexte graphique
			g2d.setColor(Color.black);
			g2d.setBackground(Color.white);


			// 7. Changement de rep�re
			g2d.translate(format.getImageableWidth() / 2., format.getImageableHeight() / 2.);
			g2d.scale(-1, 1);
			g2d.rotate(Math.toRadians(180));
			g2d.translate(-format.getImageableWidth() / 2., -format.getImageableHeight() / 2.);

			// 8. Trace du composant
			g2d.draw(shape);

			return Printable.PAGE_EXISTS;

		}
	}

	/** Nombre de pages */
	protected int pageCount;

	/** Index de la page visualis�e. */
	protected int pageIndex;

	/** Layout du panneau. */
	protected CardLayout layout;

	/** Composants internes. */
	protected JPanel canvas;
	protected JButton first;
	protected JButton last;
	protected JButton next;
	protected JButton previous;
	protected JButton print;
	protected JButton save;
	protected JComboBox imgfmt;
	
	/**
	 * Constructeur
	 */
	public DrawPane() {
		super(new BorderLayout());

		// 1. Initialisation attributs
		this.layout = new CardLayout();
		this.canvas = new JPanel(this.layout);
		this.pageIndex = -1;

		// 3. Cr�ation des composants du panneau
		JPanel buttonsPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));

		this.first = new JButton(CIniFileReader.getString("P3B1"));
		this.last = new JButton(CIniFileReader.getString("P3B4"));
		this.next = new JButton(CIniFileReader.getString("P3B3"));
		this.previous = new JButton(CIniFileReader.getString("P3B2"));
		this.print = new JButton(CIniFileReader.getString("P3B5"));
		this.save = new JButton("Enregistrer...");
		this.imgfmt = new JComboBox(ImageIO.getReaderFormatNames());
		
		// 4. Assemblage des composants
		buttonsPane.add(this.print);
		buttonsPane.add(this.save);
		buttonsPane.add(new JLabel("Format"));
		buttonsPane.add(this.imgfmt);
		buttonsPane.add(Box.createHorizontalStrut(30));
		buttonsPane.add(this.first);
		buttonsPane.add(this.previous);
		buttonsPane.add(this.next);
		buttonsPane.add(this.last);

		this.add(canvas, BorderLayout.CENTER);
		this.add(buttonsPane, BorderLayout.SOUTH);

		// 5. Installation des listeners
		this.first.addActionListener(this);
		this.last.addActionListener(this);
		this.next.addActionListener(this);
		this.previous.addActionListener(this);
		this.print.addActionListener(this);
		this.save.addActionListener(this);

		// 6. Actualisation etat des composants
		this.updateButtonsState();

		// 3. Initialisation taille par d�faut du panneau
		setPreferredSize(DEFAULT_SIZE);
	}

	/**
	 * Lecture du fichier de dessin
	 * 
	 * @param aPath
	 *            Chemin du fichier de dessin
	 */
	public void loadFile(String aPath) {
		BufferedReader ir = null;
		double tmp[] = new double[3];
		GeneralPath drawList = null;
		int ltc = 0;

		// 1. R�initialisation de la liste des pages
		this.clearPages();

		try {
			// 1. Cr�ation lecteur du fichier
			ir = new BufferedReader(new FileReader(aPath));

			// 2. Cr�ation d'un parseur du fichier
			StreamTokenizer tokenizer = new StreamTokenizer(ir);

			// 3. Configuration du parseur
			tokenizer.eolIsSignificant(true);
			tokenizer.parseNumbers();

			while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
				switch (tokenizer.ttype) {
				case StreamTokenizer.TT_EOL: {
					// 1.
					if (ltc == 3) {
						// 1. Cr�ation si n�cessaire de la liste de trace
						if (drawList == null) {
							drawList = new GeneralPath();
						}

						switch ((int) (tmp[2])) {
						case 0: {
							// 1. Enregistrement du d�placement
							drawList.moveTo(tmp[0], tmp[1]);

							break;
						}
						case 1: {
							// 1. Enregistrement du trace
							drawList.lineTo(tmp[0], tmp[1]);

							break;
						}
						case -8: {
							// 1. D�tection d'une nouvelle page
							this.addPage(drawList);

							// 2. R�initialisation liste de trace
							drawList = null;
						}
						default: {

						}
						}
					}

					// 2. R�initialisation nombre de mots par lignes
					ltc = 0;

					break;
				}
				case StreamTokenizer.TT_NUMBER: {
					// 1. Enregistrement coordonn�e du point
					tmp[ltc++] = tokenizer.nval;

					break;
				}
				case StreamTokenizer.TT_WORD: {
					break;
				}
				}
			}

			if (drawList != null) {
				this.addPage(drawList);
			}

			// 1. Visualisation de la premiere page
			this.showPage(0);

			// 2. Actualisation etat des boutons
			this.updateButtonsState();

		} catch (IOException ioex) {

		} finally {
			try {
				// 1. Fermeture lecteur du fichier
				if (ir != null)
					ir.close();
			} catch (IOException ign) {

			}
		}
	}

	/**
	 * Ajout d'une nouvelle page
	 * 
	 * @param aDrawList
	 *            Liste de trac� de la page
	 */
	protected void addPage(GeneralPath aDrawList) {
		// 1. Cr�ation composant de visualisation de la page
		Page page = new Page(aDrawList);

		// 2. Initialisation taille par d�faut du panneau
		page.setPreferredSize(DEFAULT_SIZE);

		// 3. Enregistrement de la page
		this.canvas.add(page, "Page" + this.pageCount);

		// 4. Actualisation nombre de pages
		this.pageCount++;
	}

	/**
	 * R�initialisation de la liste des pages
	 */
	protected void clearPages() {
		// 1. Suppression de toutes les pages
		this.canvas.removeAll();

		// 2. R�initialisation contexte
		this.pageCount = 0;
		this.pageIndex = -1;

		// 3. Actualisation etat des boutons
		this.updateButtonsState();
	}

	/**
	 * Visualisation de la page
	 * 
	 * @param aIndex
	 *            Index de la page
	 */
	protected void showPage(int aIndex) {
		if (aIndex >= 0 && aIndex < this.pageCount) {
			// 1. Actualisation index de la page courante
			this.pageIndex = aIndex;

			// 2. Visualisation de la page
			this.layout.show(this.canvas, "Page" + aIndex);

			// 3. Actualisation etat des boutons
			this.updateButtonsState();
		}
	}

	/**
	 * Enregistrement de la page courante
	 */
	public void savePage() {
		// 1. Cr�ation d'un s�lecteur de fichier
		JFileChooser chooser = new JFileChooser();
		
		// 2. Configuration du s�lecteur de fichier
		chooser.setMultiSelectionEnabled(false);
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
		// 3. Initialisation r�pertoire par d�faur du s�lecteur
		chooser.setCurrentDirectory(new File("db/graphics"));
		
		// 3. Ouverture du dialogue
		if ( chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {	
			// 1. Lecture chemin du fichier s�lectionn�
			String filePath = chooser.getSelectedFile().getAbsolutePath();
			
			// 2. Lecture format de l'image s�lectionn�e
			String fmt = (String)this.imgfmt.getSelectedItem();
			
			// 3. Ajout de l'extension
			if ( !filePath.endsWith("."+fmt)) filePath = filePath + "."+fmt; 
		
			// 3. Cr�ation d'un image bufferis�e
			BufferedImage img = new BufferedImage(this.canvas.getWidth(),this.canvas.getHeight(),BufferedImage.TYPE_INT_RGB);
		
			// 4. Lecture contexte graphique de l'image
			Graphics gr = img.createGraphics();
		
			// 5. Impression de la page dans l'image
			this.canvas.paint(gr);
		
			try {
				// 1. Enregistrement de la page courante au format d�sir�
				ImageIO.write(img,fmt, new File(filePath));
			} catch (IOException e) {
			} finally {
				// 1. Lib�ration des ressources
				gr.dispose();
			}
		}
	}
	
	/**
	 * Lecture r�f�rence � la page visualis�e
	 */
	protected Page getPage() {
		if (this.pageCount != 0) {
			for (int i = 0; i < this.pageCount; i++) {
				// 1. Lecture r�f�rence � la ieme page
				Page page = (Page) this.canvas.getComponent(i);

				if (page.isVisible()) {
					return page;
				}
			}
		}

		return null;
	}

	/**
	 * Impression de la page courante
	 * 
	 */
	protected void printPage() {
		// 1. Cr�ation d'un job d'impression
		PrinterJob printJob = PrinterJob.getPrinterJob();

		// 2. Cr�ation param�tres d'impression
		PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();

		// 3. Initialisation des param�tres d'impression
		aset.add(OrientationRequested.LANDSCAPE);
		aset.add(MediaSizeName.ISO_A4);

		// 2. Visualisation page de configuration de l'impression
		if (printJob.printDialog()) {
			// 1. Lecture r�f�rence � la page � imprimer
			Page page = this.getPage();

			if (page != null) {
				// 1 Enregistrement de la page � imprimer
				printJob.setPrintable(page);

				try {
					// 1. Impression de la page
					printJob.print(aset);
				} catch (PrinterException ign) {
				}
			}
		}
	}

	/**
	 * Actualisation etat des boutons
	 */
	protected void updateButtonsState() {
		if (this.pageIndex == -1) {
			this.first.setEnabled(false);
			this.last.setEnabled(false);
			this.next.setEnabled(false);
			this.previous.setEnabled(false);
			this.print.setEnabled(false);
		} else {
			this.first.setEnabled(true);
			this.last.setEnabled(true);
			this.next.setEnabled(this.pageIndex < (pageCount - 1));
			this.previous.setEnabled(this.pageIndex >= 1);
			this.print.setEnabled(true);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.first) {
			this.showPage(0);
		} else if (e.getSource() == this.last) {
			this.showPage(this.pageCount - 1);
		} else if (e.getSource() == this.next) {
			this.showPage(this.pageIndex + 1);
		} else if (e.getSource() == this.previous) {
			this.showPage(this.pageIndex - 1);
		} else if (e.getSource() == this.print) {
			this.printPage();
		} else if (e.getSource() == this.save) {
			this.savePage();
		}
	}
}
