package tools;

import java.util.Iterator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;


/**
 * Classe permettant de lire le fichier d'initialisation de l'application.
 * 
 * @author  :  soci�t� CS SI
 * @version : 1.0 du 01/12/09
 */
public class CIniFileReader
{
    public static String sIniFileName;
    
    
    /**
     * Accesseur en �criture du nom du fichier d'initialisation de la langue.
     * 
     * @author  :  soci�t� CS SI
     * @version : 1.0 du 14/12/09
     * 
     * @param pIniFileName : le nom du fichier d'initialisation de la langue.
     */
    public static void setIniFileName(String pIniFileName)
    {
    	sIniFileName = pIniFileName;
    }
    
    /**
     * M�thode permettant de retourner la cha�ne dont la cl� est pass�e en
     * param�tre.
     * 
     * @author  :  soci�t� CS SI
     * @version : 1.0 du 09/11/09
     * 
     * @param pKey : la cl� de la cha�ne.
     * 
     * @return la cha�ne dont la cl� est pass�e en param�tre.
     */
    public static String getString(String pKey)
    {
    	String lString = "* ERROR *";
    	
    	
		try
		{
			XMLInputFactory lInputFactory = XMLInputFactory.newInstance();
			InputStream     lInputStream  = new FileInputStream(sIniFileName);
			XMLEventReader  lEventReader  = lInputFactory.createXMLEventReader(
					lInputStream);
			
			
			while (lEventReader.hasNext())
			{
				XMLEvent lEvent = lEventReader.nextEvent();

				
				if (lEvent.isStartElement())
				{
					StartElement lStartElement = lEvent.asStartElement();
					Iterator<Attribute> attributes = lStartElement.getAttributes();

					
					while (attributes.hasNext())
					{
						Attribute attribute = attributes.next();
						
												
						if (attribute.getName().toString().equals("key") == true)
						{
							if (attribute.getValue().equals(pKey) == true)
							{
								lEvent = lEventReader.nextEvent();
								lString = lEvent.asCharacters().getData();
							}
						}
					}
				}
			}
		}
		catch (FactoryConfigurationError lException1)
		{
			lException1.printStackTrace();
		}
		catch (FileNotFoundException lException2)
		{
			lException2.printStackTrace();
		}
		catch (XMLStreamException lException3)
		{
			lException3.printStackTrace();
		}
		
		return lString; 
    }
}
