package tools.csv;

import java.io.IOException;

public class CSVHandlerAdapter implements CSVHandler {

	@Override
	public void commentsLine(CSVReader reader, String line) throws IOException {
	}

	@Override
	public void emptyLine(CSVReader reader) throws IOException {
	}

	@Override
	public void endDocument(CSVReader reader) throws IOException {
	}

	@Override
	public void fieldsLine(CSVReader reader, String[] fields)
			throws IOException {
	}

	@Override
	public void startDocument(CSVReader reader) throws IOException {
	}
	
	@Override
	public boolean isEndOfDocument(CSVReader aReader) {
		return false;
	}
}
