package tools.csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;

public class CSVReader {
	/** Etats possibles du lecteur. */
	public enum State {
		SEEKINGSTART, INPLAIN, INQUOTED, AFTER_QUOTE, SKIPPINGTAIL
	}

	/** Cat�gories des caract�res. */
	public enum Category {
		ORDINARY, SPACE, SEPARATOR, QUOTE
	}

	/** Lecteur du fichier. */
	protected BufferedReader reader;

	/** S�parateur de champ. */
	protected char fs;

	/** S�parateur de texte. */
	protected char ts;

	/** Caract�re de d�but d'une ligne de commentaires. */
	protected String commentChar;

	/** Num�ro de ligne courant. */
	protected int lineNumber;

	/** Nombre de lignes a ignorer. */
	protected int skippedLines;

	/**
	 * Constructeur
	 * 
	 * @param aFs
	 *            S�parateur de champ
	 * @param aTs
	 *            S�parateur de texte
	 * @param aCs
	 *            D�but d'une ligne de commentaires
	 */
	public CSVReader(char aFs, char aTs, char aCs) {
		// 1. Initialisation des attributs
		this.fs = aFs;
		this.ts = aTs;
		this.commentChar = "" + aCs;
	}

	/**
	 * Constructeur
	 * 	 * @param aFile 
	 *            Fichier au format CSV
	 */
	public CSVReader(File aFile) throws FileNotFoundException {
		// 1. Initialisation des attributs
		this.fs = ';';
		this.ts = '"';
		this.commentChar = "#";
		this.reader = new BufferedReader(new FileReader(aFile));
	}

	/**
	 * Constructeur
	 * 
	 * @param aStream
	 *            Flux au format CSV
	 */
	public CSVReader(InputStream aStream) {
		// 1. Initialisation des attributs
		this.fs = ';';
		this.ts = '"';
		this.commentChar = "#";
		this.reader = new BufferedReader(new InputStreamReader(aStream));
	}
	
	/**
	 * Constructeur
	 * 
	 * @param aReader
	 *            Lecteur d'un flux au format CSV
	 */
	public CSVReader(Reader aReader) {
		// 1. Initialisation des attributs
		this.fs = ';';
		this.ts = '"';
		this.commentChar = "#";
		this.reader = new BufferedReader(aReader);
	}
	
	/**
	 * Lecture valeur du s�parateur de champ
	 * @return S�parateur de champ
	 */
	public char getFs() {
		return fs;
	}

	/**	
	 * Modification du s�parateur de champ
	 * @param aFs S�parateur de champ
	 */
	public void setFs(char aFs) {
		this.fs = aFs;
	}

	/**
	 * Lecture valeur du s�parateur de texte
	 * @return S�parateur de texte
	 */
	public char getTs() {
		return ts;
	}

	/**
	 * Modification du s�parateur de texte
	 * @param aTs S�paarteur de texte
	 */
	public void setTs(char aTs) {
		this.ts = aTs;
	}

	
	/**
	 * Lecture d�limiteur de commentaire
	 * @return D�limiteur de commentaire
	 */
	public char getCommentChar() {
		return commentChar.charAt(0);
	}

	/**
	 * Modification d�limiteur de commentaire
	 * @return D�limiteur de commentaire
	 */
	public void setCommentChar(char aCommentChar) {
		this.commentChar = "" +aCommentChar;
	}

	/**
	 * Lecture num�ro de la ligne courante
	 * @return Num�ro de la ligne courante
	 */
	public int getLineNumber() {
		return lineNumber;
	}

	/** 
	 * Initialisation nombre de lignes � ignorer
	 * @param aCount
	 */
	public void setSkippedLines(int aCount) {
		this.skippedLines = aCount;
	}
	
	/**
	 * Lecture du flux au format CSV
	 * 
	 * @param aReader
	 *            Lecteur d'un flux au format CSV
	 * @param aHandler Handler du fichier CSV
	 * @throws IOException
	 */
	public void parse(CSVHandler aHandler) throws IOException {
		String line = null;

		// 2. Notification de d�but de lecture du document
		aHandler.startDocument(this);

		while ((line = this.nextLine()) != null) {
			if (line.startsWith(this.commentChar)) {
				// 1. Notification de lecture d'une ligne de commentaire
				aHandler.commentsLine(this,line);
			} else if (isEmptyLine(line)) {
				// 1. Notification de lecture d'une ligne vide
				aHandler.emptyLine(this);
			} else {
				// 1. Analyse de la ligne courante
				String tokens[] = this.parseLine(line);

				// 2. Notification de lecture de la ligne
				aHandler.fieldsLine(this,tokens);
			}
			
			if ( aHandler.isEndOfDocument(this)) {
				// 1. Arret de la lecture du fichier
				break;
			}
		}

		// 3. Notification de fin de lecture du document
		aHandler.endDocument(this);
	}

	/**
	 * Lecture ligne suivante
	 * 
	 * @return Ligne suivante
	 */
	protected String nextLine() throws IOException {
		String line = null;

		while (this.skippedLines > 0) {
			// 1. Lecture de la ligne suivante
			line = this.reader.readLine();

			if (line != null) {
				// 1. Actualisation du num�ro de ligne
				this.lineNumber++;
			}

			// 2. Actualisation nombre de lignes � ignorer
			this.skippedLines--;
		}

		// 1. Lecture de la ligne suivante
		line = this.reader.readLine();

		if (line != null) {
			// 1. Actualisation du num�ro de ligne
			this.lineNumber++;
		}
		return line;
	}

	/**
	 * Analyse de la ligne
	 * 
	 * @param aLine
	 *            Une ligne de texte
	 * @return Liste des mots de la ligne
	 */
	protected String[] parseLine(String aLine) throws IOException {
		// 1. Initialisation etat du parseur
		State state = State.SEEKINGSTART;

		// 2. Conversion ligne en tableau de caract�res
		char buffer[] = aLine.toCharArray();

		// 3. Cr�ation liste des mots de la lignes
		ArrayList<String> words = new ArrayList<String>();

		// 4. Cr�ation valeur du mot courant
		StringBuffer field = new StringBuffer();

		for (int i = 0; i < buffer.length + 1; i++) {
			if (i == buffer.length) {
				if (state == State.INPLAIN || state == State.AFTER_QUOTE
						|| state == State.SKIPPINGTAIL) {
					// 1. Actualisation liste des champs
					words.add(field.toString());
				} else if (state == State.INQUOTED) {
					// 1. Syntaxe incorrecte
					throw new IOException(
							"Malformed CSV : Missing quote after field on line "
									+ this.lineNumber);
				}

				return words.toArray(new String[words.size()]);

			} else {
				// 1. Lecture valeur du caract�re courant
				char current = buffer[i];

				// 2. Cat�gorisation du caract�re
				Category category = this.getCategory(current);

				if (state == State.SEEKINGSTART) {
					if (category == Category.SEPARATOR) {
						// 1. D�tection d'une champ vide
						words.add("");
					} else if (category == Category.QUOTE) {
						// 1. Actualisation etat du parseur
						state = State.INQUOTED;
					} else if (category == Category.ORDINARY) {
						// 1. Actualisation etat du parseur
						state = State.INPLAIN;

						// 2. Actualisation valeur du champ
						field.append(current);
					}
				} else if (state == State.INPLAIN) {
					if (category == Category.SEPARATOR) {
						// 1. Enregistrement valeur du champ
						words.add(field.toString().trim());

						// 2. R�initialisation valeur champ
						field.setLength(0);

						// 3. Actualisation etat de l'automate
						state = State.SEEKINGSTART;
					} else if (category == Category.QUOTE) {
						// 1. Syntaxe incorrecte
						throw new IOException(
								"Malformed CSV : Missing quote at start of field on line "
										+ this.lineNumber);
					} else {
						// 1. Actualisation valeur du champ
						field.append(current);
					}
				} else if (state == State.INQUOTED) {
					if (category == Category.QUOTE) {
						// 1. actualisation etat du parseur
						state = State.AFTER_QUOTE;
					} else {
						// 1. Actualisation valeur du champ
						field.append(current);
					}
				} else if (state == State.AFTER_QUOTE) {
					if (category == Category.QUOTE) {
						// 1. Actualisation valeur du champ
						field.append(current);

						// 2. Actualisation etat du parseur
						state = State.INQUOTED;
					} else if (category == Category.ORDINARY) {
						throw new IOException(
								"Malformed CSV : Missing separator after field on line "
										+ this.lineNumber);
					} else if (category == Category.SEPARATOR) {
						// 1. Enregistrement du champ
						words.add(field.toString());

						// 2. R�initialisation valeur du cmap
						field.setLength(0);

						// 2. Actualisation etat du parseur
						state = State.SEEKINGSTART;
					} else if (category == Category.SPACE) {
						// 1. Actualisation etat du parseur
						state = State.SKIPPINGTAIL;
					}
				} else if (state == State.SKIPPINGTAIL) {
					if (category == Category.SEPARATOR) {
						// 1. Enregistrement du champ
						words.add(field.toString());

						// 2. r�initialisation valeur du champ
						field.setLength(0);

						// 3. Actualisation etat du parseur
						state = State.SEEKINGSTART;
					} else if (category == Category.ORDINARY
							|| category == Category.QUOTE) {
						// 1. Propagation d'une exception
						throw new IOException(
								"Malformed CSV : Missing separator after field on line "
										+ this.lineNumber);
					} else if (category == Category.SPACE) {

					}
				}
			}
		}
		return words.toArray(new String[words.size()]);
	}

	/**
	 * Cat�gorisation d'un caract�re
	 * 
	 * @param aChar
	 *            Un caract�re quelconque
	 * @return Catagorie du caract�re
	 */
	protected Category getCategory(char aChar) {
		if (aChar == this.fs) {
			return Category.SEPARATOR;
		} else if (aChar == this.ts) {
			return Category.QUOTE;
		} else if (aChar == ' ' || aChar == '\t') {
			return Category.SPACE;
		} else {
			return Category.ORDINARY;
		}
	}
	
	/**
	 * La ligne  est-elle vide
	 * @param aLine Une ligne de texte au format CSV
	 * @return true si la ligne est vide
	 */
	protected boolean isEmptyLine(String aLine) {
		for ( int i = 0 ; i < aLine.length(); i++) {
			if ( getCategory(aLine.charAt(i)) == Category.ORDINARY ) {
				return false;
			}
		}
		
		return true;
	}
	
}
