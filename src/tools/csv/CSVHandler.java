package tools.csv;

import java.io.IOException;

public interface CSVHandler {
	/**
	 * Notification de d�but de lecture du fichier
	 * @param aReader Lecteur du fichier CSV
	 */
	public void startDocument(CSVReader aReader) throws IOException;
	
	/**
	 * Notification de fin de lecture du fichier
	 * @param aReader Lecteur du fichier CSV
	 */
	public void endDocument(CSVReader aReader) throws IOException;
	
	/**
	 * Notification de lecture d'une ligne de commentaire
	 * @param aReader Lecteur du fichier CSV
	 * @param aLine La ligne de commentaire 
	 */
	public void commentsLine(CSVReader aReader,String aLine) throws IOException;
	
	
	/**
	 * Notification de lecture d'une ligne vide
	 * @param aReader Lecteur du fichier CSV
	 */
	public void emptyLine(CSVReader aReader) throws IOException;
	
	
	/**
	 * Notification de lecture d'une ligne de donn�es
	 * @param aReader Lecteur du fichier CSV
	 * @param aFields Liste des champs lus
	 */
	public void fieldsLine(CSVReader aReader,String aFields[])throws IOException;
	
	/**
	 * La fin du document at-elle �t� atteinte
	 * @return true si la fin du dosumen a �t� atteinte.
	 */
	public boolean isEndOfDocument(CSVReader aReader);
}
