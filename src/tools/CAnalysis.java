package tools;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingWorker;

import calcul.AverageRainfall;
import calcul.NASOUS;
import calcul.ZipAnalysisFile;
import cs.util.DateUtils;
import cs.util.FileUtils;
import db.Barrage;
import db.Sensor;
import db.SensorMeasures;
import db.SensorResultats;
import db.Survey;

/**
 * Classe permettant de m�moriser les param�tres d'une analyse et permettant de
 * lancer une analyse bas�e sur ces param�tres.
 * 
 * @author : soci�t� CS SI
 * @version : 1.0 du 03/02/10
 */
public class CAnalysis extends SwingWorker<Integer, SensorResultats> {
	private class ResultatTokenizer {
		/** Flux de lecture. */
		protected BufferedReader reader;

		/** Numero de la ligne courante. */
		protected int lineno;

		/**
		 * Constructeur
		 */
		public ResultatTokenizer(Reader aReader) {
			// 1. Initialisation des attributs
			this.reader = new BufferedReader(aReader);
			this.lineno = 0;
		}

		/**
		 * Lecture valeur enti�re suivante
		 * 
		 * @param aParser
		 *            Parseur du fichier r�sultat
		 * @return Valeur enti�re
		 * @throws IOException
		 */
		private int nextInt() throws IOException {
			// 1. Lecture mot suivant
			String word = nextString();
			try {
				// 1 D�codage de la valeur enti�re
				return Integer.parseInt(word);
			} catch (NumberFormatException e) {
				// 1. Propagation d'un exception
				throw new IOException("Format erreur ligne " + this.lineno);
			}
		}

		/**
		 * Lecture valeur suivante
		 * 
		 * @param aParser
		 *            Parseur du fichier r�sultat
		 * @return Mot suivant
		 * @throws IOException
		 */
		private String nextString() throws IOException {
			try {
				// 1. Actualisation du num�ro de ligne
				this.lineno++;

				// 2. Lecture de la ligne suivante
				String line = reader.readLine();

				// 3. Suppression des espaces
				line = line.trim();

				return line;
			} catch (IOException ex) {
				throw new IOException("Format erreur ligne " + this.lineno);
			}
		}

		/**
		 * Lecture valeur num�rique suivante
		 * 
		 * @param aParser
		 *            Parseur du fichier r�sultat
		 * @return Valeur num�rique
		 * @throws IOException
		 */
		private double nextDouble() throws IOException {
			// 1. Lecture mot suivant
			String word = nextString();

			try {
				// 1 D�codage de la valeur enti�re
				return Double.parseDouble(word);
			} catch (NumberFormatException e) {
				// 1. Propagation d'un exception
				throw new IOException("Format erreur ligne " + this.lineno);
			}
		}

		/**
		 * Lecture valeur num�rique suivante
		 * 
		 * @param aParser
		 *            Parseur du fichier r�sultat
		 * @return Une date
		 * @throws IOException
		 */
		private Date nextDate() throws IOException {
			// 1. Lecture mot suivant
			String word = nextString();

			if (word.length() == 8) {
				try {
					// 1. D�codage jour,mois,ann�e
					int year = Integer.parseInt(word.substring(0, 4));
					int month = Integer.parseInt(word.substring(4, 6));
					int day = Integer.parseInt(word.substring(6, 8));

					// 2. Lecture gestionnaire de dates
					Calendar calendar = Calendar.getInstance();

					// 2. Initialisation date
					calendar.set(year, month - 1, day, 0, 0, 0);

					return calendar.getTime();
				} catch (NumberFormatException e) {
					// 1. Propagation d'une exception
					throw new IOException("Format erreur ligne " + this.lineno);
				}
			} else {
				// 1. Propagation d'une exception
				throw new IOException("Format erreur ligne " + this.lineno);
			}
		}
	}

	public class Stderr extends Thread {
		/** Flux de sortie d'erreur du processus. */
		protected InputStream out;

		/** Flux de redirection de la sortie d'erreur . */
		protected ByteArrayOutputStream redirect;

		/** Buffer de lecture du flux de sortie. */
		protected byte buffer[];

		/**
		 * Constructeur
		 * 
		 * @param aProcess
		 *            Processus d'analyse d'un instrument
		 */
		public Stderr(Process aProcess) {
			// 1. Initialisation des attributs
			this.out = aProcess.getErrorStream();
			this.redirect = new ByteArrayOutputStream();
			this.buffer = new byte[1024 * 8];
		}

		/**
		 * Lecture valeur du flux d'erreur
		 * 
		 * @returl Valeur du flux de lecture
		 */
		public String getErrors() {
			return this.redirect.toString();
		}

		@Override
		public void run() {
			try {
				while (this != null) {
					// 1. Lecture caract�res suivant sur la sortie standard
					int count = this.out.read(buffer, 0, buffer.length);

					if (count == -1) {
						// 1. Le flux de sortie de la sortie standard est ferm�.
						break;
					} else {
						this.redirect.write(buffer, 0, count);
					}
				}
			} catch (IOException ioex) {
				// 1. Log des traces d'ex�cution
				logger.log(Level.SEVERE, "stderr", ioex);
			} finally {
				try {
					// 1. Fermeture du flux de sortie standard
					this.out.close();
				} catch (IOException ioex) {
					// 1. Log des traces d'ex�cution
					logger.log(Level.SEVERE, "stderr", ioex);
				}
			}
		}
	}

	public class Stdout extends Thread {
		/** Flux de sortie standard du processus. */
		protected InputStream out;

		/** Flux de redirection de la sortie standard. */
		protected OutputStream redirect;

		/** Buffer de lecture de la sortie standard. */
		protected byte buffer[];

		/**
		 * Constructeur
		 * 
		 * @param aProcess
		 *            Processus d'analyse d'un instrument
		 * @param aRedirect
		 *            Flux de redirection de la sortie standard
		 */
		public Stdout(Process aProcess, FileOutputStream aRedirect) {
			// 1. Initialisation des attributs
			this.out = aProcess.getInputStream();
			this.redirect = aRedirect;
			this.buffer = new byte[1024 * 8];
		}

		@Override
		public void run() {
			try {
				while (this != null) {
					// 1. Lecture caract�res suivant sur la sortie standard
					int count = this.out.read(buffer, 0, buffer.length);

					if (count == -1) {
						// 1. Le flux de sortie de la sortie standard est ferm�.
						break;
					} else {
						this.redirect.write(buffer, 0, count);
					}
				}
			} catch (IOException ioex) {
				// 1. Log des traces d'ex�cution
				logger.log(Level.SEVERE, "stdout", ioex);
			} finally {
				try {
					// 1. Fermeture du flux de sortie standard
					this.out.close();
				} catch (IOException ioex) {
					// 1. Log des traces d'ex�cution
					logger.log(Level.SEVERE, "stdout", ioex);
				}
			}
		}
	}

	// Noms des fichiers utilis�s.
	public static final String GENERAL_FILE = "analysis/GENERAL.PAR";
	public static final String REGRESSI_FILE = "analysis/REGRESSI.TXT";
	public static final String ANSWERS_FILE = "analysis/REPONSES.TXT";
	public static final String RESULT_FILE = "analysis/RESULTAT.REG";
	public static final String DRAW_FILE = "analysis/DESSIN.$$$";
	public static final String DRAW_CONCAT_FILE = "analysis/DESSINS.$$$";
	public static final String TMP1 = "analysis/SURBID1";
	public static final String TMP2 = "analysis/SURBID2";
	public static final String TMP3 = "analysis/donnees.dat";

	/** S�parateur de fin de ligne (D�pendant syst�me). */
	protected static final String EOL = System.getProperty("line.separator");

	/** Liste des instruments/variables � analyser. */
	public Sensor[] sItemsList;

	// ** Le barrage propri�taire des instruments/variables � analyser. */
	private Barrage barrage;

	/** Liste des mesures de la cote du barrage. */
	private SensorMeasures z;

	/** Liste des mesures de la pluie. */
	private SensorMeasures pluie;

	// Champs valu�s par l'�cran 2 de l'analyse d'un instrument.
	public NASOUS sAdjustmentModel;
	public Date sBeginDate;
	public Date sEndDate;
	public boolean sTableEdit;
	public boolean sGraphEdit;
	public boolean sRangeEdit;
	public boolean sDrawCurveGCC;
	public String outFile;

	// Champs valu�s par l'�cran 3 de l'analyse d'un instrument.
	public boolean sSelectVariables;
	public double sFisherSnedecorInputThreshold1;
	public double sFisherSnedecorOutputThreshold1;
	public boolean sPreselectTerms;
	public double sFisherSnedecorInputThreshold2;
	public double sFisherSnedecorOutputThreshold2;

	/** Logger des traces d'ex�cutions. */
	private static final Logger logger = Logger.getLogger(CAnalysis.class
			.getName());

	/**
	 * Constructeur
	 * 
	 * @param aBarrage
	 *            Le barrage propri�taire des instruments/variables a analyser
	 */
	public CAnalysis(Barrage aBarrage) {
		// 1. Initialisation des attributs
		this.barrage = aBarrage;

		// 2. Recherche d�finition des variables cote et pluie
		Sensor zVariable = this.barrage.getSensor("Z");
		Sensor pluieVariable = this.barrage.getSensor("PLUIE");

		if (zVariable != null) {
			// 1. Lecture liste des mesures pour la cote du barrage
			this.z = zVariable.getMeasures();
		}

		if (pluieVariable != null) {
			// 1. Lecture liste des mesures de la pluie
			this.pluie = pluieVariable.getMeasures();
		}
	}

	/**
	 * Lecture logger des traces d'ex�cution
	 * 
	 * @return Logger des traces d'ex�cution
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * Lecture r�f�rence au barrage
	 * 
	 * @return R�f�rence au barrage
	 */
	public Barrage getBarrage() {
		return barrage;
	}

	@Override
	protected Integer doInBackground() throws Exception {
		ZipAnalysisFile outAnalysis = null;
		FileOutputStream outRun = null;
		
		// 1. Cr�ation liste des r�sultats des calculs
		List<SensorResultats> allResults = new ArrayList<SensorResultats>();

		// 2. Cr�ation r�f�rence au r�pertoire d'ex�cution des calculs
		File runDir = new File("analysis");


		try {
			// 1. Cr�ation archive de stockage des r�sultats et donn�es
			outAnalysis = new ZipAnalysisFile();
			
			// 2. Ouverture du fichier archive
			outAnalysis.open(outFile);
			
			// 3. Nettoyage de l'espace de calcul
			deleteFiles(true);

			// 4. Calcul du pas de progression des traitements
			double step = 100. / this.sItemsList.length;

			// 5. Initialisation avancement des calculs
			double advancement = 0;

			for (int i = 0; i < this.sItemsList.length; i++) {
				SensorResultats resultat = null;

				// 1. Actualisation avancement des calculs
				advancement += step;

				// 2. Notificattion avancement dans les calculs
				this.setProgress((int) advancement);

				// 3. Lecture reference � l'instrument/variable � analyser
				Sensor sensor = sItemsList[i];

				// 4. Notification nom de l'instrument � analyser
				this.firePropertyChange("sensor", null, sensor.getId());

				// 5. Impression trace d'ex�cution
				logger.log(Level.INFO, "Analyse instrument<" + sensor.getId()
						+ ">");

				// 6. Impression trace d'ex�cution
				logger.log(Level.INFO, "... Generation GENERAL.PAR");

				// 7. G�n�ration du fichier GENERAL.PAR
				if (generateGeneralFile(sensor,outAnalysis)) {
					// 1. Impression trace d'ex�cution
					logger.log(Level.INFO, "... Generation REGRESSI.TXT");

					// 2. G�n�ration du fichier REGRESSI.TXT
					int nbObservations = generateRegressionFile(i + 1, sensor,outAnalysis);

					if (nbObservations >= 20) {
						// 1. Impression trace d'ex�cution
						logger.log(Level.INFO, "... Generation REPONSES.TXT");

						// 2. G�n�ration du fichier REPONSES.TXT
						generateAnswersFile();

						try {
							// 1. Impression trace d'ex�cution
							logger.log(Level.INFO, "... Calcul de regression");

							// 2. Cr�ation processus d'ex�cution des calculs
							Process process = Runtime.getRuntime().exec(
									runDir.getAbsolutePath() + "/PcPal.exe",
									null, runDir.getAbsoluteFile());

							// 3. Cr�ation flux de redirection des sorties du processus
							outRun = new FileOutputStream("analysis/pcpal.out");
							
							// 4. Lecture flux d'entr�e/sortie/erreur du
							// processus
							PrintStream in = new PrintStream(process
									.getOutputStream());
							Stderr err = new Stderr(process);
							Stdout out = new Stdout(process, outRun);

							err.start();
							out.start();
							in.println("1");
							in.println("DESSIN");
							in.flush();

							// 5. Attente de terminaison du process
							if (process.waitFor() != 0) {
								// 1. Enregistrement trace d'ex�cution
								logger.log(Level.SEVERE, err.getErrors());
							}
							
							// 6. Fermeture flux de redirection des sorties du processus
							outRun.close();
							
							// 7. Enregistrement fichier donn�es.dat
							outAnalysis.addFile(sensor.getId()+"/DONNEES.txt", TMP3);

							// 8. Enregistrement fichier trace d'ex�cution
							outAnalysis.addFile(sensor.getId()+"/OUTPUT.txt", "analysis/pcpal.out");
						} catch (Throwable iex) {
							// 1. Enregistrement trace d'ex�cution
							logger
									.log(
											Level.SEVERE,
											"Erreur d'ex�cution du calcul de regression",
											iex);
						}

						// 3. Impression trace d'ex�cution
						logger.log(Level.INFO, "... Lecture RESULTAT.REG");

						if ((resultat = this.exploitResultFile(sensor,outAnalysis)) != null) {
							// 1. R�cup�ration des graphiques g�n�r�s par le
							// calcul
							concatDraws(i);
						} else {
							// 1 . Echec de lecture du fichier de REGRESSION
							logger.log(Level.SEVERE,
									"Echec lecture fichier REGRESSI.REG");
						}
					} else {
						// 1. Nombre de mesures insuffisant
						logger.log(Level.WARNING, "Nombre de mesures ("
								+ nbObservations + ") insuffisantes");
					}
				} else {
					// 1. Echec g�n�ration fichier GENERAL.PAR
					logger.log(Level.SEVERE,
							"Echec cr�ation fichier GENERAL.PAR");
				}

				// 8. Enregistrement r�sultats du ieme calcul
				allResults.add(resultat);

				// 9. Publication des r�sultats
				this.firePropertyChange("resultats", null, allResults);

				// 5. Suppression des fichiers d'E/S du calcul
				deleteFiles(false);
			}

		} catch (Throwable th) {
			// 1. Impression trace d'ex�cution
			logger.log(Level.SEVERE, "Erreur fatale non classifi�", th);

			return -1;
		} finally {
			// 1. Fermeture du fichier de trace
			if ( outAnalysis != null ) outAnalysis.close();
			if ( outRun != null ) outRun.close();

			// 1. Notification de fin du calcul
			this.firePropertyChange("done", null, allResults);
		}
		return 0;
	}

	@Override
	protected void done() {
		try {
			// 1. Le traitement est termin�.
			setProgress(100);
		} catch (Exception e) {
		}

	}

	/**
	 * Permet d'effacer les fichiers n�cessaires au code Fortran.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 04/02/10
	 */
	private void deleteFiles(boolean mode) {
		// 1. Cr�ation r�f�rence aux fichiers de calcul
		File genFile = new File(GENERAL_FILE);
		File regFile = new File(REGRESSI_FILE);
		File ansFile = new File(ANSWERS_FILE);
		File resFile = new File(RESULT_FILE);
		File draFile = new File(DRAW_FILE);
		File graFile = new File(DRAW_CONCAT_FILE);
		File tmp1 = new File(TMP1);
		File tmp2 = new File(TMP2);
		File tmp3 = new File(TMP3);

		// 2. Suppression r�f�rence aux fichiers de calcul
		genFile.delete();
		regFile.delete();
		ansFile.delete();
		resFile.delete();
		draFile.delete();
		tmp1.delete();
		tmp2.delete();
		tmp3.delete();

		if (mode) {
			graFile.delete();
		}
	}

	/**
	 * Permet de g�n�rer le fichier GENERAL.PAR.
	 * @param pSensor
	 *            le nom de l'instrument/de la variable.
	 * @param aResult
	 *            Fichier de stockage des r�sultats de calcul
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 04/02/10
	 */
	private boolean generateGeneralFile(Sensor aSensor,ZipAnalysisFile aResult) {
		PrintStream out = null;

		try {
			// 1. Cr�ation flux de g�n�ration du fichier de param�tres
			out = new PrintStream(GENERAL_FILE);

			// 2. Generation des champs
			out.println("CON");
			out.printf(Locale.ENGLISH, "%-8s%s", barrage.getId(), EOL);
			out.printf(Locale.ENGLISH, "%-20s%s", barrage.getTitle(), EOL);
			out.printf(Locale.ENGLISH, "%7.2f%s", barrage.getWaterMinLevel(),
					EOL);
			out.printf(Locale.ENGLISH, "%7.2f%s", barrage
					.getWaterNominalLevel(), EOL);
			out.printf(Locale.ENGLISH, "%7.2f%s", barrage.getWaterMaxLevel(),
					EOL);
			out.printf(Locale.ENGLISH, "0 %d%s", sAdjustmentModel.getAjustmentModel().ordinal(), EOL);
			out.println(Survey.format(sBeginDate));
			out.println(Survey.format(sEndDate));

			out.println((sTableEdit ? "1 " : "0 ") + (sGraphEdit ? "1 " : "0 ")
					+ (sRangeEdit ? "1" : "0"));

			if (!sSelectVariables) {
				out.println("0");
			} else {
				out.printf(Locale.ENGLISH, "1\n%8.3f %8.3f%s",
						sFisherSnedecorInputThreshold1,
						sFisherSnedecorOutputThreshold1, EOL);

				if (!sPreselectTerms) {
					out.println("0");
				} else {
					out.printf(Locale.ENGLISH, "1\n%8.3f %8.3f%s",
							sFisherSnedecorInputThreshold2,
							sFisherSnedecorOutputThreshold2, EOL);
				}
			}

			// 3. Option de gen�ration des graphes
			out.println("1");
			out.println("1");
			out.println("1");
			out.println("1");
			out.println("1");
			out.println("1");
			out.println(sDrawCurveGCC ? "1" : "0");
			
			out.println("0");
			
			// 4. Enregistrement dans l'archive r�sultat
			aResult.addFile(aSensor.getId()+"/GENERAL.txt", GENERAL_FILE);
		} catch (IOException e) {
			// 1. Enregistrement trace d'ex�cution
			logger.log(Level.SEVERE, "", e);

			return false;
		} finally {
			// 1. Fermeture du flux de generation
			if (out != null)
				out.close();
		}

		return true;
	}

	/**
	 * Permet de g�n�rer le fichier REGRESSI.TXT.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 04/02/10
	 * 
	 * @param pItemNumber
	 *            le num�ro de l'instrument/de la variable.
	 * @param pSensor
	 *            le nom de l'instrument/de la variable.
	 * @param aResult Fichier archive r�sultat des calculs
	 * @return Le nombre de mesures g�n�r�es
	 */
	private int generateRegressionFile(int pItemNumber, Sensor aSensor,ZipAnalysisFile aResult) {
		PrintStream out = null;
		int nbObservations = 0;

		try {
			// 1. Ouverture du flux d'�criture
			out = new PrintStream(REGRESSI_FILE);

			// 2. G�n�ration rang de l'instrument/variable dans la liste
			out.println(pItemNumber);

			// 3. G�n�ration date de d�but et de fin de l'analyse
			out.println(Survey.format(sBeginDate));
			out.println(Survey.format(sEndDate));

			// 4. G�n�ration nombre de jours s�parant la date de d�but de la
			// date de fin d'analyse
			out
					.printf("%d%s",
							DateUtils.getDayCount(sBeginDate, sEndDate), EOL);

			// 5. G�n�ration nom de l'instrument, cote de reference, titre
			out.printf(Locale.ENGLISH, "%-8s%9.3f %-32s", aSensor.getId(),
					aSensor.getRefValue(), aSensor.getTitle());

			// 6. Lecture liste des mesures disponibles
			SensorMeasures measures = aSensor.getMeasures();

			// 7. Construction liste des dates de mesures
			List<Date> dates = SensorMeasures
					.getDatesMeasure(new SensorMeasures[] { z, measures });

			// 8. Cr�ation liste des mesures retard�es de la pluie
			AverageRainfall pluieRetardees = new AverageRainfall();

			for (int j = 1; j < dates.size(); j++) {
				// 1. Lecture date courante
				Date jdate = dates.get(j - 1);

				if (z.hasMeasure(jdate) && measures.hasMeasure(jdate)) {
					// 1. Calcul age de la mesure
					int ageMeasure = DateUtils.getDayCount(sBeginDate, jdate);

					// 2. Lecture valeur de la cote du barrage
					double cote = this.z.getMeasure(jdate);

					// 3. Lecture valeur de la mesure sur l'instrument
					double valMes = measures.getMeasure(jdate);

					if (pluie != null && this.sAdjustmentModel.isRainMeasuresRequired()) {
						// 1. Enregistrement des valeurs retard�es de la pluie
						pluieRetardees.setMeasures(pluie, jdate);

						try {
							// 1. Calcul des valeurs moyennes de la pluie
							double p1 = pluieRetardees.mean(7, 7);
							double p2 = pluieRetardees.mean(14, 7);
							double p3 = pluieRetardees.mean(21, 7);
							double p4 = pluieRetardees.mean(28, 7);
							double p56 = pluieRetardees.mean(42, 14);
							double p78 = pluieRetardees.mean(56, 14);

							if (jdate.before(sBeginDate)
									|| jdate.after(sEndDate)) {
								continue;
							} else {
								// 1. Impression des mesures
								out
										.printf(
												Locale.ENGLISH,
												"%s%s%5d%8.3f%8.3f%8.3f%8.3f%8.3f%8.3f%8.3f%8.3f",
												EOL, Survey.format(jdate),
												ageMeasure, cote, p1, p2, p3,
												p4, p56, p78, valMes);

								// 3. Actualisation nombre de mesures effectives
								nbObservations++;
							}
						} catch (IllegalArgumentException ex) {
							// 1. Poursuite du traitement
							continue;
						}
					} else {
						if (jdate.before(sBeginDate) || jdate.after(sEndDate)) {
							continue;
						} else {
							// 1. Impression des mesures
							out.printf(Locale.ENGLISH, "%s%s%5d%8.3f%8.3f",
									EOL, Survey.format(jdate), ageMeasure,
									cote, valMes);

							// 2. Actualisation nombre de mesures effectives
							nbObservations++;
						}
					}
				}
			}
			
			// 9. Enregistrement dans l'archive r�sultat
			aResult.addFile(aSensor.getId()+"/REGRESSI.txt", REGRESSI_FILE);
			
		} catch (IOException e) {
			// 1. Impression trace d'ex�cution
			logger.log(Level.SEVERE, "", e);
		} finally {
			// 1. Fermeture du flux de g�n�ration
			if (out != null)
				out.close();
		}

		return nbObservations;
	}

	/**
	 * Permet de g�n�rer le fichier REPONSES.TXT.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 11/02/10
	 */
	private void generateAnswersFile() {
		PrintStream out = null;
		try {
			// 1. Cr�ation flux de g�n�ration
			out = new PrintStream(ANSWERS_FILE);

			out.println("1");
			out.println("DESSIN");
			out.println();
			out.println();
			out.println();
			out
					.println("C ce fichier automatise pcpal.for si on lance c>pcpal <reponses.txt");
		} catch (FileNotFoundException e) {
			// 1. Impression trace d'ex�cution
			logger.log(Level.SEVERE, "", e);
		} finally {
			// 1. Fermeture flux de generation
			if (out != null)
				out.close();
		}
	}

	/**
	 * Permet de concat�ner le contenu du fichier DESSIN.$$$ s'il existe avec le
	 * fichier DESSINS.$$$.
	 * 
	 * @author : soci�t� CS SI
	 * @version : 1.0 du 04/02/10
	 */
	private void concatDraws(int cindx) {
		// 1. Cr�ation r�f�rences aux fichiers de dessins global et du ieme
		// calcul
		File drawG = new File(DRAW_CONCAT_FILE);
		File drawI = new File(DRAW_FILE);

		if (drawI.exists()) {
			if (drawG.exists()) {
				FileOutputStream writer = null;
				FileInputStream reader = null;
				int nbytes = -1;

				try {
					// 1. Cr�ation flux d'�criture dans le fichier destination
					writer = new FileOutputStream(DRAW_CONCAT_FILE, true);
					
					// 2. G�n�ration marque de fin de fichier
					String meof = "  0.0000   0.0000 -8"+System.getProperty("line.separator");
					writer.write(meof.getBytes());
					
					// 3. Cr�ation flux de lecture du fichier sourc
					reader = new FileInputStream(DRAW_FILE);
				} catch (IOException ign) {
				}

				// 3. Allocation buffer d'E/S
				byte buffer[] = new byte[4096];

				try {
					while ((nbytes = reader.read(buffer, 0, buffer.length)) != -1) {
						writer.write(buffer, 0, nbytes);
					}

				} catch (IOException ioex) {
					// 1. Impression trace d'ex�cution
					Survey.logger.log(Level.SEVERE, "", ioex);
				} finally {
					try {
						// 1. Fermeture des flux d'E/S
						writer.close();
						reader.close();
					} catch (IOException ign) {

					}
				}
			} else {
				drawI.renameTo(drawG);
			}
		}
	}

	/**
	 * Lecture du fichier de r�sultat de l'analyse des mesures d'un instrument
	 * 
	 * @param aSensor
	 *            L'instrument en cours d'analyse.
	 * @param aResult Archive des r�sultats
	 */
	private SensorResultats exploitResultFile(Sensor aSensor,ZipAnalysisFile aResult) {
		// 1. Cr�ation r�f�rence au fichier de r�sultat
		File resultatFile = new File(CAnalysis.RESULT_FILE);

		if (resultatFile.exists()) {

			FileInputStream in = null;
			try {
				// 1. Enregistregement fichier de r�sultat dans l'archive
				aResult.addFile(aSensor.getId()+"/RESULTAT.txt", CAnalysis.RESULT_FILE);
				
				// 2. Cr�ation d'un flux de lecture du fichier de r�sultat
				in = new FileInputStream(resultatFile);
			} catch (IOException e) {
				// 1. Impression trace d'ex�cution
				logger.log(Level.SEVERE, "", e);

				return null;
			}

			try {
				// 1. Cr�ation d'un parseur du fichier de r�sultat
				ResultatTokenizer parser = new ResultatTokenizer(
						new BufferedReader(new InputStreamReader(in)));

				// 2. Cr�ation r�sultat d'un calcul de regression
				SensorResultats resultat = new SensorResultats();

				// 3. Initialisation r�f�rence � l'instrument variable
				resultat.setSensor(aSensor);
				resultat.setId(aSensor.getId());

				// 4. Lecture r�sultat de l'analyse
				parser.nextString();
				parser.nextString();
				resultat.setLNasous(parser.nextInt());
				resultat.setLDuration((int) parser.nextDouble());
				resultat.setLAnalyseBeginDate(parser.nextDate());
				resultat.setLAnalyseEndDate(parser.nextDate());
				resultat.setLValuesCount(parser.nextInt());
				resultat.setLR2(parser.nextDouble());
				resultat.setLMeanCote(parser.nextDouble());
				resultat.setLSigCote(parser.nextDouble());
				resultat.setLConstantTerm(parser.nextDouble());
				resultat.setLT(parser.nextDouble());
				resultat.setLExpT(parser.nextDouble());
				resultat.setLExpMT(parser.nextDouble());
				resultat.setLSin(parser.nextDouble());
				resultat.setLCos(parser.nextDouble());
				resultat.setLSico(parser.nextDouble());
				resultat.setLSin2(parser.nextDouble());
				resultat.setLZ(parser.nextDouble());
				resultat.setLZ2(parser.nextDouble());
				resultat.setLZ3(parser.nextDouble());
				resultat.setLZ4(parser.nextDouble());
				resultat.setLP1(parser.nextDouble());
				resultat.setLP2(parser.nextDouble());
				resultat.setLP3(parser.nextDouble());
				resultat.setLP4(parser.nextDouble());
				resultat.setLP56(parser.nextDouble());
				resultat.setLP78(parser.nextDouble());

				// 5. Enregistrement r�sultat de la mesure
				Survey.getInstance().updateSensorResultats(resultat, false);

				return resultat;
			} catch (IOException ioex) {
				// 1. Enregistrement trace d'ex�cution
				logger.log(Level.SEVERE, "Fichier <"
						+ resultatFile.getAbsolutePath() + "> : ", ioex);
			} finally {
				if (in != null) {
					try {
						// 1. Fermeture du flux de lecture
						in.close();
					} catch (IOException ign) {
					}
				}
			}

		}
		return null;
	}
}
